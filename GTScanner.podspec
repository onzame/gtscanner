Pod::Spec.new do |s|

	s.name = "GTScanner"
	s.version = "1.0.13"
	s.summary = "GTScanner"
	s.homepage = "https://bitbucket.org/onzame/gtscanner.git"
	s.author = {
    "Sergei Fabian" => "sf.welcome@icloud.com"
  }
	s.source = {
    :git => "https://bitbucket.org/onzame/gtscanner.git",
    :tag => "#{s.version}"
  }
	s.license = {
    :type => 'MIT',
    :file => 'LICENSE'
  }
	s.source_files = "GTScanner*", "Classes/**/*.{swift,h,m}"
  s.resource_bundles = {
    "GTScanner" => ["Resources/**/*.{xcassets}", "Resources/**/*.{ttf}"]
  }
	s.framework = "UIKit"
	s.swift_version = "5.2"

	s.ios.deployment_target = "10.0"

	# Architecture
	s.dependency "ReactorKit"

	# Rx
	s.dependency "RxSwift", "~> 5"
	s.dependency "RxCocoa", "~> 5"
	s.dependency "RxViewController"
	s.dependency "RxDataSources", "~> 4.0"
	s.dependency "RxSwiftExt", "~> 5"
	s.dependency "RxOptional", "~> 4.1"

	# DI
	s.dependency "Swinject", "~> 2.7"
	s.dependency "SwinjectAutoregistration", "2.6.0"

	# UI
	s.dependency "SkyFloatingLabelTextField", "~> 3.0"
	s.dependency "MaterialComponents/BottomSheet", "~> 107.0"
	s.dependency "MaterialComponents/Snackbar", "~> 107.0"
	s.dependency "SnapKit", "~> 5.0.0"
	s.dependency "Cosmos", "~> 20.0"
	s.dependency "SwipeCellKit", "~> 2.7"
	s.dependency "ImageSlideshow", "~> 1.8.3"
	s.dependency "ImageSlideshow/Kingfisher", "~> 1.8.3"

	# Networking
	s.dependency "Moya", "13.0.1"
	s.dependency "Kingfisher", "~> 5.0"

	# Misc.
	s.dependency "Then"
	s.dependency "ReusableKit"
	
end
