//
//  BaseTableViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 27.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class BaseTableViewController<R: Reactor>: UITableViewController, View {

    typealias Reactor = R

    var disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(reactor: Reactor) {
        defer { self.reactor = reactor }
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationControllerDelegate()
    }

    // MARK: - Bindings

    func bind(reactor: Reactor) {
        fatalError("bind(reactor:) has not been overriden")
    }

}
