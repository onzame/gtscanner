//
//  BaseNavigationController.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class BaseNavigationController: CoordinatorNavigationController {

    // MARK: - Lifecycle

    init(rootViewController: UIViewController, modalPresentationStyle: UIModalPresentationStyle = .fullScreen) {
        super.init(rootViewController: rootViewController)
        self.modalPresentationStyle = modalPresentationStyle
        commonInit()
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        customizeBackButton(backButtonImage: Media.icon(.leftChevron, size: .x18), shouldUseViewControllersTitle: false)
        enableSwipeBack()

        navigationBar.tintColor = Color.tintColor
        navigationBar.barTintColor = Color.barTintColor
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [.foregroundColor: Color.titleColor, .font: Font.titleFont]
    }

}

// MARK: - Extensions
// MARK: - Constants

extension BaseNavigationController {
    fileprivate enum Color {
        static let tintColor = Style.Color.black
        static let barTintColor = Style.Color.white
        static let titleColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleFont = Style.Font.sanFrancisco(.bold, size: 14)
    }
}
