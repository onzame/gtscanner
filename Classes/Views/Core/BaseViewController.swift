//
//  BaseViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class BaseViewController<R: Reactor>: UIViewController, View {

    typealias Reactor = R

    var disposeBag = DisposeBag()

    // MARK: - Lifecycle

    init(reactor: Reactor) {
        defer { self.reactor = reactor }
        super.init(nibName: nil, bundle: nil)
    }

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        log.info("deinit view controller", String(describing: self))
    }

    // MARK: - Overrides

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if shouldSetupDismissButtonItem() {
            setupDismissButtonItem()
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if shouldSetupPopButtonItem() {
            setupNavigationControllerDelegate()
        }
    }

    // MARK: - Bindings

    func bind(reactor: Reactor) {
        fatalError("bind(reactor:) has not been overriden")
    }

    // MARK: - Private methods

    private func shouldSetupPopButtonItem() -> Bool {
        return navigationController?.viewControllers.count ?? 0 > 1
    }

    private func shouldSetupDismissButtonItem() -> Bool {
        return presentingViewController != nil && navigationController?.viewControllers.count == 1
    }

}
