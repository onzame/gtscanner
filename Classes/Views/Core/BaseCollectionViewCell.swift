//
//  BaseCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 26.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

class BaseCollectionViewCell: UICollectionViewCell {

    var disposeBag = DisposeBag()

    // MARK: - Overrides

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

}
