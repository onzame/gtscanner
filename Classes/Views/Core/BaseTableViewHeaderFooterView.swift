//
//  BaseTableViewHeaderFooterView.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

class BaseTableViewHeaderFooterView: UITableViewHeaderFooterView {

    var disposeBag = DisposeBag()

    // MARK: - Overrides

    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

}
