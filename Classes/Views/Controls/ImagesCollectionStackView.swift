//
//  ImagesCollectionStackView.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import Kingfisher

class ImagesCollectionStackView: UIStackView {

    struct Settings {
        let cornerRadius: CGFloat
        let partSize: CGFloat
        let borderColor: UIColor?
        let borderWidth: CGFloat
        let backgroundColor: UIColor?

        var size: CGSize {
            return CGSize(width: partSize, height: partSize)
        }
    }

    // MARK: - Public methods

    func updateItems(urls: [URL?], placeholder: Placeholder, settings: Settings) {

        for viewToRemove in arrangedSubviews {
            viewToRemove.removeFromSuperview()
        }

        var viewsToAdd = [UIView]()

        for url in urls {
            let view = ImagesCollectionStackView.createView(url: url, placeholder: placeholder, settings: settings)
            viewsToAdd.append(view)
        }

        for viewToAdd in viewsToAdd {
            addArrangedSubview(viewToAdd)

            viewToAdd.snp.makeConstraints { (make) in
                make.width.height.equalTo(settings.partSize)
            }
        }

    }

    // MARK: - Private methods

    private static func createView(url: URL?, placeholder: Placeholder, settings: Settings) -> UIView {
        return ImageContainerView().then {
            $0.backgroundColor = settings.backgroundColor
            $0.layer.masksToBounds = true
            $0.layer.cornerRadius = settings.cornerRadius
            $0.layer.borderColor = settings.borderColor?.cgColor
            $0.layer.borderWidth = settings.borderWidth
            $0.imageView.kf.setImage(with: url, placeholder: placeholder)
        }
    }
}
