//
//  FormTextField.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class FormTextField: SkyFloatingLabelTextField {

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func titleLabelRectForBounds(_ bounds: CGRect, editing: Bool) -> CGRect {
        if editing {
            return CGRect(x: 0, y: -6, width: bounds.size.width, height: titleHeight())
        }
        return CGRect(x: 0, y: titleHeight(), width: bounds.size.width, height: titleHeight())
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        font = Font.textFont
        textColor = Color.textColor

        titleFont = Font.titleFont
        titleColor = Color.titleColor
        titleFormatter = { $0.capitalizedFirstLetter }
        selectedTitleColor = Color.selectedTitleColor

        placeholderFont = Font.placeholderFont
        placeholderColor = Color.placeholderColor

        lineColor = Color.lineColor
        selectedLineColor = Color.selectedLineColor
    }

}

// MARK: - Extensions
// MARK: - Constants

extension FormTextField {
    fileprivate enum Color {
        static let textColor = Style.Color.black
        static let titleColor = Style.Color.newGray ?? UIColor.gray
        static let selectedTitleColor = Style.Color.newGray ?? UIColor.gray
        static let placeholderColor = Style.Color.newGray ?? UIColor.gray
        static let lineColor = Style.Color.grayBlue ?? UIColor.gray
        static let selectedLineColor = Style.Color.brightYellow ?? UIColor.black
    }

    fileprivate enum Font {
        static let textFont = Style.Font.sanFrancisco(.regular, size: 24)
        static let titleFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let placeholderFont = Style.Font.sanFrancisco(.regular, size: 24)
    }
}
