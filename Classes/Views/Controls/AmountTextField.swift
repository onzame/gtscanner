//
//  AmountTextField.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AmountTextField: UITextField {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let minusContainerView = UIView(frame: CGRect(squareSide: Metric.containerPartSize))

    let minusButton = UIButton(type: .system).then {
        $0.setImage(Image.minusButtonImage, for: .normal)
        $0.tintColor = Color.actionTintColor
    }

    let plusContainerView = UIView(frame: CGRect(squareSide: Metric.containerPartSize))

    let plusButton = UIButton(type: .system).then {
        $0.setImage(Image.plusButtonImage, for: .normal)
        $0.tintColor = Color.actionTintColor
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        minusContainerView.addSubview(minusButton)
        plusContainerView.addSubview(plusButton)
    }

    private func setupLayout() {
        [minusButton, plusButton].forEach {
            $0.snp.makeConstraints { (make) in
                make.edges.equalToSuperview()
                make.width.height.equalTo(Metric.containerPartSize)
            }
        }
    }

    private func setupView() {
        delegate = self

        textAlignment = .center
        leftView = minusContainerView
        leftViewMode = .always
        rightView = plusContainerView
        rightViewMode = .always
        keyboardType = .numberPad
        placeholder = Localization.ProductAmount.amountTextFieldPlaceholder
        font = Font.amountTextFieldFont

        layer.borderWidth = 1
        layer.borderColor = Color.amountTextFieldBorderColor?.cgColor
        layer.cornerRadius = CornerRadius.amountTextFieldBorderCornerRadius
    }

    // MARK: - Bindings

    private func setupBinding() {
        plusButton.rx.tap
            .withLatestFrom(rx.text.orEmpty)
            .mapToInt(placeholder: 0)
            .increment()
            .mapToString()
            .bind(to: rx.updateText)
            .disposed(by: disposeBag)

        minusButton.rx.tap
            .withLatestFrom(rx.text.orEmpty)
            .mapToInt(placeholder: 0)
            .decrement()
            .mapToString()
            .bind(to: rx.updateText)
            .disposed(by: disposeBag)

        rx.text.orEmpty
            .mapToInt(placeholder: 0)
            .map({ $0 > 0 })
            .bind(to: minusButton.rx.isEnabled)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension AmountTextField {
    fileprivate enum Color {
        static let actionTintColor = Style.Color.black
        static let amountTextFieldBorderColor = UIColor(hex: "#EFEFEF")
    }

    fileprivate enum CornerRadius {
        static let amountTextFieldBorderCornerRadius: CGFloat = 8
    }

    fileprivate enum Font {
        static let amountTextFieldFont = Style.Font.sanFrancisco(.bold, size: 20)
    }

    fileprivate enum Image {
        static let minusButtonImage = Media.icon(.minus, size: .x18)
        static let plusButtonImage = Media.icon(.plus, size: .x18)
    }

    fileprivate enum Metric {
        static let containerPartSize: CGFloat = 58
    }
}

// MARK: - UITextFieldDelegate

extension AmountTextField: UITextFieldDelegate {
    func textField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        return TextFieldValidator.amountTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }
}
