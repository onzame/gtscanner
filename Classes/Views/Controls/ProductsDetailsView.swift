//
//  ProductsDetailsView.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductsDetailsView: UIView {

    let mode: Mode

    // MARK: - UI components

    private let productsStackView = ImagesCollectionStackView().then {
        $0.spacing = Metric.productsStackViewSpacing
    }

    private let amountLabel = UILabel().then {
        $0.font = Font.amountLabelFont
    }

    private let sumContainerView = UIView().then {
        $0.layer.cornerRadius = CornerRadius.sumContainerViewCornerRadius
    }

    private let sumLabel = UILabel().then {
        $0.font = Font.sumLabelFont
    }

    // MARK: - Lifecycle

    init(mode: Mode) {
        self.mode = mode
        super.init(frame: .zero)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(productsStackView)
        addSubview(amountLabel)
        addSubview(sumContainerView)

        sumContainerView.addSubview(sumLabel)
    }

    private func setupLayout() {
        productsStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.productsStackViewLeftOffset)
            make.centerY.equalToSuperview()
        }

        sumContainerView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.sumContainerViewRightOffset)
            make.centerY.equalToSuperview()
        }

        sumLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.sumLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.sumLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.sumLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.sumLabelBottomOffset)
        }

        amountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(productsStackView.snp.right).offset(Metric.amountLabelLeftOffset)
            make.right.lessThanOrEqualTo(sumContainerView.snp.left).offset(Metric.amountLabelRightOffset)
            make.centerY.equalToSuperview()
        }
    }

    private func setupView() {
        switch mode {
        case .bright:
            backgroundColor = Color.brightBackgroundColor
            sumContainerView.backgroundColor = Color.brightSumContainerViewBackgroundColor
            sumLabel.textColor = Color.brightSumLabelTextColor
            amountLabel.textColor = Color.brightAmountLabelTextColor
        case .dark:
            backgroundColor = Color.darkBackgroundColor
            sumContainerView.backgroundColor = Color.darkSumContainerViewBackgroundColor
            sumLabel.textColor = Color.darkSumLabelTextColor
            amountLabel.textColor = Color.darkAmountLabelTextColor
        }
    }

    // MARK: - Public methods

    func update(details: FavoriteProductsDetails) {
        let productsPreview = details.products.prefix(Constant.productPreviewAmount)

        let urls = productsPreview.map({ $0.product.images.first?.url })
        let placeholderSize = CGSize(squareSide: Metric.productsImageViewPartSize / 2)
        let placeholder = ImagePlaceholderView.product(imageSize: placeholderSize)
        let settings = ImagesCollectionStackView.Settings(
            cornerRadius: CornerRadius.productsImageViewCornerRadius,
            partSize: Metric.productsImageViewPartSize,
            borderColor: mode == .dark ? Color.darkProductsImageViewBorderColor : Color.brightProductsImageViewBorderColor,
            borderWidth: Metric.productsImageViewBorderWidth,
            backgroundColor: Color.productsImageViewBackgroundColor
        )

        productsStackView.updateItems(urls: urls, placeholder: placeholder, settings: settings)

        let resultAmount = details.products.count - productsPreview.count

        if resultAmount > 0 {
            amountLabel.text = "+\(resultAmount)"
        } else {
            amountLabel.text = ""
        }

        let totalSum = details.products
            .map({ savedProduct -> Double in
                return Double(savedProduct.count) * (savedProduct.product.avgPrice ?? 0)
            })
            .reduce(into: Double(0), { result, totalSavedProductSum in
                result += totalSavedProductSum
            })

        if totalSum > 0 {
            sumLabel.text = "~\(totalSum.clean) ₽"
        } else {
            sumLabel.text = "0 ₽"
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductsDetailsView {
    enum Mode {
        case bright
        case dark
    }

    fileprivate enum Constant {
        static let productPreviewAmount = 6
    }

    fileprivate enum Color {
        static let productsImageViewBackgroundColor = Style.Color.white
        static let brightProductsImageViewBorderColor = UIColor(hex: "#FCD846")
        static let brightBackgroundColor = Style.Color.brightYellow
        static let brightSumContainerViewBackgroundColor = UIColor(hex: "#262A3C")?.withAlphaComponent(0.05)
        static let brightSumLabelTextColor = Style.Color.black
        static let brightAmountLabelTextColor = UIColor(hex: "#262A3C")
        static let darkProductsImageViewBorderColor = Style.Color.black
        static let darkBackgroundColor = Style.Color.black
        static let darkSumContainerViewBackgroundColor = Style.Color.white.withAlphaComponent(0.15)
        static let darkSumLabelTextColor = Style.Color.white
        static let darkAmountLabelTextColor = Style.Color.white
    }

    fileprivate enum CornerRadius {
        static let productsImageViewCornerRadius: CGFloat = 14
        static let sumContainerViewCornerRadius: CGFloat = 14
    }

    fileprivate enum Font {
        static let sumLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let amountLabelFont = Style.Font.sanFrancisco(.semibold, size: 14)
    }

    fileprivate enum Metric {
        static let productsStackViewSpacing: CGFloat = -12
        static let productsImageViewPartSize: CGFloat = 28
        static let productsStackViewLeftOffset: CGFloat = 20
        static let productsImageViewBorderWidth: CGFloat = 2
        static let amountLabelLeftOffset: CGFloat = 8
        static let amountLabelRightOffset: CGFloat = -8
        static let sumContainerViewRightOffset: CGFloat = -20
        static let sumLabelLeftOffset: CGFloat = 14
        static let sumLabelTopOffset: CGFloat = 12
        static let sumLabelRightOffset: CGFloat = -14
        static let sumLabelBottomOffset: CGFloat = -12
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductsDetailsView {
    var update: Binder<FavoriteProductsDetails> {
        return Binder(base, binding: { view, details in
            view.update(details: details)
        })
    }
}
