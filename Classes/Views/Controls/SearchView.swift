//
//  SearchView.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchView: UIView {

    // MARK: - UI components

    let searchTextField = SearchTextField()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(searchTextField)
    }

    private func setupLayout() {
        searchTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.searchTextFieldLeftOffset)
            make.top.equalToSuperview().offset(Metric.searchTextFieldTopOffset)
            make.right.equalToSuperview().offset(Metric.searchTextFieldRightOffset)
            make.bottom.equalToSuperview().offset(Metric.searchTextFieldBottomOffset)
            make.height.equalTo(Metric.searchTextFieldHeight)
        }
    }

    private func setupView() {
        backgroundColor = Color.backgroundColor
    }
}

// MARK: - Extensions
// MARK: - Constants

extension SearchView {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.white
    }

    fileprivate enum Metric {
        static let searchTextFieldLeftOffset: CGFloat = 16
        static let searchTextFieldTopOffset: CGFloat = 24
        static let searchTextFieldRightOffset: CGFloat = -16
        static let searchTextFieldBottomOffset: CGFloat = -20
        static let searchTextFieldHeight: CGFloat = 40
    }
}

// MARK: - Reactive interface

extension Reactive where Base: SearchView {
    var query: ControlEvent<String> {
        return ControlEvent(events: base.searchTextField.rx.text.orEmpty)
    }

    var clear: Binder<Void> {
        return Binder(base, binding: { view, _ in
            view.searchTextField.text = ""
        })
    }
}
