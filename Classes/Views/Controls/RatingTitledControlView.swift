//
//  RatingTitledControlView.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RatingTitledControlView: RatingControlView {

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.textAlignment = .center
        $0.numberOfLines  = 0
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        stackView.insertArrangedSubview(titleLabel, at: 0)
    }

    private func setupLayout() {
        stackView.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(Metric.stackViewTopOffset)
            make.bottom.equalToSuperview().offset(Metric.stackViewBottomOffset)
            make.centerX.equalToSuperview()
            make.left.greaterThanOrEqualToSuperview()
            make.right.lessThanOrEqualToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.width.equalTo(ratingView)
        }
    }

    private func setupView() {
        stackView.spacing = Metric.stackViewSpacing
    }

}

// MARK: - Extensions
// MARK: - Constants

extension RatingTitledControlView {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Metric {
        static let stackViewSpacing: CGFloat = 16
        static let stackViewTopOffset: CGFloat = 28
        static let stackViewBottomOffset: CGFloat = -28
    }
}

// MARK: - Reactive interface

extension Reactive where Base: RatingTitledControlView {
    var title: Binder<String?> {
        return base.titleLabel.rx.text
    }
}
