//
//  RatingView.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import Cosmos
import RxSwift
import RxCocoa

class RatingView: CosmosView {

    // MARK: - Publishers

    fileprivate let changeRatingActionPublisher = PublishSubject<Double>()

    fileprivate let selectRatingActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    init(frame: CGRect) {
        var settings = CosmosSettings.default
        settings.filledImage = Media.image(.starFilled)
        settings.emptyImage = Media.image(.star)
        settings.totalStars = Rating.totalStars
        super.init(frame: frame, settings: settings)
        commonInit()
    }

    convenience init() {
        self.init(frame: .zero)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        didTouchCosmos = { [weak self] rating in self?.changeRatingActionPublisher.onNext(rating) }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        if let touch = touches.first, touch.view === self {
            selectRatingActionPublisher.onNext(())
        }
    }

}

extension Reactive where Base: RatingView {
    var changeRatingAction: ControlEvent<Double> {
        return ControlEvent(events: base.changeRatingActionPublisher)
    }

    var selectRatingAction: ControlEvent<Void> {
        return ControlEvent(events: base.selectRatingActionPublisher)
    }

    var rating: Binder<Double> {
        return Binder(base, binding: { view, rating in
            view.rating = rating
        })
    }

    var text: Binder<String?> {
        return Binder(base, binding: { view, text in
            view.text = text
        })
    }
}
