//
//  FeatureSnackbackView.swift
//  Gorod
//
//  Created by Sergei Fabian on 02.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class FeatureSnackbackView: UIView {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.backgroundColor = Color.containerViewBackgroundColor
        $0.layer.cornerRadius = CornerRadius.containerViewCornerRadius
        $0.setupShadow(.longBlue)
    }

    let textLabel = UILabel().then {
        let text = NSMutableAttributedString(string: Localization.Scanner.featureTitle, font: Font.textLabelTitleFont)
        text.append(NSAttributedString.space)
        text.append(NSAttributedString(string: Localization.Scanner.featureSubtitle, font: Font.textLabelSubtitleFont))
        $0.attributedText = text
        $0.numberOfLines = 0
    }

    let iconImageView = UIImageView().then {
        $0.image = Media.image(.featureScan)
    }

    let cancelButton = UIButton(type: .system).then {
        $0.setImage(Media.icon(.clear, size: .x18), for: .normal)
        $0.tintColor = Color.cancelButtonTintColor
    }

    let pointerView = UIView().then {
        $0.setBackgroundColor(Color.pointerViewBackgroundColor)
        $0.setupShadow(.longBlue)
        $0.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 4)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        pointerView.setCornerRadius(corners: .layerMaxXMaxYCorner, radius: CornerRadius.pointerViewCornerRadius)
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(containerView)
        addSubview(pointerView)
        addSubview(iconImageView)

        containerView.addSubview(textLabel)
        containerView.addSubview(cancelButton)
    }

    private func setupLayout() {
        pointerView.snp.makeConstraints { (make) in
            make.bottom.equalTo(Metric.pointerBottomOffset)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Metric.pointerViewPartSize)
        }

        containerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.containerViewLeftOffset)
            make.top.greaterThanOrEqualToSuperview()
            make.right.equalToSuperview().offset(Metric.containerViewRightOffset)
            make.bottom.equalTo(pointerView.snp.centerY)
        }

        cancelButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.cancelButtonRightOffset)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(Metric.cancelButtonPartSize)
        }

        iconImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().priority(250)
            make.bottom.equalTo(containerView)
            make.right.equalTo(cancelButton.snp.left).offset(Metric.iconImageRightOffset)
            make.width.height.equalTo(Metric.iconImageViewPartSize)
        }

        textLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.textLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.textLabelTopOffset)
            make.right.equalTo(iconImageView.snp.left).offset(Metric.textLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.textLabelBottomOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension FeatureSnackbackView {
    fileprivate enum Color {
        static let pointerViewBackgroundColor = Style.Color.brightYellow
        static let containerViewBackgroundColor = Style.Color.brightYellow
        static let cancelButtonTintColor = Style.Color.black.withAlphaComponent(0.15)
    }

    fileprivate enum CornerRadius {
        static let pointerViewCornerRadius: CGFloat = 2
        static let containerViewCornerRadius: CGFloat = 16
    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {
        static let textLabelTitleFont = Style.Font.sanFrancisco(.bold, size: 12)
        static let textLabelSubtitleFont = Style.Font.sanFrancisco(.regular, size: 12)
    }

    fileprivate enum Metric {
        static let iconImageViewPartSize: CGFloat = 88
        static let iconImageRightOffset: CGFloat = -8
        static let pointerViewPartSize: CGFloat = 14
        static let pointerBottomOffset: CGFloat = -4
        static let containerViewLeftOffset: CGFloat = 8
        static let containerViewRightOffset: CGFloat = -8
        static let cancelButtonPartSize: CGFloat = 18
        static let cancelButtonRightOffset: CGFloat = -16
        static let textLabelLeftOffset: CGFloat = 16
        static let textLabelTopOffset: CGFloat = 8
        static let textLabelRightOffset: CGFloat = -8
        static let textLabelBottomOffset: CGFloat = -12
    }
}
