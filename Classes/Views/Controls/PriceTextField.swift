//
//  PriceTextField.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class PriceTextField: SkyFloatingLabelTextFieldWithIcon {

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        font = Font.textFont
        delegate = self
        keyboardType = .decimalPad

        iconText = "₽"
        iconType = .font
        iconFont = Font.iconFont
        iconColor = Color.iconColor
        iconMarginBottom = Metric.iconMarginBottom
        selectedIconColor = Color.iconColor

        titleFont = Font.titleFont
        titleColor = Color.titleColor
        titleFormatter = { $0.capitalizedFirstLetter }
        selectedTitleColor = Color.selectedTitleColor

        placeholder = Localization.ProductPrice.priceTextFieldPlaceholder
        placeholderFont = Font.placeholderFont
        placeholderColor = Color.placeholderColor

        lineColor = Color.lineColor
        selectedLineColor = Color.selectedLineColor
    }

}

// MARK: - Extensions
// MARK: - Constants

extension PriceTextField {
    fileprivate enum Color {
        static let textColor = Style.Color.black
        static let iconColor = Style.Color.newGray ?? UIColor.gray
        static let titleColor = Style.Color.newGray ?? UIColor.gray
        static let selectedTitleColor = Style.Color.newGray ?? UIColor.gray
        static let placeholderColor = Style.Color.newGray ?? UIColor.gray
        static let lineColor = Style.Color.grayBlue ?? UIColor.gray
        static let selectedLineColor = Style.Color.brightYellow ?? UIColor.black
    }

    fileprivate enum Font {
        static let textFont = Style.Font.sanFrancisco(.regular, size: 24)
        static let iconFont = Style.Font.sanFrancisco(.regular, size: 24)
        static let titleFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let placeholderFont = Style.Font.sanFrancisco(.regular, size: 24)
    }

    fileprivate enum Metric {
        static let iconMarginBottom: CGFloat = 1
    }
}

// MARK: - UITextFieldDelegate

extension PriceTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return TextFieldValidator.priceTextField(textField, shouldChangeCharactersIn: range, replacementString: string)
    }
}
