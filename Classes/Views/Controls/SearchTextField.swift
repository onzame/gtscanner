//
//  SearchTextField.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchTextField: UITextField {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    lazy var rect = CGRect(width: Metric.containerWidth, height: Metric.containerHeight)

    lazy var searchContainerView = UIView(frame: rect)

    lazy var searchImageView = UIImageView(frame: rect).then {
        $0.image = Image.searchImageViewImage
        $0.contentMode = .center
    }

    lazy var clearContainerView = UIView(frame: rect)

    lazy var clearButton = UIButton(type: .system).then {
        $0.setImage(Image.clearButtonImage, for: .normal)
        $0.contentMode = .center
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        searchContainerView.addSubview(searchImageView)
        clearContainerView.addSubview(clearButton)
    }

    private func setupLayout() {
        [searchImageView, clearButton].forEach {
            $0.snp.makeConstraints { (make) in
                make.top.bottom.equalToSuperview()
                make.left.equalToSuperview().offset(Metric.containerElementLeftOffset)
                make.right.equalToSuperview().offset(Metric.containerElementRightOffset)
                make.width.height.equalTo(Metric.containerElementPartSize)
            }
        }
    }

    private func setupView() {
        leftView = searchContainerView
        leftViewMode = .always

        rightView = clearContainerView
        rightViewMode = .whileEditing

        font = Font.font
        textColor = Color.textColor
        backgroundColor = Color.backgroundColor

        layer.cornerRadius = CornerRadius.cornerRadius

        setupShadow(.longBlue)
    }

    private func setupBinding() {
        clearButton.rx.tap
            .mapTo("")
            .bind(to: rx.updateText)
            .disposed(by: disposeBag)
    }

    // MARK: - Public methods

    func setPlaceholder(string: String) {
        attributedPlaceholder = NSAttributedString(string: string, font: Font.placeholderFont, color: Color.placeholderColor)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension SearchTextField {
    fileprivate enum Color {
        static let textColor = Style.Color.black
        static let placeholderColor = UIColor(hex: "#706E8B")
        static let backgroundColor = Style.Color.white
    }

    fileprivate enum CornerRadius {
        static let cornerRadius: CGFloat = 12
    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {
        static let font = Style.Font.sanFrancisco(.regular, size: 14)
        static let placeholderFont = Style.Font.sanFrancisco(.regular, size: 14)
    }

    fileprivate enum Image {
        static let searchImageViewImage = Media.icon(.search, size: .x24)
        static let clearButtonImage = Media.icon(.searchClear, size: .x24)
    }

    fileprivate enum Metric {
        static let containerWidth: CGFloat = 40
        static let containerHeight: CGFloat = 40
        static let containerElementPartSize: CGFloat = 24
        static let containerElementLeftOffset: CGFloat = 8
        static let containerElementRightOffset: CGFloat = -8
    }
}

// MARK: - Reactive interface

extension Reactive where Base: SearchTextField {
    var clearAction: ControlEvent<Void> {
        return ControlEvent(events: base.clearButton.rx.tap)
    }
}
