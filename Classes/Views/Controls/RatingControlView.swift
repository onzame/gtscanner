//
//  RatingControlView.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class RatingControlView: UIView {

    // MARK: - UI components

    let ratingView = RatingView().then {
        $0.rating = 0
        $0.settings.starSize = Metric.rateViewHeight
        $0.settings.starMargin = Metric.rateViewMargin
    }

    let stackView = UIStackView().then {
        $0.axis = .vertical
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(stackView)

        stackView.addArrangedSubview(ratingView)
    }

    private func setupLayout() {
        stackView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Metric.stackViewTopOffset)
            make.bottom.equalToSuperview().offset(Metric.stackViewBottomOffset)
            make.centerX.equalToSuperview()
            make.left.greaterThanOrEqualToSuperview()
            make.right.lessThanOrEqualToSuperview()
        }
    }

    private func setupView() {
        backgroundColor = Color.backgroundColor
        layer.cornerRadius = CornerRadius.cornerRadius
    }

}

// MARK: - Extensions
// MARK: - Constants

extension RatingControlView {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.brightYellow?.withAlphaComponent(0.1)
    }

    fileprivate enum CornerRadius {
        static let cornerRadius: CGFloat = 28
    }

    fileprivate enum Metric {
        static let rateViewHeight: Double = 32
        static let rateViewMargin: Double = Screen.isLowWidthScreen ? 8 : 16
        static let stackViewTopOffset: CGFloat = 20
        static let stackViewBottomOffset: CGFloat = -20
    }
}

// MARK: - Reactive interface

extension Reactive where Base: RatingControlView {
    var changeRatingAction: ControlEvent<Double> {
        return base.ratingView.rx.changeRatingAction
    }

    var selectRatingAction: ControlEvent<Void> {
        return base.ratingView.rx.selectRatingAction
    }

    var rating: Binder<Double> {
        return base.ratingView.rx.rating
    }

    var ratingText: Binder<String?> {
        return base.ratingView.rx.text
    }
}
