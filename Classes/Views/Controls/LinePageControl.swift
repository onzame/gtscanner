//
//  LinePageControl.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ImageSlideshow
import RxSwift
import RxCocoa

class LineView: UIView {

    var selectedColor: UIColor?
    var defaultColor: UIColor?

    var isSelected: Bool = false {
        didSet {
            isSelected ? setBackgroundColor(selectedColor) : setBackgroundColor(defaultColor)
        }
    }
}

class LinePageControl: UIView, PageIndicatorView {

    var view: UIView {
        return self
    }

    var page: Int = 0 {
        didSet {
            updateSelected(page)
        }
    }

    var numberOfPages: Int = 0 {
        didSet {
            updateAmount(numberOfPages)
        }
    }

    // MARK: - UI components

    let stackView = UIStackView().then {
        $0.spacing = Metric.stackViewSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(stackView)
    }

    private func setupLayout() {
        stackView.snp.makeConstraints { (make) in
            make.height.equalTo(Metric.height)
            make.centerX.equalToSuperview()
        }
    }

    // MARK: - Private methods

    private func updateSelected(_ index: Int) {
        stackView.arrangedSubviews.enumerated().forEach { lineIndex, lineView in
            if let lineView = lineView as? LineView {
                lineView.isSelected = lineIndex == index
            }
        }
    }

    private func updateAmount(_ amount: Int) {
        guard amount > 1 else { return }

        let arrangedSubviews = stackView.arrangedSubviews

        for view in arrangedSubviews {
            view.removeFromSuperview()
        }

        for _ in 0 ..< amount {
            stackView.addArrangedSubview(
                LineView().then {
                    $0.setCornerRadius(CornerRadius.lineViewCornerRadius)
                    $0.defaultColor = Color.lineViewDefaultColor
                    $0.selectedColor = Color.lineViewSelectedColor
                }
            )
        }

        stackView.arrangedSubviews.forEach { view in
            view.snp.makeConstraints { (make) in
                make.width.equalTo(Metric.lineViewWidth)
                make.height.equalTo(Metric.height)
            }
        }

        arrangedSubviews.first?.setBackgroundColor(Color.lineViewSelectedColor)
    }

    override func sizeToFit() {
        frame.size = CGSize(width: Metric.width, height: Metric.height)
    }

    // MARK: - Utils

    func refresh() {
        updateSelected(page)
    }
}

// MARK: - Extensions
// MARK: - Constants

extension LinePageControl {
    enum Color {
        static let lineViewDefaultColor = UIColor(hex: "#A7B3CB")
        static let lineViewSelectedColor = Style.Color.black
    }

    enum CornerRadius {
        static let lineViewCornerRadius: CGFloat = 1
    }

    enum Metric {
        static let width: CGFloat = 269
        static let height: CGFloat = 2
        static let stackViewSpacing: CGFloat = 6
        static let lineViewWidth: CGFloat = 49
    }
}

// MARK: - Reactive interface

extension Reactive where Base: LinePageControl {
    var numberOfPages: Binder<Int> {
        return Binder(base, binding: { view, numberOfPages in
            view.numberOfPages = numberOfPages
        })
    }

    var refresh: Binder<Void> {
        return Binder(base, binding: { view, _ in
            view.refresh()
        })
    }
}
