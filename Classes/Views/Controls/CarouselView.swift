//
//  CarouselView.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher
import ImageSlideshow

class CarouselView: UIView {

    // MARK: - UI components

    let imageSlideshowView = ImageSlideshow().then {
        let horizontalPosition = PageIndicatorPosition.Horizontal.center
        let verticalPosition = PageIndicatorPosition.Vertical.customUnder(padding: Metric.imageSlideshowViewPageIndicatorBottomPadding)
        $0.pageIndicatorPosition = .init(horizontal: horizontalPosition, vertical: verticalPosition)
        $0.pageIndicator = LinePageControl()
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(imageSlideshowView)
    }

    private func setupLayout() {
        imageSlideshowView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

    func resetPageIndicator() {
        if let pageIndicator = imageSlideshowView.pageIndicator as? LinePageControl {
            pageIndicator.refresh()
        }
    }
}

// MARK: - Extensions
// MARK: - Constants

extension CarouselView {
    fileprivate enum Color {
        static let pageIndicatorTextColor = Style.Color.white
    }

    fileprivate enum Metric {
        static let imageSlideshowViewTopOffset: CGFloat = 20
        static let imageSlideshowViewBottomOffset: CGFloat = -20
        static let imageSlideshowViewPageIndicatorBottomPadding: CGFloat = 20
    }
}

extension Reactive where Base: CarouselView {
    var images: Binder<[URL]> {
        return Binder(base, binding: { view, urls in
            let transition = ImageTransition.fade(0.2)
            let transitionOption = KingfisherOptionsInfoItem.transition(transition)
            let options = [transitionOption]
            let placeholderImage = Media.image(.productPreviewPlaceholder)

            var inputs: [InputSource] = urls
                .prefix(3)
                .map({ KingfisherSource(url: $0, placeholder: placeholderImage, options: options) })

            if let placeholderImage = placeholderImage, inputs.isEmpty {
                inputs.append(ImageSource(image: placeholderImage))
            }

            view.imageSlideshowView.setImageInputs(inputs)
        })
    }

    var refresh: Binder<Void> {
        return Binder(base, binding: { view, _ in
            view.resetPageIndicator()
        })
    }
}
