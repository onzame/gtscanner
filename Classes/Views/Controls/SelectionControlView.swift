//
//  SelectionControlView.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SelectionControlView: UIView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    let actionButton = UIButton().then {
        $0.setTitleFont(Font.actionButtonTitleFont)
        $0.setTitleColor(Color.actionButtonTitleColor, for: .normal)
        $0.setTitleColor(Color.actionButtonFilledTitleColor, for: .disabled)
        $0.contentEdgeInsets = .zero
    }

    let arrowButton = UIButton().then {
        $0.setImage(Image.arrowButtonImage, for: .normal)
        $0.tintColor = Color.arrowButtonTintColor
    }

    let changeButton = UIButton().then {
        $0.setTitleFont(Font.changeButtonTitleFont)
        $0.setTitleColor(Color.changeButtonTintColor, for: .normal)
        $0.isHidden = true
    }

    let dividerView = UIView().then {
        $0.setBackgroundColor(Color.dividerViewBackgroundColor)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
        addSubview(changeButton)
        addSubview(actionButton)
        addSubview(arrowButton)
        addSubview(dividerView)
    }

    private func setupLayout() {
        dividerView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(Metric.dividerViewHeight)
        }

        actionButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.bottom.equalTo(dividerView.snp.top).offset(Metric.actionButtonBottomOffset)
            make.height.equalTo(Metric.actionButtonHeight)
        }

        arrowButton.snp.makeConstraints { (make) in
            make.left.greaterThanOrEqualTo(actionButton.snp.right).offset(Metric.arrowButtonLeftOffset)
            make.right.equalToSuperview()
            make.centerY.equalTo(actionButton)
            make.width.height.equalTo(Metric.arrowButtonPartSize)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(actionButton.snp.top).offset(Metric.titleLabelBottomOffset)
        }

        changeButton.snp.makeConstraints { (make) in
            make.top.right.equalToSuperview()
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension SelectionControlView {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.newGray
        static let actionButtonTitleColor = Style.Color.brightBlue
        static let actionButtonFilledTitleColor = Style.Color.black
        static let dividerViewBackgroundColor = Style.Color.grayBlue
        static let arrowButtonTintColor = Style.Color.secondaryText
        static let changeButtonTintColor = Style.Color.brightBlue
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let actionButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let actionButtonFilledTitleFont = Style.Font.sanFrancisco(.regular, size: 18)
        static let changeButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Image {
        static let arrowButtonImage = Media.icon(.rightChevron, size: .x18)
    }

    fileprivate enum Metric {
        static let dividerViewHeight: CGFloat = 1

        static let actionButtonBottomOffset: CGFloat = -4
        static let actionButtonHeight: CGFloat = 22

        static let arrowButtonLeftOffset: CGFloat = 8
        static let arrowButtonPartSize: CGFloat = 18

        static let titleLabelBottomOffset: CGFloat = -6
    }
}

// MARK: - Reactive interface

extension Reactive where Base: SelectionControlView {
    var selectionAction: ControlEvent<Void> {
        let actionTap = base.actionButton.rx.tap.asObservable()
        let arrowTap = base.arrowButton.rx.tap.asObservable()
        let changeTap = base.changeButton.rx.tap.asObservable()
        return ControlEvent(events: Observable.merge(actionTap, arrowTap, changeTap))
    }

    var selectionBinding: Binder<String> {
        return Binder(base, binding: { view, title in
            view.actionButton.setTitle(title, for: .normal)
            view.actionButton.setTitleFont(SelectionControlView.Font.actionButtonFilledTitleFont)
            view.actionButton.isEnabled = false
            view.changeButton.isHidden = false
            view.arrowButton.isHidden = true
        })
    }
}
