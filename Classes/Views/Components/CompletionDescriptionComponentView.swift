//
//  CompletionDescriptionComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class CompletionDescriptionComponentView: UIView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.text = "Title"
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.numberOfLines = 0
    }

    let subtitleLabel = UILabel().then {
        $0.text = "Subtitle"
        $0.font = Font.subtitleLabelFont
        $0.textColor = Color.subtitleLabelTextColor
        $0.numberOfLines = 0
    }

    let stackView = UIStackView().then {
        $0.axis = .vertical
        $0.spacing = Metric.stackViewSpacing
    }

    let iconImageView = UIImageView()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(stackView)
        addSubview(iconImageView)

        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
    }

    private func setupLayout() {
        stackView.snp.makeConstraints { (make) in
            make.left.bottom.equalToSuperview()
        }

        iconImageView.snp.makeConstraints { (make) in
            make.left.equalTo(stackView.snp.right).offset(Metric.iconImageLeftOffset)
            make.right.top.equalToSuperview()
            make.centerY.equalTo(titleLabel)
            make.width.height.equalTo(Metric.iconImageViewPartSize)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompletionDescriptionComponentView {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
        static let subtitleLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let subtitleLabelFont = Style.Font.sanFrancisco(.regular, size: 14)
    }

    fileprivate enum Metric {
        static let stackViewSpacing: CGFloat = 24
        static let iconImageViewPartSize: CGFloat = 80
        static let iconImageLeftOffset: CGFloat = 8
    }
}
