//
//  CompletionActionComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class CompletionActionComponentView: UIView {

    // MARK: - UI components

    let primaryContainerView = UIView().then {
        $0.backgroundColor = Color.primaryContainerViewBackgroundColor
    }

    let primaryButton = UIButton(type: .system).then {
        $0.setTitleColor(Color.primaryButtonTitleColor, for: .normal)
        $0.titleLabel?.font = Font.actionTitleFont
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        primaryContainerView.setCornerRadius(
            corners: .layerMaxXMinYCorner,
            radius: CornerRadius.primaryContainerViewCornerRadius
        )
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(primaryContainerView)
        primaryContainerView.addSubview(primaryButton)
    }

    private func setupLayout() {
        primaryContainerView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(Metric.primaryContainerViewBottomOffset)
        }

        primaryButton.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(safeBottomConstraintItem)
            make.height.equalTo(Metric.actionHeight)
        }
    }

    private func setupView() {

    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompletionActionComponentView {
    fileprivate enum Color {
        static let primaryContainerViewBackgroundColor = Style.Color.brightYellow
        static let primaryButtonTitleColor = Style.Color.black
    }

    fileprivate enum CornerRadius {
        static let primaryContainerViewCornerRadius: CGFloat = 70
    }

    fileprivate enum Font {
        static let actionTitleFont = Style.Font.sanFrancisco(.medium, size: 18)
    }

    fileprivate enum Metric {
        static let actionHeight: CGFloat = 80
        static let primaryContainerViewBottomOffset: CGFloat = 60
    }
}
