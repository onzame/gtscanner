//
//  ActionsComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ActionsComponentView: UIView {

    // MARK: - UI components

    let primaryContainerView = UIView()

    let primaryButton = UIButton(type: .system).then {
        $0.setTitleColor(Color.primaryButtonTitleColor, for: .normal)
        $0.setTitleColor(Color.primaryButtonDisabledTitleColor, for: .disabled)
        $0.titleLabel?.font = Font.actionTitleFont
    }

    let secondaryContainerView = UIView()

    let secondaryButton = UIButton(type: .system).then {
        $0.setTitleColor(Color.secondaryButtonTitleColor, for: .normal)
        $0.titleLabel?.font = Font.actionTitleFont
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        primaryContainerView.setCornerRadius(
            corners: .layerMinXMinYCorner,
            radius: CornerRadius.primaryContainerViewCornerRadius
        )
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(primaryContainerView)
        addSubview(secondaryContainerView)

        primaryContainerView.addSubview(primaryButton)
        secondaryContainerView.addSubview(secondaryButton)
    }

    private func setupLayout() {
        primaryContainerView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
        }

        secondaryContainerView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.right.equalTo(primaryContainerView.snp.left)
            make.width.equalTo(primaryContainerView)
        }

        [primaryButton, secondaryButton].forEach {
            $0.snp.makeConstraints { (make) in
                make.left.top.right.equalToSuperview()
                make.bottom.equalTo(safeBottomConstraintItem)
                make.height.equalTo(Metric.actionHeight)
            }
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ActionsComponentView {
    fileprivate enum Color {
        static let primaryButtonTitleColor = Style.Color.black
        static let primaryButtonDisabledTitleColor = Style.Color.black.withAlphaComponent(0.5)
        static let secondaryButtonTitleColor = Style.Color.newGray
    }

    fileprivate enum CornerRadius {
        static let primaryContainerViewCornerRadius: CGFloat = 14
    }

    fileprivate enum Font {
        static let actionTitleFont = Style.Font.sanFrancisco(.medium, size: 18)
    }

    fileprivate enum Metric {
        static let actionHeight: CGFloat = 64
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ActionsComponentView {
    var primaryAction: ControlEvent<Void> {
        return ControlEvent(events: base.primaryButton.rx.tap)
    }

    var secondaryAction: ControlEvent<Void> {
        return ControlEvent(events: base.secondaryButton.rx.tap)
    }
}
