//
//  BadgeView.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BadgeView: UIView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.textAlignment = .center
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.titleLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.titleLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.titleLabelBottomOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension BadgeView {
    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 8
        static let titleLabelTopOffset: CGFloat = 4
        static let titleLabelRightOffset: CGFloat = -8
        static let titleLabelBottomOffset: CGFloat = -4
    }
}

// MARK: - Reactive interface

extension Reactive where Base: BadgeView {
    var text: Binder<String?> {
        return base.titleLabel.rx.text
    }
}
