//
//  DescriptionStackView.swift
//  Gorod
//
//  Created by Sergei Fabian on 26.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DescriptionStackView: UIStackView {

    // MARK: - UI components

    let titleLabel = UILabel()
    let subtitleLabel = UILabel()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addArrangedSubview(titleLabel)
        addArrangedSubview(subtitleLabel)
    }

    private func setupLayout() {
        axis = .vertical
    }

}

extension Reactive where Base: DescriptionStackView {
    var title: Binder<String?> {
        return Binder(base, binding: { stackView, title in
            stackView.titleLabel.text = title
            stackView.layoutIfNeeded()
        })
    }

    var subtitle: Binder<String?> {
        return Binder(base, binding: { stackView, subtitle in
            stackView.subtitleLabel.text = subtitle
            stackView.layoutIfNeeded()
        })
    }
}
