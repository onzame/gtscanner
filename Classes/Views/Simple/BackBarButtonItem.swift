//
//  BackBarButtonItem.swift
//  Gorod
//
//  Created by Sergei Fabian on 09.04.2020.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class BackBarButtonItem: UIBarButtonItem {
    var button: UIButton {
        return customView as? UIButton ?? UIButton()
    }
}

extension Reactive where Base: BackBarButtonItem {
    var tapAction: ControlEvent<Void> {
        return base.button.rx.tap
    }
}

extension UIButton {
    static var backButton: UIButton {
        return UIButton(type: .system).then {
            $0.frame = CGRect(width: 37, height: 44)
            $0.setImage(Media.icon(.leftChevron, size: .x18), for: .normal)
        }
    }
}
