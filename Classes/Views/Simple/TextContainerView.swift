//
//  TextContainerView.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TextContainerView: UIView {

    // MARK: - UI Components

    let textLabel = UILabel().then {
        $0.text = "Text"
        $0.textAlignment = .center
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(textLabel)
    }

    private func setupLayout() {
        textLabel.snp.makeConstraints { (make) in
            make.left.greaterThanOrEqualToSuperview().offset(Metric.labelLeftOffset)
            make.top.greaterThanOrEqualToSuperview().offset(Metric.labelTopOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.labelRightOffset)
            make.bottom.lessThanOrEqualToSuperview().offset(Metric.labelBottomOffset)
            make.center.equalToSuperview()
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension TextContainerView {
    fileprivate enum Metric {
        static let labelBottomOffset: CGFloat = -8
        static let labelRightOffset: CGFloat = -16
        static let labelLeftOffset: CGFloat = 16
        static let labelTopOffset: CGFloat = 8
    }
}

// MARK: - Reactive interface

extension Reactive where Base: TextContainerView {
    var text: Binder<String?> {
        return Binder(base, binding: { view, text in
            view.textLabel.text = text
        })
    }
}
