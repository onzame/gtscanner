//
//  ImageContainerView.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class ImageContainerView: UIView {

    let imageView = UIImageView()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(imageView)
    }

    private func setupLayout() {
        imageView.addEdgesConstraints()
    }

}
