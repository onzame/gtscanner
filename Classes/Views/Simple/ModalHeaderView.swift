//
//  ModalHeaderView.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class ModalHeaderView: UIView {

    let indicatorImageView = UIImageView().then {
        $0.image = Image.indicatorImageViewImage
    }

    let leftView = UIView().then {
        $0.backgroundColor = Color.leftViewBackgroundColor
    }

    let rightView = UIView().then {
        $0.backgroundColor = Color.rightViewBackgroundColor
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(leftView)
        addSubview(indicatorImageView)
        addSubview(rightView)
    }

    private func setupLayout() {
        indicatorImageView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.width.equalTo(Metric.indicatorImageViewWidth)
            make.height.equalTo(Metric.indicatorImageViewHeight)
            make.center.equalToSuperview()
        }

        leftView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.right.equalTo(indicatorImageView.snp.left)
        }

        rightView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.left.equalTo(indicatorImageView.snp.right)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ModalHeaderView {
    fileprivate enum Color {
        static let leftViewBackgroundColor = Style.Color.white
        static let rightViewBackgroundColor = Style.Color.white
    }

    fileprivate enum Image {
        static let indicatorImageViewImage = Media.image(.interactiveIndicator)
    }

    fileprivate enum Metric {
        static let indicatorImageViewHeight: CGFloat = 16
        static let indicatorImageViewWidth: CGFloat = 92
    }
}
