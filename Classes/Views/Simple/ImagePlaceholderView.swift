//
//  ImagePlaceholderView.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import Kingfisher

class ImagePlaceholderView: UIView, Placeholder {

    let imageView = UIImageView().then {
        $0.contentMode = .center
    }

    // MARK: - Lifecycle

    init(image: UIImage?, imageSize: CGSize? = nil) {
        super.init(frame: .zero)
        commonInit()

        if let imageSize = imageSize {
            imageView.image = image?.resize(targetSize: imageSize)
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(imageView)
    }

    private func setupLayout() {
        imageView.addEdgesConstraints()
    }

}

// MARK: - Extensions
// MARK: - Factories

extension ImagePlaceholderView {
    static func product(imageSize: CGSize = CGSize(width: 32, height: 36)) -> ImagePlaceholderView {
        return ImagePlaceholderView(image: Media.image(.productPlaceholder), imageSize: imageSize)
    }
}
