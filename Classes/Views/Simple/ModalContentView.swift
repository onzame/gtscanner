//
//  ModalContentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class ModalContentView: UIView {

    // MARK: - UI components

    let headerView = ModalHeaderView()

    let bodyView = UIView().then {
        $0.backgroundColor = Color.bodyViewBackgroundColor
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        setCornerRadius(corners: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: CornerRadius.cornerRadius)
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(headerView)
        addSubview(bodyView)
    }

    private func setupLayout() {
        headerView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }

        bodyView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
        }
    }

    private func setupView() {
        layer.masksToBounds = true
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ModalContentView {
    fileprivate enum Color {
        static let bodyViewBackgroundColor = Style.Color.white
    }

    fileprivate enum CornerRadius {
        static let cornerRadius: CGFloat = 12
    }
}
