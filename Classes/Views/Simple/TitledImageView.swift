//
//  TitledImageView.swift
//  Gorod
//
//  Created by Sergei Fabian on 26.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TitledImageView: UIView {

    // MARK: - UI components

    let imageView = UIImageView()

    let titleLabel = UILabel().then {
        $0.text = "Title"
    }

    let stackView = UIStackView().then {
        $0.alignment = .center
    }

    // MARK: - Lifecycle

    init(imageViewSize: CGSize) {
        super.init(frame: .zero)
        commonInit(imageViewSize: imageViewSize)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit(imageViewSize: CGSize? = nil) {
        setupHierarchy()
        setupLayout(imageViewSize: imageViewSize)
    }

    private func setupHierarchy() {
        addSubview(stackView)
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
    }

    private func setupLayout(imageViewSize: CGSize? = nil) {
        stackView.addEdgesConstraints()

        if let imageViewSize = imageViewSize {
            imageView.snp.makeConstraints { (make) in
                make.width.equalTo(imageViewSize.width)
                make.height.equalTo(imageViewSize.height)
            }
        }
    }

}

// MARK: - Extensions
// MARK: - Reactive interface

extension Reactive where Base: TitledImageView {
    var title: Binder<String?> {
        return Binder(base, binding: { view, title in
            view.titleLabel.text = title
        })
    }
}
