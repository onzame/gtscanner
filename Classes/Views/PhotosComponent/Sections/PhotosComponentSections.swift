//
//  PhotosComponentSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources

// MARK: - Sections

enum PhotosComponentSectionModel {
    case section(items: [PhotosComponentItemModel])
}

extension PhotosComponentSectionModel: AnimatableSectionModelType {
    var identity: String {
        switch self {
        case .section:
            return "section"
        }
    }

    var items: [PhotosComponentItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: PhotosComponentSectionModel, items: [PhotosComponentItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum PhotosComponentItemModel {
    case photo(PhotosComponentPhotoCollectionViewModel)
    case placeholder
}

extension PhotosComponentItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .photo(let viewModel):
            return viewModel.identity
        case .placeholder:
            return 0
        }
    }
}

extension PhotosComponentItemModel: Equatable {
    static func == (lhs: PhotosComponentItemModel, rhs: PhotosComponentItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.photo(let left), .photo(let right)):
            return left.currentState == right.currentState
        case (.placeholder, .placeholder):
            return true
        default:
            return false
        }
    }
}

extension Array where Element == Photo {
    func mapToSections() -> [PhotosComponentSectionModel] {
        var items = map({ photo -> PhotosComponentItemModel in
            let viewModel = PhotosComponentPhotoCollectionViewModel(photo: photo)
            let itemModel = PhotosComponentItemModel.photo(viewModel)
            return itemModel
        })

        if items.count < 10 {
            items.append(.placeholder)
        }

        return [PhotosComponentSectionModel.section(items: items)]
    }
}
