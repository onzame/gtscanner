//
//  PhotosComponentPhotoCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxDataSources

final class PhotosComponentPhotoCollectionViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(photo: Photo) {
        initialState = State(photo: photo)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension PhotosComponentPhotoCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension PhotosComponentPhotoCollectionViewModel {
    struct State {
        let photo: Photo
    }
}

// MARK: - Equatable

extension PhotosComponentPhotoCollectionViewModel.State: Equatable {
    static func == (lhs: PhotosComponentPhotoCollectionViewModel.State, rhs: PhotosComponentPhotoCollectionViewModel.State) -> Bool {
        return lhs.photo == rhs.photo
    }
}

// MARK: - IdentifiableType

extension PhotosComponentPhotoCollectionViewModel: IdentifiableType {
    var identity: Int {
        return currentState.photo.image.hashValue
    }
}
