//
//  PhotosComponentPlaceholderCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class PhotosComponentPlaceholderCollectionViewCell: BaseCollectionViewCell {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.setCornerRadius(CornerRadius.containerViewCornerRadius)
    }

    let imageView = UIImageView().then {
        $0.image = Image.imageViewImage
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(containerView)
        containerView.addSubview(imageView)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension PhotosComponentPlaceholderCollectionViewCell {
    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 6
    }

    fileprivate enum Image {
        static let imageViewImage = Media.image(.photoPlaceholder)
    }
}
