//
//  PhotosComponentPhotoCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift

class PhotosComponentPhotoCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.setCornerRadius(CornerRadius.containerViewCornerRadius)
    }

    let imageView = UIImageView().then {
        $0.contentMode = .scaleAspectFill
    }

    let deleteButton = UIButton(type: .system).then {
        $0.setImage(Image.deleteButtonImage, for: .normal)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(containerView)
        containerView.addSubview(imageView)
        containerView.addSubview(deleteButton)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        imageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        deleteButton.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Metric.deleteButtonTopOffset)
            make.right.equalToSuperview().offset(Metric.deleteButtonRightOffset)
            make.width.height.equalTo(Metric.deleteButtonPartSize)
        }
    }

    // MARK: - Bindings

    func bind(reactor: PhotosComponentPhotoCollectionViewModel) {
        reactor.state
            .map({ $0.photo.image })
            .bind(to: imageView.rx.image)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension PhotosComponentPhotoCollectionViewCell {
    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 6
    }

    fileprivate enum Image {
        static let deleteButtonImage = Media.icon(.searchClear, size: .x24)
    }

    fileprivate enum Metric {
        static let deleteButtonTopOffset: CGFloat = 6
        static let deleteButtonRightOffset: CGFloat = -6
        static let deleteButtonPartSize: CGFloat = 24
    }
}
