//
//  PhotosComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class PhotosComponentView: UIView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.text = Localization.Photos.title
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    let collectionViewLayout = UICollectionViewFlowLayout().then {
        $0.scrollDirection = .horizontal
        $0.itemSize = CGSize(width: Metric.collectionViewItemWidth, height: Metric.collectionViewItemHeight)
        $0.minimumInteritemSpacing = Metric.collectionViewItemSpacing
        $0.sectionInset = UIEdgeInsets(
            top: Metric.collectionViewSectionVerticalSpacing,
            left: Metric.collectionViewSectionHorizontalSpacing,
            bottom: Metric.collectionViewSectionVerticalSpacing,
            right: Metric.collectionViewSectionHorizontalSpacing
        )
    }

    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout).then {
        $0.showsHorizontalScrollIndicator = false
        $0.setBackgroundColor(Color.collectionViewBackgroundColor)
        $0.register(Reusable.photoCell)
        $0.register(Reusable.placeholderCell)
    }

    // MARK: - DataSource

    lazy var dataSource = RxCollectionViewSectionedAnimatedDataSource<PhotosComponentSectionModel>(
        configureCell: { [unowned deleteActionPublisher] (_, collectionView, indexPath, model) -> UICollectionViewCell in
            switch model {
            case .photo(let viewModel):
                let cell = collectionView.dequeue(Reusable.photoCell, for: indexPath)
                cell.reactor = viewModel
                cell.deleteButton.rx.tap
                    .withLatestFrom(viewModel.state)
                    .map({ $0.photo })
                    .bind(to: deleteActionPublisher)
                    .disposed(by: cell.disposeBag)

                return cell
            case .placeholder:
                return collectionView.dequeue(Reusable.placeholderCell, for: indexPath)
            }
        }
    )

    // MARK: - Publishers

    fileprivate let deleteActionPublisher = PublishSubject<Photo>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
        addSubview(collectionView)
    }

    private func setupLayout() {
        collectionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(Metric.collectionViewHeight)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(Metric.titleLabelRightOffset)
            make.bottom.equalTo(collectionView.snp.top).offset(Metric.titleLabelBottomOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension PhotosComponentView {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.newGray
        static let collectionViewBackgroundColor = Style.Color.white
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Metric {
        static let titleLabelBottomOffset: CGFloat = -6
        static let titleLabelLeftOffset: CGFloat = 20
        static let titleLabelRightOffset: CGFloat = -20

        static let collectionViewHeight: CGFloat = 128
        static let collectionViewItemWidth: CGFloat = 128
        static let collectionViewItemHeight: CGFloat = 128
        static let collectionViewItemSpacing: CGFloat = 8
        static let collectionViewSectionVerticalSpacing: CGFloat = 0
        static let collectionViewSectionHorizontalSpacing: CGFloat = 20
    }

    fileprivate enum Reusable {
        static let photoCell = ReusableCell<PhotosComponentPhotoCollectionViewCell>()
        static let placeholderCell = ReusableCell<PhotosComponentPlaceholderCollectionViewCell>()
    }
}

// MARK: - Reactive interface

extension Reactive where Base: PhotosComponentView {
    var addNewAction: ControlEvent<Void> {
        let source = base.collectionView.rx.itemSelected(dataSource: base.dataSource)
            .filter({ model -> Bool in
                if case PhotosComponentItemModel.placeholder = model {
                    return true
                } else {
                    return false
                }
            })
            .mapToVoid()

        return ControlEvent(events: source)
    }

    var deletePhotoAction: ControlEvent<Photo> {
        return ControlEvent(events: base.deleteActionPublisher)
    }
}
