//
//  ScanModuleFactory.swift
//  GTScanner
//
//  Created by Sergei Fabian on 22.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxViewController
import RxOptional
import RxSwiftExt
import Then
import SnapKit
import Kingfisher
import ReusableKit
import Moya

public final class ScanModuleFactory {

    private static var coordinator: ScannerCoordinator?

    private static var startSubscribption: Disposable?

    private init() { }

    public static func configure(with configuration: EnvironmentConfiguration = .default) {
        Environment.setup(configuration: configuration)
    }

    public static func buildRootViewController(provider: AccessTokenProviderType, analyticsDelegate: GorodGoodsDelegate) -> UIViewController {
        let container = DependenciesContainer(accessTokenProvider: provider, analyticsDelegate: analyticsDelegate)
        let coordinator = ScannerCoordinator(container: container)
        self.coordinator = coordinator
        startSubscribption = coordinator.start().subscribe()
        return coordinator.rootViewController
    }

    public static func freeRootViewController() {
        startSubscribption?.dispose()
        coordinator = nil
    }
}
