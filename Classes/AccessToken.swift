//
//  AccessToken.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

public protocol AccessTokenType {
    var token: String { get }
}
