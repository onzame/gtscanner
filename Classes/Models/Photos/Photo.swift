//
//  Photo.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

struct Photo {
    let image: UIImage
}

// MARK: - Extensions
// MARK: - Equatable

extension Photo: Equatable {
    static func == (lhs: Photo, rhs: Photo) -> Bool {
        return lhs.image == rhs.image
    }
}

// MARK: - Array

extension Array where Element == Photo {
    func mapToData() -> [Data] {
        return compactMap({ $0.image.jpegData(compressionQuality: 0.5) })
    }
}
