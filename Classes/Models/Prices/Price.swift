//
//  Price.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct Price: Codable {
    let id: Int
    let shopName: String
    let shopLogoUrl: URL?
    let price: Double
    let branch: ShopBranch
    let permissions: PricePermissions

    enum CodingKeys: String, CodingKey {
        case id
        case shopName = "name"
        case shopLogoUrl = "logo"
        case price
        case branch
        case permissions
    }
}

// MARK: - Extensions
// MARK: - QueryableType

extension Price: QueryableType {
    var identifier: Int {
        return id
    }
}

// MARK: - Equatable

extension Price: Equatable {
    static func == (lhs: Price, rhs: Price) -> Bool {
        return lhs.id == rhs.id
            && lhs.shopName == rhs.shopName
            && lhs.shopLogoUrl == rhs.shopLogoUrl
            && lhs.price == rhs.price
            && lhs.branch == rhs.branch
            && lhs.permissions == rhs.permissions
    }
}
