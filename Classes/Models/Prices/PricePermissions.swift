//
//  PricePermissions.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct PricePermissions: Codable {
    let canDelete: Bool
}

// MARK: - Extensions
// MARK: - Equatable

extension PricePermissions: Equatable {
    static func == (lhs: PricePermissions, rhs: PricePermissions) -> Bool {
        return lhs.canDelete == rhs.canDelete
    }
}
