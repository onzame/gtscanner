//
//  ScannedObjectMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import CoreLocation

struct ScannedObjectMetadata {
    let gtin: String
    let location: Location?
}
