//
//  ScannedObject.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import CoreGraphics

struct ScannedObject {
    let value: String
}
