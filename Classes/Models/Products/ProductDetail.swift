//
//  ProductDetail.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductDetail: Codable {
    let id: Int?
    let title: String
    let value: String
}

// MARK: - Equatable

extension ProductDetail: Equatable {
    static func == (lhs: ProductDetail, rhs: ProductDetail) -> Bool {
        return lhs.id == rhs.id
            && lhs.title == rhs.title
            && lhs.value == rhs.value
    }
}
