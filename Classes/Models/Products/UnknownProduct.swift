//
//  UnknownProduct.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct UnknownProduct {
    let gtin: String
}

// MARK: - Extensions
// MARK: - Equatable

extension UnknownProduct: Equatable {
    static func == (lhs: UnknownProduct, rhs: UnknownProduct) -> Bool {
        return lhs.gtin == rhs.gtin
    }
}
