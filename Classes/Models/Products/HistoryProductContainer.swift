//
//  HistoryProductContainer.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct HistoryProductContainer: Codable {
    let id: Int
    let createdAt: TimeInterval
    let updatedAt: TimeInterval
    let product: HistoryProduct
}

// MARK: - Extensions
// MARK: - Queryable

extension HistoryProductContainer: QueryableType {
    var identifier: Int {
        return id
    }
}

// MARK: - Dated

extension HistoryProductContainer: Dated {
    var date: Date {
        return Date(timeIntervalSince1970: updatedAt)
    }
}
