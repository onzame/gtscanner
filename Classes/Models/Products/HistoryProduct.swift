//
//  HistoryProduct.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct HistoryProduct: Codable {
    let id: Int
    let name: String
    let gtin: String
    let description: String
    let rating: Double
    let images: [ProductImage]
    let avgPrice: Double?
    let productionPlace: String

    var domainProduct: Product {
        return Product(
            id: id,
            name: name,
            gtin: gtin,
            category: Category.stub,
            amounts: ProductAmounts.stub,
            permissions: ProductPermissions.stub,
            lists: ProductLists.stub,
            shops: [],
            reviews: [],
            description: description,
            avgPrice: avgPrice,
            productionPlace: productionPlace,
            rating: rating,
            images: images,
            source: nil
        )
    }
}

// MARK: - Extensions
// MARK: - Equatable

extension HistoryProduct: Equatable {
    static func == (lhs: HistoryProduct, rhs: HistoryProduct) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.gtin == rhs.gtin
            && lhs.description == rhs.description
            && lhs.rating == rhs.rating
            && lhs.images == rhs.images
            && lhs.avgPrice == rhs.avgPrice
            && lhs.productionPlace == rhs.productionPlace
    }
}
