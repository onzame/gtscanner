//
//  ProductImage.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductImage: Codable {
    let id: Int
    let url: URL
}

// MARK: - Extensions
// MARK: - Equatable

extension ProductImage: Equatable {
    static func == (lhs: ProductImage, rhs: ProductImage) -> Bool {
        return lhs.id == rhs.id
            && lhs.url == rhs.url
    }
}
