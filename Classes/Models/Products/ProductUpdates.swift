//
//  ProductUpdates.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductUpdates: Codable {
    let id: Int
    let avgPrice: Double?
    let rating: Double
    let amounts: ProductAmounts
    let permissions: ProductPermissions
}
