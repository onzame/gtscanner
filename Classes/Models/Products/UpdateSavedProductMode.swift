//
//  UpdateSavedProductMode.swift
//  Gorod
//
//  Created by Sergei Fabian on 26.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum UpdateSavedProductMode {
    case add
    case change
}
