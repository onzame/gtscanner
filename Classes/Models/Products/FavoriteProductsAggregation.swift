//
//  FavoriteProductsAggregation.swift
//  Gorod
//
//  Created by Sergei Fabian on 26.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct FavoriteProductsAggregation: Codable {
    let category: Category
    let products: [FavoriteProduct]
}

// MARK: - Extensions
// MARK: - Queryable

extension FavoriteProductsAggregation: QueryableType {
    var identifier: Int {
        return category.id
    }
}
