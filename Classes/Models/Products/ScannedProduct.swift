//
//  ScannedProduct.swift
//  Gorod
//
//  Created by Sergei Fabian on 03.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum ScannedProduct {
    case founded(Product)
    case unknown(UnknownProduct)
}

// MARK: - Extensions
// MARK: - Queryable

extension ScannedProduct: QueryableType {
    var identifier: String {
        switch self {
        case .founded(let product):
            return product.gtin
        case .unknown(let product):
            return product.gtin
        }
    }
}

// MARK: - Equatable

extension ScannedProduct: Equatable {
    static func == (lhs: ScannedProduct, rhs: ScannedProduct) -> Bool {
        switch (lhs, rhs) {
        case (.founded(let left), .founded(let right)):
            return left == right
        case (.unknown(let left), .unknown(gtin: let right)):
            return left == right
        default:
            return false
        }
    }
}

// MARK: - Array

extension Array where Element == ScannedProduct {
    func contains(gtin: String) -> Bool {
        return contains(where: { product -> Bool in
            switch product {
            case .founded(let product):
                return product.gtin == gtin
            case .unknown(let product):
                return product.gtin == gtin
            }
        })
    }
}
