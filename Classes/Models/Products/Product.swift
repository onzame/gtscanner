//
//  Product.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct Product: Codable {
    let id: Int
    let name: String
    let gtin: String
    let category: Category
    let amounts: ProductAmounts
    let permissions: ProductPermissions
    let lists: ProductLists
    let shops: [Shop]?
    let reviews: [Review]
    let description: String?
    let avgPrice: Double?
    let productionPlace: String
    let rating: Double
    let images: [ProductImage]
    let source: String?

    func update(
        id: Int? = nil,
        name: String? = nil,
        gtin: String? = nil,
        category: Category? = nil,
        amounts: ProductAmounts? = nil,
        permissions: ProductPermissions? = nil,
        lists: ProductLists? = nil,
        shops: [Shop]? = nil,
        reviews: [Review]? = nil,
        description: String? = nil,
        avgPrice: Double? = nil,
        productionPlace: String? = nil,
        rating: Double? = nil,
        images: [ProductImage]? = nil,
        source: String? = nil
    ) -> Product {
        return Product(
            id: id ?? self.id,
            name: name ?? self.name,
            gtin: gtin ?? self.gtin,
            category: category ?? self.category,
            amounts: amounts ?? self.amounts,
            permissions: permissions ?? self.permissions,
            lists: lists ?? self.lists,
            shops: shops ?? self.shops,
            reviews: reviews ?? self.reviews,
            description: description ?? self.description,
            avgPrice: avgPrice ?? self.avgPrice,
            productionPlace: productionPlace ?? self.productionPlace,
            rating: rating ?? self.rating,
            images: images ?? self.images,
            source: source ?? self.source
        )
    }

    func update(inCompare: Bool) -> Product {
        return update(lists: ProductLists(inFavorites: lists.inFavorites, inCompare: inCompare))
    }

    func update(updates: ProductUpdates) -> Product {
        return update(amounts: updates.amounts, permissions: updates.permissions, avgPrice: updates.avgPrice, rating: updates.rating)
    }

    func update(updates: CompareProductUpdates) -> Product {
        return update(lists: updates.lists)
    }
}

// MARK: - Extensions
// MARK: - Equatable

extension Product: Equatable {
    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.gtin == rhs.gtin
            && lhs.category == rhs.category
            && lhs.amounts == rhs.amounts
            && lhs.permissions == rhs.permissions
            && lhs.lists == rhs.lists
            && lhs.shops == rhs.shops
            && lhs.reviews == rhs.reviews
            && lhs.description == rhs.description
            && lhs.avgPrice == rhs.avgPrice
            && lhs.productionPlace == rhs.productionPlace
            && lhs.rating == rhs.rating
            && lhs.images == rhs.images
            && lhs.source == rhs.source
    }
}

// MARK: - Query

extension Product: QueryableType {
    var identifier: Int {
        return id
    }
}

// MARK: - Array

extension Array where Element == Product {
    func contains(gtin: String) -> Bool {
        return contains(where: { $0.gtin == gtin })
    }
}
