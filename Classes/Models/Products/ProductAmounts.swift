//
//  ProductAmounts.swift
//  Gorod
//
//  Created by Sergei Fabian on 26.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductAmounts: Codable {
    static let stub = ProductAmounts(reviews: 0, prices: 0, rates: 0)

    let reviews: Int
    let prices: Int
    let rates: Int
}

// MARK: - Extensions
// MARK: - Equatable

extension ProductAmounts: Equatable {
    static func == (lhs: ProductAmounts, rhs: ProductAmounts) -> Bool {
        return lhs.reviews == rhs.reviews
            && lhs.prices == rhs.prices
            && lhs.rates == rhs.rates
    }
}
