//
//  ProductPermissions.swift
//  Gorod
//
//  Created by Sergei Fabian on 27.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductPermissions: Codable {
    static let stub = ProductPermissions(hasAddedPrice: false, hasAddedReview: false)

    let hasAddedPrice: Bool
    let hasAddedReview: Bool
}

// MARK: - Extensions
// MARK: - Equatable

extension ProductPermissions: Equatable {
    static func == (lhs: ProductPermissions, rhs: ProductPermissions) -> Bool {
        return lhs.hasAddedPrice == rhs.hasAddedPrice
            && lhs.hasAddedReview == rhs.hasAddedReview
    }
}
