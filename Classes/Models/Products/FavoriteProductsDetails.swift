//
//  FavoriteProductsDetails.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct FavoriteProductsDetails {
    let products: [FavoriteProduct]
}

// MARK: - Extensions
// MARK: - Equatable

extension FavoriteProductsDetails: Equatable {
    static func == (lhs: FavoriteProductsDetails, rhs: FavoriteProductsDetails) -> Bool {
        return lhs.products == rhs.products
    }
}
