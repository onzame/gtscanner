//
//  ProductLists.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductLists: Codable {
    static let stub = ProductLists(inFavorites: false, inCompare: false)

    let inFavorites: Bool
    let inCompare: Bool
}

// MARK: - Extensions
// MARK: - Equatable

extension ProductLists: Equatable {
    static func == (lhs: ProductLists, rhs: ProductLists) -> Bool {
        return lhs.inFavorites == rhs.inFavorites
            && lhs.inCompare == rhs.inCompare
    }
}
