//
//  FavoriteProduct.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct FavoriteProduct: Codable {
    let id: Int
    let count: Int
    let product: Product
}

// MARK: - Extensions
// MARK: - Equatable

extension FavoriteProduct: Equatable {
    static func == (lhs: FavoriteProduct, rhs: FavoriteProduct) -> Bool {
        return lhs.id == rhs.id
            && lhs.count == rhs.count
            && lhs.product == rhs.product
    }
}
