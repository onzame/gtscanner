//
//  Shop.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources

struct Shop: Codable {
    let id: Int
    let name: String
    let logo: URL?
    let branch: ShopBranch
}

// MARK: - Extensions
// MARK: - Equatable

extension Shop: Equatable {
    static func == (lhs: Shop, rhs: Shop) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.logo == rhs.logo
            && lhs.branch == rhs.branch
    }
}

// MARK: - IdentifiableType

extension Shop: IdentifiableType {
    var identity: Int {
        return branch.id
    }
}
