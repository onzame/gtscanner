//
//  ShopBranch.swift
//  Gorod
//
//  Created by Sergei Fabian on 03.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ShopBranch: Codable {
    let id: Int
    let distance: String
    let address: String?
    let location: Location?
    let category: ShopCategoryType
}

// MARK: - Extensions
// MARK: - Equatable

extension ShopBranch: Equatable {
    static func == (lhs: ShopBranch, rhs: ShopBranch) -> Bool {
        return lhs.id == rhs.id
            && lhs.distance == rhs.distance
            && lhs.address == rhs.address
            && lhs.location == rhs.location
            && lhs.location == rhs.location
    }
}
