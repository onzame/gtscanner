//
//  ShopCategoryType.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.04.2020.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

enum ShopCategoryType: String, Codable {
    case supermarketi
    case prodovolstvennyeMagaziny = "prodovolstvennye_magaziny"
    case optika
    case gipermarkety
    case bitovayaHimia = "bitovaya_himia"
    case apteka
    case alkogolnieNapitky = "alkogolnie_napitky"
    case unknown
}
