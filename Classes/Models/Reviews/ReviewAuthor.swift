//
//  ReviewAuthor.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ReviewAuthor: Codable {
    let name: String?
    let avatarUrl: URL?
}

// MARK: - Extensions
// MARK: - Equatable

extension ReviewAuthor: Equatable {
    static func == (lhs: ReviewAuthor, rhs: ReviewAuthor) -> Bool {
        return lhs.name == rhs.name
            && lhs.avatarUrl == rhs.avatarUrl
    }
}
