//
//  Review.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct Review: Codable {
    let id: Int
    let productId: Int
    let author: ReviewAuthor
    let updatedAt: TimeInterval
    let createdAt: TimeInterval
    let rating: Double
    let positiveText: String?
    let negativeText: String?
    let text: String?
    let permissions: ReviewPermissions
}

// MARK: - Extensions
// MARK: - QueryableType

extension Review: QueryableType {
    var identifier: Int {
        return id
    }
}

// MARK: - Equatable

extension Review: Equatable {
    static func == (lhs: Review, rhs: Review) -> Bool {
        return lhs.id == rhs.id
            && lhs.productId == rhs.productId
            && lhs.author == rhs.author
            && lhs.updatedAt == rhs.updatedAt
            && lhs.createdAt == rhs.createdAt
            && lhs.rating == rhs.rating
            && lhs.positiveText == rhs.positiveText
            && lhs.negativeText == rhs.negativeText
            && lhs.text == rhs.text
            && lhs.permissions == rhs.permissions
    }
}
