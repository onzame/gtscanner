//
//  ReviewText.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ReviewText {
    static let stub = ReviewText(positiveText: nil, negativeText: nil, text: nil)

    let positiveText: String?
    let negativeText: String?
    let text: String?

    var hasReview: Bool {
        return positiveText.orEmpty.isNotEmpty || negativeText.orEmpty.isNotEmpty || text.orEmpty.isNotEmpty
    }
}
