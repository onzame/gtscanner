//
//  ReviewPermissions.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ReviewPermissions: Codable {
    let canDelete: Bool
}

// MARK: - Equatable

extension ReviewPermissions: Equatable {
    static func == (lhs: ReviewPermissions, rhs: ReviewPermissions) -> Bool {
        return lhs.canDelete == rhs.canDelete
    }
}
