//
//  CompareProduct.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareProduct: Codable {
    let id: Int
    let name: String
    let amounts: ProductAmounts
    let image: ProductImage?
    let avgPrice: Double?
    let rating: Double
    let productionPlace: String
}

// MARK: - Extensions
// MARK: - Equatable

extension CompareProduct: Equatable {
    static func == (lhs: CompareProduct, rhs: CompareProduct) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.amounts == rhs.amounts
            && lhs.image == rhs.image
            && lhs.avgPrice == rhs.avgPrice
            && lhs.rating == rhs.rating
            && lhs.productionPlace == rhs.productionPlace
    }
}
