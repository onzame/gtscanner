//
//  CompareCategory.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareCategory: Codable {
    let categoryId: Int
    let name: String
    let count: Int
}

// MARK: - Extensions
// MARK: - Equatable

extension CompareCategory: Equatable {
    static func == (lhs: CompareCategory, rhs: CompareCategory) -> Bool {
        return lhs.categoryId == rhs.categoryId
            && lhs.name == rhs.name
            && lhs.count == rhs.count
    }
}
