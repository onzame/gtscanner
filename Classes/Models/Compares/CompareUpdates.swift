//
//  CompareUpdates.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareUpdates: Codable {
    let totalAmount: Int
}
