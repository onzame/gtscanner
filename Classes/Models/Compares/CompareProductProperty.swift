//
//  CompareProductProperty.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareProductProperty: Codable {
    let id: String
    let title: String
    let values: [CompareProductPropertyValue]
}
