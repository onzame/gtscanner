//
//  CompareProductImage.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareProductImage: Codable {
    let productId: Int
    let image: URL?
}

// MARK: - Extensions
// MARK: - Queryable

extension CompareProductImage: QueryableType {
    var identifier: Int {
        return productId
    }
}
