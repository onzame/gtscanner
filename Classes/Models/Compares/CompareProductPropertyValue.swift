//
//  CompareProductPropertyValue.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareProductPropertyValue: Codable {
    let id: String
    let value: String
}

// MARK: - Equatable

extension CompareProductPropertyValue: Equatable {
    static func == (lhs: CompareProductPropertyValue, rhs: CompareProductPropertyValue) -> Bool {
        return lhs.id == rhs.id
            && lhs.value == rhs.value
    }
}
