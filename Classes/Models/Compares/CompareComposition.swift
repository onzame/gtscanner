//
//  CompareComposition.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareComposition: Codable {
    let products: [CompareProduct]
    let details: [CompareProductDetails]
}
