//
//  CompareProductUpdates.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareProductUpdates: Codable {
    let id: Int
    let lists: ProductLists
}
