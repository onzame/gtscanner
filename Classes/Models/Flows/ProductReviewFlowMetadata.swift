//
//  ProductReviewFlowMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductReviewFlowMetadata {
    let product: Product
    let selectedRating: Double?
    let selectedShop: Shop?
    let reviewText: ReviewText

    init(product: Product, selectedRating: Double?, selectedShop: Shop?, reviewText: ReviewText) {
        if let selectedShop = selectedShop {
            if let shops = product.shops {
                if shops.contains(where: { $0.branch.id == selectedShop.branch.id }) {
                    self.product = product
                } else {
                    self.product = product.update(shops: [selectedShop] + shops)
                }
            } else {
                self.product = product.update(shops: [selectedShop])
            }
        } else {
            self.product = product
        }

        self.selectedRating = selectedRating
        self.selectedShop = selectedShop
        self.reviewText = reviewText
    }

    func updated(
        product: Product? = nil,
        selectedRating: Double? = nil,
        selectedShop: Shop? = nil,
        reviewText: ReviewText? = nil
    ) -> ProductReviewFlowMetadata {
        return ProductReviewFlowMetadata(
            product: product ?? self.product,
            selectedRating: selectedRating ?? self.selectedRating,
            selectedShop: selectedShop ?? self.selectedShop,
            reviewText: reviewText ?? self.reviewText
        )
    }
}
