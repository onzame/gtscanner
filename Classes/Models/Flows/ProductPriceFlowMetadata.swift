//
//  ProductPriceFlowMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 08.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductPriceFlowMetadata {
    let product: Product
    let selectedPrice: Double?
    let selectedShop: Shop?

    init(product: Product, selectedPrice: Double?, selectedShop: Shop?) {
        if let selectedShop = selectedShop {
            if let shops = product.shops {
                if shops.contains(where: { $0.branch.id == selectedShop.branch.id }) {
                    self.product = product
                } else {
                    self.product = product.update(shops: [selectedShop] + shops)
                }
            } else {
                self.product = product.update(shops: [selectedShop])
            }
        } else {
            self.product = product
        }

        self.selectedPrice = selectedPrice
        self.selectedShop = selectedShop
    }

    func updated(product: Product? = nil, selectedPrice: Double? = nil, selectedShop: Shop? = nil) -> ProductPriceFlowMetadata {
        return ProductPriceFlowMetadata(
            product: product ?? self.product,
            selectedPrice: selectedPrice ?? self.selectedPrice,
            selectedShop: selectedShop ?? self.selectedShop
        )
    }
}
