//
//  ProductAddFlowMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

struct ProductAddFlowMetadata {
    let name: String?
    let gtin: String?
    let price: Double?
    let selectedShop: Shop?
    let selectedCategory: Category?
    let photos: [Photo]
    let rating: Double?

    init(
        name: String? = nil,
        gtin: String? = nil,
        price: Double? = nil,
        selectedShop: Shop? = nil,
        selectedCategory: Category? = nil,
        photos: [Photo] = [],
        rating: Double? = nil
    ) {
        self.name = name
        self.gtin = gtin
        self.price = price
        self.selectedShop = selectedShop
        self.selectedCategory = selectedCategory
        self.photos = photos
        self.rating = rating
    }

    init(state: ProductAddViewModel.State) {
        self.name = state.name
        self.gtin = state.gtin
        self.price = state.price
        self.selectedShop = state.shop
        self.selectedCategory = state.category
        self.photos = state.photos
        self.rating = state.rating
    }

    func updated(
        name: String? = nil,
        gtin: String? = nil,
        price: Double? = nil,
        selectedShop: Shop? = nil,
        selectedCategory: Category? = nil,
        photos: [Photo]? = nil,
        rating: Double? = nil
    ) -> ProductAddFlowMetadata {
        return ProductAddFlowMetadata(
            name: name ?? self.name,
            gtin: gtin ?? self.gtin,
            price: price ?? self.price,
            selectedShop: selectedShop ?? self.selectedShop,
            selectedCategory: selectedCategory ?? self.selectedCategory,
            photos: photos ?? self.photos,
            rating: rating ?? self.rating
        )
    }
}
