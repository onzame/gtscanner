//
//  Pagination.swift
//  Gorod
//
//  Created by Sergei Fabian on 31.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct Pagination {
    static let initial = Pagination(nextPage: nil)

    let nextPage: Int?
    let hasNext: Bool

    init(nextPage: Int?) {
        self.nextPage = nextPage
        self.hasNext = nextPage != nil
    }
}
