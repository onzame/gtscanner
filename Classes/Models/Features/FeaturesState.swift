//
//  FeaturesState.swift
//  Gorod
//
//  Created by Sergei Fabian on 02.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

class FeaturesState: NSObject, NSCoding, Codable {

    static let stub = FeaturesState(isStartBannerShown: false)

    let isStartBannerShown: Bool

    init(isStartBannerShown: Bool) {
        self.isStartBannerShown = isStartBannerShown
    }

    required convenience init?(coder: NSCoder) {
        self.init(isStartBannerShown: coder.decodeBool(forKey: CodingKeys.isStartBannerShown.stringValue))
    }

    func encode(with coder: NSCoder) {
        coder.encode(isStartBannerShown, forKey: CodingKeys.isStartBannerShown.stringValue)
    }

}
