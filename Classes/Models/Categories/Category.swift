//
//  Category.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources

struct Category: Codable {
    static let stub = Category(id: 0, name: "")

    let id: Int
    let name: String
}

// MARK: - Extensions
// MARK: - Equatable

extension Category: Equatable {
    static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
    }
}

// MARK: - Hashable

extension Category: Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

// MARK: - IdentifiableType

extension Category: IdentifiableType {
    var identity: Int {
        return id
    }
}
