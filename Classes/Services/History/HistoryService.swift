//
//  HistoryService.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol HistoryServiceType {
    func observeHistoryProducts() -> Observable<[HistoryProductContainer]>
    func observeHistoryProductsWithReset() -> Observable<[HistoryProductContainer]>
    func refresh() -> Observable<Pagination>
    func loadNext(page: Int) -> Observable<Pagination>
}

final class HistoryService: HistoryServiceType {

    private let historyProvider: HistoryProviderType
    private let historyProductsRepository: HistoryProductsRepositoryType

    init(historyProvider: HistoryProviderType, historyProductsRepository: HistoryProductsRepositoryType) {
        self.historyProvider = historyProvider
        self.historyProductsRepository = historyProductsRepository
    }

    func observeHistoryProducts() -> Observable<[HistoryProductContainer]> {
        return historyProductsRepository.rx.observe()
    }

    func observeHistoryProductsWithReset() -> Observable<[HistoryProductContainer]> {
        return historyProductsRepository.rx.deleteAll()
            .andThen(.deferred(historyProductsRepository.rx.observe))
    }

    func refresh() -> Observable<Pagination> {
        return historyProvider.rx.request(.fetchHistory(page: 0))
            .catchBackendError()
            .map(RequestDataResponse<[HistoryProductContainer]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<Pagination> in
                guard let `self` = self else { return .empty() }
                return self.historyProductsRepository.rx.deleteAll()
                    .andThen(self.historyProductsRepository.rx.save(entities: response.data))
                    .andThen(.just(response.pagination))
            })
    }

    func loadNext(page: Int) -> Observable<Pagination> {
        return historyProvider.rx.request(.fetchHistory(page: page))
            .catchBackendError()
            .map(RequestDataResponse<[HistoryProductContainer]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<Pagination> in
                guard let `self` = self else { return .empty() }
                return self.historyProductsRepository.rx.save(entities: response.data)
                    .andThen(.just(response.pagination))
            })
    }

}
