//
//  CategoriesService.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol CategoriesServiceType {
    func fetchCategories() -> Observable<[Category]>
}

final class CategoriesService: CategoriesServiceType {

    private let categoriesProvider: CategoriesProviderType

    init(categoriesProvider: CategoriesProviderType) {
        self.categoriesProvider = categoriesProvider
    }

    func fetchCategories() -> Observable<[Category]> {
        return categoriesProvider.rx.request(.fetchCategories)
            .catchBackendError()
            .map(RequestDataResponse<[Category]>.self, using: .snakeDecoder)
            .map({ $0.data })
            .asObservable()
    }

}
