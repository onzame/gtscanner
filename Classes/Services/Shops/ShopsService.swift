//
//  ShopsService.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol ShopsServiceType {
    func fetchShops(location: Location?, page: Int) -> Observable<FetchShopsResult>
    func searchShops(location: Location?, query: String) -> Observable<[Shop]>
}

final class ShopsService: ShopsServiceType {

    private let shopsProvider: ShopsProviderType

    init(shopsProvider: ShopsProviderType) {
        self.shopsProvider = shopsProvider
    }

    func fetchShops(location: Location?, page: Int) -> Observable<FetchShopsResult> {
        return Observable.just(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<[Shop]>> in
                guard let `self` = self else { return .empty() }
                return self.shopsProvider.rx.request(.fetchShops(location: location, page: page))
                    .catchBackendError()
                    .map(RequestDataResponse<[Shop]>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .map(FetchShopsResult.init)
    }

    func searchShops(location: Location?, query: String) -> Observable<[Shop]> {
        return Observable.just(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<[Shop]>> in
                guard let `self` = self else { return .empty() }
                return self.shopsProvider.rx.request(.searchShops(location: location, query: query))
                    .catchBackendError()
                    .map(RequestDataResponse<[Shop]>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .map({ $0.data })
    }
}
