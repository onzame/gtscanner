//
//  FavoritesService.swift
//  Gorod
//
//  Created by Sergei Fabian on 27.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

protocol FavoritesServiceType {
    func observeSavedProducts() -> Observable<[FavoriteProductsAggregation]>
    func observeSavedProductsWithReset() -> Observable<[FavoriteProductsAggregation]>
    func refresh(location: Location?) -> Completable
    func append(product: Product, amount: Int) -> Completable
    func update(product: Product, amount: Int) -> Completable
    func delete(product: Product) -> Completable
}

final class FavoritesService: FavoritesServiceType {

    private let favoritesProvider: FavoritesProviderType
    private let favoritesProductsRepository: FavoriteProductsRepositoryType
    private let analyticsService: AnalyticsServiceType

    init(favoritesProvider: FavoritesProviderType, savedProductsRepository: FavoriteProductsRepositoryType, analyticsService: AnalyticsServiceType) {
        self.favoritesProvider = favoritesProvider
        self.favoritesProductsRepository = savedProductsRepository
        self.analyticsService = analyticsService
    }

    func observeSavedProducts() -> Observable<[FavoriteProductsAggregation]> {
        return favoritesProductsRepository.rx.observe()
    }

    func observeSavedProductsWithReset() -> Observable<[FavoriteProductsAggregation]> {
        return favoritesProductsRepository.rx.deleteAll()
            .andThen(.deferred(favoritesProductsRepository.rx.observe))
    }

    func refresh(location: Location?) -> Completable {
        if location == nil {
            return Completable.error(LocationSessionError.accessDenied)
        } else {
            return favoritesProvider.rx.request(.fetchFavorites)
                .catchBackendError()
                .map(RequestDataResponse<[FavoriteProductsAggregation]>.self, using: .snakeDecoder)
                .asObservable()
                .flatMap({ [weak self] response -> Observable<[FavoriteProductsAggregation]> in
                    guard let `self` = self else { return .empty() }
                    return self.favoritesProductsRepository.rx.deleteAll()
                        .andThen(self.favoritesProductsRepository.rx.save(entities: response.data))
                        .andThen(.just(response.data))
                })
                .asCompletable()
        }
    }

    func append(product: Product, amount: Int) -> Completable {
        return favoritesProvider.rx.request(.append(productId: product.id, amount: amount))
            .catchBackendError()
            .map(RequestDataResponse<[FavoriteProductsAggregation]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<[FavoriteProductsAggregation]> in
                guard let `self` = self else { return .empty() }
                return self.favoritesProductsRepository.rx.save(entities: response.data)
                    .andThen(self.analyticsService.sendEvent(event: .none, inCategory: .list, tag: .none))
                    .andThen(.just(response.data))
            })
            .asCompletable()
    }

    func update(product: Product, amount: Int) -> Completable {
        return favoritesProvider.rx.request(.update(productId: product.id, amount: amount))
            .catchBackendError()
            .map(RequestDataResponse<[FavoriteProductsAggregation]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<[FavoriteProductsAggregation]> in
                guard let `self` = self else { return .empty() }
                return self.favoritesProductsRepository.rx.save(entities: response.data)
                    .andThen(.just(response.data))
            })
            .asCompletable()
    }

    func delete(product: Product) -> Completable {
        return favoritesProvider.rx.request(.delete(productId: product.id))
            .catchBackendError()
            .map(RequestDataResponse<[FavoriteProductsAggregation]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<[FavoriteProductsAggregation]> in
                guard let `self` = self else { return .empty() }
                return self.favoritesProductsRepository.rx.deleteByProduct(product)
                    .andThen(.just(response.data))
            })
            .asCompletable()
    }

}

