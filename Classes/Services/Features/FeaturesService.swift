//
//  FeaturesService.swift
//  Gorod
//
//  Created by Sergei Fabian on 02.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol FeaturesServiceType {
    func fetchState() -> FeaturesState
    func markStartBannerAsShown() -> Completable
    func updateFeatureState(state: FeaturesState) -> Completable
}

final class FeaturesService: FeaturesServiceType {

    fileprivate static let featuresStateKey = "com.gtscanner.defaults.feature-state"

    func fetchState() -> FeaturesState {
        if let state: FeaturesState = UserDefaults.standard.read(forKey: FeaturesService.featuresStateKey) {
            return state
        } else {
            return FeaturesState.stub
        }
    }

    func markStartBannerAsShown() -> Completable {
        return updateFeatureState(state: FeaturesState(isStartBannerShown: true))
    }

    func updateFeatureState(state: FeaturesState) -> Completable {
        return Completable.create { (eventEmitter) -> Disposable in
            UserDefaults.standard.update(state, forKey: FeaturesService.featuresStateKey)
            eventEmitter(.completed)
            return Disposables.create()
        }
    }

}
