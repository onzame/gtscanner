//
//  AnalyticsEvent.swift
//  Gorod
//
//  Created by Sergei Fabian on 14.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum AnalyticsEvent: String {
    case scan = "ProductScan"
    case product = "OpenProduct"
    case productAdd = "AddProduct"
    case productMore = "ProductMore"
    case productPrice = "ProductCost"
    case productComparison = "ProductComparison"
    case productComparisonAdd = "AddProductComparison"
    case review = "ProductReview"
    case reviewMore = "MoreReviews"
    case rate = "ProductEvaluation"
    case history = "HistoryProductScan"
}
