//
//  AnalyticsTag.swift
//  Gorod
//
//  Created by Sergei Fabian on 14.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum AnalyticsTag {

    typealias RawValue = String

    case productId(id: Int)
    case productIds(ids: [Int])
    case error
    case page(index: Int)
    case evaluation
    case cost(value: Double)

    var rawValue: String {
        switch self {
        case .productId(id: let id):
            return "\(id)"
        case .productIds(ids: let ids):
            return ids.map({ "\($0)" }).joined(separator: "&")
        case .error:
            return "Error_code"
        case .page(index: let index):
            return "\(index)"
        case .evaluation:
            return "Evaluation"
        case .cost(value: let value):
            return "\(value)"
        }
    }

}
