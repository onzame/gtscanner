//
//  AnalyticsCategory.swift
//  Gorod
//
//  Created by Sergei Fabian on 14.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum AnalyticsCategory: String {
    case scan = "GoodsScan"
    case info = "GoodsInfo"
    case review = "GoodsReview"
    case compare = "GoodsCompare"
    case rate = "GoodsRate"
    case price = "GoodsPrice"
    case add = "GoodsAdd"
    case history = "GoodsHistory"
    case list = "GoodsList"
}
