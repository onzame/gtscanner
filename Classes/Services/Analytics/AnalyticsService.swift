//
//  AnalyticsService.swift
//  Gorod
//
//  Created by Sergei Fabian on 14.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol AnalyticsServiceType {
    func sendEventSync(event: AnalyticsEvent?, inCategory: AnalyticsCategory?, tag: AnalyticsTag?)
    func sendEvent(event: AnalyticsEvent?, inCategory: AnalyticsCategory?, tag: AnalyticsTag?) -> Completable
}

final class AnalyticsService: AnalyticsServiceType {

    private let delegate: GorodGoodsDelegate

    init(delegate: GorodGoodsDelegate) {
        self.delegate = delegate
    }

    func sendEventSync(event: AnalyticsEvent?, inCategory category: AnalyticsCategory?, tag: AnalyticsTag?) {
        delegate.sendEvent((event?.rawValue).orEmpty, inCategory: (category?.rawValue).orEmpty, withTag: (tag?.rawValue).orEmpty)
    }

    func sendEvent(event: AnalyticsEvent?, inCategory category: AnalyticsCategory?, tag: AnalyticsTag?) -> Completable {
        return Completable.create { [weak self] (eventEmitter) -> Disposable in
            self?.sendEventSync(event: event, inCategory: category, tag: tag)
            eventEmitter(.completed)
            return Disposables.create()
        }
    }

}
