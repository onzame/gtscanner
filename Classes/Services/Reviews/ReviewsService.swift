//
//  ReviewsService.swift
//  Gorod
//
//  Created by Sergei Fabian on 31.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol ReviewsServiceType {
    func observeReviews() -> Observable<[Review]>
    func observeReviewsWithReset() -> Observable<[Review]>
    func refreshReviews(product: Product) -> Observable<Pagination>
    func loadReviews(product: Product, page: Int) -> Observable<Pagination>
    func addReview(product: Product, rating: Double, shop: Shop, reviewText: ReviewText) -> Observable<AddReviewResult>
    func saveReview(review: Review) -> Completable
    func deleteReview(product: Product, review: Review) -> Observable<DeleteReviewResult>
}

final class ReviewsService: ReviewsServiceType {

    private let reviewsProvider: ReviewsProviderType
    private let reviewsRepository: ReviewsRepositoryType
    private let scannedProductsRepository: ScannedProductsRepositoryType
    private let historyProductsRepository: HistoryProductsRepositoryType
    private let savedProductsRepository: FavoriteProductsRepositoryType
    private let analyticsService: AnalyticsServiceType

    init(
        reviewsProvider: ReviewsProviderType,
        reviewsRepository: ReviewsRepositoryType,
        scannedProductsRepository: ScannedProductsRepositoryType,
        historyProductsRepository: HistoryProductsRepositoryType,
        savedProductsRepository: FavoriteProductsRepositoryType,
        analyticsService: AnalyticsServiceType
    ) {
        self.reviewsProvider = reviewsProvider
        self.reviewsRepository = reviewsRepository
        self.scannedProductsRepository = scannedProductsRepository
        self.historyProductsRepository = historyProductsRepository
        self.savedProductsRepository = savedProductsRepository
        self.analyticsService = analyticsService
    }

    func observeReviews() -> Observable<[Review]> {
        return reviewsRepository.rx.observe()
    }

    func observeReviewsWithReset() -> Observable<[Review]> {
        return reviewsRepository.rx.deleteAll()
            .andThen(.deferred(reviewsRepository.rx.observe))
    }

    func refreshReviews(product: Product) -> Observable<Pagination> {
        return reviewsProvider.rx.request(.fetchReviews(productId: product.id, page: 0))
            .catchBackendError()
            .map(RequestDataResponse<[Review]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<Pagination> in
                guard let `self` = self else { return .empty() }
                return self.reviewsRepository.rx.deleteAll()
                    .andThen(self.reviewsRepository.rx.save(entities: response.data))
                    .andThen(.just(Pagination(nextPage: response.nextPage)))
            })
    }

    func loadReviews(product: Product, page: Int) -> Observable<Pagination> {
        return reviewsProvider.rx.request(.fetchReviews(productId: product.id, page: page))
            .catchBackendError()
            .map(RequestDataResponse<[Review]>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<Pagination> in
                guard let `self` = self else { return .empty() }
                return self.reviewsRepository.rx.save(entities: response.data)
                    .andThen(self.analyticsService.sendEvent(event: .reviewMore, inCategory: .review, tag: .page(index: page)))
                    .andThen(.just(Pagination(nextPage: response.nextPage)))
            })
    }

    func addReview(product: Product, rating: Double, shop: Shop, reviewText: ReviewText) -> Observable<AddReviewResult> {
        return reviewsProvider.rx.request(.addReview(productId: product.id, rating: rating, shopBranchId: shop.branch.id, reviewText: reviewText))
            .catchBackendError()
            .map(RequestDataResponse<AddReviewResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<AddReviewResult> in
                guard let `self` = self else { return .empty() }
                let updatedProduct = product.update(updates: response.data.productUpdates)
                return self.scannedProductsRepository.rx.updateProductIfNeed(product: updatedProduct)
                    .andThen(self.analyticsService.sendEvent(event: .none, inCategory: .rate, tag: .none))
                    .andThen(.just(response.data))
            })
            .do(onNext: { [weak self] _ in
                if reviewText.hasReview {
                    self?.analyticsService.sendEventSync(event: .review, inCategory: .review, tag: .productId(id: product.id))
                }
            })
    }

    func saveReview(review: Review) -> Completable {
        return reviewsRepository.rx.save(entity: review)
    }

    func deleteReview(product: Product, review: Review) -> Observable<DeleteReviewResult> {
        return reviewsProvider.rx.request(.deleteReview(reviewId: review.id))
            .catchBackendError()
            .map(RequestDataResponse<DeleteReviewResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<DeleteReviewResult> in
                guard let `self` = self else { return .empty() }
                let updatedProduct = product.update(updates: response.data.productUpdates)
                return self.reviewsRepository.rx.delete(entity: review)
                    .andThen(self.scannedProductsRepository.rx.updateProductIfNeed(product: updatedProduct))
                    .andThen(.just(response.data))
            })
    }

}
