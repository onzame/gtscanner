//
//  PricesService.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol PricesServiceType {
    func observePrices() -> Observable<[Price]>
    func observePricesWithReset() -> Observable<[Price]>
    func refreshPrices(product: Product, location: Location?) -> Observable<Pagination>
    func loadPrices(product: Product, location: Location?, page: Int) -> Observable<Pagination>
    func addPrice(product: Product, price: Double, shop: Shop) -> Observable<AddPriceResult>
    func addPrice(price: Price) -> Completable
    func deletePrice(product: Product, price: Price) -> Observable<DeletePriceResult>
}

final class PricesService: PricesServiceType {

    private let pricesProvider: PricesProviderType
    private let pricesRepository: PricesRepositoryType
    private let scannedProductsRepository: ScannedProductsRepositoryType
    private let historyProductsRepository: HistoryProductsRepositoryType
    private let savedProductsRepository: FavoriteProductsRepositoryType
    private let analyticService: AnalyticsServiceType

    init(
        pricesProvider: PricesProviderType,
        pricesRepository: PricesRepositoryType,
        scannedProductsRepository: ScannedProductsRepositoryType,
        historyProductsRepository: HistoryProductsRepositoryType,
        savedProductsRepository: FavoriteProductsRepositoryType,
        analyticService: AnalyticsServiceType
    ) {
        self.pricesProvider = pricesProvider
        self.pricesRepository = pricesRepository
        self.scannedProductsRepository = scannedProductsRepository
        self.historyProductsRepository = historyProductsRepository
        self.savedProductsRepository = savedProductsRepository
        self.analyticService = analyticService
    }

    func observePrices() -> Observable<[Price]> {
        return pricesRepository.rx.observe()
    }

    func observePricesWithReset() -> Observable<[Price]> {
        return pricesRepository.rx.deleteAll()
            .andThen(.deferred(pricesRepository.rx.observe))
    }

    func refreshPrices(product: Product, location: Location?) -> Observable<Pagination> {
        return Observable.just(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<[Price]>> in
                guard let `self` = self else { return .empty() }
                return self.pricesProvider.rx.request(.fetchPrices(productId: product.id, location: location, page: 0))
                    .catchBackendError()
                    .map(RequestDataResponse<[Price]>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .flatMap({ [weak self] response -> Observable<Pagination> in
                guard let `self` = self else { return .empty() }
                return self.pricesRepository.rx.deleteAll()
                    .andThen(self.pricesRepository.rx.save(entities: response.data))
                    .andThen(.just(response.pagination))
            })
    }

    func loadPrices(product: Product, location: Location?, page: Int) -> Observable<Pagination> {
        return Observable.just(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<[Price]>> in
                guard let `self` = self else { return .empty() }
                return self.pricesProvider.rx.request(.fetchPrices(productId: product.id, location: location, page: page))
                    .catchBackendError()
                    .map(RequestDataResponse<[Price]>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .flatMap({ [weak self] response -> Observable<Pagination> in
                guard let `self` = self else { return .empty() }
                return self.pricesRepository.rx.save(entities: response.data)
                    .andThen(.just(response.pagination))
            })
    }

    func addPrice(product: Product, price: Double, shop: Shop) -> Observable<AddPriceResult> {
        return pricesProvider.rx.request(.addPrice(productId: product.id, price: price, shopBranchId: shop.branch.id))
            .catchBackendError()
            .map(RequestDataResponse<AddPriceResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<AddPriceResult> in
                guard let `self` = self else { return .empty() }
                let updatedProduct = product.update(updates: response.data.productUpdates)
                return self.scannedProductsRepository.rx.updateProductIfNeed(product: updatedProduct)
                    .andThen(self.analyticService.sendEvent(event: .productPrice, inCategory: .price, tag: .cost(value: price)))
                    .andThen(.just(response.data))
            })
    }

    func addPrice(price: Price) -> Completable {
        return pricesRepository.rx.saveIgnoringDuplicates(price: price)
    }

    func deletePrice(product: Product, price: Price) -> Observable<DeletePriceResult> {
        return pricesProvider.rx.request(.deletePrice(priceId: price.id))
            .catchBackendError()
            .map(RequestDataResponse<DeletePriceResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<DeletePriceResult> in
                guard let `self` = self else { return .empty() }
                let updatedProduct = product.update(updates: response.data.productUpdates)
                return self.pricesRepository.rx.delete(entity: price)
                    .andThen(self.scannedProductsRepository.rx.updateProductIfNeed(product: updatedProduct))
                    .andThen(.just(response.data))
            })
    }

}


