//
//  ProductsService.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
import Moya

protocol ProductsServiceType {
    func observeScannedProducts() -> Observable<[ScannedProduct]>
    func observeScannedProductsWithReset() -> Observable<[ScannedProduct]>
    func fetchProduct(product: Product, location: Location?) -> Observable<Product>
    func searchProduct(gtin: String, location: Location?) -> Observable<ScannedProduct>
    func updatedProduct(product: Product) -> Completable
    func deleteAllScannedProducts() -> Completable
    func fetchDetails(product: Product) -> Observable<[ProductDetailsAggregation]>
    func addProduct(parameters: AddProductParameters, location: Location?) -> Observable<Product>
}

final class ProductsService: ProductsServiceType {

    private let productsProvider: ProductsProviderType
    private let scannedProductsRepository: ScannedProductsRepositoryType
    private let favoriteProductsRepository: FavoriteProductsRepositoryType
    private let compareProductsRepository: CompareProductsRepositoryType
    private let analyticsService: AnalyticsServiceType

    init(
        productsProvider: ProductsProviderType,
        scannedProductsRepository: ScannedProductsRepositoryType,
        favoriteProductsRepository: FavoriteProductsRepositoryType,
        compareProductsRepository: CompareProductsRepositoryType,
        analyticsService: AnalyticsServiceType
    ) {
        self.productsProvider = productsProvider
        self.scannedProductsRepository = scannedProductsRepository
        self.favoriteProductsRepository = favoriteProductsRepository
        self.compareProductsRepository = compareProductsRepository
        self.analyticsService = analyticsService
    }

    func observeScannedProducts() -> Observable<[ScannedProduct]> {
        return scannedProductsRepository.rx.observe()
    }

    func observeScannedProductsWithReset() -> Observable<[ScannedProduct]> {
        return scannedProductsRepository.rx.deleteAll()
            .andThen(.deferred(scannedProductsRepository.rx.observe))
    }

    func fetchProduct(product: Product, location: Location?) -> Observable<Product> {
        return Observable.just(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<FetchProductResult>> in
                guard let `self` = self else { return .empty() }
                return self.productsProvider.rx.request(.fetchProduct(productId: product.id, location: location))
                    .catchBackendError()
                    .map(RequestDataResponse<FetchProductResult>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .map({ $0.data.product })
            .flatMap({ [weak self] product -> Observable<Product> in
                guard let `self` = self else { return .empty() }
                return self.scannedProductsRepository.rx.save(entity: ScannedProduct.founded(product))
                    .andThen(self.favoriteProductsRepository.rx.updateProductIfNeed(product: product))
                    .andThen(.just(product))
            })
    }

    func searchProduct(gtin: String, location: Location?) -> Observable<ScannedProduct> {
        return scannedProductsRepository.rx.checkIfExistsBy(gtin: gtin)
            .filter({ !$0 })
            .asObservable()
            .mapTo(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<SearchProductResult>> in
                guard let `self` = self else { return .empty() }
                return self.productsProvider.rx.request(.searchProduct(gtin: gtin, location: location))
                    .catchBackendError()
                    .map(RequestDataResponse<SearchProductResult>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .map({ $0.data })
            .flatMap({ [weak self] result -> Observable<SearchProductResult> in
                guard let `self` = self else { return .empty() }
                return self.compareProductsRepository.rx.deleteAll()
                    .andThen(.just(result))
            })
            .map({ ScannedProduct.founded($0.product) })
            .catchError(SearchProductErrorResolverFactory.resolveSearchProductError(gtin: gtin))
            .do(onNext: { [weak self] product in
                switch product {
                case .founded(let foundedProduct):
                    self?.analyticsService.sendEventSync(event: .scan, inCategory: .scan, tag: .productId(id: foundedProduct.id))
                    self?.analyticsService.sendEventSync(event: .productComparisonAdd, inCategory: .compare, tag: .productId(id: foundedProduct.id))
                case .unknown:
                    self?.analyticsService.sendEventSync(event: .scan, inCategory: .scan, tag: .none)
                }
            })
            .flatMap({ [weak self] product -> Observable<ScannedProduct> in
                guard let `self` = self else { return .empty() }
                return self.scannedProductsRepository.rx.save(entity: product)
                    .andThen(.just(product))
            })
    }

    func updatedProduct(product: Product) -> Completable {
        return scannedProductsRepository.rx.updateProductIfNeed(product: product)
    }

    func deleteAllScannedProducts() -> Completable {
        return scannedProductsRepository.rx.deleteAll()
    }

    func fetchDetails(product: Product) -> Observable<[ProductDetailsAggregation]> {
        return productsProvider.rx.request(.fetchProductDetails(productId: product.id))
            .catchBackendError()
            .map(RequestDataResponse<[ProductDetailsAggregation]>.self, using: .snakeDecoder)
            .asObservable()
            .map({ $0.data })
    }

    func addProduct(parameters: AddProductParameters, location: Location?) -> Observable<Product> {
        return Observable.just(location)
            .filterLocation()
            .flatMap({ [weak self] location -> Observable<RequestDataResponse<Product>> in
                guard let `self` = self else { return .empty() }
                return self.productsProvider.rx.request(.addProduct(parameters: parameters, location: location))
                    .catchBackendError()
                    .map(RequestDataResponse<Product>.self, using: .snakeDecoder)
                    .asObservable()
            })
            .map({ $0.data })
            .flatMap({ [weak self] product -> Observable<Product> in
                guard let `self` = self else { return .empty() }
                return self.scannedProductsRepository.rx.save(entity: .founded(product))
                    .andThen(self.analyticsService.sendEvent(event: .none, inCategory: .add, tag: .none))
                    .andThen(.just(product))
            })
    }

}

// MARK: - Utils

private struct SearchProductErrorResolverFactory {

    static func resolveSearchProductError(gtin: String) -> (Error) -> Observable<ScannedProduct> {
        return SearchProductErrorResolverFactory(gtin: gtin).validateError
    }

    let gtin: String

    func validateError(error: Error) -> Observable<ScannedProduct> {
        if let error = error as? NetworkErrorWrapper, error.statusCode == NetworkClientStatusCode.notFound.rawValue {
            return .just(.unknown(UnknownProduct(gtin: gtin)))
        } else {
            return .error(error)
        }
    }

}
