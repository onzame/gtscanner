//
//  ComparesService.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol ComparesServiceType {
    func observeCompareProducts() -> Observable<[CompareProductImage]>
    func fetchCategories() -> Observable<[CompareCategory]>
    func deleteCategory(category: CompareCategory) -> Observable<[CompareCategory]>
    func fetchProducts(category: CompareCategory, targetProduct: Product?) -> Observable<CompareComposition>
    func addProduct(product: Product) -> Completable
    func deleteProduct(product: Product) -> Completable
    func deleteProduct(product: CompareProduct) -> Observable<CompareComposition>
}

final class ComparesService: ComparesServiceType {

    private let comparesProvider: ComparesProviderType
    private let scannedProductsRepository: ScannedProductsRepositoryType
    private let historyProductsRepository: HistoryProductsRepositoryType
    private let favoriteProductsRepository: FavoriteProductsRepositoryType
    private let compareProductsRepository: CompareProductsRepositoryType
    private let analyticsService: AnalyticsServiceType

    init(
        comparesProvider: ComparesProviderType,
        scannedProductsRepository: ScannedProductsRepositoryType,
        historyProductsRepository: HistoryProductsRepositoryType,
        favoriteProductsRepository: FavoriteProductsRepositoryType,
        compareProductsRepository: CompareProductsRepositoryType,
        analyticsService: AnalyticsServiceType
    ) {
        self.comparesProvider = comparesProvider
        self.scannedProductsRepository = scannedProductsRepository
        self.historyProductsRepository = historyProductsRepository
        self.favoriteProductsRepository = favoriteProductsRepository
        self.compareProductsRepository = compareProductsRepository
        self.analyticsService = analyticsService
    }

    func observeCompareProducts() -> Observable<[CompareProductImage]> {
        return compareProductsRepository.rx.observe()
    }

    func fetchCategories() -> Observable<[CompareCategory]> {
        return comparesProvider.rx.request(.fetchCategories)
            .catchBackendError()
            .map(RequestDataResponse<FetchCompareCategoriesResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<[CompareCategory]> in
                guard let `self` = self else { return .empty() }
                return self.compareProductsRepository.rx.deleteAll()
                    .andThen( self.compareProductsRepository.rx.save(entities: response.data.compareImages))
                    .andThen(.just(response.data.categories))
            })
    }

    func deleteCategory(category: CompareCategory) -> Observable<[CompareCategory]> {
        return comparesProvider.rx.request(.deleteCategory(categoryId: category.categoryId))
            .catchBackendError()
            .map(RequestDataResponse<DeleteCompareCategoryResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<[CompareCategory]> in
                guard let `self` = self else { return .empty() }
                return self.compareProductsRepository.rx.deleteAll()
                    .andThen(self.compareProductsRepository.rx.save(entities: response.data.compareImages))
                    .andThen(.just(response.data.list))
            })
    }

    func fetchProducts(category: CompareCategory, targetProduct: Product?) -> Observable<CompareComposition> {
        return comparesProvider.rx.request(.fetchProducts(categoryId: category.categoryId, targetProductId: targetProduct?.id))
            .catchBackendError()
            .map(RequestDataResponse<CompareComposition>.self, using: .snakeDecoder)
            .asObservable()
            .map({ $0.data })
            .do(onNext: { [weak self] composition in
                let ids = composition.products.map({ $0.id })
                self?.analyticsService.sendEventSync(event: .productComparison, inCategory: .compare, tag: .productIds(ids: ids))
            })
    }

    func addProduct(product: Product) -> Completable {
        return comparesProvider.rx.request(.addProduct(productId: product.id, isCompositionNeeded: false))
            .catchBackendError()
            .map(RequestDataResponse<AddCompareProductResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<AddCompareProductResult> in
                guard let `self` = self else { return .empty() }
                let updatedProduct = product.update(updates: response.data.compareProductUpdates)
                return self.scannedProductsRepository.rx.updateProductIfNeed(product: updatedProduct)
                    .andThen(self.favoriteProductsRepository.rx.updateProductIfNeed(product: updatedProduct))
                    .andThen(self.compareProductsRepository.rx.deleteAll())
                    .andThen(self.compareProductsRepository.rx.save(entities: response.data.compareImages))
                    .andThen(.just(response.data))
            })
            .do(onNext: { [weak self] _ in
                self?.analyticsService.sendEventSync(event: .productComparisonAdd, inCategory: .compare, tag: .productId(id: product.id))
            })
            .ignoreElements()
    }

    func deleteProduct(product: Product) -> Completable {
        return comparesProvider.rx.request(.deleteProduct(productId: product.id, isCompositionNeeded: false))
            .catchBackendError()
            .map(RequestDataResponse<DeleteCompareProductResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<DeleteCompareProductResult> in
                guard let `self` = self else { return .empty() }
                let updatedProduct = product.update(updates: response.data.compareProductUpdates)
                return self.scannedProductsRepository.rx.updateProductIfNeed(product: updatedProduct)
                    .andThen(self.favoriteProductsRepository.rx.updateProductIfNeed(product: updatedProduct))
                    .andThen(self.compareProductsRepository.rx.deleteAll())
                    .andThen(self.compareProductsRepository.rx.save(entities: response.data.compareImages))
                    .andThen(.just(response.data))
            })
            .asCompletable()
    }

    func deleteProduct(product: CompareProduct) -> Observable<CompareComposition> {
        return comparesProvider.rx.request(.deleteProduct(productId: product.id, isCompositionNeeded: true))
            .catchBackendError()
            .map(RequestDataResponse<DeleteCompareProductResult>.self, using: .snakeDecoder)
            .asObservable()
            .flatMap({ [weak self] response -> Observable<CompareComposition?> in
                guard let `self` = self else { return .empty() }
                return self.scannedProductsRepository.rx.updateCompareProduct(productId: product.id, inCompare: false)
                    .andThen(self.favoriteProductsRepository.rx.updateCompareProduct(productId: product.id, inCompare: false))
                    .andThen(self.compareProductsRepository.rx.deleteAll())
                    .andThen(self.compareProductsRepository.rx.save(entities: response.data.compareImages))
                    .andThen(.just(response.data.composition))
            })
            .filterNil()
    }

}
