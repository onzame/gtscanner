//
//  EnvironmentConfiguration.swift
//  Alamofire
//
//  Created by Sergei Fabian on 27.03.2020.
//

import Foundation

public class EnvironmentConfiguration {

    public static var `default`: EnvironmentConfiguration {
        let apiUrl = URL(string: "http://project003.dev.onza.me/api")!
        return EnvironmentConfiguration(apiUrl: apiUrl, isDebugMode: true)
    }

    public let apiUrl: URL
    public let isDebugMode: Bool

    public init(apiUrl: URL, isDebugMode: Bool) {
        self.apiUrl = apiUrl
        self.isDebugMode = isDebugMode
    }
}
