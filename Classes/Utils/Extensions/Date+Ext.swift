//
//  Date+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

extension Date {
    func stringBy(dateFormat: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat
        formatter.timeZone = .current
        formatter.locale = Locale(identifier: "ru-RU")
        return formatter.string(from: self)
    }
}
