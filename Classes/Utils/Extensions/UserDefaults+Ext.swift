//
//  UserDefaults+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 02.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

extension UserDefaults {
    func update<T: NSCoding>(_ object: T, forKey key: String) {
        let objectAsData: Data?

        if #available(iOS 12.0, *) {
            objectAsData = try? NSKeyedArchiver.archivedData(withRootObject: object, requiringSecureCoding: false)
        } else {
            objectAsData = NSKeyedArchiver.archivedData(withRootObject: object)
        }

        if let objectAsData = objectAsData {
            UserDefaults.standard.set(objectAsData, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }

    func read<T: NSCoding>(forKey key: String) -> T? {
        if let objectAsData = UserDefaults.standard.data(forKey: key) {
            if #available(iOS 12.0, *) {
                return try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(objectAsData) as? T
            } else {
                return NSKeyedUnarchiver.unarchiveObject(with: objectAsData) as? T
            }
        } else {
            return nil
        }
    }
}
