//
//  UIApplication+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension UIApplication {
    var keyWindow: UIWindow? {
        return windows.first(where: { $0.isKeyWindow })
    }

    var rootViewController: UIViewController? {
        return keyWindow?.rootViewController
    }

    var topViewController: UIViewController? {
        var topViewController = rootViewController

        while topViewController?.presentedViewController != nil {
            topViewController = topViewController?.presentedViewController
        }

        return topViewController
    }

    func beginIgnoringEvents() {
        if #available(iOS 13.0, *) {
            ios13BeginIgnoringInteractionEvents()
        } else {
            beginIgnoringInteractionEvents()
        }
    }

    func endIgnoringEvents() {
        if #available(iOS 13.0, *) {
            ios13EndIgnoringInteractionEvents()
        } else {
            endIgnoringInteractionEvents()
        }
    }

    var isIgnoringEvents: Bool {
        if #available(iOS 13.0, *) {
            return rootViewController?.view.isUserInteractionEnabled == false
        } else {
            return isIgnoringInteractionEvents
        }
    }

    private func ios13BeginIgnoringInteractionEvents() {
        setViewControllerInteractionEnabled(isEnabled: false)
    }

    private func ios13EndIgnoringInteractionEvents() {
        setViewControllerInteractionEnabled(isEnabled: true)
    }

    private func setViewControllerInteractionEnabled(isEnabled: Bool) {
        UIApplication.shared.keyWindow?.subviews.forEach { view in
            view.isUserInteractionEnabled = isEnabled
        }

        var viewController = rootViewController
        setViewControllerInteractionEnabled(viewController: viewController, isEnabled: isEnabled)

        while viewController?.presentedViewController != nil {
            viewController = viewController?.presentedViewController
            setViewControllerInteractionEnabled(viewController: viewController, isEnabled: isEnabled)
        }
    }

    private func setViewControllerInteractionEnabled(viewController: UIViewController?, isEnabled: Bool) {
        viewController?.view.isUserInteractionEnabled = isEnabled

        if let navigationController = viewController as? UINavigationController {
            navigationController.viewControllers.forEach { viewController in
                viewController.view.isUserInteractionEnabled = isEnabled
            }
        }
    }
}

extension UIApplication {
    func openAppSettings(completion: ((Bool) -> Void)? = nil) {
        if let settings = URL(string: UIApplication.openSettingsURLString) {
            open(settings, completionHandler: completion)
        } else {
            completion?(false)
        }
    }
}

extension Reactive where Base: UIApplication {
    func didBecomeActiveObservable() -> Observable<Void> {
        return NotificationCenter.default.rx.notification(UIApplication.willEnterForegroundNotification)
            .mapToVoid()
    }
}
