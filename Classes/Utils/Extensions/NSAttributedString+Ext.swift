//
//  NSAttributedString+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

extension NSAttributedString {
    static var space: NSAttributedString {
        return NSAttributedString(string: " ")
    }

    convenience init(
        string: String,
        font: UIFont? = nil,
        color: UIColor? = nil,
        underlineStyle: NSUnderlineStyle? = nil,
        underlineColor: UIColor? = nil) {
        var attributes = [NSAttributedString.Key: Any]()

        if let font = font {
            attributes.updateValue(font, forKey: .font)
        }

        if let color = color {
            attributes.updateValue(color, forKey: .foregroundColor)
        }

        if let underlineStyle = underlineStyle {
            attributes.updateValue(underlineStyle.rawValue, forKey: .underlineStyle)
        }

        if let underlineColor = underlineColor {
            attributes.updateValue(underlineColor, forKey: .underlineColor)
        }

        self.init(string: string, attributes: attributes)
    }
}
