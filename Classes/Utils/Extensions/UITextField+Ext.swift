//
//  UITextField+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UITextField {
    var updateText: Binder<String?> {
        return Binder(base, binding: { textField, value in
            textField.text = value
            textField.sendActions(for: .valueChanged)
        })
    }
}
