//
//  UIImageView+Rx.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Kingfisher

extension UIImage {
    func resize(targetSize: CGSize) -> UIImage? {
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height

        var newSize: CGSize

        if widthRatio > heightRatio {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }

        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }

    func tint(with fillColor: UIColor?) -> UIImage? {
        guard let fillColor = fillColor else {
            return self
        }

        let image = withRenderingMode(.alwaysTemplate)

        UIGraphicsBeginImageContextWithOptions(size, false, scale)

        fillColor.set()

        image.draw(in: CGRect(origin: .zero, size: size))

        guard let imageColored = UIGraphicsGetImageFromCurrentImageContext() else {
            return self
        }

        UIGraphicsEndImageContext()
        return imageColored
    }
}

extension Reactive where Base: UIImageView {
    func image(placeholder: Placeholder?) -> Binder<URL?> {
        return Binder(base, binding: { imageView, url in
            imageView.kf.setImage(with: url, placeholder: placeholder)
        })
    }

    var imageWithPlaceholder: Binder<(URL?, Placeholder?)> {
        return Binder(base, binding: { imageView, input in
            imageView.kf.setImage(with: input.0, placeholder: input.1)
        })
    }
}

extension KingfisherWrapper where Base: UIImageView {
    func prepareForReuse() {
        cancelDownloadTask()
        base.image = nil
    }

    func setImage(with source: URL?, placeholder: Placeholder?) {
        cancelDownloadTask()
        setImage(with: source, placeholder: placeholder, options: [.transition(ImageTransition.fade(0.2))])
    }
}
