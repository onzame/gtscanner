//
//  UITableView+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 08.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UITableView {
    func showFooterActivityIndicatorView() -> Binder<Bool> {
        return Binder(base, binding: { tableView, shouldShow in
            if shouldShow {
                let activityIndicatorView = UIActivityIndicatorView.build(type: .gray).then {
                    $0.frame = CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: 70)
                    $0.startAnimating()
                }

                tableView.tableFooterView = activityIndicatorView
            } else {
                tableView.tableFooterView = UIView()
            }
        })
    }
}
