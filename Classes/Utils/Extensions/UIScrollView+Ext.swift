//
//  UIScrollView+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 08.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

extension UIScrollView {
    func fitContentInsets(_ insets: UIEdgeInsets) {
        contentInset = insets
        scrollIndicatorInsets = insets
    }

    func setBottomInset(_ inset: CGFloat) {
        contentInset.bottom = inset

        if #available(iOS 13.0, *) {
            verticalScrollIndicatorInsets.bottom = inset
        } else {
            scrollIndicatorInsets.bottom = inset
        }
    }
}
