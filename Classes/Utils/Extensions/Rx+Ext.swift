//
//  Rx+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CoreLocation

// MARK: - ObservableType

extension ObservableType {
    func mapToVoid() -> Observable<Void> {
        return map { _ in }
    }

    func catchErrorJustComplete() -> Observable<Element> {
        return catchError { _ in
            return .empty()
        }
    }

    func asCompletable() -> Completable {
        return asSingle()
            .asCompletable()
    }
}

// MARK: - ObservableType + Int

extension ObservableType where Element == Int {
    func increment() -> Observable<Int> {
        return map({ $0 + 1 })
    }

    func decrement() -> Observable<Int> {
        return map({ $0 - 1 })
    }

    func mapToString() -> Observable<String> {
        return map({ "\($0)" })
    }
}

// MARK: - ObservableType + Double

extension ObservableType where Element == Double {
    func mapToPrice() -> Observable<String> {
        return map({ $0.mapToPrice })
    }

    func mapToInt() -> Observable<Int> {
        return map({ Int($0) })
    }

    func mapToString() -> Observable<String> {
        return map({ $0.clean })
    }
}

extension ObservableType where Element == Double? {
    func mapToPrice() -> Observable<String> {
        return map({ price in
            if let price = price {
                return price.mapToPrice
            } else {
                return "~"
            }
        })
    }

    func isPositive() -> Observable<Bool> {
        return map({ value -> Bool in
            if let value = value {
                return value > 0
            } else {
                return false
            }
        })
    }

    func mapToString() -> Observable<String> {
        return map({ value in
            return value.orZero.clean
        })
    }
}

// MARK: - ObservableType + String

extension ObservableType where Element == String {
    func mapToInt(placeholder: Int) -> Observable<Int> {
        return map({ Int($0) ?? placeholder })
    }

    func mapToInt() -> Observable<Int?> {
        return map({ Int($0) })
    }

    func mapToDouble() -> Observable<Double?> {
        return map({ Double($0.replacingOccurrences(of: ",", with: ".")) })
    }

    func mapToControllerTitle() -> Observable<String> {
        return map({ $0.count > 24 ? "\($0.prefix(24))..." : $0 })
    }
}

// MAKR: - PrimitiveSequence

extension ObservableType where Element == Location? {
    func filterLocation() -> Observable<Location> {
        return flatMap { location in
            return Observable.create { (eventEmitter) -> Disposable in
                if let location = location {
                    eventEmitter.onNext(location)
                } else {
                    if LocationSessionController.shared.locationStatus.value == .denied {
                        eventEmitter.onError(LocationSessionError.accessDenied)
                    } else {
                        eventEmitter.onError(LocationSessionError.unknown)
                    }
                }

                return Disposables.create()
            }
        }
    }
}


// MARK: - SharedSequenceConvertibleType

extension SharedSequenceConvertibleType {
    func mapToVoid() -> SharedSequence<SharingStrategy, Void> {
        return map { _ in }
    }
}

// MARK: DomainType

extension Sequence where Iterator.Element: DomainConvertibleType {
    typealias DomainType = Iterator.Element.DomainModelType

    func mapToDomain() -> [DomainType] {
        return map { $0.converteToDomain() }
    }
}

extension Observable where Element: Sequence, Element.Iterator.Element: DomainConvertibleType {
    typealias DomainType = Element.Element.DomainModelType

    func mapToDomain() -> Observable<[DomainType]> {
        return map({ $0.mapToDomain() })
    }
}

extension PrimitiveSequenceType where Trait == SingleTrait, Element: Sequence, Element.Iterator.Element: DomainConvertibleType {
    typealias DomainType = Element.Element.DomainModelType

    func mapToDomain() -> Single<[DomainType]> {
        return map({ $0.mapToDomain() })
    }
}

extension ObservableType where Element == CLLocation? {
    func mapToDomain() -> Observable<Location?> {
        return map({ location in
            if let location = location {
                return Location(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            } else {
                return nil
            }
        })
    }
}
