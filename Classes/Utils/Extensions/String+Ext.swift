//
//  String+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

extension String {
    var capitalizedFirstLetter: String {
        return prefix(1).uppercased() + lowercased().dropFirst()
    }

    func prepareForFilter() -> String {
        return lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
    }

    var dataOrEmpty: Data {
        return data(using: .utf8) ?? Data()
    }
}

extension Optional where Wrapped == String {
    var orEmpty: String {
        return self ?? ""
    }

    var isNotEmpty: Bool {
        return orEmpty.isNotEmpty
    }

    func orPlaceholder(_ placeholder: String) -> String {
        return self ?? placeholder
    }
}

extension String {
    func boundingSize(size: CGSize, attributes: [NSAttributedString.Key: Any]?) -> CGSize {
        let attributedString = NSAttributedString(string: self, attributes: attributes)
        let options: NSStringDrawingOptions = [.usesLineFragmentOrigin, .usesFontLeading]
        let boundedRect = attributedString.boundingRect(with: size, options: options, context: nil)

        let width = ceil(boundedRect.width)
        let height = ceil(boundedRect.height)

        return CGSize(width: width, height: height)
    }

    func boundingSize(size: CGSize, font: UIFont) -> CGSize {
        return boundingSize(size: size, attributes: [.font: font])
    }

    func boundingHeight(width: CGFloat, attributes: [NSAttributedString.Key: Any]?) -> CGFloat {
        let size = CGSize(width: width, height: .greatestFiniteMagnitude)
        return boundingSize(size: size, attributes: attributes).height
    }

    func boundingHeight(width: CGFloat, font: UIFont) -> CGFloat {
        let size = CGSize(width: width, height: .greatestFiniteMagnitude)
        return boundingSize(size: size, attributes: [.font: font]).height
    }

    func boundingWidth(height: CGFloat, attributes: [NSAttributedString.Key: Any]?) -> CGFloat {
        let size = CGSize(width: .greatestFiniteMagnitude, height: height)
        return boundingSize(size: size, attributes: attributes).width
    }

    func boundingWidth(height: CGFloat, font: UIFont) -> CGFloat {
        let size = CGSize(width: .greatestFiniteMagnitude, height: height)
        return boundingSize(size: size, attributes: [.font: font]).width
    }
}

extension String {
    static var elementKindSectionHeader: String {
        return UICollectionView.elementKindSectionHeader
    }

    static var elementKindSectionFooter: String {
        return UICollectionView.elementKindSectionFooter
    }
}
