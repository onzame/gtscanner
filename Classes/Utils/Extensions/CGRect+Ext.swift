//
//  CGRect+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 08.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import CoreGraphics

extension CGRect {
    init(squareSide: CGFloat) {
        self.init(origin: .zero, size: CGSize(squareSide: squareSide))
    }

    init(width: CGFloat, height: CGFloat) {
        self.init(origin: .zero, size: CGSize(width: width, height: height))
    }
}
