//
//  UIActivityIndicatorView+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

extension UIActivityIndicatorView {
    enum IndicatorType {
        case whiteLarge
        case white
        case gray

        var style: Style {
            if #available(iOS 13.0, *) {
                switch self {
                case .gray, .white:
                    return .medium
                case .whiteLarge:
                    return .large
                }
            } else {
                switch self {
                case .whiteLarge:
                    return .whiteLarge
                case .white:
                    return .white
                case .gray:
                    return .gray
                }
            }
        }

        var color: UIColor {
            switch self {
            case .gray:
                return .gray
            case .white, .whiteLarge:
                return .white
            }
        }
    }

    static func build(type: IndicatorType) -> UIActivityIndicatorView {
        if #available(iOS 13.0, *) {
            return UIActivityIndicatorView().then {
                $0.style = type.style
                $0.color = type.color
            }
        } else {
            return UIActivityIndicatorView(style: type.style)
        }
    }
}
