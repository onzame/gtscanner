//
//  UIView+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

extension UIView {

    var identifier: String? {
        get {
            return accessibilityIdentifier
        }
        set {
            accessibilityIdentifier = newValue
        }
    }

    func viewWithIdentifier(identifier: String) -> UIView? {
        if self.identifier == identifier {
            return self
        }

        for view in self.subviews {
            if let view = view.viewWithIdentifier(identifier: identifier) {
                return view
            }
        }

        return nil
    }

    // MARK: - Style

    func setCornerRadius(corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11, *) {
            layer.maskedCorners = corners
            layer.cornerRadius = radius
        } else {
            var cornerMask = UIRectCorner()

            if corners.contains(.layerMinXMinYCorner) {
                cornerMask.insert(.topLeft)
            }

            if corners.contains(.layerMaxXMinYCorner) {
                cornerMask.insert(.topRight)
            }

            if corners.contains(.layerMinXMaxYCorner) {
                cornerMask.insert(.bottomLeft)
            }

            if corners.contains(.layerMaxXMaxYCorner) {
                cornerMask.insert(.bottomRight)
            }

            let path = UIBezierPath(
                roundedRect: bounds,
                byRoundingCorners: cornerMask,
                cornerRadii: CGSize(width: radius, height: radius)
            )

            layer.mask = CAShapeLayer().then({
                $0.path = path.cgPath
            })
        }
    }

    func setCornerRadius(_ radius: CGFloat, masksToBounds: Bool = true) {
        layer.cornerRadius = radius
        layer.masksToBounds = masksToBounds
        clipsToBounds = masksToBounds
    }

    func setBackgroundColor(_ color: UIColor?) {
        backgroundColor = color
    }

    // MARK: - Keyboard

    func setHideKeyboardOnTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        endEditing(true)
    }

    // MARK: - Layout

    func findViewController() -> UIViewController? {
        if let nextResponder = self.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = self.next as? UIView {
            return nextResponder.findViewController()
        } else {
            return nil
        }
    }

    func removeAllSubviews() {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }
}
