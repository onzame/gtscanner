//
//  UIEdgeInsets+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

extension UIEdgeInsets {
    init(side: CGFloat) {
        self.init(top: side, left: side, bottom: side, right: side)
    }
}
