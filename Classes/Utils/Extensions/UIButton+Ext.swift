//
//  UIButton+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

extension UIButton {
    func setTitleFont(_ font: UIFont?) {
        titleLabel?.font = font
    }

    func setTintColor(_ color: UIColor?) {
        tintColor = color
    }

    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
        setHorizontalInsets(insetAmount)
    }

    func setHorizontalInsets(_ margin: CGFloat) {
        let leftInset = contentEdgeInsets.left + margin
        let rightInset = contentEdgeInsets.right + margin
        contentEdgeInsets = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }

    func setRightToLeftSemanticContentAttribute() {
        transform = CGAffineTransform(scaleX: -1, y: 1)
        titleLabel?.transform = CGAffineTransform(scaleX: -1, y: 1)
        imageView?.transform = CGAffineTransform(scaleX: -1, y: 1)
    }
}
