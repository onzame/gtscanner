//
//  Numbers+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

// MARK: - Int

extension Int {
    var isZero: Bool {
        return self == 0
    }

    var isNotZero: Bool {
        return !isZero
    }

    var dataOrEmpty: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<Int>.size)
    }

    var string: String {
        return "\(self)"
    }
}

extension Optional where Wrapped == Int {
    var orZero: Int {
        return self ?? 0
    }

    var isNotZero: Bool {
        return orZero > 0
    }
}

// MARK: - Double

extension Double {
    var clean: String {
        return Formatter.clean.string(from: NSDecimalNumber(value: self)) ?? "\(self)"
    }

    var mapToPrice: String {
        let result = Formatter.price.string(from: NSDecimalNumber(value: self)) ?? "\(self)"
        return "~\(result) ₽"
    }

    var date: Date {
        return Date(timeIntervalSince1970: self)
    }

    var dataOrEmpty: Data {
        var double = self
        return Data(bytes: &double, count: MemoryLayout<Double>.size)
    }

    var string: String {
        return "\(self)"
    }

    var mapMetersToKilometers: Double {
        return self / 1000
    }

    var withoutZero: String {
        return String(Formatter.withoutZero.string(from: NSNumber(value: self)) ?? "")
    }
}

extension Optional where Wrapped == Double {
    var orZero: Double {
        return self ?? 0
    }

    var isNotZero: Bool {
        return orZero > 0
    }
}

// MARK: - Formatter

extension Formatter {
    static var clean = NumberFormatter().then {
        $0.maximumFractionDigits = 2
    }

    static var price = NumberFormatter().then {
        $0.numberStyle = .decimal
        $0.groupingSeparator = " "
        $0.maximumFractionDigits = 2
    }

    static var withoutZero = NumberFormatter().then {
        $0.minimumFractionDigits = 0
        $0.maximumFractionDigits = 16
    }
}
