//
//  Bundle+Ext.swift
//  Alamofire
//
//  Created by Sergei Fabian on 02.03.2020.
//

import Foundation

private class BundleMemebr { }

extension Bundle {
    enum GTScanner { }
}

extension Bundle.GTScanner {
    static var bundle: Bundle? {
        let classBundle = Bundle(for: BundleMemebr.self)

        guard let url = classBundle.url(forResource: "GTScanner", withExtension: "bundle") else {
            assertionFailure("Can't get class bundle URL")
            return nil
        }

        guard let bundle = Bundle(url: url) else {
            assertionFailure("Can't get bundle for URL: \(url.absoluteString)")
            return nil
        }

        return bundle
    }
}
