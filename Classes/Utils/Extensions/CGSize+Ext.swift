//
//  CGSize+Ext.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import CoreGraphics

extension CGSize {
    init(squareSide: CGFloat) {
        self.init(width: squareSide, height: squareSide)
    }

    static var greatestFiniteMagnitude: CGSize {
        return CGSize(squareSide: .greatestFiniteMagnitude)
    }
}
