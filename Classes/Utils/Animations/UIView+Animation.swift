//
//  UIView+Animation.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class AnimationFactory {
    static func animate(
        duration: TimeInterval = 0,
        delay: TimeInterval = 0,
        dampingRatio: CGFloat = 1,
        velocity: CGFloat = 0,
        options: UIView.AnimationOptions = [],
        animations: (() -> Void)? = nil,
        completion: (() -> Void)? = nil
    ) {
        UIView.animate(
            withDuration: duration,
            delay: delay,
            usingSpringWithDamping: dampingRatio,
            initialSpringVelocity: velocity,
            options: options,
            animations: { animations?() },
            completion: { _ in completion?() }
        )
    }
}
