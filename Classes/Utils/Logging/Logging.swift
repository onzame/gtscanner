//
//  Logging.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.04.2020.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

let log = Logger.self

final class Logger {
    static func info(_ message: @autoclosure () -> Any, _ file: String = #file, _ function: String = #function, line: Int = #line) {
        if Environment.isDebugMode {
            print("\(file) - \(function) (\(line): \(message())")
        }
    }
}
