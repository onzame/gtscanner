//
//  Constants+Images.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

enum Media { }

extension Media {
    enum IconSize: String {
        case x18 = "18"
        case x24 = "24"
    }

    enum IconName: String {
        case history = "iconHistory"
        case fileText = "iconFileText"
        case leftChevron = "iconLeftChevron"
        case rightChevron = "iconRightChevron"
        case clear = "iconClear"
        case minus = "iconMinus"
        case plus = "iconPlus"
        case dialogBubble = "iconDialogBubble"
        case profile = "iconProfile"
        case trash = "iconTrash"
        case starFilled = "iconStarFilled"
        case save = "iconSave"
        case share = "iconShare"
        case search = "iconSearch"
        case searchClear = "iconSearchClear"
        case add = "iconAdd"
        case info = "iconInfo"
    }

    enum ImageName: String {
        case star = "imageStar"
        case starFilled = "imageStarFilled"
        case starFat = "imageStarFat"
        case starFatFilled = "imageStarFatFilled"
        case handsHeart = "imageHandsHeart"
        case handLike = "imageHandLike"
        case featureScan = "imageFeatureScan"
        case handsError = "imageHandsError"
        case interactiveIndicator = "imageInteractiveIndicator" // 92x16
        case productPlaceholder = "imageProductPlaceholder" // 32x36
        case productPreviewPlaceholder = "imageProductPreviewPlaceholder"
        case avatarPlaceholder = "imageAvatarPlaceholder"
        case photoPlaceholder = "imagePhotoPlaceholder" // 128x128
        case circleError = "imageCircleError" // 74x74
        case supermarketi = "imageSupermarketi"
        case prodovolstvennyeMagaziny = "imageProdovolstvennyeMagaziny"
        case optika = "imageOptika"
        case gipermarkety = "imageGipermarkety"
        case bitovayaHimia = "imageBitovayaHimia"
        case apteka = "imageApteka"
        case alkogolnieNapitky = "imageAlkogolnieNapitky"
    }

    static func icon(_ named: IconName, size: IconSize) -> UIImage? {
        return UIImage(named: "\(named.rawValue)\(size.rawValue)", in: Bundle.GTScanner.bundle, compatibleWith: nil)
    }

    static func image(_ named: ImageName) -> UIImage? {
        return UIImage(named: named.rawValue, in: Bundle.GTScanner.bundle, compatibleWith: nil)
    }

    static func placeholder(for shopCategory: ShopCategoryType, imageSize: CGSize? = nil) -> ImagePlaceholderView? {
        return ImagePlaceholderView(image: shopCategoryPlaceholderImage(for: shopCategory), imageSize: imageSize)
    }

    static func shopCategoryPlaceholderImage(for shopCategory: ShopCategoryType) -> UIImage? {
        switch shopCategory {
        case .alkogolnieNapitky:
            return image(.alkogolnieNapitky)
        case .apteka:
            return image(.apteka)
        case .bitovayaHimia:
            return image(.bitovayaHimia)
        case .gipermarkety:
            return image(.gipermarkety)
        case .optika:
            return image(.optika)
        case .prodovolstvennyeMagaziny:
            return image(.prodovolstvennyeMagaziny)
        case .supermarketi:
            return image(.supermarketi)
        default:
            return image(.productPlaceholder)
        }
    }
}
