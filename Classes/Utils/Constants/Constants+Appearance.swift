//
//  Constants+Appearance.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

enum Style { }

extension Style {
    enum Color {
        static let white = UIColor.white
        static let black = UIColor.black
        static let brightYellow = UIColor(hex: "#FFE249")
        static let brightBlue = UIColor(hex: "#2A83FF")
        static let pink = UIColor(hex: "#FB0164")
        static let darkViolet = UIColor(hex: "#7927EB")
        static let lightBlue = UIColor(hex: "#F4F6FA")
        static let newGray = UIColor(hex: "#A4B1C2")
        static let grayBlue = UIColor(hex: "#E9EEF3")
        static let gray = UIColor(hex: "#818091")
        static let blackout = UIColor(hex: "#001326", alpha: 0.6)
        static let secondaryText = UIColor(hex: "#A3B1C1")
        static let green = UIColor(hex: "#26CB90")
        static let aquamarine = UIColor(hex: "#1AD6BF")
    }
}

extension Style {
    enum Font {
        static func sanFrancisco(_ style: UIFont.FontStyle, size: CGFloat) -> UIFont {
            return UIFont.fontOrSystem(.sanFranciscoProDisplay, style, size: size)
        }

        static func sanFrancisco(_ style: UIFont.FontStyle, size: FontSize) -> UIFont {
            return sanFrancisco(style, size: size.value)
        }
    }

    enum FontSize {
        case headline
        case subheadline

        var value: CGFloat {
            switch self {
            case .headline:
                return Screen.isLowWidthScreen ? 14 : 16
            case .subheadline:
                return Screen.isLowWidthScreen ? 10 : 12
            }
        }
    }
}

extension UIFont {
    enum FontFamily: String, CaseIterable {
        case sanFranciscoProDisplay = "SFProDisplay"
    }

    enum FontStyle: String, CaseIterable {
        case black
        case blackItalic
        case bold
        case boldItalic
        case heavy
        case heavyItalic
        case light
        case lightItalic
        case medium
        case mediumItalic
        case regular
        case regularItalic
        case semibold
        case semiboldItalic
        case thin
        case thinItalic
        case ultralight
        case ultralightItalic
    }

    enum FontSize {
        case headline
        case subheadline

        var value: CGFloat {
            switch self {
            case .headline:
                return Screen.isLowWidthScreen ? 18 : 20
            case .subheadline:
                return Screen.isLowWidthScreen ? 12 : 14
            }
        }
    }

    // MARK: - Utils

    class func buildFontName(_ family: FontFamily, _ style: FontStyle) -> String {
        return "\(family.rawValue)-\(style.rawValue)"
    }

    class func fontOrSystem(_ family: FontFamily, _ style: FontStyle, size: CGFloat) -> UIFont {
        if let font = UIFont(family: family, style: style, size: size) {
            return font
        } else if let font = returnRegisteredFont(family, style, size: size) {
            return font
        } else {
            return UIFont.systemFont(ofSize: size)
        }
    }

    class func returnRegisteredFont(_ family: FontFamily, _ style: FontStyle, size: CGFloat) -> UIFont? {
        guard let url = Bundle.GTScanner.bundle?.url(forResource: buildFontName(family, style), withExtension: "ttf") else {
            assertionFailure("Can't get URL for font name: \(buildFontName(family, style))")
            return nil
        }

        do {
            let fontData = try Data(contentsOf: url)

            guard let fontDataProvider = CGDataProvider(data: fontData as CFData) else {
                assertionFailure("Can't resolve data provider for URL: \(url.absoluteString)")
                return nil
            }

            guard let font = CGFont(fontDataProvider) else {
                assertionFailure("Can't create font from data: \(fontDataProvider)")
                return nil
            }

            var error: Unmanaged<CFError>?

            let success = CTFontManagerRegisterGraphicsFont(font, &error)

            guard success else {
                assertionFailure("Can't register font with name: \(buildFontName(family, style)), error: \(error.debugDescription)")
                return nil
            }

            guard let registeredFontName = font.postScriptName as String? else {
                assertionFailure("Can't get registered font name for \(buildFontName(family, style))")
                return nil
            }

            return UIFont(name: registeredFontName, size: size)

        } catch {
            assertionFailure("Can't get data for font: \(buildFontName(family, style))")
            return nil
        }

    }

    // MARK: - Lifecycle

    convenience init?(family: FontFamily, style: FontStyle, size: CGFloat) {
        self.init(name: "\(family.rawValue)-\(style.rawValue)", size: size)
    }

}
