//
//  Constants+Device.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

enum Device {
    static var isPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
}
