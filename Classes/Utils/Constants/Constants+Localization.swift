//
//  Constants+Localization.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

enum Localization { }

extension Localization {
    enum Common {
        static let okTitle = "ОК"
        static let errorTitle = "Ошибка!"
        static let pricesAmount = { (value: Int) -> String in value.pluralize(one: "цена", few: "цены", many: "цен") }
    }

    enum Network {
        static let statusCode = { (value: Int) -> String in "Код ответа: \(value)" } 
        static let backendErrorDescription = "Не удалось обработать ответ от сервера (код 5**)"
        static let unknownErrorDescription = "Неизвестная ошибка"
    }

    enum Location {
        static let noPermissionTitle = "Нет доступа к геопозиции"
        static let noPermissionSubtitle = "Необходимо перейти в настройки и разрешить приложению использовать геопозицию"
        static let noPermissionActionTitle = "Открыть настройки"
        static let noPermissionMessage = "Не удалось определить вашу геопозицию, проверьте настройки приложения"
    }

    enum Decoding {
        static let decodingError = "Не удалось обработать результат ответа от сервера"
    }

    enum Scanner {
        static let title: String? = nil
        static let tipEmptyState = "Наведите камеру на штрихкод"
        static let tipFilledState = "Продолжайте сканирование"
        static let featureTitle = "Новинка!"
        static let featureSubtitle = "Сканируйте штрихкоды и получайте бонусы за покупку товаров от производителей"
        static let noAccessTitle = "Нет доступа к камере"
        static let noAccessSubtitle = "Необходимо перейти в настройки и разрешить приложению использовать камеру"
        static let noAccessActionTitle = "Открыть настройки"
        static let notFoundTitle = "Товар не найден"
        static let notFoundSubtitle = "Видимо, вы отсканировали что-то уникальное. Его нет в базе, но вы можете его добавить"
        static let notFoundActionTitle = "+ Добавить товар"
        static let addPriceActionTitle = "+ Добавить цену"
        static let compareBannerActionTitle = "Сравнить"
    }

    enum ScanList {
        static let title = "Список товаров"
        static let noDataPlaceholderTitle = "В списке пока ничего нет"
        static let noDataPlaceholderSubtitle = "Добавляйте товары из карточки товара или из истории просмотров"
        static let noDataPlaceholderActionTitle = "История просмотров"
        static let errorPlaceholderTitle = "Не удалось загрузить список товаров"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum ScanHistory {
        static let title = "История"
        static let noDataPlaceholderTitle = "У вас нет отсканированных товаров"
        static let noDataPlaceholderSubtitle = "Сканируйте товары, чтобы они появились в этом разделе"
        static let noDataPlaceholderActionTitle = "Сканировать товары"
        static let errorPlaceholderTitle = "Не удалось загрузить историю товаров"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum ProductAmount {
        static let cancelActionTitle = "Отменить"
        static let addActionTitle = "Добавить"
        static let deleteActionTitle = "Удалить"
        static let amountTextFieldPlaceholder = "Введите количество"
    }

    enum ProductPrice {
        static let submitActionTitle = "Оставить цену"
        static let cancelActionTitle = "Отмена"
        static let priceTextFieldPlaceholder = "Цена товара"
    }

    enum ProductPriceCompletion {
        static let title = "Спасибо за цену"
        static let subtitle = { (value: Double) -> String in "Вы оценили товар в \(value.withoutZero) рублей.\nВаши цены будут отображаться в карточке товара" }
        static let actionTitle = "Продолжить"
    }

    enum ProductRating {
        static let submitActionTitle = "Оценить товар"
        static let cancelActionTitle = "Отмена"
    }

    enum ProductRatingCompletion {
        static let title = "Спасибо за оценку"
        static let subtitle = { (value: Int, maxValue: Int) -> String in "Вы оценили этот товар на \(value) из \(maxValue).\nВаша оценка учтена." }
        static let actionTitle = "Продолжить"
    }

    enum ProductReview {
        static let title = "Оставить отзыв"
        static let positiveTextFieldPlaceholder = "Достоинства"
        static let negativeTextFieldPlaceholder = "Недостатки"
        static let textFieldPlaceholder = "Комментарий"
        static let submitActionTitle = "Оставить отзыв"
        static let cancelActionTitle = "Отмена"
    }

    enum ProductAdd {
        static let title = "Добавить товар"
        static let nameTextFieldTitle = "Название"
        static let barcodeTextFieldTitle = "Штрихкод"
        static let priceTextFieldTitle = "Цена"
        static let shopTitle = "Магазин"
        static let shopPlaceholder = "Выбрать магазин"
        static let categoryTitle = "Категория"
        static let categoryPlaceholder = "Выбрать категорию"
        static let changeActionTitle = "Изменить"
        static let ratingTitle = "Поставьте свою оценку"
        static let submitActionTitle = "Отправить"
        static let cancelActionTitle = "Отмена"
    }

    enum ProductInfo {
        static let addPriceActionTitle = "+ Добавить цену"
        static let addReviewActionTitle = "+ Оставить отзыв"
        static let saveProductActionTitle = "+ Добавить в список"
        static let ratingEmptyState = "Поставьте свою оценку"
        static let reviewsTitle = "Отзывы"
        static let aboutTitle = "О товаре"
        static let showDetailsActionTitle = "Все"
        static let showReviewsActionTitle = { (amount: Int) -> String in amount > 0 ? "Еще \(amount)" : "Все" }
        static let nameTitle = "Название"
        static let barcodeTitle = "Штрихкод"
        static let locationTitle = "Страна"
        static let reviewAuthorNamePlaceholder = "Пользователь"
        static let reviewEmptyTitle = "Нет отзывов"
        static let reviewEmptySubtitle = "У товара пока нет отзывов. Будьте первым кто его оставит"
        static let compareButtonAddActionTitle = "+ Сравнить"
        static let compareButtonDeleteActionTitle = "Товар в сравнении"
        static let compareAmountTitle = "Сравнение"
    }

    enum ProductDetails {
        static let title = "О товаре"
        static let noDataPlaceholderTitle = "Об этом товаре нет никаких сведений"
        static let noDataPlaceholderSubtitle = "Возможно, что они появятся позже"
        static let noDataPlaceholderActionTitle = "Обновить"
        static let errorPlaceholderTitle = "Не удалось загрузить данные о товаре"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum ProductPricesList {
        static let title = "Цены"
        static let addPriceActionTitle = "+ Добавить цену"
        static let noDataPlaceholderTitle = "У этого товара пока нет цен"
        static let noDataPlaceholderSubtitle = "Будьте первым, кто добавит цену на этот товар"
        static let noDataPlaceholderActionTitle = "+ Добавить цену"
        static let errorPlaceholderTitle = "Не удалось загрузить список цен"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum ProductReviewsList {
        static let title = "Отзывы"
        static let addReviewActionTitle = "+ Добавить отзыв"
        static let noDataPlaceholderTitle = "У этого товара пока нет отзывов"
        static let noDataPlaceholderSubtitle = "Будьте первым, кто оставит отзыв на этот товар"
        static let noDataPlaceholderActionTitle = "+ Оставить отзыв"
        static let errorPlaceholderTitle = "Не удалось загрузить список отзывов"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum Shops {
        static let title = "Все магазины"
        static let searchTextPlaceholder = "Введите название или адрес магазина"
        static let noDataPlaceholderTitle = "Не удалось найти магазины поблизости"
        static let noDataPlaceholderActionTitle = "Обновить"
        static let errorPlaceholderTitle = "Не удалось загрузить список магазинов"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
        static let noPermissionPlaceholderTitle = "Не удалось загрузить список магазинов"
        static let noPermissionPlaceholderSubtitle = "Разрешите доступ к геопозиции, чтобы увидеть ближайшие к вам магазины"
        static let noPermissionPlaceholderActionTitle = "Открыть настройки"
        static let filterNoDataPlaceholderTitle = "Не найдено"
        static let filterNoDataPlaceholderSubtitle = "Попробуйте изменить параметры поиска"
    }

    enum ShopsPreview {
        static let title = "Выберите магазин"
        static let openShopsActionTitle = "Все магазины"
        static let errorNoLocationTitle = "Разрешите доступ к геопозиции, чтобы увидеть ближайшие к вам магазины"
        static let errorNoLocationActionTitle = "Открыть настройки"
        static let noDataTitle = "Нет магазинов рядом"
        static let noDataSubtitle = "К сожалению, мы не нашли информацию о местах продажи этого товара рядом с вами"
    }

    enum Photos {
        static let title = "Фото товара"
        static let cameraSourceTitle = "Сделать фотографию"
        static let gallerySourceTitle = "Выбрать из галереи"
        static let cancel = "Отмена"
    }

    enum Categories {
        static let title = "Все категории"
        static let searchTextPlaceholder = "Введите название категории"
        static let noDataPlaceholderTitle = "Нет доступных категорий"
        static let noDataPlaceholderSubtitle = "Возможно, что они появятся позже"
        static let noDataPlaceholderActionTitle = "Обновить"
        static let errorPlaceholderTitle = "Не удалось загрузить список категорий"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum CompareCategories {
        static let title = "Сравнение товаров"
        static let noDataPlaceholderTitle = "Вам нечего пока сравнивать"
        static let noDataPlaceholderSubtitle = "Для того чтобы сравнить товары, вам нужно отсканировать хотя-бы несколько товаров"
        static let noDataPlaceholderActionTitle = "Сканировать товары"
        static let errorPlaceholderTitle = "Не удалось загрузить список категорий"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }

    enum CompareProducts {
        static let ratingHeaderTitle = "Рейтинг"
        static let reviewsHeaderTitle = "Количество отзывов"
        static let noDataPlaceholderTitle = "Вам нечего пока сравнивать"
        static let noDataPlaceholderSubtitle = "Для того чтобы сравнить товары, вам нужно отсканировать хотя-бы несколько товаров"
        static let noDataPlaceholderActionTitle = "Сканировать товары"
        static let errorPlaceholderTitle = "Не удалось загрузить список для сравнения"
        static let errorPlaceholderSubtitle = "Проверьте соедениеи с интернетом и попробуйте еще раз"
        static let errorPlaceholderActionTitle = "Повторить"
    }
}
