//
//  Constants+Environment.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

final class Environment {

    private static var configurationContainer: EnvironmentConfiguration?

    private static var configuration: EnvironmentConfiguration {
        if let configuration = configurationContainer {
            return configuration
        } else {
            fatalError("Configure module with EnvironmentConfiguration: ScanModuleFactory.configure(configuration:)")
        }
    }

    static var apiUrl: URL {
        return configuration.apiUrl
    }

    static var isDebugMode: Bool {
        return configuration.isDebugMode
    }

    static func setup(configuration: EnvironmentConfiguration) {
        self.configurationContainer = configuration
    }
}
