//
//  Constants+Number.swift
//  Alamofire
//
//  Created by Sergei Fabian on 27.03.2020.
//

import Foundation

enum Rating {
    static let totalStars = 5
}

enum ProductPhoto {
    static let sendTotalAmount = 10
    static let sendSize = CGSize(width: 1000, height: 1000)
}
