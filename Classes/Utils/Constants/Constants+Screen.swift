//
//  Constants+Screen.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

enum Screen {
    static var width: CGFloat {
        return UIScreen.main.bounds.width
    }

    static var height: CGFloat {
        return UIScreen.main.bounds.height
    }

    static var safeHeight: CGFloat {
        return UIScreen.main.bounds.height - topSafeInset - bottomSafeInset
    }

    static var isLowWidthScreen: Bool {
        return UIScreen.main.bounds.width <= 320
    }

    static var topSafeInset: CGFloat {
        if #available(iOS 11.0, *) {
            return Application.keyWindow?.safeAreaInsets.top ?? 0
        } else {
            return Application.keyWindow?.rootViewController?.topLayoutGuide.length ?? 0
        }
    }

    static var bottomSafeInset: CGFloat {
        if #available(iOS 11.0, *) {
            return Application.keyWindow?.safeAreaInsets.bottom ?? 0
        } else {
            return Application.keyWindow?.rootViewController?.bottomLayoutGuide.length ?? 0
        }
    }
}
