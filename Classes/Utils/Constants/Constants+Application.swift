//
//  Constants+Application.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

struct Application {
    static var keyWindow: UIWindow? {
        return UIApplication.shared.windows.first(where: { $0.isKeyWindow })
    }
}
