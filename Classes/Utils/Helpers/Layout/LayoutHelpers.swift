//
//  LayoutHelpers.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import SnapKit

extension UIViewController {
    var safeTopConstraintItem: ConstraintItem {
        if #available(iOS 11.0, *) {
            return view.safeAreaLayoutGuide.snp.top
        } else {
            return topLayoutGuide.snp.bottom
        }
    }

    var safeBottomConstraintItem: ConstraintItem {
        if #available(iOS 11.0, *) {
            return view.safeAreaLayoutGuide.snp.bottom
        } else {
            return bottomLayoutGuide.snp.top
        }
    }

    var safeBottomInset: CGFloat {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets.bottom
        } else {
            return bottomLayoutGuide.length
        }
    }
}

extension UIView {
    var safeTopConstraintItem: ConstraintItem {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.snp.top
        } else if let viewController = findViewController() {
            return viewController.topLayoutGuide.snp.bottom
        } else {
            return snp.top
        }
    }

    var safeBottomConstraintItem: ConstraintItem {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.snp.bottom
        } else if let viewController = findViewController() {
            return viewController.bottomLayoutGuide.snp.top
        } else {
            return snp.bottom
        }
    }

    var safeLeftConstraintItem: ConstraintItem {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.snp.left
        } else {
            return snp.left
        }
    }

    var safeRightConstraintItem: ConstraintItem {
        if #available(iOS 11.0, *) {
            return safeAreaLayoutGuide.snp.right
        } else {
            return snp.right
        }
    }

    func addSafeEdgesConstraints() {
        guard let superview = superview else { return }

        snp.makeConstraints { (make) in
            make.left.equalTo(superview.safeLeftConstraintItem)
            make.top.equalTo(superview.safeTopConstraintItem)
            make.right.equalTo(superview.safeRightConstraintItem)
            make.bottom.equalTo(superview.safeBottomConstraintItem)
        }
    }

    func addSafeEdgesConstraints(insideOf superview: UIView) {
        snp.makeConstraints { (make) in
            make.left.equalTo(superview.safeLeftConstraintItem)
            make.top.equalTo(superview.safeTopConstraintItem)
            make.right.equalTo(superview.safeRightConstraintItem)
            make.bottom.equalTo(superview.safeBottomConstraintItem)
        }
    }

    func addEdgesConstraints() {
        snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

    func removeAllConstraints() {
        constraints.forEach(removeConstraint)
    }
}
