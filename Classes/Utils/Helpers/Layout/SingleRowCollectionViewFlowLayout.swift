//
//  SingleRowCollectionViewFlowLayout.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class SingleRowCollectionViewFlowLayout: UICollectionViewFlowLayout {

    private var contentSize: CGSize = .zero

    override var collectionViewContentSize: CGSize {
        return contentSize
    }

    private var attributes: [SectionAttributes] = []

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributes[indexPath.section].headerAttributes
    }

    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributes[indexPath.section].itemsAttributes[indexPath.row]
    }

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()

        for row in attributes {
            if let sectionAttributes = row.headerAttributes {
                let targetRect = CGRect(origin: CGPoint(x: 0, y: rect.origin.y), size: rect.size)
                let headerRect = CGRect(origin: CGPoint(x: 0, y: sectionAttributes.frame.origin.y), size: sectionAttributes.frame.size)

                if targetRect.intersects(headerRect) {
                    let updatedSectionAttributes = sectionAttributes
                    let updatedOrigin = CGPoint(x: collectionView!.contentOffset.x, y: sectionAttributes.frame.origin.y)
                    let updatedRect = CGRect(origin: updatedOrigin, size: sectionAttributes.frame.size)
                    updatedSectionAttributes.frame = updatedRect
                    layoutAttributes.append(updatedSectionAttributes)
                }
            }

            for itemAttributes in row.itemsAttributes where rect.intersects(itemAttributes.frame) {
                layoutAttributes.append(itemAttributes)
            }
        }

        return layoutAttributes
    }

    override func prepare() {
        setupAttributes()
        setupContentSize()
    }

    private func setupAttributes() {

        attributes = []

        var xOffset = CGFloat.zero
        var yOffset = CGFloat.zero

        for row in 0 ..< rowsCount {
            xOffset = .zero

            let headerIndexPath = IndexPath(row: row, column: 0)
            let headerAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: .elementKindSectionHeader, with: headerIndexPath)
            let headerSize = calcHeaderSize(for: row)
            headerAttributes.frame = CGRect(x: xOffset, y: yOffset, width: headerSize.width, height: headerSize.height).integral

            yOffset += headerSize.height

            if row == 0 {
                yOffset += sectionInset.top * 4
            } else if columnsCount(in: row) == 0 || (1...2).contains(row) {
                yOffset += sectionInset.top * 3
            } else {
                yOffset += sectionInset.top
            }

            xOffset += sectionInset.left

            var itemsAttributes = [UICollectionViewLayoutAttributes]()

            for column in 0 ..< columnsCount(in: row) {
                let itemSize = calcItemSize(for: row, column: column)
                let indexPath = IndexPath(row: row, column: column)
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: xOffset, y: yOffset, width: itemSize.width, height: itemSize.height)

                itemsAttributes.append(attributes)

                xOffset += itemSize.width + minimumInteritemSpacing
            }

            yOffset += itemsAttributes.map({ $0.frame.height }).max() ?? .zero

            if columnsCount(in: row) > 0 {
                yOffset += sectionInset.bottom
            }

            let sectionAttributes = SectionAttributes(headerAttributes: headerAttributes, itemsAttributes: itemsAttributes)
            attributes.append(sectionAttributes)
        }
    }

    private func setupContentSize() {
        let frames = attributes.map({ $0.itemsAttributes.last?.frame ?? .zero })
        let largestSectionWidth = frames.map({ $0.maxX }).max() ?? .zero
        let largestSectionHeight = frames.map({ $0.maxY }).max() ?? .zero
        contentSize = CGSize(width: largestSectionWidth + sectionInset.right, height: largestSectionHeight)
    }

    // MARK: - Sizing

    private var rowsCount: Int {
        return collectionView!.numberOfSections
    }

    private func columnsCount(in row: Int) -> Int {
        return collectionView!.numberOfItems(inSection: row)
    }

    private func calcHeaderSize(for row: Int) -> CGSize {
        guard let delegate = collectionView?.delegate as? UICollectionViewDelegateFlowLayout else {
            assertionFailure("Implement layout as UICollectionViewDelegateFlowLayout")
            return .zero
        }

        guard let size = delegate.collectionView?(collectionView!, layout: self, referenceSizeForHeaderInSection: row) else {
            assertionFailure("Implement collectionView?(:layout:referenceSizeForHeaderInSection:)")
            return .zero
        }

        return size
    }

    private func calcItemSize(for row: Int, column: Int) -> CGSize {
        guard let delegate = collectionView?.delegate as? UICollectionViewDelegateFlowLayout else {
            assertionFailure("Implement layout as UICollectionViewDelegateFlowLayout")
            return .zero
        }

        guard let size = delegate.collectionView?(collectionView!, layout: self, sizeForItemAt: IndexPath(row: row, column: column)) else {
            assertionFailure("Implement collectionView?(:layout:sizeForItemAt:)")
            return .zero
        }

        return size
    }

}
