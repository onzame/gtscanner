//
//  SectionAttributes.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class SectionAttributes {
    let headerAttributes: UICollectionViewLayoutAttributes?
    let itemsAttributes: [UICollectionViewLayoutAttributes]

    init(headerAttributes: UICollectionViewLayoutAttributes?, itemsAttributes: [UICollectionViewLayoutAttributes]) {
        self.headerAttributes = headerAttributes
        self.itemsAttributes = itemsAttributes
    }
}
