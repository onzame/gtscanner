//
//  UIViewController+Error.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Moya
import MaterialComponents

private enum UserErrorDescription: LocalizedError {
    case parse(description: Error)
    case backend(code: Int, response: String)

    var errorDescription: String? {
        switch self {
        case .parse(description: let error):
            return "\(error)"
        case .backend(_, response: let response):
            return "\(response)"
        }
    }

    var title: String {
        switch self {
        case .parse:
            return Localization.Decoding.decodingError
        case .backend(code: let code, _):
            return "\(Localization.Network.unknownErrorDescription). \(Localization.Network.statusCode(code))"
        }
    }
}

fileprivate extension UIViewController {
    func showAlert(title: String?, message: String) {
        let alertControllert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertControllert.modalPresentationStyle = .overCurrentContext
        alertControllert.addAction(UIAlertAction(title: Localization.Common.okTitle, style: .cancel))
        present(alertControllert, animated: true)
    }

    func showSnackbar(message: String) {
        MDCSnackbarManager.default.snackbarMessageViewShadowColor = .clear
        MDCSnackbarManager.default.setPresentationHostView(view)
        MDCSnackbarManager.default.show(MDCSnackbarMessage(text: message))
    }

    func showMessage(errorDescription: UserErrorDescription) {
        if Environment.isDebugMode {
            showAlert(title: errorDescription.title, message: errorDescription.localizedDescription)
        } else {
            showSnackbar(message: errorDescription.title)
        }
    }
}

extension Reactive where Base: UIViewController {
    var errorSnackbar: Binder<Error> {
        return Binder(base, binding: { viewController, error in
            if case MoyaError.objectMapping(let error, _) = error {
                viewController.showMessage(errorDescription: .parse(description: error))
            } else if let error = error as? DecodingError {
                viewController.showMessage(errorDescription: .parse(description: error))
            } else if case NetworkErrorWrapper.clientUncommented(code: let code, response: let response) = error {
                viewController.showMessage(errorDescription: .backend(code: code, response: response))
            } else if case NetworkErrorWrapper.clientCommented(_, response: let response) = error {
                if let type = response.error.displayingType, type == .dialog {
                    viewController.showAlert(title: response.error.title, message: response.error.description)
                } else {
                    viewController.showSnackbar(message: error.localizedDescription)
                }
            } else {
                viewController.showSnackbar(message: error.localizedDescription)
            }
        })
    }
}
