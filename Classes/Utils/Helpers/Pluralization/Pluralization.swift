//
//  Pluralization.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.04.2020.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

extension Int {
    func pluralize(one: String, few: String, many: String) -> String {
        if isZero {
            return "\(self) \(many)"
        }

        if self % 10 == 1 && self % 100 != 11 {
            return "\(self) \(one)"
        } else if (self % 10 >= 2 && self % 10 <= 4) && !(self % 100 >= 12 && self % 100 <= 14) {
            return "\(self) \(few)"
        } else if self % 10 == 0 || (self % 10 >= 5 && self % 10 <= 9) || (self % 100 >= 11 && self % 100 <= 14) {
            return "\(self) \(many)"
        } else {
            return "\(self) \(one)"
        }
    }
}
