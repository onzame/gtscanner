//
//  UIViewController+Loading.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension UIViewController {
    func showLoadingView() {
        if view.viewWithIdentifier(identifier: loadingViewIdentifier) == nil {
            let loadingView = LoadingView().then {
                $0.activityIndicatorView.startAnimating()
                $0.alpha = 0
                $0.layer.cornerRadius = 12
                $0.identifier = loadingViewIdentifier
                $0.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            }

            view.addSubview(loadingView)

            loadingView.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.width.height.equalTo(64)
            }

            UIView.animate(
                withDuration: loadingViewAnimationDuration,
                animations: {
                    loadingView.alpha = 1
                }
            )
        }
    }

    func hideLoadingView() {
        if let loadingView = view.viewWithIdentifier(identifier: loadingViewIdentifier) {
            UIView.animate(
                withDuration: loadingViewAnimationDuration,
                animations: { loadingView.alpha = 0 },
                completion: { _ in
                    loadingView.removeFromSuperview()
                }
            )
        }
    }
}

extension UIView {
    var loadingSubview: UIView? {
        return viewWithIdentifier(identifier: loadingViewIdentifier)
    }
}

// MARK: - Reactive interface

extension Reactive where Base: UIViewController {
    var loading: Binder<Bool> {
        return Binder(base, binding: { viewController, isLoading in
            let application = UIApplication.shared

            if isLoading && !application.isIgnoringEvents {
                application.beginIgnoringEvents()
            }

            if !isLoading && application.isIgnoringEvents {
                application.endIgnoringEvents()
            }

            isLoading ? viewController.showLoadingView() : viewController.hideLoadingView()
        })
    }
}

// MARK: - Keys

private let loadingViewIdentifier = "utils.view.loading.identifier"
private let loadingViewAnimationDuration: Double = 0.2
//private let loadingViewAnimationDelay: TimeInterval = 0.4
