//
//  CaptureCameraVideoView.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa

class CaptureCameraVideoView: UIView {

    // MARK: - Class overrides

    override class var layerClass: AnyClass {
        return AVCaptureVideoPreviewLayer.self
    }

    // MARK: - Public properties

    var previewLayer: AVCaptureVideoPreviewLayer? {
        return layer as? AVCaptureVideoPreviewLayer
    }

    var session: AVCaptureSession? {
        set {
            previewLayer?.session = newValue
        }
        get {
            return previewLayer?.session
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        previewLayer?.videoGravity = .resizeAspectFill
    }

    // MARK: - Public methods

    func bindSession(_ session: AVCaptureSession) {
        previewLayer?.session = session
    }

}
