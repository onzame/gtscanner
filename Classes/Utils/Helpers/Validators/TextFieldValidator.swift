//
//  TextFieldValidator.swift
//  Gorod
//
//  Created by Sergei Fabian on 02.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class TextFieldValidator {
    static func amountTextField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        let fullString = textField.text.orEmpty + string
        let ruleCharacterSet = CharacterSet(charactersIn: "0123456789")
        let checkCharacterSet = CharacterSet(charactersIn: fullString)
        return ruleCharacterSet.isSuperset(of: checkCharacterSet) && fullString.count < 10
    }

    // MARK: - Implement characters amount checker
    static func priceTextField(
        _ textField: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        let fullString = textField.text.orEmpty + string

        if fullString.first == "," || fullString.first == "." {
            return false
        }

        let dotsAmount = fullString.reduce(0) { $1 == "," || $1 == "." ? $0 + 1 : $0 }

        if dotsAmount > 1 {
            return false
        }

        var parts: [Substring]?

        if fullString.contains(",") {
            parts = fullString.split(separator: ",")
        } else if fullString.contains(".") {
            parts = fullString.split(separator: ".")
        } else {
            parts = [Substring(fullString)]
        }

        if let parts = parts {
            if parts.count > 2 {
                return false
            }

            if let firstPart = parts.first, firstPart.count > 6 {
                return false
            }

            if let lastPart = parts.last, lastPart.count > 2, parts.count == 2 {
                return false
            }
        }

        let ruleCharacterSet = CharacterSet(charactersIn: "0123456789,.")
        let checkCharacterSet = CharacterSet(charactersIn: fullString)

        return ruleCharacterSet.isSuperset(of: checkCharacterSet)
    }
}
