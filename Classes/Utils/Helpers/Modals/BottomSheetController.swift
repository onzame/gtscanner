//
//  BottomSheetController.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import MaterialComponents
import RxSwift
import RxCocoa

class BottomSheetController: MDCBottomSheetController {

    // MARK: - Publishers

    fileprivate let dismissActionPublisher = PublishSubject<Void>()

    // MARK: - Gestures

    fileprivate lazy var swipeToDismissGesture = UISwipeGestureRecognizer(
        target: self,
        action: #selector(swipeDownEvent)
    ).then {
        $0.direction = .down
    }

    // MARK: - Lifecycle

    override init(contentViewController: UIViewController) {
        super.init(contentViewController: contentViewController)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        delegate = self
        dismissOnDraggingDownSheet = false
        contentViewController.view.isUserInteractionEnabled = true
        contentViewController.view.addGestureRecognizer(swipeToDismissGesture)
    }

    // MARK: - Actions

    @objc private func swipeDownEvent(event: UISwipeGestureRecognizer) {
        if KeyboardPresentationListener.shared.isPresented {
            contentViewController.view.endEditing(true)
        } else {
            dismiss(animated: true) { [weak self] in
                self?.dismissActionPublisher.onNext(())
            }
        }
    }
}

// MARK: - Extensions
// MARK: - MDCBottomSheetControllerDelegate

extension BottomSheetController: MDCBottomSheetControllerDelegate {
    func bottomSheetControllerDidDismissBottomSheet(_ controller: MDCBottomSheetController) {
        dismissActionPublisher.onNext(())
    }
}

// MARK: - Reactive interface

extension Reactive where Base: BottomSheetController {
    var dismissAction: ControlEvent<Void> {
        return ControlEvent(events: base.dismissActionPublisher)
    }
}
