//
//  LocationSessionController.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa

enum LocationSessionError: LocalizedError {
    case accessDenied
    case unknown

    var errorDescription: String? {
        return Localization.Location.noPermissionMessage
    }
}

class LocationSessionController: NSObject {

    static let shared = LocationSessionController()

    fileprivate let locationPublisher = BehaviorRelay<CLLocation?>(value: nil)

    let locationStatus = BehaviorRelay<CLAuthorizationStatus>(value: .notDetermined)

    fileprivate let locationManager = CLLocationManager()

    private override init() { }

    func prepareSession() -> Completable {
        return Completable.create { [weak self] (eventEmitter) -> Disposable in
            self?.locationManager.delegate = self
            self?.locationManager.requestWhenInUseAuthorization()
            eventEmitter(.completed)
            return Disposables.create()
        }
    }

    func startUpdatingLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }

    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
}

extension LocationSessionController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationStatus.accept(status)

        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            startUpdatingLocation()
        default:
            break
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationPublisher.accept(locations.last)
    }
}

extension Reactive where Base: LocationSessionController {
    var location: Observable<CLLocation?> {
        return base.locationPublisher.asObservable()
    }

    var status: Observable<CLAuthorizationStatus> {
        return base.locationStatus.asObservable()
    }

    var enableUpdating: Binder<Void> {
        return Binder(base, binding: { controller, _ in
            controller.startUpdatingLocation()
        })
    }

    var disableUpdating: Binder<Void> {
        return Binder(base, binding: { controller, _ in
            controller.stopUpdatingLocation()
        })
    }
}
