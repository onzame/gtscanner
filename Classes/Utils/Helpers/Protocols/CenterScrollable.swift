//
//  CenterScrollable.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

protocol CollectionViewCenterScrollable {
    var collectionView: UICollectionView { get }
}

extension CollectionViewCenterScrollable {
    func collectionViewWillScrollToVisibleCellCenter(completion: ((IndexPath) -> Void)? = nil) {
        if Device.isPad { return }

        let visibleRect = CGRect(origin: collectionView.contentOffset, size: collectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)

        if let indexPath = collectionView.indexPathForItem(at: visiblePoint) {
            collectionViewWillScrollCellCenter(at: indexPath)
            completion?(indexPath)
        }
    }

    func collectionViewWillScrollCellCenter(at indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}
