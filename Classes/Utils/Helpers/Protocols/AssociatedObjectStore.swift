//
//  AssociatedObjectStore.swift
//  Gorod
//
//  Created by Sergei Fabian on 27.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import ObjectiveC

protocol AssociatedObjectStore { }

extension AssociatedObjectStore {
    func associatedObject<T>(forKey key: UnsafeRawPointer) -> T? {
        return objc_getAssociatedObject(self, key) as? T
    }

    func associatedObject<T>(forKey key: UnsafeRawPointer, default defaultObjectFactory: @autoclosure () -> T) -> T {
        if let object: T = associatedObject(forKey: key) {
            return object
        }

        let object = defaultObjectFactory()
        setAssociatedObject(object, forKey: key)

        return object
    }

    func setAssociatedObject<T>(_ object: T?, forKey key: UnsafeRawPointer) {
        objc_setAssociatedObject(self, key, object, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}
