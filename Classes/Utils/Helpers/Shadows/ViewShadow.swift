//
//  ViewShadow.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

extension UIView {

    enum ShadowType {

        case lightBlue
        case longBlue
        case shortBlue
        case yellowGlow

        var shadowRadius: CGFloat {
            switch self {
            case .lightBlue, .shortBlue:
                return 8
            case .longBlue, .yellowGlow:
                return 10
            }
        }

        var shadowColor: CGColor? {
            switch self {
            case .lightBlue, .longBlue, .shortBlue:
                return UIColor(hex: "#2B6CC8")?.cgColor
            case .yellowGlow:
                return Style.Color.brightYellow?.cgColor
            }
        }

        var shadowOpacity: Float {
            switch self {
            case .lightBlue:
                return 0.08
            case .longBlue:
                return 0.1
            case .shortBlue:
                return 0.1
            case .yellowGlow:
                return 0.3
            }
        }

        var shadowOffset: CGSize {
            switch self {
            case .lightBlue:
                return CGSize(width: 0, height: 3)
            case .longBlue:
                return CGSize(width: -4, height: 6)
            case .yellowGlow:
                return CGSize(width: -2, height: 4)
            case .shortBlue:
                return CGSize(width: 0, height: 0)
            }
        }
    }

    func setupShadow(_ shadowType: ShadowType) {
        layer.shadowOpacity = shadowType.shadowOpacity
        layer.shadowRadius = shadowType.shadowRadius
        layer.shadowOffset = shadowType.shadowOffset
        layer.shadowColor = shadowType.shadowColor
    }

}
