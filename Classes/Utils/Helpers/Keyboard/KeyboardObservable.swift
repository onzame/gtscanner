//
//  KeyboardObservable.swift
//  Gorod
//
//  Created by Sergei Fabian on 09.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

protocol KeyboardObservable: class {
    func keyboardWillShow(withSize size: CGSize, duration: TimeInterval, options: UIView.AnimationOptions)
    func keyboardWillHide(duration: TimeInterval, options: UIView.AnimationOptions)
}

extension KeyboardObservable {
    func addKeyboardObservers(to notificationCenter: NotificationCenter) {
        let willShowNotificationName = UIResponder.keyboardWillShowNotification
        let willHideNotificationName = UIResponder.keyboardWillHideNotification

        // swiftlint:disable:next discarded_notification_center_observer
        notificationCenter.addObserver(forName: willShowNotificationName, object: nil, queue: nil) { [weak self] notification in
            let frameKey = UIResponder.keyboardFrameEndUserInfoKey
            let animationCurveKey = UIResponder.keyboardAnimationCurveUserInfoKey
            let animationDurationKey = UIResponder.keyboardAnimationDurationUserInfoKey

            guard let animationCurve = notification.userInfo?[animationCurveKey] as? NSNumber,
                let animationDuration = notification.userInfo?[animationDurationKey] as? NSNumber,
                let keyboardSizeValue = notification.userInfo?[frameKey] as? NSValue else { return }

            let keyboardSize = keyboardSizeValue.cgRectValue
            let curve = animationCurve.uintValue
            let duration = animationDuration.doubleValue

            self?.keyboardWillShow(withSize: keyboardSize.size, duration: duration, options: .init(rawValue: curve))
        }

        // swiftlint:disable:next discarded_notification_center_observer
        notificationCenter.addObserver(forName: willHideNotificationName, object: nil, queue: nil) { [weak self] notification in
            let animationCurveKey = UIResponder.keyboardAnimationCurveUserInfoKey
            let animationDurationKey = UIResponder.keyboardAnimationDurationUserInfoKey

            guard let animationCurve = notification.userInfo?[animationCurveKey] as? NSNumber,
                let animationDuration = notification.userInfo?[animationDurationKey] as? NSNumber else { return }

            let curve = animationCurve.uintValue
            let duration = animationDuration.doubleValue

            self?.keyboardWillHide(duration: duration, options: .init(rawValue: curve))
        }
    }

    func removeKeyboardObservers(from notificationCenter: NotificationCenter) {
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}
