//
//  ScrollViewKeyboardObservable.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

protocol ScrollViewKeyboardObservable: KeyboardObservable {
    var keyboardObservingScrollView: UIScrollView { get }
}

extension ScrollViewKeyboardObservable {
    func keyboardWillShow(withSize size: CGSize, duration: TimeInterval, options: UIView.AnimationOptions) {
        keyboardObservingScrollView.contentInset.bottom = size.height

        if #available(iOS 13.0, *) {
            keyboardObservingScrollView.verticalScrollIndicatorInsets.bottom = size.height
        } else {
            keyboardObservingScrollView.scrollIndicatorInsets.bottom = size.height
        }
    }

    func keyboardWillHide(duration: TimeInterval, options: UIView.AnimationOptions) {
        keyboardObservingScrollView.contentInset.bottom = 0

        if #available(iOS 13.0, *) {
            keyboardObservingScrollView.verticalScrollIndicatorInsets.bottom = 0
        } else {
            keyboardObservingScrollView.scrollIndicatorInsets.bottom = 0
        }
    }
}
