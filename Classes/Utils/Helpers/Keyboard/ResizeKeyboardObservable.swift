//
//  ResizeKeyboardObservable.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import SnapKit

protocol ResizeKeyboardObservable: KeyboardObservable {
    var constraint: Constraint? { get }
    var contentContainer: UIView { get }
}

extension ResizeKeyboardObservable {
    func keyboardWillShow(withSize size: CGSize, duration: TimeInterval, options: UIView.AnimationOptions) {
        constraint?.update(inset: size.height)

        UIView.animate(withDuration: duration, delay: duration, options: options, animations: {
            self.contentContainer.layoutIfNeeded()
        })
    }

    func keyboardWillHide(duration: TimeInterval, options: UIView.AnimationOptions) {
        constraint?.update(inset: 0)

        UIView.animate(withDuration: duration, delay: duration, options: options, animations: {
            self.contentContainer.layoutIfNeeded()
        })
    }
}
