//
//  KeyboardPresentationListener.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class KeyboardPresentationListener: KeyboardObservable {

    static let shared = KeyboardPresentationListener()

    private(set) var isPresented = false

    private init() { }

    deinit {
        removeKeyboardObservers(from: .default)
    }

    func setup() {
        addKeyboardObservers(to: .default)
    }

    func keyboardWillShow(withSize size: CGSize, duration: TimeInterval, options: UIView.AnimationOptions) {
        isPresented = true
    }

    func keyboardWillHide(duration: TimeInterval, options: UIView.AnimationOptions) {
        isPresented = false
    }

}
