//
//  LoadingStateView.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class LoadingStateView: UIView {

    // MARK: - UI components

    let progressActivityIndicatorView = UIActivityIndicatorView.build(type: .gray).then {
        $0.startAnimating()
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(progressActivityIndicatorView)
    }

    private func setupLayout() {
        progressActivityIndicatorView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
    }

    private func setupView() {
        setBackgroundColor(Color.backgroundColor)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension LoadingStateView {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.lightBlue
    }
}
