//
//  StateResolvable.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol StateResolvable {
    func buildDefaultStateView() -> UIView
    func buildLoadingStateView() -> UIView
    func buildErrorStateView(error: Error) -> UIView
    func buildNoDataStateView() -> UIView
}

protocol StateBindable {
    func showStateView(view: UIView)
}

extension StateResolvable {
    func buildLoadingStateView() -> UIView {
        return LoadingStateView()
    }

    func buildErrorStateView(error: Error) -> UIView {
        let error = error as NSError

        return ErrorStateView().then {
            $0.title = error.localizedFailureReason
            $0.subtitle = error.localizedDescription
        }
    }

    func buildNoDataStateView() -> UIView {
        return UIView()
    }
}

extension StateResolvable {
    func buildLocationErrorStateView() -> ErrorStateView {
        return ErrorStateView().then {
            $0.title = Localization.Location.noPermissionTitle
            $0.subtitle = Localization.Location.noPermissionSubtitle
            $0.actionTitle = Localization.Location.noPermissionActionTitle
        }
    }
}

extension UIViewController: StateBindable {
    @objc func showStateView(view: UIView) {
        self.view.removeAllSubviews()
        self.view.addSubview(view)
        view.addEdgesConstraints()
    }
}

extension Reactive where Base: UIViewController & StateResolvable {
    var defaultStateBinding: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.showStateView(view: viewController.buildDefaultStateView())
        })
    }

    var loadingStateBinding: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.showStateView(view: viewController.buildLoadingStateView())
        })
    }

    var errorStateBinding: Binder<Error> {
        return Binder(base, binding: { viewController, error in
            viewController.showStateView(view: viewController.buildErrorStateView(error: error))
        })
    }

    var noDataStateBinding: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.showStateView(view: viewController.buildNoDataStateView())
        })
    }
}
