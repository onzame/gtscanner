//
//  ErrorStateView.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

class ErrorStateView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let iconView = ImageContainerView().then {
        $0.isHidden = true
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.numberOfLines = 0
        $0.titleLabel.textAlignment = .center
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.titleLabel.isHidden = true
        $0.subtitleLabel.numberOfLines = 0
        $0.subtitleLabel.textAlignment = .center
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleTextColor
        $0.subtitleLabel.isHidden = true
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let actionButton = UIButton(type: .system).then {
        $0.setHorizontalInsets(Metric.actionButtonHorizontalInsets)
        $0.setTitleColor(Color.actionButtonTitleColor, for: .normal)
        $0.setupShadow(.yellowGlow)
        $0.backgroundColor = Color.actionButtonBackgroundColor
        $0.layer.cornerRadius = CornerRadius.actionButtonCornerRadius
        $0.isHidden = true
    }

    // MARK: - Accessors

    var image: UIImage? {
        didSet {
            iconView.imageView.image = image
            iconView.isHidden = image == nil
        }
    }

    var title: String? {
        didSet {
            descriptionStackView.titleLabel.text = title
            descriptionStackView.titleLabel.isHidden = title == nil
        }
    }

    var subtitle: String? {
        didSet {
            descriptionStackView.subtitleLabel.text = subtitle
            descriptionStackView.subtitleLabel.isHidden = subtitle == nil
        }
    }

    var actionTitle: String? {
        didSet {
            actionButton.setTitle(actionTitle, for: .normal)
            actionButton.isHidden = actionTitle == nil
        }
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(iconView)
        addSubview(descriptionStackView)
        addSubview(actionButton)
    }

    private func setupLayout() {
        iconView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Metric.iconContainerViewTopOffset)
            make.width.height.equalTo(Metric.iconContainerViewPartSize)
            make.centerX.equalToSuperview()
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.top.equalTo(iconView.snp.bottom).offset(Metric.descriptionStackViewTopOffset)
            make.left.greaterThanOrEqualToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.descriptionStackViewRightOffset)
            make.centerX.equalToSuperview()
        }

        actionButton.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(Metric.actionButtonBottomOffset)
            make.height.equalTo(Metric.actionButtonHeight)
            make.centerX.equalToSuperview()
        }
    }

    private func setupView() {
        setBackgroundColor(Color.backgroundColor)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ErrorStateView {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.lightBlue
        static let iconContainerViewBackground = Style.Color.brightYellow
        static let actionButtonBackgroundColor = Style.Color.brightYellow
        static let actionButtonTitleColor = Style.Color.black
        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitleTextColor = Style.Color.newGray
    }

    fileprivate enum CornerRadius {
        static let actionButtonCornerRadius: CGFloat = 24
        static let iconContainerViewCornerRadius: CGFloat = 37
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.medium, size: 16)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Metric {
        static let iconContainerViewTopOffset: CGFloat = 128
        static let iconContainerViewPartSize: CGFloat = 74

        static let descriptionStackViewTopOffset: CGFloat = 24
        static let descriptionStackViewSpacing: CGFloat = 12
        static let descriptionStackViewLeftOffset: CGFloat = 48
        static let descriptionStackViewRightOffset: CGFloat = -48

        static let actionButtonLeftOffset: CGFloat = 48
        static let actionButtonRightOffset: CGFloat = -48
        static let actionButtonBottomOffset: CGFloat = -56
        static let actionButtonHeight: CGFloat = 48
        static let actionButtonHorizontalInsets: CGFloat = 24
    }
}
