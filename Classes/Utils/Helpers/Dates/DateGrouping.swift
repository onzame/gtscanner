//
//  DateGrouping.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

protocol Dated {
    var date: Date { get }
}

extension Array where Element: Dated {
    func group(by dateComponents: Set<Calendar.Component>) -> [Date: [Element]] {
        return reduce(into: [Date: [Element]]()) { (state, element) in
            if let date = Calendar.current.date(from: Calendar.current.dateComponents(dateComponents, from: element.date)) {
                let currentArrayByDate = state[date] ?? []
                state.updateValue(currentArrayByDate + [element], forKey: date)
            }
        }
    }
}
