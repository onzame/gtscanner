//
//  RxTableView.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

extension Reactive where Base: UITableView {
    func itemSelected<S>(dataSource: TableViewSectionedDataSource<S>) -> ControlEvent<S.Item> {
        let source = itemSelected.map { indexPath in dataSource[indexPath] }
        return ControlEvent(events: source)
    }
}
