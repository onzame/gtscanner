//
//  RxView.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension Reactive where Base: UIView {
    var removeFromSuperview: ControlEvent<Void> {
        let source = methodInvoked(#selector(Base.removeFromSuperview)).mapToVoid()
        return ControlEvent(events: source)
    }

    var didMoveToSuperview: ControlEvent<Void> {
        let source = methodInvoked(#selector(Base.didMoveToSuperview)).mapToVoid()
        return ControlEvent(events: source)
    }
}
