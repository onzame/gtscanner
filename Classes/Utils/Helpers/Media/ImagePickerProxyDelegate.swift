//
//  ImagePickerProxyDelegate.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum ImagePickerResult {
    case selected(UIImage)
    case canceled
}

class ImagePickerProxyDelegate: NSObject {
    let resultPublisher = PublishSubject<ImagePickerResult>()
}

extension ImagePickerProxyDelegate: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: {
            if let image = info[.editedImage] as? UIImage, let preparedImage = image.resize(targetSize: ProductPhoto.sendSize) {
                self.resultPublisher.onNext(.selected(preparedImage))
            } else {
                self.resultPublisher.onNext(.canceled)
            }

            self.resultPublisher.onCompleted()
        })
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            self.resultPublisher.onNext(.canceled)
        }
    }
}
