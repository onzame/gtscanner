//
//  VideoCaptureSessionController.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift
import RxCocoa

enum VideoCaptureSessionError: Error {
    case accessDenied
    case inputConnectionDenied(details: Error?)
    case outputConnectionDenied
    case unknown
}

class VideoCaptureSessionController: NSObject {

    fileprivate static let metadataObjectTypes: [AVMetadataObject.ObjectType] = [.ean8, .ean13, .itf14]

    let disposeBag = DisposeBag()

    // MARK: - Properties

    fileprivate var scanResultPublisher = PublishSubject<ScannedObject>()

    fileprivate var readySignalPublisher = PublishSubject<Void>()

    fileprivate var errorSignalPublisher = PublishSubject<Error>()

    let captureSession = AVCaptureSession()

    // MARK: - Public methods

    func prepareSession() -> Completable {
        return requestPermission()
            .andThen(setupSession(captureSession, delegate: self))
    }

    // MARK: - Private methods

    private func requestPermission() -> Completable {
        return Completable.create { (eventEmitter) -> Disposable in

            switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
            case .authorized:
                eventEmitter(CompletableEvent.completed)
            case .denied:
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.accessDenied))
            case .restricted:
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.unknown))
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { (isSuccess) in
                    if isSuccess {
                        eventEmitter(CompletableEvent.completed)
                    } else {
                        eventEmitter(CompletableEvent.error(VideoCaptureSessionError.accessDenied))
                    }
                }
            @unknown default:
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.unknown))
            }

            return Disposables.create()
        }
    }

    private func setupSession(_ session: AVCaptureSession, delegate: AVCaptureMetadataOutputObjectsDelegate) -> Completable {
        return Completable.create { (eventEmitter) -> Disposable in

            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else {
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.unknown))
                return Disposables.create()
            }

            let videoInput: AVCaptureDeviceInput

            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.inputConnectionDenied(details: error)))
                return Disposables.create()
            }

            guard session.canAddInput(videoInput) else {
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.inputConnectionDenied(details: nil)))
                return Disposables.create()
            }

            session.addInput(videoInput)

            let metadataOutput = AVCaptureMetadataOutput()

            guard session.canAddOutput(metadataOutput) else {
                eventEmitter(CompletableEvent.error(VideoCaptureSessionError.outputConnectionDenied))
                return Disposables.create()
            }

            session.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(delegate, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = VideoCaptureSessionController.metadataObjectTypes

            eventEmitter(CompletableEvent.completed)

            return Disposables.create()
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension VideoCaptureSessionController {
    fileprivate enum Duration {
        static let scanDelay = RxTimeInterval.seconds(2)
    }
}

// MARK: - AVCaptureMetadataOutputObjectsDelegate

extension VideoCaptureSessionController: AVCaptureMetadataOutputObjectsDelegate {
    func metadataOutput(
        _ output: AVCaptureMetadataOutput,
        didOutput metadataObjects: [AVMetadataObject],
        from connection: AVCaptureConnection
    ) {
        guard
            let readableObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject,
            let stringValue = readableObject.stringValue
            else { return }

        scanResultPublisher.onNext(ScannedObject(value: stringValue))
    }
}

// MARK: - Reactive Interface

extension Reactive where Base: VideoCaptureSessionController {
    var runSession: Binder<Bool> {
        return Binder(base, binding: { view, shouldRun in
            shouldRun ? view.captureSession.startRunning() : view.captureSession.stopRunning()
        })
    }

    var scannedObject: ControlEvent<ScannedObject> {
        let source = base.scanResultPublisher.throttle(Base.Duration.scanDelay, scheduler: MainScheduler.instance)
        return ControlEvent(events: source)
    }
}
