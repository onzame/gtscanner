//
//  DomainConvertibleType.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

protocol DomainConvertibleType {

    associatedtype DomainModelType

    func converteToDomain() -> DomainModelType

}
