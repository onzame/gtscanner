//
//  QueryableType.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

protocol QueryableType {

    associatedtype IdentifierType: Hashable

    static var identifierKey: String { get }

    var identifier: IdentifierType { get }

}

extension QueryableType {
    static var identifierKey: String {
        return "id"
    }
}
