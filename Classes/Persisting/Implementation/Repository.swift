//
//  Repository.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

protocol RepositoryType: class, ReactiveCompatible {

    associatedtype EntityType: QueryableType

    associatedtype DataSourceType: StorageType

    var dataSource: DataSourceType { get }

    init(dataSource: DataSourceType)

    func loadAll() throws -> [EntityType]

    func loadBy(identifier: EntityType.IdentifierType) throws -> EntityType

    func save(entity: EntityType) throws

    func save(entities: [EntityType]) throws

    func delete(entity: EntityType) throws

    func deleteBy(identifier: EntityType.IdentifierType) throws

    func deleteAll() throws

    func checkIsExistsBy(identifier: EntityType.IdentifierType) throws -> Bool

}

final class Repository<DataSourceType: StorageType>: RepositoryType {

    typealias EntityType = DataSourceType.EntityType

    let dataSource: DataSourceType

    required init(dataSource: DataSourceType) {
        self.dataSource = dataSource
    }

    func loadAll() throws -> [EntityType] {
        return try dataSource.loadAll()
    }

    @discardableResult
    func loadBy(identifier: EntityType.IdentifierType) throws -> EntityType {
        return try dataSource.loadBy(identifier: identifier)
    }

    func save(entity: EntityType) throws {
        try dataSource.save(entity: entity)
    }

    func save(entities: [EntityType]) throws {
        try dataSource.save(entities: entities)
    }

    func delete(entity: EntityType) throws {
        try dataSource.delete(entity: entity)
    }

    func deleteBy(identifier: EntityType.IdentifierType) throws {
        try dataSource.deleteBy(identifier: identifier)
    }

    func deleteAll() throws {
        try dataSource.deleteAll()
    }

    func checkIsExistsBy(identifier: EntityType.IdentifierType) throws -> Bool {
        return try dataSource.checkIsExistsBy(identifier: identifier)
    }

}

extension Reactive where Base: RepositoryType, Base.EntityType == Base.DataSourceType.EntityType {
    func observe() -> Observable<[Base.EntityType]> {
        return base.dataSource.rx.observe()
    }

    func loadAll() -> Single<[Base.EntityType]> {
        return base.dataSource.rx.loadAll()
    }

    func loadBy(identifier: Base.EntityType.IdentifierType) -> Single<Base.EntityType> {
        return base.dataSource.rx.loadBy(identifier: identifier)
    }

    func save(entity: Base.EntityType) -> Completable {
        return base.dataSource.rx.save(entity: entity)
    }

    func save(entities: [Base.EntityType]) -> Completable {
        return base.dataSource.rx.save(entities: entities)
    }

    func delete(entity: Base.EntityType) -> Completable {
        return base.dataSource.rx.delete(entity: entity)
    }

    func deleteBy(identifier: Base.EntityType.IdentifierType) -> Completable {
        return base.dataSource.rx.deleteBy(identifier: identifier)
    }

    func deleteAll() -> Completable {
        return base.dataSource.rx.deleteAll()
    }

    func checkIsExistsBy(identifier: Base.EntityType.IdentifierType) -> Single<Bool> {
        return base.dataSource.rx.checkIsExistsBy(identifier: identifier)
    }
}
