//
//  MemoryStorage.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

final class MemoryStorage<EntityType>: StorageType where EntityType: QueryableType {

    private let changesPublisher = PublishSubject<[EntityType]>()

    private var entities = [EntityType]() {
        didSet {
            changesPublisher.onNext(entities)
        }
    }

    func observe() -> Observable<[EntityType]> {
        return changesPublisher
            .asObservable()
            .startWith(entities)
    }

    func loadAll() throws -> [EntityType] {
        return entities
    }

    func loadBy(identifier: EntityType.IdentifierType) throws -> EntityType {
        if let entity = entities.first(where: { $0.identifier == identifier }) {
            return entity
        } else {
            throw StorageError.notFound
        }
    }

    func save(entity: EntityType) throws {
        if entities.contains(where: { $0.identifier == entity.identifier }) {
            entities = entities.map({
                if $0.identifier == entity.identifier {
                    return entity
                } else {
                    return $0
                }
            })
        } else {
            entities = entities.filter({ $0.identifier != entity.identifier }) + [entity]
        }
    }

    func save(entities: [EntityType]) throws {
        let filteredSavedEntities = self.entities.filter({ savedEntity -> Bool in
            return !entities.contains(where: { return $0.identifier == savedEntity.identifier })
        })

        self.entities = filteredSavedEntities + entities
    }

    func delete(entity: EntityType) throws {
        entities = entities.filter({ $0.identifier != entity.identifier })
    }

    func deleteBy(identifier: EntityType.IdentifierType) throws {
        entities = entities.filter({ $0.identifier != identifier })
    }

    func deleteAll() throws {
        entities = []
    }

    func checkIsExistsBy(identifier: EntityType.IdentifierType) throws -> Bool {
        return entities.contains(where: { $0.identifier == identifier })
    }

}
