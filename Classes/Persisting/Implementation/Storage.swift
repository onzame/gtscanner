//
//  Storage.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

enum StorageError: Error {
    case lifecycle
    case notFound
}

protocol StorageType: class, ReactiveCompatible {

    associatedtype EntityType: QueryableType

    func observe() -> Observable<[EntityType]>

    func loadAll() throws -> [EntityType]

    func loadBy(identifier: EntityType.IdentifierType) throws -> EntityType

    func save(entity: EntityType) throws

    func save(entities: [EntityType]) throws

    func delete(entity: EntityType) throws

    func deleteBy(identifier: EntityType.IdentifierType) throws

    func deleteAll() throws

    func checkIsExistsBy(identifier: EntityType.IdentifierType) throws -> Bool

}

// MARK: - Reactive interface

extension Reactive where Base: StorageType {
    func observe() -> Observable<[Base.EntityType]> {
        return base.observe()
    }

    func loadAll() -> Single<[Base.EntityType]> {
        return Single.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let entities = try base.loadAll()
                eventEmitter(.success(entities))
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func loadBy(identifier: Base.EntityType.IdentifierType) -> Single<Base.EntityType> {
        return Single.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let entity = try base.loadBy(identifier: identifier)
                eventEmitter(.success(entity))
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func save(entity: Base.EntityType) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.completed)
                return Disposables.create()
            }

            do {
                try base.save(entity: entity)
                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func save(entities: [Base.EntityType]) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.completed)
                return Disposables.create()
            }

            do {
                try base.save(entities: entities)
                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func delete(entity: Base.EntityType) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.completed)
                return Disposables.create()
            }

            do {
                try base.delete(entity: entity)
                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func deleteBy(identifier: Base.EntityType.IdentifierType) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.completed)
                return Disposables.create()
            }

            do {
                try base.deleteBy(identifier: identifier)
                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func deleteAll() -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.completed)
                return Disposables.create()
            }

            do {
                try base.deleteAll()
                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func checkIsExistsBy(identifier: Base.EntityType.IdentifierType) -> Single<Bool> {
        return Single.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let isExists = try base.checkIsExistsBy(identifier: identifier)
                eventEmitter(.success(isExists))
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }
}
