//
//  CompareProductsRepositoryType.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

typealias CompareProductsStorageType = MemoryStorage<CompareProductImage>
typealias CompareProductsRepositoryType = Repository<CompareProductsStorageType>
