//
//  PricesRepositoryType.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

typealias PricesStorageType = MemoryStorage<Price>
typealias PricesRepositoryType = Repository<PricesStorageType>

extension Reactive where Base: PricesRepositoryType {
    func saveIgnoringDuplicates(price: Price) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let entities = try base.loadAll()

                let containsDublicateClosure: (Price, Price) -> Bool = { lhs, rhs -> Bool in
                    lhs.id == rhs.id && lhs.branch.id == rhs.branch.id
                }

                if entities.contains(where: { containsDublicateClosure(price, $0) }) {
                    let updatesEntities = entities.map({
                        return containsDublicateClosure(price, $0) ? price : $0
                    })

                    try base.save(entities: updatesEntities)
                } else {
                    try base.save(entity: price)
                }

                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }
}
