//
//  ScannedProductsRepository.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

typealias ScannedProductsStorageType = MemoryStorage<ScannedProduct>
typealias ScannedProductsRepositoryType = Repository<ScannedProductsStorageType>

extension Reactive where Base: ScannedProductsRepositoryType {
    func checkIfExistsBy(gtin: String) -> Single<Bool> {
        return loadAll().map({ $0.contains(gtin: gtin) })
    }

    func updateProductIfNeed(product: Product) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                try base.loadBy(identifier: product.gtin)
                try base.save(entity: .founded(product))
                eventEmitter(.completed)
            } catch StorageError.notFound {
                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func updateCompareProduct(productId: Int, inCompare: Bool) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let products = try base.loadAll()

                let updatedProducts = products.map({ product -> ScannedProduct in
                    if case ScannedProduct.founded(let foundedProduct) = product {
                        return .founded(foundedProduct.update(inCompare: inCompare))
                    } else {
                        return product
                    }
                })

                try base.deleteAll()
                try base.save(entities: updatedProducts)

                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }
}
