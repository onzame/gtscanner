//
//  HistoryProductsRepositoryType.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

typealias HistoryProductsStorageType = MemoryStorage<HistoryProductContainer>
typealias HistoryProductsRepositoryType = Repository<HistoryProductsStorageType>
