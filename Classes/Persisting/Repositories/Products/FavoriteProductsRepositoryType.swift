//
//  FavoriteProductsRepositoryType.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

typealias FavoriteProductsStorageType = MemoryStorage<FavoriteProductsAggregation>
typealias FavoriteProductsRepositoryType = Repository<FavoriteProductsStorageType>

extension Reactive where Base: FavoriteProductsRepositoryType {
    func deleteByProduct(_ product: Product) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let entities = try base.loadAll()

                if let aggregation = entities.filter({ $0.products.map({ $0.product }).contains(gtin: product.gtin) }).first {
                    let products = aggregation.products.filter({ $0.product.id != product.id })

                    if products.isNotEmpty {
                        try base.save(entity: FavoriteProductsAggregation(category: aggregation.category, products: products))
                    } else {
                        try base.delete(entity: aggregation)
                    }

                    eventEmitter(.completed)
                } else {
                    eventEmitter(.completed)
                }

            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func updateProductIfNeed(product: Product) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let aggregations = try base.loadAll()

                let updatedAggregations = aggregations.map({ aggreagation -> FavoriteProductsAggregation in
                    return FavoriteProductsAggregation(
                        category: aggreagation.category,
                        products: aggreagation.products.map({ aggreagationProduct -> FavoriteProduct in
                            if aggreagationProduct.product.id == product.id {
                                return FavoriteProduct(id: aggreagationProduct.id, count: aggreagationProduct.count, product: product)
                            } else {
                                return aggreagationProduct
                            }
                        })
                    )
                })

                try base.deleteAll()
                try base.save(entities: updatedAggregations)

                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

    func updateCompareProduct(productId: Int, inCompare: Bool) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let aggregations = try base.loadAll()

                let updatedAggregations = aggregations.map({ aggreagation -> FavoriteProductsAggregation in
                    return FavoriteProductsAggregation(
                        category: aggreagation.category,
                        products: aggreagation.products.map({ aggreagationProduct -> FavoriteProduct in
                            if aggreagationProduct.product.id == productId {
                                return FavoriteProduct(
                                    id: aggreagationProduct.id,
                                    count: aggreagationProduct.count,
                                    product: aggreagationProduct.product.update(inCompare: inCompare)
                                )
                            } else {
                                return aggreagationProduct
                            }
                        })
                    )
                })

                try base.deleteAll()
                try base.save(entities: updatedAggregations)

                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }

}
