//
//  ReviewsRepositoryType.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

typealias ReviewsStorageType = MemoryStorage<Review>
typealias ReviewsRepositoryType = Repository<ReviewsStorageType>

extension Reactive where Base: ReviewsRepositoryType {
    func saveIgnoringDuplicates(review: Review) -> Completable {
        return Completable.create { [weak base] (eventEmitter) -> Disposable in
            guard let base = base else {
                eventEmitter(.error(StorageError.lifecycle))
                return Disposables.create()
            }

            do {
                let entities = try base.loadAll()

                let containsDublicateClosure: (Review, Review) -> Bool = { lhs, rhs -> Bool in
                    lhs.productId == rhs.productId
                }

                if entities.contains(where: { containsDublicateClosure(review, $0) }) {
                    let updatesEntities = entities.map({
                        return containsDublicateClosure(review, $0) ? review : $0
                    })

                    try base.save(entities: updatesEntities)
                } else {
                    try base.save(entity: review)
                }

                eventEmitter(.completed)
            } catch {
                eventEmitter(.error(error))
            }

            return Disposables.create()
        }
    }
}
