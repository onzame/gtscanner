//
//  RequestDataResponse.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct RequestDataResponse<T: Codable>: Codable {
    let data: T
    let nextPage: Int?
}

extension RequestDataResponse {
    var pagination: Pagination {
        return Pagination(nextPage: nextPage)
    }
}
