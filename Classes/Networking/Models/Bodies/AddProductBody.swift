//
//  AddProductBody.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

struct AddProductBody: Encodable {
    let name: String
    let gtin: String
    let price: Double
    let rating: Double
    let categoryId: Int
    let shopBranchId: Int
    let images: [Data]

    init(parameters: AddProductParameters) {
        self.name = parameters.name
        self.gtin = parameters.gtin
        self.price = parameters.price
        self.rating = parameters.rating
        self.categoryId = parameters.category.id
        self.shopBranchId = parameters.shop.branch.id
        self.images = parameters.photos.mapToData()
    }

    func mapToMultipartFormData() -> [MultipartFormData] {
        var result: [MultipartFormData] = []

        result.append(.init(provider: .data(name.dataOrEmpty), name: "name"))
        result.append(.init(provider: .data(gtin.dataOrEmpty), name: "gtin"))
        result.append(.init(provider: .data(price.string.dataOrEmpty), name: "price"))
        result.append(.init(provider: .data(rating.string.dataOrEmpty), name: "rating"))
        result.append(.init(provider: .data(categoryId.string.dataOrEmpty), name: "category_id"))
        result.append(.init(provider: .data(shopBranchId.string.dataOrEmpty), name: "shop_branch_id"))

        images.enumerated().forEach { (index, value) in
            result.append(.init(provider: .data(value), name: "images[\(index)]", fileName: "image_\(index).jpg", mimeType: "image/jpeg"))
        }

        return result
    }

}
