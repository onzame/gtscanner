//
//  AddPriceBody.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct AddPriceBody: Encodable {
    let price: Double
    let shopBranchId: Int
}
