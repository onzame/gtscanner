//
//  AddReviewBody.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct AddReviewBody: Encodable {
    let rating: Double
    let shopBranchId: Int
    let positiveText: String?
    let negativeText: String?
    let text: String?

    init(rating: Double, shopBranchId: Int, reviewText: ReviewText) {
        self.rating = rating
        self.shopBranchId = shopBranchId
        self.positiveText = reviewText.positiveText
        self.negativeText = reviewText.negativeText
        self.text = reviewText.text
    }
}
