//
// Created by Sergei Fabian on 27.02.2020.
// Copyright (c) 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct UpdateFavoriteProductBody: Encodable {
    let count: Int
}
