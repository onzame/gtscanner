//
//  NetworkErrorWrapper.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

enum NetworkErrorWrapper: Error, LocalizedError {
    case clientUncommented(code: Int, response: String)
    case clientCommented(code: Int, response: RequestErrorResponse)
    case server(code: Int)
    case unknown(code: Int)

    var errorDescription: String? {
        switch self {
        case .clientCommented(_, response: let response):
            return response.error.description
        case .clientUncommented(_, response: let response):
            return response
        case .server:
            return Localization.Network.backendErrorDescription
        case .unknown:
            return Localization.Network.unknownErrorDescription
        }
    }

    var statusCode: Int {
        switch self {
        case .clientUncommented(code: let code, _):
            return code
        case .clientCommented(code: let code, _):
            return code
        case .server(code: let code):
            return code
        case .unknown(code: let code):
            return code
        }
    }

    init(response: Response) throws {
        do {
            self.init(statusCode: response.statusCode, response: try response.map(RequestErrorResponse.self, using: .snakeDecoder))
        } catch {
            do {
                self.init(statusCode: response.statusCode, response: try response.mapString())
            } catch {
                throw error
            }
        }
    }

    init(statusCode: Int, response: RequestErrorResponse) {
        self = .clientCommented(code: statusCode, response: response)
    }

    init(statusCode: Int, response: String) {
        self = .clientUncommented(code: statusCode, response: response)
    }
}

enum NetworkClientStatusCode: Int {
    case badRequest = 400
    case notFound = 404
}
