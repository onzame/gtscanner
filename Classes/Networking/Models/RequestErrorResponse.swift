//
//  RequestErrorResponse.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation

struct RequestErrorResponse: Codable {
    let error: RequestErrorDescription
}

struct RequestErrorDescription: Codable {
    let title: String?
    let description: String
    let displayingType: ErrorDisplayingType?
}

enum ErrorDisplayingType: String, Codable {
    case snackbar
    case dialog
}
