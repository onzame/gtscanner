//
//  MoyaProvider+Factory.swift
//  Alamofire
//
//  Created by Sergei Fabian on 27.03.2020.
//

import Moya

extension MoyaProvider {
    static func buildDefault(plugins: [PluginType]) -> MoyaProvider {
        return MoyaProvider(plugins: plugins)
    }
}
