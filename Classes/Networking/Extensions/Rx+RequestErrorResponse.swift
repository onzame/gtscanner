//
//  Rx+RequestErrorResponse.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import Moya

extension PrimitiveSequence where Trait == SingleTrait, Element == Response {
    func catchBackendError() -> Single<Element> {
        return flatMap({ response in
            switch response.statusCode {
            case (200...299):
                return .just(response)
            case 401:
                return Single<Element>.create { (eventEmitter) -> Disposable in
                    InvalidAccessTokenInterceptor.shared.resolveInvalidAccessToken { (error) in
                        if let error = error {
                            log.info(error)
                        }

                        do {
                            eventEmitter(.error(try NetworkErrorWrapper(response: response)))
                        } catch {
                            eventEmitter(.error(error))
                        }
                    }

                    return Disposables.create()
                }
            case (400...499):
                throw try NetworkErrorWrapper(response: response)
            case (500...):
                throw NetworkErrorWrapper.server(code: response.statusCode)
            default:
                throw NetworkErrorWrapper.unknown(code: response.statusCode)
            }
        })
    }
}
