//
//  DeleteReviewResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct DeleteReviewResult: Codable {
    let productUpdates: ProductUpdates
}
