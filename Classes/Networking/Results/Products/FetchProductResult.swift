//
//  FetchProductResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 09.04.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct FetchProductResult: Codable {
    let product: Product
}
