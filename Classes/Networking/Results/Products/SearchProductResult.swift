//
//  SearchProductResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct SearchProductResult: Codable {
    let product: Product
    let compareImages: [CompareProductImage]
}
