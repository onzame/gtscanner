//
//  DeletePriceResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct DeletePriceResult: Codable {
    let productUpdates: ProductUpdates
}
