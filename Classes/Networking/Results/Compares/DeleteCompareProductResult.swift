//
//  DeleteCompareProductResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct DeleteCompareProductResult: Codable {
    let compareUpdates: CompareUpdates
    let compareProductUpdates: CompareProductUpdates
    let composition: CompareComposition?
    let compareImages: [CompareProductImage]
}
