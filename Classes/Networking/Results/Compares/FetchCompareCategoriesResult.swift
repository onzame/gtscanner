//
//  FetchCompareCategoriesResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 05.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct FetchCompareCategoriesResult: Codable {
    let categories: [CompareCategory]
    let compareImages: [CompareProductImage]
}
