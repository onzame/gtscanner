//
//  AddCompareProductResult.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct AddCompareProductResult: Codable {
    let compareUpdates: CompareUpdates
    let compareProductUpdates: CompareProductUpdates
    let composition: CompareProductDetails?
    let compareImages: [CompareProductImage]
}
