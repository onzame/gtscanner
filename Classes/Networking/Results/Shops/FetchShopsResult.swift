//
//  FetchShopsResult.swift
//  Alamofire
//
//  Created by Sergei Fabian on 06.04.2020.
//

import Foundation

struct FetchShopsResult {
    let pagination: Pagination
    let shops: [Shop]

    init(response: RequestDataResponse<[Shop]>) {
        self.pagination = response.pagination
        self.shops = response.data
    }
}
