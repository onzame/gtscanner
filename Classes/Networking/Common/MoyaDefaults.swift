//
//  MoyaDefaults.swift
//  Alamofire
//
//  Created by Sergei Fabian on 16.03.2020.
//

import Foundation
import Moya

extension TargetType {
    var baseURL: URL {
        return Environment.apiUrl
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return nil
    }
}
