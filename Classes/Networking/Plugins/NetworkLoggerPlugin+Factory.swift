//
//  NetworkLoggerPlugin+Factory.swift
//  Gorod
//
//  Created by Sergei Fabian on 07.04.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

extension NetworkLoggerPlugin {
    static func buildDefault() -> NetworkLoggerPlugin {
        return NetworkLoggerPlugin(requestDataFormatter: logRequest, responseDataFormatter: logResponse)
    }

    static func logRequest(data: Data) -> String {
        let request = String(data: data, encoding: .utf8).orPlaceholder("No request data")
        return "[Moya_Logger: Request: \(request)]"
    }

    static func logResponse(data: Data) -> Data {
        let response = String(data: data, encoding: .utf8).orPlaceholder("No response data")
        log.info("[Moya_Logger: Response: \(response)]")
        return data
    }
}
