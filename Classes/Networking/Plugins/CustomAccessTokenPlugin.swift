//
//  CustomAccessTokenPlugin.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

class CustomAccessTokenPlugin: PluginType {
    private let accessTokenManager: AccessTokenManagerType

    init(accessTokenManager: AccessTokenManagerType) {
        self.accessTokenManager = accessTokenManager
    }

    func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        var request = request
        
        if let authorizableTarget = target as? CustomAccessTokenAuthorizable, authorizableTarget.authorizationRequired {
            let accessToken = accessTokenManager.resolveAccessToken()
            request.addValue(accessToken.token, forHTTPHeaderField: "Authorization")
        }

        return request
    }
}
