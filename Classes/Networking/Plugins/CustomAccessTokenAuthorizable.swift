//
//  CustomAccessTokenAuthorizable.swift
//  Alamofire
//
//  Created by Sergei Fabian on 20.03.2020.
//

import Foundation

protocol CustomAccessTokenAuthorizable {
    var authorizationRequired: Bool { get }
}

extension CustomAccessTokenAuthorizable {
    var authorizationRequired: Bool {
        return true
    }
}
