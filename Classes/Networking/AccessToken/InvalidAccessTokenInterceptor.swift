//
//  InvalidAccessTokenInterceptor.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

class InvalidAccessTokenInterceptor {

    static let shared = InvalidAccessTokenInterceptor()

    private var accessTokenManager: AccessTokenManagerType!

    private init() { }

    func setup(accessTokenManager: AccessTokenManagerType) {
        self.accessTokenManager = accessTokenManager
    }

    func resolveInvalidAccessToken(completion: @escaping (Error?) -> Void) {
        self.accessTokenManager.resolveInvalidAccessToken(completion: completion)
    }

}
