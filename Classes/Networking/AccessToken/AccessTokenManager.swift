//
//  AccessTokenManager.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

protocol AccessTokenManagerType {
    func resolveAccessToken() -> AccessTokenType
    func resolveInvalidAccessToken(completion: @escaping (Error?) -> Void)
}

class AccessTokenManager: AccessTokenManagerType {

    private let accessTokenProvider: AccessTokenProviderType
    private var accessToken: AccessTokenType?

    init(accessTokenProvider: AccessTokenProviderType) {
        self.accessTokenProvider = accessTokenProvider
    }

    func resolveAccessToken() -> AccessTokenType {
        if let accessToken = accessToken {
            return accessToken
        }

        let semaphore = DispatchSemaphore(value: 0)

        var resolvedAccessToken: AccessTokenType!

        accessTokenProvider.resolveAccessToken { [weak self] (accessToken) in
            self?.accessToken = accessToken
            resolvedAccessToken = accessToken
            semaphore.signal()
        }

        let _ = semaphore.wait(timeout: .distantFuture)

        return resolvedAccessToken
    }

    func resolveInvalidAccessToken(completion: @escaping (Error?) -> Void) {
        accessTokenProvider.resolveInvalidAccessToken { [weak self] (result) in
            switch result {
            case .success(let accessToken):
                self?.accessToken = accessToken
                completion(nil)
            case .failure(let error):
                completion(error)
            }
        }
    }
}
