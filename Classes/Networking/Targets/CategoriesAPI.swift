//
//  CategoriesAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias CategoriesProviderType = MoyaProvider<CategoriesAPI>

enum CategoriesAPI {
    case fetchCategories
}

extension CategoriesAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        return "/categories"
    }

    var method: Moya.Method {
        switch self {
        case .fetchCategories:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .fetchCategories:
            return .requestPlain
        }
    }
}
