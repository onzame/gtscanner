//
//  ProductsAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias ProductsProviderType = MoyaProvider<ProductsAPI>

enum ProductsAPI {
    case fetchProduct(productId: Int, location: Location?)
    case searchProduct(gtin: String, location: Location?)
    case fetchProductDetails(productId: Int)
    case addProduct(parameters: AddProductParameters, location: Location?)
}

extension ProductsAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchProduct(productId: let productId, _):
            return "/products/\(productId)"
        case .searchProduct:
            return "/products"
        case .fetchProductDetails(productId: let id):
            return "/products/\(id)/details"
        case .addProduct:
            return "/products"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchProduct:
            return .get
        case .searchProduct:
            return .get
        case .fetchProductDetails:
            return .get
        case .addProduct:
            return .post
        }
    }

    var task: Task {
        switch self {
        case .fetchProduct(_, location: let location):
            if let location = location {
                var parameters: [String: Any] = [:]
                parameters.updateValue(location.longitude, forKey: "lon")
                parameters.updateValue(location.latitude, forKey: "lat")
                return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
            } else {
                return .requestPlain
            }

        case .searchProduct(gtin: let gtin, location: let location):
            var parameters: [String: Any] = ["gtin": gtin]

            if let location = location {
                parameters.updateValue(location.longitude, forKey: "lon")
                parameters.updateValue(location.latitude, forKey: "lat")
            }

            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)

        case .fetchProductDetails:
            return .requestPlain

        case .addProduct(parameters: let parameters, location: let location):
            let body = AddProductBody(parameters: parameters)
            let bodyParameters = body.mapToMultipartFormData()

            var urlParameters: [String: Any] = [:]

            if let location = location {
                urlParameters.updateValue(location.longitude, forKey: "lon")
                urlParameters.updateValue(location.latitude, forKey: "lat")
            }

            return .uploadCompositeMultipart(bodyParameters, urlParameters: urlParameters)
        }
    }
}
