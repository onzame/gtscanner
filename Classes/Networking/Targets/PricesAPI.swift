//
//  PricesAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias PricesProviderType = MoyaProvider<PricesAPI>

enum PricesAPI {
    case fetchPrices(productId: Int, location: Location?, page: Int)
    case addPrice(productId: Int, price: Double, shopBranchId: Int)
    case deletePrice(priceId: Int)
}

extension PricesAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchPrices(productId: let productId, _, _):
            return "/products/\(productId)/prices"
        case .addPrice(productId: let productId, _, _):
            return "/products/\(productId)/prices"
        case .deletePrice(priceId: let priceId):
            return "/prices/\(priceId)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchPrices:
            return .get
        case .addPrice:
            return .post
        case .deletePrice:
            return .delete
        }
    }

    var task: Task {
        switch self {
        case .fetchPrices(_, location: let location, page: let page):
            var parameters: [String: Any] = ["page": page]

            if let location = location {
                parameters.updateValue(location.longitude, forKey: "lon")
                parameters.updateValue(location.latitude, forKey: "lat")
            }

            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)

        case .addPrice(_, price: let price, shopBranchId: let shopBranchId):
            let body = AddPriceBody(price: price, shopBranchId: shopBranchId)
            return .requestCustomJSONEncodable(body, encoder: .snakeEncoder)
        case .deletePrice:
            return .requestPlain
        }
    }
}
