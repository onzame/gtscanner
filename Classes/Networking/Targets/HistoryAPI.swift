//
//  HistoryAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias HistoryProviderType = MoyaProvider<HistoryAPI>

enum HistoryAPI {
    case fetchHistory(page: Int)
}

extension HistoryAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchHistory:
            return "/persons/history"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchHistory:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .fetchHistory(page: let page):
            return .requestParameters(parameters: ["page": page], encoding: URLEncoding.default)
        }
    }
}
