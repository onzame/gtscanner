//
//  ShopsAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias ShopsProviderType = MoyaProvider<ShopsAPI>

enum ShopsAPI {
    case fetchShops(location: Location?, page: Int)
    case searchShops(location: Location?, query: String)
}

extension ShopsAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchShops, .searchShops:
            return "/shops/nearest"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchShops, .searchShops:
            return .get
        }
    }

    var task: Task {
        switch self {
        case .fetchShops(location: let location, page: let page):
            var parameters: [String: Any] = ["page": page]

            if let location = location {
                parameters.updateValue(location.longitude, forKey: "lon")
                parameters.updateValue(location.latitude, forKey: "lat")
            }

            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        case .searchShops(location: let location, query: let query):
            var parameters: [String: Any] = ["q": query]

            if let location = location {
                parameters.updateValue(location.longitude, forKey: "lon")
                parameters.updateValue(location.latitude, forKey: "lat")
            }

            return .requestParameters(parameters: parameters, encoding: URLEncoding.default)
        }
    }
}
