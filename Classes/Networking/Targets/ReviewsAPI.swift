//
//  ReviewsAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias ReviewsProviderType = MoyaProvider<ReviewsAPI>

enum ReviewsAPI {
    case fetchReviews(productId: Int, page: Int)
    case addReview(productId: Int, rating: Double, shopBranchId: Int, reviewText: ReviewText)
    case deleteReview(reviewId: Int)
}

extension ReviewsAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchReviews(productId: let productId, _):
            return "/products/\(productId)/reviews"
        case .addReview(productId: let productId, _, _, _):
            return "/products/\(productId)/reviews"
        case .deleteReview(reviewId: let reviewId):
            return "/reviews/\(reviewId)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchReviews:
            return .get
        case .addReview:
            return .post
        case .deleteReview:
            return .delete
        }
    }

    var task: Task {
        switch self {
        case .fetchReviews(_, page: let page):
            return .requestParameters(parameters: ["page": page], encoding: URLEncoding.default)
        case .addReview(_, rating: let rating, shopBranchId: let shopBranchId, reviewText: let reviewText):
            let body = AddReviewBody(rating: rating, shopBranchId: shopBranchId, reviewText: reviewText)
            return .requestCustomJSONEncodable(body, encoder: .snakeEncoder)
        case .deleteReview:
            return .requestPlain
        }
    }
}
