//
//  FavoritesAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias FavoritesProviderType = MoyaProvider<FavoritesAPI>

enum FavoritesAPI {
    case fetchFavorites
    case append(productId: Int, amount: Int)
    case update(productId: Int, amount: Int)
    case delete(productId: Int)
}

extension FavoritesAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchFavorites:
            return "/persons/favorites"
        case .append(productId: let productId, _):
            return "/persons/favorites/product/\(productId)"
        case .update(productId: let productId, _):
            return "/persons/favorites/product/\(productId)"
        case .delete(productId: let productId):
            return "/persons/favorites/product/\(productId)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchFavorites:
            return .get
        case .append:
            return .post
        case .update:
            return .patch
        case .delete:
            return .delete
        }
    }

    var task: Task {
        switch self {
        case .fetchFavorites:
            return .requestPlain
        case .append(_, amount: let count):
            let body = UpdateFavoriteProductBody(count: count)
            return .requestJSONEncodable(body)
        case .update(_, amount: let count):
            let body = UpdateFavoriteProductBody(count: count)
            return .requestJSONEncodable(body)
        case .delete:
            return .requestPlain
        }
    }
}
