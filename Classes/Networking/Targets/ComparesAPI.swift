//
//  ComparesAPI.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import Moya

typealias ComparesProviderType = MoyaProvider<ComparesAPI>

enum ComparesAPI {
    case fetchCategories
    case deleteCategory(categoryId: Int)
    case fetchProducts(categoryId: Int, targetProductId: Int?)
    case addProduct(productId: Int, isCompositionNeeded: Bool)
    case deleteProduct(productId: Int, isCompositionNeeded: Bool)
}

extension ComparesAPI: TargetType, CustomAccessTokenAuthorizable {
    var path: String {
        switch self {
        case .fetchCategories:
            return "/persons/compare/categories"
        case .deleteCategory(categoryId: let categoryId):
            return "/persons/compare/categories/\(categoryId)"
        case .fetchProducts(categoryId: let categoryId, _):
            return "/persons/compare/categories/\(categoryId)/products"
        case .addProduct(productId: let productId, _):
            return "/persons/compare/products/\(productId)"
        case .deleteProduct(productId: let productId, _):
            return "/persons/compare/products/\(productId)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .fetchCategories:
            return .get
        case .deleteCategory:
            return .delete
        case .fetchProducts:
            return .get
        case .addProduct:
            return .post
        case .deleteProduct:
            return .delete
        }
    }

    var task: Task {
        switch self {
        case .fetchCategories:
            return .requestPlain
        case .deleteCategory:
            return .requestPlain
        case .fetchProducts(_, targetProductId: let targetProductId):
            if let targetProductId = targetProductId {
                return .requestParameters(parameters: ["target_product_id": targetProductId], encoding: URLEncoding.default)
            } else {
                return .requestPlain
            }
        case .addProduct(_, isCompositionNeeded: let isCompositionNeeded):
            return .requestParameters(parameters: ["is_composition_needed": isCompositionNeeded], encoding: URLEncoding.default)
        case .deleteProduct(_, isCompositionNeeded: let isCompositionNeeded):
            return .requestParameters(parameters: ["is_composition_needed": isCompositionNeeded], encoding: URLEncoding.default)
        }
    }
}
