//
//  DependenciesContainer.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import Swinject
import SwinjectAutoregistration
import Moya
import Alamofire

final class DependenciesContainer {

    private let container = Container()

    var analyticsService: AnalyticsServiceType {
        return container ~> AnalyticsServiceType.self
    }

    var scannerViewModel: ScannerViewModel {
        return container ~> ScannerViewModel.self
    }

    var scanListViewModel: ScanListViewModel {
        return container ~> ScanListViewModel.self
    }

    var categoriesViewModel: CategoriesViewModel {
        return container ~> CategoriesViewModel.self
    }

    var scanHistoryViewModel: ScanHistoryViewModel {
        return container ~> ScanHistoryViewModel.self
    }

    func shopsViewModel(product: Product?) -> ShopsViewModel {
        return container ~> (ShopsViewModel.self, argument: product)
    }

    var compareCategoriesViewModel: CompareCategoriesViewModel {
        return container ~> CompareCategoriesViewModel.self
    }

    func productInfoViewModel(input: ProductInfoInput) -> ProductInfoViewModel {
        return container ~> (ProductInfoViewModel.self, argument: input)
    }

    func productAmountViewModel(input: ProductAmountInput) -> ProductAmountViewModel {
        return container ~> (ProductAmountViewModel.self, argument: input)
    }

    func productAddViewModel(metadata: ProductAddFlowMetadata) -> ProductAddViewModel {
        return container ~> (ProductAddViewModel.self, argument: metadata)
    }

    func productDetailsViewModel(input: ProductDetailsInput) -> ProductDetailsViewModel {
        return container ~> (ProductDetailsViewModel.self, argument: input)
    }

    func compareProductsViewModel(input: CompareProductsInput) -> CompareProductsViewModel {
        return container ~> (CompareProductsViewModel.self, argument: input)
    }

    func productPriceViewModel(metadata: ProductPriceFlowMetadata) -> ProductPriceViewModel {
        return container ~> (ProductPriceViewModel.self, argument: metadata)
    }

    func productReviewViewModel(metadata: ProductReviewFlowMetadata) -> ProductReviewViewModel {
        return container ~> (ProductReviewViewModel.self, argument: metadata)
    }

    func productPricesListViewModel(input: ProductPricesListInput) -> ProductPricesListViewModel {
        return container ~> (ProductPricesListViewModel.self, argument: input)
    }

    func productReviewsListViewModel(input: ProductReviewsListInput) -> ProductReviewsListViewModel {
        return container ~> (ProductReviewsListViewModel.self, argument: input)
    }

    func productPriceCompletionViewModel(input: ProductPriceCompletionInput) -> ProductPriceCompletionViewModel {
        return container ~> (ProductPriceCompletionViewModel.self, argument: input)
    }

    func productReviewCompletionViewModel(input: ProductReviewCompletionInput) -> ProductReviewCompletionViewModel {
        return container ~> (ProductReviewCompletionViewModel.self, argument: input)
    }

    init(accessTokenProvider: AccessTokenProviderType, analyticsDelegate: GorodGoodsDelegate) {

        // MARK: - Input

        container.register(GorodGoodsDelegate.self, factory: { _ in analyticsDelegate })
            .inObjectScope(.container)

        container.register(AccessTokenProviderType.self, factory: { _ in accessTokenProvider })
            .inObjectScope(.container)

        container.autoregister(AccessTokenManagerType.self, initializer: AccessTokenManager.init)
            .inObjectScope(.container)

        // MARK: - Network settings

        InvalidAccessTokenInterceptor.shared.setup(accessTokenManager: container ~> AccessTokenManagerType.self)

        container.autoregister(CustomAccessTokenPlugin.self, initializer: CustomAccessTokenPlugin.init)

        container.autoregister(NetworkLoggerPlugin.self, initializer: NetworkLoggerPlugin.buildDefault)

        container.register([PluginType].self) { resolver in
            var plugins = [PluginType]()

            plugins.append(resolver ~> CustomAccessTokenPlugin.self)

            if Environment.isDebugMode {
                plugins.append(resolver ~> NetworkLoggerPlugin.self)
            }

            return plugins
        }

        // MARK: - Network

        container.autoregister(ShopsProviderType.self, initializer: ShopsProviderType.buildDefault)

        container.autoregister(PricesProviderType.self, initializer: PricesProviderType.buildDefault)

        container.autoregister(ReviewsProviderType.self, initializer: ReviewsProviderType.buildDefault)

        container.autoregister(HistoryProviderType.self, initializer: HistoryProviderType.buildDefault)

        container.autoregister(ProductsProviderType.self, initializer: ProductsProviderType.buildDefault)

        container.autoregister(ComparesProviderType.self, initializer: ComparesProviderType.buildDefault)

        container.autoregister(FavoritesProviderType.self, initializer: FavoritesProviderType.buildDefault)

        container.autoregister(CategoriesProviderType.self, initializer: CategoriesProviderType.buildDefault)

        // MARK: - Storages

        container.autoregister(PricesStorageType.self, initializer: PricesStorageType.init)
            .inObjectScope(.container)

        container.autoregister(ReviewsStorageType.self, initializer: ReviewsStorageType.init)
            .inObjectScope(.container)

        container.autoregister(HistoryProductsStorageType.self, initializer: HistoryProductsStorageType.init)
            .inObjectScope(.container)

        container.autoregister(CompareProductsStorageType.self, initializer: CompareProductsStorageType.init)
            .inObjectScope(.container)

        container.autoregister(ScannedProductsStorageType.self, initializer: ScannedProductsStorageType.init)
            .inObjectScope(.container)

        container.autoregister(FavoriteProductsStorageType.self, initializer: FavoriteProductsStorageType.init)
            .inObjectScope(.container)

        // MARK: - Repositories

        container.autoregister(PricesRepositoryType.self, initializer: PricesRepositoryType.init)
            .inObjectScope(.container)

        container.autoregister(ReviewsRepositoryType.self, initializer: ReviewsRepositoryType.init)
            .inObjectScope(.container)

        container.autoregister(HistoryProductsRepositoryType.self, initializer: HistoryProductsRepositoryType.init)
            .inObjectScope(.container)

        container.autoregister(CompareProductsRepositoryType.self, initializer: CompareProductsRepositoryType.init)
            .inObjectScope(.container)

        container.autoregister(ScannedProductsRepositoryType.self, initializer: ScannedProductsRepositoryType.init)
            .inObjectScope(.container)

        container.autoregister(FavoriteProductsRepositoryType.self, initializer: FavoriteProductsRepositoryType.init)
            .inObjectScope(.container)

        // MARK: - Services

        container.autoregister(ShopsServiceType.self, initializer: ShopsService.init)

        container.autoregister(PricesServiceType.self, initializer: PricesService.init)

        container.autoregister(ReviewsServiceType.self, initializer: ReviewsService.init)

        container.autoregister(HistoryServiceType.self, initializer: HistoryService.init)

        container.autoregister(FeaturesServiceType.self, initializer: FeaturesService.init)

        container.autoregister(ProductsServiceType.self, initializer: ProductsService.init)

        container.autoregister(ComparesServiceType.self, initializer: ComparesService.init)

        container.autoregister(FavoritesServiceType.self, initializer: FavoritesService.init)

        container.autoregister(AnalyticsServiceType.self, initializer: AnalyticsService.init)

        container.autoregister(CategoriesServiceType.self, initializer: CategoriesService.init)

        // MARK: - View models

        container.autoregister(ScannerViewModel.self, initializer: ScannerViewModel.init)

        container.autoregister(ScanListViewModel.self, initializer: ScanListViewModel.init)

        container.autoregister(CategoriesViewModel.self, initializer: CategoriesViewModel.init)

        container.autoregister(ScanHistoryViewModel.self, initializer: ScanHistoryViewModel.init)

        container.autoregister(CompareCategoriesViewModel.self, initializer: CompareCategoriesViewModel.init)

        container.autoregister(ShopsViewModel.self, argument: Product?.self, initializer: ShopsViewModel.init)

        container.autoregister(ProductInfoViewModel.self, argument: ProductInfoInput.self, initializer: ProductInfoViewModel.init)

        container.autoregister(ProductAddViewModel.self, argument: ProductAddFlowMetadata.self, initializer: ProductAddViewModel.init)

        container.autoregister(ProductAmountViewModel.self, argument: ProductAmountInput.self, initializer: ProductAmountViewModel.init)

        container.autoregister(ProductDetailsViewModel.self, argument: ProductDetailsInput.self, initializer: ProductDetailsViewModel.init)

        container.autoregister(ProductPriceViewModel.self, argument: ProductPriceFlowMetadata.self, initializer: ProductPriceViewModel.init)

        container.autoregister(CompareProductsViewModel.self, argument: CompareProductsInput.self, initializer: CompareProductsViewModel.init)

        container.autoregister(ProductReviewViewModel.self, argument: ProductReviewFlowMetadata.self, initializer: ProductReviewViewModel.init)

        container.autoregister(ProductPricesListViewModel.self, argument: ProductPricesListInput.self, initializer: ProductPricesListViewModel.init)

        container.autoregister(ProductReviewsListViewModel.self, argument: ProductReviewsListInput.self, initializer: ProductReviewsListViewModel.init)

        container.autoregister(ProductPriceCompletionViewModel.self, argument: ProductPriceCompletionInput.self, initializer: ProductPriceCompletionViewModel.init)

        container.autoregister(ProductReviewCompletionViewModel.self, argument: ProductReviewCompletionInput.self, initializer: ProductReviewCompletionViewModel.init)

        // MARK: - Misc.

        KeyboardPresentationListener.shared.setup()
    }
}
