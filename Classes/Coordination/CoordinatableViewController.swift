//
//  CoordinatableViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 27.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MaterialComponents

protocol CoordinatableType: class, AssociatedObjectStore {

    var popActionPublisher: PublishSubject<Void> { get set }

    var dismissBarButtonItem: BackBarButtonItem { get set }

}

// MARK: - Extensions
// MARK: - Default methods

extension CoordinatableType {
    private var defaultActionPublisher: PublishSubject<Void> {
        return PublishSubject<Void>()
    }

    private var defaultDismissButtonItem: BackBarButtonItem {
        return BackBarButtonItem(customView: UIButton.backButton)
    }

    var popActionPublisher: PublishSubject<Void> {
        get { return associatedObject(forKey: &popActionKey, default: defaultActionPublisher) }
        set { setAssociatedObject(newValue, forKey: &popActionKey) }
    }

    var dismissBarButtonItem: BackBarButtonItem {
        get { return associatedObject(forKey: &dismissButtonKey, default: defaultDismissButtonItem) }
        set { setAssociatedObject(newValue, forKey: &dismissButtonKey) }
    }
}

// MARK: - CoordinatorNavigationControllerDelegate + CoordinatableType

protocol CoordinatableViewControllerType: CoordinatableType, CoordinatorNavigationControllerDelegate { }

extension CoordinatableViewControllerType where Self: UIViewController {
    func transitionBackFinished() {
        popActionPublisher.onNext(())
    }

    func didSelectBackAction() {
        navigationController?.popViewController(animated: true)
        popActionPublisher.onNext(())
    }

    func setupDismissButtonItem() {
        navigationItem.setLeftBarButton(dismissBarButtonItem, animated: false)
    }
}

// MARK: - UIViewController + CoordinatableViewControllerType

extension CoordinatableViewControllerType where Self: UIViewController {
    func setupNavigationControllerDelegate() {
        if let navigationController = navigationController as? CoordinatorNavigationController {
            navigationController.swipeBackDelegate = self
        }
    }
}

extension UIViewController: CoordinatableViewControllerType { }

// MARK: - Reactive interface

extension Reactive where Base: UIViewController {
    var popAction: ControlEvent<Void> {
        return ControlEvent(events: base.popActionPublisher)
    }

    var dismissAction: ControlEvent<Void> {
        return ControlEvent(events: base.dismissBarButtonItem.rx.tapAction)
    }

    var dismissBinding: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.dismiss(animated: true)
        })
    }
}

// MARK: - Extended system navigation

extension Reactive where Base: UIViewController {
    var openAppSettings: Binder<Void> {
        return Binder(base, binding: { _, _ in
            UIApplication.shared.openAppSettings()
        })
    }
}

extension Reactive where Base: UIApplication {
    func openSettingsAction() -> Observable<Void> {
        return Observable.create { (eventEmitter) -> Disposable in
            UIApplication.shared.openAppSettings { isSuccess in
                if isSuccess {
                    eventEmitter.onNext(())
                }

                eventEmitter.onCompleted()
            }

            return Disposables.create()
        }
    }
}

// MARK: - Keys

private var popActionKey = "popActionKey"
private var dismissButtonKey = "popButtonKey"
