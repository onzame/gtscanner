//
//  Coordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift

protocol CoordinatorType {

    associatedtype CoordinationResult

    var identifier: UUID { get }

    func coordinate<C, R>(to coordiantor: C) -> Observable<R> where C: CoordinatorType, R == C.CoordinationResult

    func start() -> Observable<CoordinationResult>

}

class Coordinator<ResultType>: CoordinatorType {

    typealias CoordinationResult = ResultType

    private var childCoordinators = [UUID: Any]()

    let disposeBag = DisposeBag()

    let identifier = UUID()

    deinit {
        log.info("deinit coordinator", String(describing: self))
    }

    // MARK: - Public methods

    func coordinate<T: CoordinatorType, R>(to coordiantor: T) -> Observable<R> where R == T.CoordinationResult {
        return coordiantor.start()
            .do(onSubscribe: { [weak self] in self?.store(coordinator: coordiantor) })
            .do(onNext: { [weak self] _ in self?.free(coordinator: coordiantor) })
    }

    func start() -> Observable<CoordinationResult> {
        fatalError("start has not been overriden")
    }

    // MARK: - Private methods

    private func store<T: CoordinatorType>(coordinator: T) {
        childCoordinators.updateValue(coordinator, forKey: coordinator.identifier)
    }

    private func free<T: CoordinatorType>(coordinator: T) {
        childCoordinators.removeValue(forKey: coordinator.identifier)
    }

}
