//
//  ProductModificationCoordinatable.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum ProductUpdateResultType {
    case price(UpdatedProductPriceMetadata)
    case review(UpdatedProductReviewMetadata)
    case add(AddedProductMetadata)
}

// MARK: - Primary scenes to modify product

protocol ProductPriceCoordinatable { }

extension ProductPriceCoordinatable where Self: CoordinatorType {
    func showProductPriceScene(
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductPriceFlowMetadata
    ) -> Observable<ProductPriceCoordinationResult> {
        let coordinator = ProductPriceCoordinator(rootViewController: viewController, container: container, metadata: metadata)
        return coordinate(to: coordinator)
    }

    func showProductPriceCompletionScene(
        viewController: UIViewController,
        container: DependenciesContainer,
        input: ProductPriceCompletionInput
    ) -> Observable<ProductPriceCompletionCoordinationResult> {
        let coordinator = ProductPriceCompletionCoordinator(rootViewController: viewController, container: container, input: input)
        return coordinate(to: coordinator)
    }
}

protocol ProductReviewCoordinatable { }

extension ProductReviewCoordinatable where Self: CoordinatorType {
    func showProductReviewScene(
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductReviewFlowMetadata
    ) -> Observable<ProductReviewCoordinationResult> {
        let coordinator = ProductReviewCoordinator(rootViewController: viewController, container: container, metadata: metadata)
        return coordinate(to: coordinator)
    }

    func showProductReviewCompletionScene(
        viewController: UIViewController,
        container: DependenciesContainer,
        input: ProductReviewCompletionInput
    ) -> Observable<ProductReviewCompletionCoordinationResult> {
        let coordinator = ProductReviewCompletionCoordinator(rootViewController: viewController, container: container, input: input)
        return coordinate(to: coordinator)
    }
}

protocol ProductAddCoordinatable { }

extension ProductAddCoordinatable where Self: CoordinatorType {
    func showProductAddScene(
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductAddFlowMetadata
    ) -> Observable<ProductAddCoordinationResult> {
        let coordinator = ProductAddCoordinator(rootViewController: viewController, container: container, metadata: metadata)
        return coordinate(to: coordinator)
    }
}

// MARK: - Secondary scenes to modify product

protocol ShopsCoordinatable { }

extension ShopsCoordinatable where Self: CoordinatorType {
    func showShopsScene(
        viewController: UIViewController,
        container: DependenciesContainer,
        product: Product?
    ) -> Observable<ShopsCoordinationResult> {
        let coordinator = ShopsCoordinator(rootViewController: viewController, container: container, product: product)
        return coordinate(to: coordinator)
    }
}

protocol CategoriesCoordinatable { }

extension CategoriesCoordinatable where Self: CoordinatorType {
    func showCategoriesScene(viewController: UIViewController, container: DependenciesContainer) -> Observable<CategoriesCoordinationResult> {
        let coordinator = CategoriesCoordinator(rootViewController: viewController, container: container)
        return coordinate(to: coordinator)
    }
}

// MARK: - Modification scenes composition

typealias PrimaryProductModificationCoordinatables = ProductPriceCoordinatable & ProductReviewCoordinatable & ProductAddCoordinatable
typealias SecondaryProductModificationCoordinatables = ShopsCoordinatable & CategoriesCoordinatable

protocol ProductModificationCoordinatable: class, PrimaryProductModificationCoordinatables, SecondaryProductModificationCoordinatables {
    func updateProduct(type: ProductUpdateResultType)
}

extension ProductModificationCoordinatable {
    func updateProduct(type: ProductUpdateResultType) { }
}

// MARK: - Price recursive part

extension ProductModificationCoordinatable where Self: CoordinatorType {
    func startProductPriceRecursiveFlow(
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductPriceFlowMetadata,
        disposeBag: DisposeBag
    ) {
        showProductPriceScene(viewController: viewController, container: container, metadata: metadata)
            .do(onNext: { [weak self] coordinationResult in
                self?.continueProductPriceRecursiveFlow(
                    coordinationResult: coordinationResult,
                    viewController: viewController,
                    container: container,
                    metadata: metadata,
                    disposeBag: disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func continueProductPriceRecursiveFlow(
        coordinationResult: ProductPriceCoordinationResult,
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductPriceFlowMetadata,
        disposeBag: DisposeBag
    ) {
        switch coordinationResult {
        case .updated(let metadata):
            updateProduct(type: .price(metadata))

            let input = ProductPriceCompletionInput(product: metadata.product, shop: metadata.shop, price: metadata.price)
            showProductPriceCompletionScene(viewController: viewController, container: container, input: input)
                .subscribe()
                .disposed(by: disposeBag)

        case .requestedShopSelection(let metadata):
            showShopsScene(viewController: viewController, container: container, product: metadata.product)
                .flatMap({ [weak self] coordinationResult -> Observable<ProductPriceCoordinationResult> in
                    guard let `self` = self else { return .empty() }

                    switch coordinationResult {
                    case .canceled:
                        return self.showProductPriceScene(viewController: viewController, container: container, metadata: metadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductPriceRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: metadata,
                                    disposeBag: disposeBag
                                )
                            })

                    case .selected(let selectedShop):
                        let updatedMetadata = metadata.updated(selectedShop: selectedShop)

                        return self.showProductPriceScene(viewController: viewController, container: container, metadata: updatedMetadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductPriceRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: updatedMetadata,
                                    disposeBag: disposeBag
                                )
                            })
                    }
                })
                .subscribe()
                .disposed(by: disposeBag)

        case .canceled:
            break
        }
    }
}

// MARK: - Review recursive part

extension ProductModificationCoordinatable where Self: CoordinatorType {
    func startProductReviewRecursiveFlow(
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductReviewFlowMetadata,
        disposeBag: DisposeBag
    ) {
        showProductReviewScene(viewController: viewController, container: container, metadata: metadata)
            .do(onNext: { [weak self] coordinationResult in
                self?.continueProductReviewRecursiveFlow(
                    coordinationResult: coordinationResult,
                    viewController: viewController,
                    container: container,
                    metadata: metadata,
                    disposeBag: disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func continueProductReviewRecursiveFlow(
        coordinationResult: ProductReviewCoordinationResult,
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductReviewFlowMetadata,
        disposeBag: DisposeBag
    ) {
        switch coordinationResult {
        case .updatedProduct(let metadata):
            updateProduct(type: .review(metadata))

            let input = ProductReviewCompletionInput(product: metadata.product, review: metadata.review)
            showProductReviewCompletionScene(viewController: viewController, container: container, input: input)
                .subscribe()
                .disposed(by: disposeBag)

        case .requestedShopSelection(let metadata):
            showShopsScene(viewController: viewController, container: container, product: metadata.product)
                .flatMap({ [weak self] coordinationResult -> Observable<ProductReviewCoordinationResult> in
                    guard let `self` = self else { return .empty() }

                    switch coordinationResult {
                    case .canceled:
                        return self.showProductReviewScene(viewController: viewController, container: container, metadata: metadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductReviewRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: metadata,
                                    disposeBag: disposeBag
                                )
                            })

                    case .selected(let selectedShop):
                        let updatedMetadata = metadata.updated(selectedShop: selectedShop)

                        return self.showProductReviewScene(viewController: viewController, container: container, metadata: updatedMetadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductReviewRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: updatedMetadata,
                                    disposeBag: disposeBag
                                )
                            })
                    }
                })
                .subscribe()
                .disposed(by: disposeBag)

        case .canceled:
            break
        }
    }
}

// MARK: - Add recursive part

extension ProductModificationCoordinatable where Self: CoordinatorType {
    func startProductAddRecursiveFlow(
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductAddFlowMetadata,
        disposeBag: DisposeBag
    ) {
        showProductAddScene(viewController: viewController, container: container, metadata: metadata)
            .do(onNext: { [weak self] coordinationResult in
                self?.continueProductAddRecursiveFlow(
                    coordinationResult: coordinationResult,
                    viewController: viewController,
                    container: container,
                    metadata: metadata,
                    disposeBag: disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)
    }

    private func continueProductAddRecursiveFlow(
        coordinationResult: ProductAddCoordinationResult,
        viewController: UIViewController,
        container: DependenciesContainer,
        metadata: ProductAddFlowMetadata,
        disposeBag: DisposeBag
    ) {
        switch coordinationResult {
        case .added(let metadata):
            updateProduct(type: .add(metadata))

        case .requestedCategorySelection(let metadata):
            showCategoriesScene(viewController: viewController, container: container)
                .flatMap({ [weak self] coordinationResult -> Observable<ProductAddCoordinationResult> in
                    guard let `self` = self else { return .empty() }

                    switch coordinationResult {
                    case .canceled:
                        return self.showProductAddScene(viewController: viewController, container: container, metadata: metadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductAddRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: metadata,
                                    disposeBag: disposeBag
                                )
                            })

                    case .selected(let selectedCategory):
                        let updatedMetadata = metadata.updated(selectedCategory: selectedCategory)

                        return self.showProductAddScene(viewController: viewController, container: container, metadata: updatedMetadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductAddRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: updatedMetadata,
                                    disposeBag: disposeBag
                                )
                            })
                    }

                })
                .subscribe()
                .disposed(by: disposeBag)

        case .requestedShopSelection(let metadata):
            showShopsScene(viewController: viewController, container: container, product: nil)
                .flatMap({ [weak self] coordinationResult -> Observable<ProductAddCoordinationResult> in
                    guard let `self` = self else { return .empty() }

                    switch coordinationResult {
                    case .canceled:
                        return self.showProductAddScene(viewController: viewController, container: container, metadata: metadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductAddRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: metadata,
                                    disposeBag: disposeBag
                                )
                            })

                    case .selected(let selectedShop):
                        let updatedMetadata = metadata.updated(selectedShop: selectedShop)

                        return self.showProductAddScene(viewController: viewController, container: container, metadata: updatedMetadata)
                            .do(onNext: { coordinationResult in
                                self.continueProductAddRecursiveFlow(
                                    coordinationResult: coordinationResult,
                                    viewController: viewController,
                                    container: container,
                                    metadata: updatedMetadata,
                                    disposeBag: disposeBag
                                )
                            })
                    }

                })
                .subscribe()
                .disposed(by: disposeBag)

        case .canceled:
            break
        }
    }
}
