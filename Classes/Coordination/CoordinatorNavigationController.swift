//
//  CoordinatorNavigationController.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

protocol CoordinatorNavigationControllerDelegate: class {
    func transitionBackFinished()
    func didSelectBackAction()
}

class CoordinatorNavigationController: UINavigationController {

    // MARK: - Helpers

    fileprivate var transition: UIViewControllerAnimatedTransitioning?

    fileprivate var shouldEnableSwipeBack = false

    fileprivate var duringPushAnimation = false

    // MARK: - Back button customization

    private var backButtonImage: UIImage?
    private var backButtonTitle: String?
    private var backButtonFont: UIFont?
    private var backButtonTitleColor: UIColor?
    private var shouldUseViewControllersTitle = false

    // MARK: - Delegates

    weak var swipeBackDelegate: CoordinatorNavigationControllerDelegate?

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }

    // MARK: - Public methods

    func setTransition(transition: UIViewControllerAnimatedTransitioning?) {
        if shouldEnableSwipeBack {
            enableSwipeBack()
        }

        self.transition = transition

        if transition != nil {
            disableSwipeBack()
        }
    }

    func customizeBackButton(
        backButtonImage: UIImage? = nil,
        backButtonTitle: String? = nil,
        backButtonFont: UIFont? = nil,
        backButtonTitleColor: UIColor? = nil,
        shouldUseViewControllersTitle: Bool = false
    ) {
        self.backButtonImage = backButtonImage
        self.backButtonTitle = backButtonTitle
        self.backButtonFont = backButtonFont
        self.backButtonTitleColor = backButtonTitleColor
        self.shouldUseViewControllersTitle = shouldUseViewControllersTitle
    }

    func enableSwipeBack() {
        shouldEnableSwipeBack = true
        interactivePopGestureRecognizer?.isEnabled = true
        interactivePopGestureRecognizer?.delegate = self
    }

    // MARK: - Private methods

    private func disableSwipeBack() {
        interactivePopGestureRecognizer?.isEnabled = false
        interactivePopGestureRecognizer?.delegate = nil
    }

    private func setupCustomBackButton(viewController: UIViewController) {
        if backButtonImage != nil || backButtonTitle != nil {

            let backButtonTitle: String?

            if shouldUseViewControllersTitle {
                if viewControllers.count > 1 {
                    backButtonTitle = viewControllers[viewControllers.count - 2].title
                } else {
                    backButtonTitle = nil
                }
            } else {
                backButtonTitle = self.backButtonTitle
            }

            let button = UIButton(type: .system).then {
                $0.frame = CGRect(width: 37, height: 44)
                $0.setImage(backButtonImage, for: .normal)
                $0.setTitle(backButtonTitle, for: .normal)
                $0.titleLabel?.font = backButtonFont
                $0.setTitleColor(backButtonTitleColor, for: .normal)
            }

            button.addTarget(self, action: #selector(backButtonAction), for: .touchUpInside)

            if #available(iOS 11.0, *) {
                viewController.navigationItem.hidesBackButton = true
                viewController.navigationItem.setLeftBarButton(UIBarButtonItem(customView: button), animated: false)
            } else {
                navigationBar.backIndicatorImage = backButtonImage
                navigationBar.backIndicatorTransitionMaskImage = backButtonImage
                viewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
            }
        }
    }

    // MARK: - Overrides

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        duringPushAnimation = true
        super.pushViewController(viewController, animated: animated)
        setupCustomBackButton(viewController: viewController)
    }

    // MARK: - Actions

    @objc private func backButtonAction(sender: UIButton) {
        swipeBackDelegate?.didSelectBackAction()
    }

}

// MARK: - Extensions
// MARK: - UIGestureRecognizerDelegate

extension CoordinatorNavigationController: UIGestureRecognizerDelegate {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer == interactivePopGestureRecognizer else { return true }
        return viewControllers.count > 1 && duringPushAnimation == false
    }

    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - UINavigationControllerDelegate

extension CoordinatorNavigationController: UINavigationControllerDelegate {
    func navigationController(
        _ navigationController: UINavigationController,
        animationControllerFor operation: UINavigationController.Operation,
        from fromVC: UIViewController,
        to toVC: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }

    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let coordinator = navigationController.topViewController?.transitionCoordinator {
            coordinator.notifyWhenInteractionChanges { [weak self] (context) in
                if !context.isCancelled {
                    self?.swipeBackDelegate?.transitionBackFinished()
                }
            }
        }
    }

    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let navigationController = navigationController as? CoordinatorNavigationController else { return }
        navigationController.duringPushAnimation = false
    }
}
