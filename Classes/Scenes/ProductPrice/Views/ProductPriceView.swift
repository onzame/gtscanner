//
//  ProductPriceView.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductPriceView: ModalContentView {

    // MARK: - UI components

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitelLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
        $0.alignment = .center
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let priceTextField = PriceTextField()

    let shopsView = ShopsComponentView()

    let actionsView = ActionsComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductPrice.submitActionTitle, for: .normal)
        $0.primaryContainerView.backgroundColor = Color.actionDisabledColor
        $0.secondaryButton.setTitle(Localization.ProductPrice.cancelActionTitle, for: .normal)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        bodyView.addSubview(descriptionStackView)
        bodyView.addSubview(priceTextField)
        bodyView.addSubview(shopsView)
        bodyView.addSubview(actionsView)
    }

    private func setupLayout() {
        actionsView.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
        }

        shopsView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(actionsView.snp.top).offset(Metric.shopsViewBottomOffset)
        }

        priceTextField.snp.makeConstraints { (make) in
            make.left.equalTo(safeLeftConstraintItem).offset(Metric.priceTextFieldLeftOffset)
            make.right.equalTo(safeRightConstraintItem).offset(Metric.priceTextFieldRightOffset)
            make.bottom.equalTo(shopsView.snp.top).offset(Metric.priceTextFieldBottomOffset)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.greaterThanOrEqualToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.descriptionStackViewRightOffset)
            make.bottom.equalTo(priceTextField.snp.top).offset(Metric.descriptionStackViewBottomOffset)
            make.centerX.equalToSuperview()
        }
    }

    private func setupView() {
        setHideKeyboardOnTap()
    }

    // MARK: - Private methods

    fileprivate func enableSubmitAction(isEnabled: Bool) {
        actionsView.primaryButton.isEnabled = isEnabled
        actionsView.primaryContainerView.backgroundColor = isEnabled ? Color.actionEnabledColor : Color.actionDisabledColor
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductPriceView {
    fileprivate enum Color {
        static let actionEnabledColor = Style.Color.brightYellow
        static let actionDisabledColor = Style.Color.lightBlue
        static let descriptionStackViewTitleLabelTextColor = Style.Color.black
        static let descriptionStackViewSubtitleLabelTextColor = Style.Color.secondaryText
    }

    fileprivate enum CornerRadius {
        static let cornerRadius: CGFloat = 12
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleLabelFont = Style.Font.sanFrancisco(.semibold, size: 20)
        static let descriptionStackViewSubtitelLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Metric {
        static let shopsViewBottomOffset: CGFloat = -16
        static let priceTextFieldLeftOffset: CGFloat = 20
        static let priceTextFieldRightOffset: CGFloat = -20
        static let priceTextFieldBottomOffset: CGFloat = -16
        static let descriptionStackViewBottomOffset: CGFloat = -24
        static let descriptionStackViewSpacing: CGFloat = 4
        static let descriptionStackViewLeftOffset: CGFloat = 20
        static let descriptionStackViewRightOffset: CGFloat = -20
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductPriceView {
    var isSubmitEnabled: Binder<Bool> {
        return Binder(base, binding: { view, isEnabled in
            view.enableSubmitAction(isEnabled: isEnabled)
        })
    }
}
