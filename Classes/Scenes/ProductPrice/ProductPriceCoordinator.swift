//
//  ProductPriceCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum ProductPriceCoordinationResult {
    case requestedShopSelection(ProductPriceFlowMetadata)
    case updated(UpdatedProductPriceMetadata)
    case canceled
}

final class ProductPriceCoordinator: Coordinator<ProductPriceCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let metadata: ProductPriceFlowMetadata

    init(rootViewController: UIViewController, container: DependenciesContainer, metadata: ProductPriceFlowMetadata) {
        self.rootViewController = rootViewController
        self.container = container
        self.metadata = metadata
    }

    override func start() -> Observable<ProductPriceCoordinationResult> {
        let viewModel = container.productPriceViewModel(metadata: metadata)
        let viewController = ProductPriceViewController(reactor: viewModel)
        let bottomSheetController = BottomSheetController(contentViewController: viewController)

        rootViewController.present(bottomSheetController, animated: true)

        let dismissingAction = bottomSheetController.rx.isDismissing
            .asObservable()

        // ProductPriceCoordinationResult.canceled

        let cancelAction = viewController.rx.cancelAction
            .asObservable()
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let dismissAction = bottomSheetController.rx.dismissAction
            .asObservable()

        let cancelTrigger = Observable.merge(cancelAction, dismissAction)
            .mapTo(ProductPriceCoordinationResult.canceled)

        // ProductPriceCoordinationResult.updated

        let updatedAction = viewController.rx.updateProductAction
            .map(ProductPriceCoordinationResult.updated)
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let updatedTrigger = Observable.combineLatest(dismissingAction, updatedAction)
            .map({ $0.1 })

        // ProductPriceCoordinationResult.requestedShopSelection

        let shopsAction = viewController.rx.shopsAction
            .asObservable()
            .do(onNext: { bottomSheetController.dismiss(animated: true) })

        let requestedShopSelectionTrigger = Observable.combineLatest(dismissingAction, shopsAction)
            .withLatestFrom(viewModel.state)
            .map({ state -> ProductPriceCoordinationResult in
                let metadata = ProductPriceFlowMetadata(product: state.product, selectedPrice: state.selectedPrice, selectedShop: state.selectedShop)
                return ProductPriceCoordinationResult.requestedShopSelection(metadata)
            })

        return Observable.merge(cancelTrigger, updatedTrigger, requestedShopSelectionTrigger)
            .do(onSubscribe: { [weak self] in
                guard let `self` = self else { return }
                self.container.analyticsService.sendEventSync(event: .productPrice, inCategory: .price, tag: .productId(id: self.metadata.product.id))
            })
            .take(1)
    }

}
