//
//  ProductPriceViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

final class ProductPriceViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let pricesService: PricesServiceType

    // MARK: - Lifecycle

    init(pricesService: PricesServiceType, metadata: ProductPriceFlowMetadata) {
        self.pricesService = pricesService
        self.initialState = State(metadata: metadata)
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .selectShop(let shop):
            return Observable.just(Mutation.selectShop(shop))
                .observeOn(MainScheduler.asyncInstance)

        case .selectPrice(let price):
            return Observable.just(Mutation.selectPrice(price))

        case .addPrice(product: let product, price: let price, shop: let shop):
            return pricesService.addPrice(product: product, price: price, shop: shop)
                .map({ UpdatedProductPriceMetadata(product: product.update(updates: $0.productUpdates), shop: shop, price: $0.productPrice) })
                .map(Mutation.updatedProductMetadata)
                .catchError({ .just(.showError($0)) })
                .startWith(.setLoading(true))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        switch mutation {
        case .selectShop(let selectedShop):

            let sections = state.sections?.map({ sectionModel -> ShopsPreviewSectionModel in
                switch sectionModel {
                case .section(items: let items):
                    let updatedItems = items.map({ itemModel -> ShopsPreviewItemModel in
                        switch itemModel {
                        case .shop(let viewModel):
                            let shop = viewModel.currentState.shop
                            let isSelected = viewModel.currentState.shop.identity == selectedShop.identity
                            let updatedViewModel = ShopsPreviewCollectionViewModel(shop: shop, isSelected: isSelected)
                            return ShopsPreviewItemModel.shop(updatedViewModel)

                        case .noData, .noPermission:
                            return itemModel
                        }
                    })

                    return ShopsPreviewSectionModel.section(items: updatedItems)
                }
            })

            state.sections = sections
            state.selectedShop = selectedShop
            state.error = nil

        case .selectPrice(let price):
            state.selectedPrice = price
            state.error = nil

        case .setLoading(let isLoading):
            state.isLoading = isLoading
            state.error = nil

        case .showError(let error):
            state.error = error
            state.isLoading = false

        case .updatedProductMetadata(let metadata):
            state.updatedProductMetadata = metadata
            state.product = metadata.product
            state.selectedShop = metadata.shop
            state.selectedPrice = metadata.price.price
            state.isLoading = false
            state.error = nil
        }

        return state
    }
}

// MARK: - Extensions
// MARK: - Actions

extension ProductPriceViewModel {
    enum Action {
        case selectShop(Shop)
        case selectPrice(Double?)
        case addPrice(product: Product, price: Double, shop: Shop)
    }
}

// MARK: - Mutations

extension ProductPriceViewModel {
    enum Mutation {
        case selectShop(Shop)
        case selectPrice(Double?)
        case setLoading(Bool)
        case updatedProductMetadata(UpdatedProductPriceMetadata)
        case showError(Error)
    }
}

// MARK: - State

extension ProductPriceViewModel {
    struct State {
        var sections: [ShopsPreviewSectionModel]?
        var product: Product
        var selectedShop: Shop?
        var selectedPrice: Double?
        var isLoading: Bool = false
        var updatedProductMetadata: UpdatedProductPriceMetadata?
        var error: Error?

        init(metadata: ProductPriceFlowMetadata) {
            self.sections = metadata.product.shops.mapToSections()
            self.product = metadata.product
            self.selectedShop = metadata.selectedShop
            self.selectedPrice = metadata.selectedPrice
        }
    }
}
