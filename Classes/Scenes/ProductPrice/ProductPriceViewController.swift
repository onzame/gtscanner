//
//  ProductPriceViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ProductPriceViewController: BaseViewController<ProductPriceViewModel> {

    // MARK: - UI components

    let contentView = ProductPriceView()

    // MARK: - Publishers

    fileprivate let updateProductMetadataPublisher = PublishSubject<UpdatedProductPriceMetadata>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.addEdgesConstraints()
    }

    private func setupView() {
        preferredContentSize = CGSize(width: Metric.width, height: Metric.height)
    }

    // MARK: - Bindings

    override func bind(reactor: ProductPriceViewModel) {

        // Settings

        let startSequence = reactor.state.take(1)

        startSequence
            .map({ $0.selectedPrice })
            .filterNil()
            .mapToString()
            .bind(to: contentView.priceTextField.rx.text)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.selectedShop })
            .filterNil()
            .map(Reactor.Action.selectShop)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.shopsView.rx.openAppSettingAction
            .bind(to: rx.openAppSettings)
            .disposed(by: disposeBag)

        // Input

        contentView.shopsView.rx.shopSelection
            .map(Reactor.Action.selectShop)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.priceTextField.rx.text.orEmpty
            .mapToDouble()
            .map(Reactor.Action.selectPrice)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.actionsView.primaryButton.rx.tap
            .withLatestFrom(reactor.state) { (_, state) -> Reactor.Action? in
                guard let selectedPrice = state.selectedPrice, let selectedShop = state.selectedShop else { return nil }
                return .addPrice(product: state.product, price: selectedPrice, shop: selectedShop)
            }
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        let collectionView = contentView.shopsView.collectionView
        let dataSource = contentView.shopsView.dataSource

        reactor.state
            .map({ $0.sections })
            .filterNil()
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.shops })
            .filterNil()
            .map({ $0.isNotEmpty })
            .bind(to: contentView.shopsView.rx.enableShopsAction)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.product.shops == nil })
            .mapToVoid()
            .bind(to: rx.nilShopsBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.shops })
            .filterNil()
            .filter({ $0.isEmpty })
            .mapToVoid()
            .bind(to: rx.emptyShopsBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.name })
            .bind(to: contentView.descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.productionPlace })
            .bind(to: contentView.descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        let shopSelectionSequence = reactor.state
            .map({ $0.selectedShop != nil })

        let amountInsertedSequence = reactor.state
            .map({ $0.selectedPrice })
            .isPositive()

        Observable.combineLatest(shopSelectionSequence, amountInsertedSequence)
            .map({ $0 && $1 })
            .bind(to: contentView.rx.isSubmitEnabled)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.updatedProductMetadata })
            .filterNil()
            .bind(to: updateProductMetadataPublisher)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductPriceViewController {
    fileprivate enum Metric {
        static var width: CGFloat = Screen.width
        static let height: CGFloat = 442
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductPriceViewController {
    var updateProductAction: ControlEvent<UpdatedProductPriceMetadata> {
        return ControlEvent(events: base.updateProductMetadataPublisher)
    }

    var cancelAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.actionsView.secondaryButton.rx.tap)
    }

    var shopsAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.shopsView.rx.allShopsAction)
    }

    var nilShopsBinding: Binder<Void> {
        return Binder(base, binding: { _, _ in
            // TODO: Impl
        })
    }

    var emptyShopsBinding: Binder<Void> {
        return Binder(base, binding: { _, _ in
            // TODO: Impl
        })
    }
}
