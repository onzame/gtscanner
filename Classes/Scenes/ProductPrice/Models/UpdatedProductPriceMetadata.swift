//
//  UpdatedProductPriceMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct UpdatedProductPriceMetadata {
    let product: Product
    let shop: Shop
    let price: Price
}
