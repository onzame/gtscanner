//
//  ProductDetailsSection.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductDetailsSection {
    let id: Int
    let title: String
}

// MARK: - Equatable

extension ProductDetailsSection: Equatable {
    static func == (lhs: ProductDetailsSection, rhs: ProductDetailsSection) -> Bool {
        return lhs.id == rhs.id
            && lhs.title == rhs.title
    }
}
