//
//  ProductDetailsView.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class ProductDetailsView: UIView {

    // MARK: - UI component

    let refreshControl = UIRefreshControl()

    let tableView = UITableView().then {
        $0.setBackgroundColor(Color.tableViewBackgroundColor)
        $0.register(Reusable.rowCell)
        $0.register(Reusable.headerView)
        $0.allowsSelection = false
        $0.separatorStyle = .none
    }

    // MARK: - DataSource

    let dataSource = RxTableViewSectionedAnimatedDataSource<ProductDetailsSectionModel>(
        configureCell: { (_, tableView, indexPath, model) -> UITableViewCell in
            switch model {
            case .row(let viewModel):
                let cell = tableView.dequeue(Reusable.rowCell, for: indexPath)
                cell.reactor = viewModel
                return cell
            }
        }
    )

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(tableView)
    }

    private func setupLayout() {
        tableView.addEdgesConstraints()
    }

    private func setupView() {
        tableView.refreshControl = refreshControl
        tableView.delegate = self
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductDetailsView {
    fileprivate enum Color {
        static let tableViewBackgroundColor = Style.Color.white
    }

    fileprivate enum Reusable {
        static let headerView = ReusableView<ProductDetailsHeaderTableViewCell>()
        static let rowCell = ReusableCell<ProductDetailsRowTableViewCell>()
    }
}

// MARK: - UITableViewDelegate

extension ProductDetailsView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch dataSource[section] {
        case .section(viewModel: let viewModel, _):
            let view = tableView.dequeue(Reusable.headerView)
            view?.reactor = viewModel
            return view
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSource[indexPath] {
        case .row(let viewModel):
            return Reusable.rowCell.class.cellHeight(
                insideOf: tableView,
                productDetail: viewModel.currentState.productDetail,
                isLast: dataSource[indexPath.section].items.count - 1 == indexPath.row
            )
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch dataSource[section] {
        case .section(viewModel: let viewModel, _):
            return Reusable.headerView.class.cellHeight(
                insideOf: tableView,
                productDetailsSection: viewModel.currentState.productDetailsSection
            )
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductDetailsView {
    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }
}
