//
//  ProductDetailsViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

class ProductDetailsViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let productsService: ProductsServiceType

    // MARK: - Lifecycle

    init(productsService: ProductsServiceType, input: ProductDetailsInput) {
        self.productsService = productsService
        self.initialState = State(product: input.product)
    }

    deinit {
        log.info("deinit ProductDetailsViewModel")
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .fetch(product: let product):
            return productsService.fetchDetails(product: product)
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh(product: let product):
            return productsService.fetchDetails(product: product)
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .update(let aggregations):
            state.sections = aggregations.mapToSections()
            state.isRefreshing = false
            state.isLoaded = true

        case .setLoaded(let isLoaded):
            state.isLoaded = isLoaded

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = error
        }

        return state
    }
}

// MARK: - Extensions
// MARK: - Actions

extension ProductDetailsViewModel {
    enum Action {
        case fetch(product: Product)
        case refresh(product: Product)
    }
}

// MARK: - Mutations

extension ProductDetailsViewModel {
    enum Mutation {
        case update([ProductDetailsAggregation])
        case setRefreshing(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
    }
}

// MARK: - State

extension ProductDetailsViewModel {
    struct State {
        var product: Product
        var sections: [ProductDetailsSectionModel] = []
        var isRefreshing: Bool = false
        var isLoaded: Bool = false
        var error: Error?
    }
}
