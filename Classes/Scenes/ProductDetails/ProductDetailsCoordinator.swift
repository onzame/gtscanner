//
//  ProductDetailsCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

final class ProductDetailsCoordinator: Coordinator<Void> {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer
    private let input: ProductDetailsInput

    init(rootNavigationController: UINavigationController, container: DependenciesContainer, input: ProductDetailsInput) {
        self.rootNavigationController = rootNavigationController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<Void> {
        let viewModel = container.productDetailsViewModel(input: input)
        let viewController = ProductDetailsViewController(reactor: viewModel)

        rootNavigationController.pushViewController(viewController, animated: true)

        let popAction = viewController.rx.popAction
            .asObservable()

        return popAction
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .productMore, inCategory: .info, tag: .none)
            })
            .take(1)
    }

}
