//
//  ProductDetailsRowTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ProductDetailsRowTableViewCell: BaseTableViewCell, View {

    static func cellHeight(insideOf tableView: UITableView, productDetail: ProductDetail, isLast: Bool) -> CGFloat {
        let bottomHeight = isLast ? abs(Metric.descriptionLabelBottomOffsetExtended) : abs(Metric.descriptionLabelBottomOffset)
        let heightConstants = abs(Metric.titleLabelTopOffset) + abs(Metric.descriptionLabelTopOffset) + bottomHeight
        let contentWidth = tableView.frame.width - abs(Metric.contentLeftOffset) - abs(Metric.contentRightOffset)
        let titleHeight = productDetail.title.boundingHeight(width: contentWidth, font: Font.titleLabelFont)
        let descriptionHeight = productDetail.value.boundingHeight(width: contentWidth, font: Font.descriptionLabelFont)
        return heightConstants + titleHeight + descriptionHeight
    }

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.numberOfLines = 0
    }

    let descriptionLabel = UILabel().then {
        $0.font = Font.descriptionLabelFont
        $0.textColor = Color.descriptionLabelTextColor
        $0.numberOfLines = 0
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
        addSubview(descriptionLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalToSuperview().offset(Metric.titleLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        descriptionLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(titleLabel.snp.bottom).offset(Metric.descriptionLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ProductDetailsRowTableViewModel) {
        reactor.state
            .map({ $0.productDetail.title })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.productDetail.value })
            .bind(to: descriptionLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductDetailsRowTableViewCell {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.secondaryText
        static let descriptionLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let descriptionLabelFont = Style.Font.sanFrancisco(.medium, size: 16)
    }

    fileprivate enum Metric {
        static let contentLeftOffset: CGFloat = 24
        static let contentRightOffset: CGFloat = -24

        static let titleLabelTopOffset: CGFloat = 8

        static let descriptionLabelTopOffset: CGFloat = 4
        static let descriptionLabelBottomOffset: CGFloat = -8
        static let descriptionLabelBottomOffsetExtended: CGFloat = -28
    }
}
