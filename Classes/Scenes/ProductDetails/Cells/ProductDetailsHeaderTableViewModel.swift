//
//  ProductDetailsHeaderTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class ProductDetailsHeaderTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(productDetailsSection: ProductDetailsSection) {
        initialState = State(productDetailsSection: productDetailsSection)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductDetailsHeaderTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ProductDetailsHeaderTableViewModel {
    struct State {
        let productDetailsSection: ProductDetailsSection
    }
}

// MARK: - Equatable

extension ProductDetailsHeaderTableViewModel.State: Equatable {
    static func == (lhs: ProductDetailsHeaderTableViewModel.State, rhs: ProductDetailsHeaderTableViewModel.State) -> Bool {
        return lhs.productDetailsSection == rhs.productDetailsSection
    }
}
