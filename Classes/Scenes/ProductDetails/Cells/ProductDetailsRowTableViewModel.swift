//
//  ProductDetailsRowTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class ProductDetailsRowTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(productDetail: ProductDetail) {
        initialState = State(productDetail: productDetail)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductDetailsRowTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ProductDetailsRowTableViewModel {
    struct State {
        let productDetail: ProductDetail
    }
}

// MARK: - Equatable

extension ProductDetailsRowTableViewModel.State: Equatable {
    static func == (lhs: ProductDetailsRowTableViewModel.State, rhs: ProductDetailsRowTableViewModel.State) -> Bool {
        return lhs.productDetail == rhs.productDetail
    }
}
