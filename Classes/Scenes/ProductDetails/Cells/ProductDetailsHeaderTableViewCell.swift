//
//  ProductDetailsHeaderTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ProductDetailsHeaderTableViewCell: BaseTableViewHeaderFooterView, View {

    static func cellHeight(insideOf tableView: UITableView, productDetailsSection: ProductDetailsSection) -> CGFloat {
        let contentWidth = tableView.frame.width - abs(Metric.titleLabelLeftOffset) - abs(Metric.titleLabelRightOffset)
        let titleHeight = productDetailsSection.title.boundingHeight(width: contentWidth, font: Font.titleLabelFont)
        return titleHeight + abs(Metric.titleLabelTopOffset) + abs(Metric.titleLabelBottomOffset) + Metric.dividerViewHeight
    }

    // MARK: - UI components

    let dividerView = UIView().then {
        $0.backgroundColor = Color.dividerViewBackground
    }

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.numberOfLines = 0
    }

    // MARK: - Lifecycle

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(dividerView)
        addSubview(titleLabel)
    }

    private func setupLayout() {
        dividerView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(Metric.dividerViewHeight)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.top.equalTo(dividerView.snp.bottom).offset(Metric.titleLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.titleLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.titleLabelBottomOffset)
        }
    }

    private func setupView() {
        contentView.backgroundColor = Color.contentViewBackgroundColor
    }

    // MARK: - Bindings

    func bind(reactor: ProductDetailsHeaderTableViewModel) {
        reactor.state
            .map({ $0.productDetailsSection.title })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductDetailsHeaderTableViewCell {
    fileprivate enum Color {
        static let contentViewBackgroundColor = Style.Color.white
        static let dividerViewBackground = UIColor(hex: "#F2F2F2")
        static let titleLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 18)
    }

    fileprivate enum Metric {
        static let dividerViewHeight: CGFloat = 1
        static let titleLabelLeftOffset: CGFloat = 24
        static let titleLabelTopOffset: CGFloat = 28
        static let titleLabelRightOffset: CGFloat = -24
        static let titleLabelBottomOffset: CGFloat = -20
    }
}
