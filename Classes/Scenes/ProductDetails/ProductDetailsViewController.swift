//
//  ProductDetailsViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ProductDetailsViewController: BaseViewController<ProductDetailsViewModel> {

    // MARK: - UI components

    let contentView = ProductDetailsView()

    // MARK: - Publishers

    fileprivate let retryActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.ProductDetails.title
    }

    // MARK: - Bindings

    override func bind(reactor: ProductDetailsViewModel) {

        // Input

        rx.viewDidLoad
            .take(1)
            .mapToVoid()
            .withLatestFrom(reactor.state)
            .map({ Reactor.Action.fetch(product: $0.product) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .withLatestFrom(reactor.state)
            .map({ Reactor.Action.fetch(product: $0.product) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.refreshAction
            .withLatestFrom(reactor.state)
            .filter({ !$0.isRefreshing })
            .map({ Reactor.Action.refresh(product: $0.product) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.tableView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.isLoaded && $0.error != nil })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.isLoaded && $0.error == nil })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.sections.isNotEmpty && $0.isLoaded })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.sections.isEmpty && $0.isLoaded })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductDetailsViewController {
    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
    }
}

// MARK: - StateResolvable

extension ProductDetailsViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.ProductDetails.errorPlaceholderTitle
            $0.subtitle = Localization.ProductDetails.errorPlaceholderSubtitle
            $0.actionTitle = Localization.ProductDetails.errorPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.ProductDetails.noDataPlaceholderTitle
            $0.subtitle = Localization.ProductDetails.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.ProductDetails.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}
