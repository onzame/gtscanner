//
//  ProductDetailsSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources

// MARK: - Sections

enum ProductDetailsSectionModel {
    case section(viewModel: ProductDetailsHeaderTableViewModel, items: [ProductDetailsItemModel])
}

extension ProductDetailsSectionModel: AnimatableSectionModelType {
    var identity: Int {
        switch self {
        case .section(viewModel: let viewModel, _):
            return viewModel.currentState.productDetailsSection.id
        }
    }

    var items: [ProductDetailsItemModel] {
        switch self {
        case .section(_, items: let items):
            return items
        }
    }

    init(original: ProductDetailsSectionModel, items: [ProductDetailsItemModel]) {
        switch original {
        case .section(viewModel: let viewModel, _):
            self = .section(viewModel: viewModel, items: items)
        }
    }
}

// MARK: - Items

enum ProductDetailsItemModel {
    case row(ProductDetailsRowTableViewModel)
}

extension ProductDetailsItemModel: IdentifiableType {
    var identity: String {
        switch self {
        case .row(let viewModel):
            if let id = viewModel.currentState.productDetail.id {
                return "\(id)"
            } else {
                return viewModel.currentState.productDetail.title
            }
        }
    }
}

extension ProductDetailsItemModel: Equatable {
    static func == (lhs: ProductDetailsItemModel, rhs: ProductDetailsItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.row(let left), .row(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Array where Element == ProductDetailsAggregation {
    func mapToSections() -> [ProductDetailsSectionModel] {
        return map({ aggregation -> ProductDetailsSectionModel in
            let productDetailsSection = ProductDetailsSection(id: aggregation.id, title: aggregation.title)
            let viewModel = ProductDetailsHeaderTableViewModel(productDetailsSection: productDetailsSection)
            let items = aggregation.properties.map({ productDetail -> ProductDetailsItemModel in
                let viewModel = ProductDetailsRowTableViewModel(productDetail: productDetail)
                return .row(viewModel)
            })

            return .section(viewModel: viewModel, items: items)
        })
    }
}
