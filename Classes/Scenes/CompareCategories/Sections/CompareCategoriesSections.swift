//
//  CompareCategoriesSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum CompareCategoriesSectionModel {
    case section(items: [CompareCategoriesItemModel])
}

extension CompareCategoriesSectionModel: AnimatableSectionModelType {
    var identity: String {
        return "section"
    }

    var items: [CompareCategoriesItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: CompareCategoriesSectionModel, items: [CompareCategoriesItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum CompareCategoriesItemModel {
    case category(CompareCategoriesRowTableViewModel)
}

extension CompareCategoriesItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .category(let viewModel):
            return viewModel.currentState.category.categoryId
        }
    }
}

extension CompareCategoriesItemModel: Equatable {
    static func == (lhs: CompareCategoriesItemModel, rhs: CompareCategoriesItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.category(let left), .category(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Array where Element == CompareCategory {
    func mapToSections() -> [CompareCategoriesSectionModel] {
        let items = map({ category -> CompareCategoriesItemModel in
            let viewModel = CompareCategoriesRowTableViewModel(category: category)
            let itemModel = CompareCategoriesItemModel.category(viewModel)
            return itemModel
        })

        return items.isEmpty ? [] : [CompareCategoriesSectionModel.section(items: items)]
    }
}

extension Observable where Element == CompareCategoriesItemModel {
    func mapToCategory() -> Observable<CompareCategory?> {
        return map({ model in
            if case CompareCategoriesItemModel.category(let viewModel) = model {
                return viewModel.currentState.category
            } else {
                return nil
            }
        })
    }
}
