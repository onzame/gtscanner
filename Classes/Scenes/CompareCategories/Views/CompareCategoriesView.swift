//
//  CompareCategoriesView.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit
import SwipeCellKit

class CompareCategoriesView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let refreshControl = UIRefreshControl()

    let tableView = UITableView().then {
        $0.setBackgroundColor(Color.tableViewBackgroundColor)
        $0.register(Reusable.categoryCell)
        $0.separatorInset = UIEdgeInsets(top: 0, left: Metric.tableViewSeparatorLeftOffset, bottom: 0, right: 0)
        $0.separatorColor = Color.tableViewSeparatorColor
        $0.tableFooterView = UIView()
    }

    // MARK: - DataSource

    lazy var dataSource = RxTableViewSectionedAnimatedDataSource<CompareCategoriesSectionModel>(
        configureCell: { [weak self] (_, tableView, indexPath, model) -> UITableViewCell in
            switch model {
            case .category(let viewModel):
                let cell = tableView.dequeue(Reusable.categoryCell, for: indexPath)
                cell.reactor = viewModel
                cell.delegate = self
                return cell
            }
        },
        canEditRowAtIndexPath: { (_, _) -> Bool in
            return true
        }
    )

    // MARK: - Publishers

    fileprivate let deleteCategoryActionPublisher = PublishSubject<CompareCategory>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        addSubview(tableView)
    }

    private func setupLayout() {
        tableView.addEdgesConstraints()
    }

    private func setupView() {
        tableView.refreshControl = refreshControl
    }

    private func setupBinding() {
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareCategoriesView {
    fileprivate enum Color {
        static let tableViewBackgroundColor = Style.Color.white
        static let tableViewSeparatorColor = UIColor(hex: "#EBEBEB")
        static let deleteActionBackgroundColor = Style.Color.pink
    }

    fileprivate enum Image {
        static let deleteActionImage = Media.icon(.trash, size: .x24)
    }

    fileprivate enum Metric {
        static let tableViewSeparatorLeftOffset: CGFloat = 32
    }

    fileprivate enum Reusable {
        static let categoryCell = ReusableCell<CompareCategoriesRowTableViewCell>()
    }
}

// MARK: - UITableViewDelegate

extension CompareCategoriesView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSource[indexPath] {
        case .category:
            return Reusable.categoryCell.class.cellHeight
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        switch dataSource[indexPath] {
        case .category(let viewModel):
            return viewModel.currentState.category.count > 1 ? indexPath : nil
        }
    }
}

// MARK: - SwipeTableViewCellDelegate

extension CompareCategoriesView: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        switch dataSource[indexPath] {
        case .category(let viewModel):
            let deleteAction = SwipeAction(style: .destructive, title: nil, handler: { [weak self] (_, _) in
                self?.deleteCategoryActionPublisher.onNext(viewModel.currentState.category)
            }).then {
                $0.backgroundColor = Color.deleteActionBackgroundColor
                $0.image = Image.deleteActionImage?.tint(with: .white)
            }

            return [deleteAction]
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CompareCategoriesView {
    var categoryDeletion: ControlEvent<CompareCategory> {
        return ControlEvent(events: base.deleteCategoryActionPublisher)
    }

    var categorySelection: ControlEvent<CompareCategory> {
        let source = base.tableView.rx.itemSelected(dataSource: base.dataSource)
            .asObservable()
            .mapToCategory()
            .filterNil()
            .filter({ $0.count > 1 })

        return ControlEvent(events: source)
    }

    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }
}
