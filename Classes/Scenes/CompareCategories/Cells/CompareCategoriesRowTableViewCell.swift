//
//  CompareCategoriesRowTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class CompareCategoriesRowTableViewCell: BaseTableViewCell, View {

    static let cellHeight: CGFloat = 48

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    let amountBadgeView = BadgeView().then {
        $0.titleLabel.font = Font.amountBadgeViewTitleFont
        $0.titleLabel.textColor = Color.amountBadgeViewTitleTextColor
        $0.setBackgroundColor(Color.amountBadgeViewBackgroundColor)
        $0.setCornerRadius(CornerRadius.amountBadgeCornerRadius)
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: -

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(amountBadgeView)
    }

    private func setupLayout() {
        amountBadgeView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.amountBadgeViewRightOffset)
            make.width.greaterThanOrEqualTo(Metric.amountBadgeViewWidth)
            make.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.right.lessThanOrEqualTo(amountBadgeView.snp.left).offset(Metric.titleLabelRightOffset)
            make.centerY.equalToSuperview()
        }
    }

    func bind(reactor: CompareCategoriesRowTableViewModel) {
        reactor.state
            .map({ $0.category.name })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.category.count })
            .mapToString()
            .bind(to: amountBadgeView.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareCategoriesRowTableViewCell {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
        static let amountBadgeViewTitleTextColor = Style.Color.black
        static let amountBadgeViewBackgroundColor = UIColor(hex: "#F5F5F5")
    }

    fileprivate enum CornerRadius {
        static let amountBadgeCornerRadius: CGFloat = 12
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.regular, size: 14)
        static let amountBadgeViewTitleFont = Style.Font.sanFrancisco(.regular, size: 14)
    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 32
        static let titleLabelRightOffset: CGFloat = -8

        static let amountBadgeViewRightOffset: CGFloat = -20
        static let amountBadgeViewWidth: CGFloat = 24
    }
}
