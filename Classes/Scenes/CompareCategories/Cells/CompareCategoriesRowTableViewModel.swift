//
//  CompareCategoriesRowTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class CompareCategoriesRowTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(category: CompareCategory) {
        initialState = State(category: category)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareCategoriesRowTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension CompareCategoriesRowTableViewModel {
    struct State {
        let category: CompareCategory
    }
}

// MARK: - Equatable

extension CompareCategoriesRowTableViewModel.State: Equatable {
    static func == (lhs: CompareCategoriesRowTableViewModel.State, rhs: CompareCategoriesRowTableViewModel.State) -> Bool {
        return lhs.category == rhs.category
    }
}
