//
//  CompareCategoriesCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum CompareCategoriesCoordinationResult {
    case popped
    case dismissed
}

class CompareCategoriesCoordinator: Coordinator<CompareCategoriesCoordinationResult> {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer

    init(rootNavigationController: UINavigationController, container: DependenciesContainer) {
        self.rootNavigationController = rootNavigationController
        self.container = container
    }

    override func start() -> Observable<CompareCategoriesCoordinationResult> {
        let viewModel = container.compareCategoriesViewModel
        let viewController = CompareCategoriesViewController(reactor: viewModel)

        rootNavigationController.pushViewController(viewController, animated: true)

        // CompareCategoriesCoordinationResult.popped

        let poppedEvent = viewController.rx.popAction
            .mapTo(CompareCategoriesCoordinationResult.popped)

        // CompareCategoriesCoordinationResult.dismissedEvent

        let dismissedEvent = viewController.rx.showScannerAction
            .mapTo(CompareCategoriesCoordinationResult.dismissed)
            .do(onNext: { [weak self] _ in
                self?.rootNavigationController.dismiss(animated: true)
            })

        let productsDismissedEvent = viewController.rx.categorySelection
            .flatMap({ [weak self] compareCategory -> Observable<CompareProductsCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = CompareProductsInput.compareCategory(compareCategory)
                return self.showCompareProductsScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                if case CompareProductsCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapTo(CompareCategoriesCoordinationResult.dismissed)

        return Observable.merge(poppedEvent, dismissedEvent, productsDismissedEvent)
            .take(1)
    }

    private func showCompareProductsScene(
        navigationController: UINavigationController,
        container: DependenciesContainer,
        input: CompareProductsInput
    ) -> Observable<CompareProductsCoordinationResult> {
        let coordinator = CompareProductsCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

}

class ModalCompareCategoriesCoordinator: Coordinator<Void> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer

    init(rootViewController: UIViewController, container: DependenciesContainer) {
        self.rootViewController = rootViewController
        self.container = container
    }

    override func start() -> Observable<Void> {
        let viewModel = container.compareCategoriesViewModel
        let viewController = CompareCategoriesViewController(reactor: viewModel)
        let navigationController = BaseNavigationController(rootViewController: viewController)

        rootViewController.present(navigationController, animated: true)

        let dismissEvent = viewController.rx.dismissAction
            .asObservable()
            .do(onNext: { navigationController.dismiss(animated: true) })

        let dismissedEvent = viewController.rx.showScannerAction
            .asObservable()
            .do(onNext: { navigationController.dismiss(animated: true) })

        let productsDismissedEvent = viewController.rx.categorySelection
            .flatMap({ [weak self] compareCategory -> Observable<CompareProductsCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = CompareProductsInput.compareCategory(compareCategory)
                return self.showCompareProductsScene(navigationController: navigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                if case CompareProductsCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapToVoid()

        return Observable.merge(dismissEvent, dismissedEvent, productsDismissedEvent)
            .take(1)
    }

    private func showCompareProductsScene(
        navigationController: UINavigationController,
        container: DependenciesContainer,
        input: CompareProductsInput
    ) -> Observable<CompareProductsCoordinationResult> {
        let coordinator = CompareProductsCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

}
