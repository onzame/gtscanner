//
//  CompareCategoriesViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

class CompareCategoriesViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let comparesService: ComparesServiceType

    // MARK: - Lifecycle

    init(comparesService: ComparesServiceType) {
        self.comparesService = comparesService
        self.initialState = State()
    }

    deinit {
        log.info("deinit CompareCategoriesViewModel")
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .fetch:
            return comparesService.fetchCategories()
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh:
            return comparesService.fetchCategories()
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))

        case .delete(category: let category):
            return comparesService.deleteCategory(category: category)
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setDeleting(true))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .update(let compareCategories):
            state.sections = compareCategories.mapToSections()
            state.isRefreshing = false
            state.isDeleting = false
            state.isLoaded = true

        case .setLoaded(let isLoaded):
            state.isLoaded = isLoaded

        case .setDeleting(let isDeleting):
            state.isDeleting = isDeleting

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isRefreshing = false
            state.isDeleting = false
            state.isLoaded = isLoaded
            state.error = error
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareCategoriesViewModel {
    enum Action {
        case fetch
        case refresh
        case delete(category: CompareCategory)
    }
}

// MARK: - Mutations

extension CompareCategoriesViewModel {
    enum Mutation {
        case update([CompareCategory])
        case setRefreshing(Bool)
        case setDeleting(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
    }
}

// MARK: - State

extension CompareCategoriesViewModel {
    struct State {
        var sections: [CompareCategoriesSectionModel] = []
        var isRefreshing: Bool = false
        var isDeleting: Bool = false
        var isLoaded: Bool = false
        var error: Error?

        var showLoadingState: Bool {
            return !isLoaded && error == nil
        }

        var showDefaultState: Bool {
            return sections.isNotEmpty && isLoaded
        }

        var showErrorState: Bool {
            return !isLoaded && error != nil
        }

        var showNoDataState: Bool {
            return sections.isEmpty && isLoaded
        }
    }
}
