//
//  CategoriesTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class CategoriesTableViewCell: BaseTableViewCell, View {

    static let cellHeight: CGFloat = 68

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.titleLabelRightOffset)
            make.centerY.equalToSuperview()
        }
    }

    // MARK: - Bindings

    func bind(reactor: CategoriesTableViewModel) {
        reactor.state
            .map({ $0.category.name })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CategoriesTableViewCell {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.bold, size: 20)
    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 20
        static let titleLabelRightOffset: CGFloat = -20
    }
}
