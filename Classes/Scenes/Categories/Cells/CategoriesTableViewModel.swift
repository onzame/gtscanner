//
//  CategoriesTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxDataSources

class CategoriesTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(category: Category) {
        initialState = State(category: category)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CategoriesTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension CategoriesTableViewModel {
    struct State {
        let category: Category
    }
}

// MARK: - Equatable

extension CategoriesTableViewModel.State: Equatable {
    static func == (lhs: CategoriesTableViewModel.State, rhs: CategoriesTableViewModel.State) -> Bool {
        return lhs.category == rhs.category
    }
}

// MARK: - IdentifiableType

extension CategoriesTableViewModel: IdentifiableType {
    var identity: Int {
        return currentState.category.identity
    }
}
