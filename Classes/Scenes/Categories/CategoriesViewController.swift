//
//  CategoriesViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class CategoriesViewController: BaseViewController<CategoriesViewModel> {

    // MARK: - UI components

    let contentView = CategoriesView()

    // MARK: - Publishers

    fileprivate let retryActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.Categories.title
    }

    // MARK: - Bindings

    override func bind(reactor: CategoriesViewModel) {

        // Input

        rx.viewDidLoad
            .take(1)
            .mapTo(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .mapTo(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.refreshAction
            .withLatestFrom(reactor.state)
            .filter({ !$0.isRefreshing })
            .mapTo(Reactor.Action.refresh)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.filterAction
            .debounce(.milliseconds(Duration.filterDelay), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .map(Reactor.Action.filter)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.tableView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ (flag: $0.showErrorState, error: $0.error) })
            .distinctUntilChanged({ $0.flag == $1.flag })
            .filter({ $0.flag })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showLoadingState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showDefaultState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showNoDataState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

    }

}

// MARK: - Extensions
// MARK: - Constants

extension CategoriesViewController {
    fileprivate enum Duration {
        static let filterDelay: Int = 500
    }

    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
    }
}

// MARK: - StateResolvable

extension CategoriesViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.Categories.errorPlaceholderTitle
            $0.subtitle = Localization.Categories.errorPlaceholderSubtitle
            $0.actionTitle = Localization.Categories.errorPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.Categories.noDataPlaceholderTitle
            $0.subtitle = Localization.Categories.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.Categories.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CategoriesViewController {
    var selectionAction: ControlEvent<Category> {
        return base.contentView.rx.selectionAction
    }
}
