//
//  CategoriesViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

class CategoriesViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let categoriesService: CategoriesServiceType

    // MARK: - Lifecycle

    init(categoriesService: CategoriesServiceType) {
        self.categoriesService = categoriesService
        self.initialState = State()
    }

    deinit {
        log.info("deinit ProductDetailsViewModel")
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .fetch:
            return categoriesService.fetchCategories()
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh:
            return categoriesService.fetchCategories()
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))

        case .filter(query: let query):
            return .just(.filter(query: query))

        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        switch mutation {
        case .update(let categories):
            state.categories = categories
            state.sections = categories.mapToSections()
            state.isRefreshing = false
            state.isLoaded = true
            state.error = nil

        case .setLoaded(let isLoaded):
            state.isLoaded = isLoaded
            state.error = nil

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing
            state.error = nil

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = error

        case .filter(query: let query):
            let filteredCategories = state.categories
                .filter({ category -> Bool in
                    let preparedQuery = query.prepareForFilter()
                    let preparedName = category.name.prepareForFilter()
                    return query.isEmpty || preparedName.contains(preparedQuery)
                })

            state.sections = filteredCategories.mapToSections()
        }

        return state
    }
}

// MARK: - Extensions
// MARK: - Actions

extension CategoriesViewModel {
    enum Action {
        case fetch
        case refresh
        case filter(query: String)
    }
}

// MARK: - Mutations

extension CategoriesViewModel {
    enum Mutation {
        case update([Category])
        case setRefreshing(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
        case filter(query: String)
    }
}

// MARK: - State

extension CategoriesViewModel {
    struct State {
        var categories: [Category] = []
        var sections: [CategoriesSectionModel] = []
        var isRefreshing: Bool = false
        var isLoaded: Bool = false
        var error: Error?

        var showLoadingState: Bool {
            return !isLoaded && error == nil
        }

        var showDefaultState: Bool {
            return categories.isNotEmpty && isLoaded
        }

        var showErrorState: Bool {
            return !isLoaded && error != nil
        }

        var showNoDataState: Bool {
            return categories.isEmpty && isLoaded
        }
    }
}
