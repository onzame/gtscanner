//
//  CategoriesView.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class CategoriesView: UIView {

    // MARK: - UI components

    let searchView = SearchView().then {
        $0.searchTextField.setPlaceholder(string: Localization.Categories.searchTextPlaceholder)
    }

    let refreshControl = UIRefreshControl()

    let tableView = UITableView().then {
        $0.contentInset = UIEdgeInsets(top: Metric.tableViewVerticalInset, left: 0, bottom: Metric.tableViewVerticalInset, right: 0)
        $0.separatorInset = UIEdgeInsets(top: 0, left: Metric.tableViewSeparatorLeftOffset, bottom: 0, right: 0)
        $0.register(Reusable.categoryCell)
        $0.separatorColor = Color.tableViewSeparatorColor
        $0.tableFooterView = UIView()
        $0.keyboardDismissMode = .interactive
    }

    // MARK: - DataSource

    let dataSource = RxTableViewSectionedAnimatedDataSource<CategoriesSectionModel>(
        configureCell: { (_, tableView, indexPath, model) -> UITableViewCell in
            switch model {
            case .category(let viewModel):
                let cell = tableView.dequeue(Reusable.categoryCell, for: indexPath)
                cell.reactor = viewModel
                return cell
            }
    })

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        removeKeyboardObservers(from: .default)
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(searchView)
        addSubview(tableView)
    }

    private func setupLayout() {
        searchView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(safeTopConstraintItem)
        }

        tableView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(searchView.snp.bottom)
        }
    }

    private func setupView() {
        addKeyboardObservers(to: .default)
        tableView.refreshControl = refreshControl
        tableView.delegate = self
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CategoriesView {
    fileprivate enum Color {
        static let tableViewSeparatorColor = Style.Color.lightBlue
    }

    fileprivate enum Metric {
        static let tableViewTopOffset: CGFloat = -12
        static let tableViewVerticalInset: CGFloat = 12
        static let tableViewSeparatorLeftOffset: CGFloat = 20
    }

    fileprivate enum Reusable {
        static let categoryCell = ReusableCell<CategoriesTableViewCell>()
    }
}

// MARK: - ScrollKeyboardObservable

extension CategoriesView: ScrollViewKeyboardObservable {
    var keyboardObservingScrollView: UIScrollView {
        return tableView
    }
}

// MARK: - UITableViewDelegate

extension CategoriesView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSource[indexPath] {
        case .category:
            return Reusable.categoryCell.class.cellHeight
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CategoriesView {
    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }

    var selectionAction: ControlEvent<Category> {
        let source = base.tableView.rx.itemSelected(dataSource: base.dataSource)
            .asObservable()
            .mapToCategory()
            .filterNil()

        return ControlEvent(events: source)
    }

    var filterAction: ControlEvent<String> {
        return ControlEvent(events: base.searchView.rx.query)
    }
}
