//
//  CategoriesSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum CategoriesSectionModel {
    case section(items: [CategoriesItemModel])
}

extension CategoriesSectionModel: AnimatableSectionModelType {
    var identity: String {
        switch self {
        case .section:
            return "section"
        }
    }

    var items: [CategoriesItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: CategoriesSectionModel, items: [CategoriesItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum CategoriesItemModel {
    case category(CategoriesTableViewModel)
}

extension CategoriesItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .category(let viewModel):
            return viewModel.identity
        }
    }
}

extension CategoriesItemModel: Equatable {
    static func == (lhs: CategoriesItemModel, rhs: CategoriesItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.category(let left), .category(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Array where Element == Category {
    func mapToSections() -> [CategoriesSectionModel] {
        let items = map({ category -> CategoriesItemModel in
            let viewModel = CategoriesTableViewModel(category: category)
            return .category(viewModel)
        })

        return items.isEmpty ? [] : [.section(items: items)]
    }
}

extension Observable where Element == CategoriesItemModel {
    func mapToCategory() -> Observable<Category?> {
        return map({ model in
            if case CategoriesItemModel.category(let viewModel) = model {
                return viewModel.currentState.category
            } else {
                return nil
            }
        })
    }
}
