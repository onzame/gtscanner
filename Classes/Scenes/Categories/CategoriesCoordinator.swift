//
//  CategoriesCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum CategoriesCoordinationResult {
    case selected(Category)
    case canceled
}

class CategoriesCoordinator: Coordinator<CategoriesCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer

    init(rootViewController: UIViewController, container: DependenciesContainer) {
        self.rootViewController = rootViewController
        self.container = container
    }

    override func start() -> Observable<CategoriesCoordinationResult> {
        let viewModel = container.categoriesViewModel
        let viewController = CategoriesViewController(reactor: viewModel)
        let navigationController = BaseNavigationController(rootViewController: viewController)

        rootViewController.present(navigationController, animated: true)

        let dismissingAction = navigationController.rx.isDismissing
            .asObservable()

        // CategoriesCoordinationResult.selected

        let selectionAction = viewController.rx.selectionAction
            .asObservable()
            .do(onNext: { _ in navigationController.dismiss(animated: true) })

        let selectedEvent = Observable.combineLatest(dismissingAction, selectionAction)
            .map({ CategoriesCoordinationResult.selected($0.1) })

        // CategoriesCoordinationResult.canceled

        let dismissAction = viewController.rx.dismissAction
            .asObservable()
            .do(onNext: { _ in navigationController.dismiss(animated: true) })

        let canceledEvent = Observable.combineLatest(dismissingAction, dismissAction)
            .mapTo(CategoriesCoordinationResult.canceled)

        return Observable.merge(selectedEvent, canceledEvent)
            .take(1)
    }

}
