//
//  PhotosCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum PhotosCoordinationResult {
    case taked(Photo)
    case canceled
}

class PhotosCoordinator: Coordinator<PhotosCoordinationResult> {

    private let rootViewController: UIViewController
    private let imagePickerProxy = ImagePickerProxyDelegate()

    fileprivate let resultPublisher = PublishSubject<PhotosCoordinationResult>()

    init(rootViewController: UIViewController) {
        self.rootViewController = rootViewController
    }

    override func start() -> Observable<PhotosCoordinationResult> {
        imagePickerProxy.resultPublisher
            .map({ result -> PhotosCoordinationResult in
                switch result {
                case .selected(let image):
                    return .taked(Photo(image: image))
                case .canceled:
                    return .canceled
                }
            })
            .bind(to: resultPublisher)
            .disposed(by: disposeBag)

        showSourceSelectionScene()

        return resultPublisher
            .take(1)
    }

    private func showSourceSelectionScene() {
        let sourceAlertControler = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let cameraSourceAction = UIAlertAction(title: Localization.Photos.cameraSourceTitle, style: .default, handler: { _ in
            if #available(iOS 13.0, *) {
                sourceAlertControler.dismiss(animated: true) {
                    self.showImagePickerScene(source: .camera)
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.showImagePickerScene(source: .camera)
                }
            }
        })

        let gallerySourceAction = UIAlertAction(title: Localization.Photos.gallerySourceTitle, style: .default, handler: { _ in
            if #available(iOS 13.0, *) {
                sourceAlertControler.dismiss(animated: true) {
                    self.showImagePickerScene(source: .photoLibrary)
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.showImagePickerScene(source: .photoLibrary)
                }
            }
        })

        let cancelAction = UIAlertAction(title: Localization.Photos.cancel, style: .cancel, handler: { _ in
            self.resultPublisher.onNext(.canceled)
        })

        [cameraSourceAction, gallerySourceAction, cancelAction].forEach(sourceAlertControler.addAction)

        rootViewController.present(sourceAlertControler, animated: true)
    }

    private func showImagePickerScene(source: UIImagePickerController.SourceType) {
        let viewController = UIImagePickerController()
        if #available(iOS 13.0, *) {
            viewController.modalPresentationStyle = .overFullScreen
        } else {
            viewController.modalPresentationStyle = .overCurrentContext
        }
        viewController.sourceType = source
        viewController.allowsEditing = true
        viewController.delegate = imagePickerProxy
        rootViewController.present(viewController, animated: true)
    }

}
