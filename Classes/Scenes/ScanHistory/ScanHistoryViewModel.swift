//
//  ScanHistoryViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

final class ScanHistoryViewModel: Reactor {

    // MARK: - Properties

    var initialState = State()

    // MARK: - Dependencies

    private let historyService: HistoryServiceType
    private let productsService: ProductsServiceType
    private let favoritesService: FavoritesServiceType

    // MARK: - Lifecycle

    init(historyService: HistoryServiceType, productsService: ProductsServiceType, favoritesService: FavoritesServiceType) {
        self.historyService = historyService
        self.productsService = productsService
        self.favoritesService = favoritesService
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .observeScannedProducts:
            return historyService.observeHistoryProductsWithReset()
                .map(Mutation.mutateScannedProductsResult)

        case .observeSavedProducts:
            return favoritesService.observeSavedProducts()
                .map(Mutation.mutateFavoritesProductsResult)

        case .fetch:
            return historyService.refresh()
                .map(Mutation.updateChanges)
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh:
            return historyService.refresh()
                .map(Mutation.updateChanges)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))

        case .loadNext(page: let page):
            return historyService.loadNext(page: page)
                .map(Mutation.updateChanges)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.beginLoadingNext)
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        switch mutation {
        case .updateSections(sections: let sections):
            state.sections = sections

        case .updateSavedProductsDetails(details: let details):
            state.savedProductsDetails = details
            state.shouldShowSavedProductsDetails = true

        case .showSavedProductsDetails(let shouldShow):
            state.shouldShowSavedProductsDetails = shouldShow

        case .updateChanges(let pagination):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.pagination = pagination
            state.isLoaded = true
            state.error = nil

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing
            state.error = nil

        case .setLoaded(let isLoaded):
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = nil

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = error

        case .beginLoadingNext:
            state.isLoadingNext = true
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScanHistoryViewModel {
    enum Action {
        case observeScannedProducts
        case observeSavedProducts
        case fetch
        case refresh
        case loadNext(page: Int)
    }
}

// MARK: - Mutations

extension ScanHistoryViewModel {
    enum Mutation {
        static func mutateScannedProductsResult(scannedProducts: [HistoryProductContainer]) -> Mutation {
            let groupedScannedProducts = scannedProducts
                .group(by: [.year, .month, .day])
                .map({ (pair) -> (key: Date, value: [HistoryProductContainer]) in
                    return (key: pair.key, value: pair.value.sorted(by: { $0.date > $1.date }))
                })
                .sorted(by: { (left, right) -> Bool in
                    return left.key > right.key
                })

            let sectionsModels = groupedScannedProducts.map({ (group) -> ScanHistorySectionModel in
                return .section(
                    viewModel: ScanHistoryDateHeaderCollectionViewModel(date: group.key),
                    items: group.value
                        .map(ScanHistoryProductCollectionViewModel.init)
                        .map(ScanHistoryItemModel.product)
                )
            })

            return Mutation.updateSections(sections: sectionsModels)
        }

        static func mutateFavoritesProductsResult(aggregations: [FavoriteProductsAggregation]) -> Mutation {
            if aggregations.isNotEmpty {
                let products = aggregations.map({ $0.products }).reduce([], +)
                let details = FavoriteProductsDetails(products: products)
                return Mutation.updateSavedProductsDetails(details: details)
            } else {
                return Mutation.showSavedProductsDetails(shouldShow: false)
            }
        }

        case updateSections(sections: [ScanHistorySectionModel])
        case updateSavedProductsDetails(details: FavoriteProductsDetails)
        case showSavedProductsDetails(shouldShow: Bool)
        case updateChanges(Pagination)
        case setRefreshing(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
        case beginLoadingNext
    }
}

// MARK: - State

extension ScanHistoryViewModel {
    struct State {
        var sections: [ScanHistorySectionModel] = []
        var savedProductsDetails: FavoriteProductsDetails?
        var shouldShowSavedProductsDetails: Bool = false
        var isLoadingNext: Bool = false
        var isRefreshing: Bool = false
        var isLoaded: Bool = false
        var pagination = Pagination.initial
        var error: Error?

        var showLoadingState: Bool {
            return !isLoaded && error == nil
        }

        var showDefaultState: Bool {
            return sections.isNotEmpty && isLoaded
        }

        var showErrorState: Bool {
            return !isLoaded && error != nil
        }

        var showNoDataState: Bool {
            return sections.isEmpty && isLoaded
        }
    }
}
