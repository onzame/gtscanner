//
//  ScanHistoryCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

private protocol ExtendedScanHistoryCoordinator { }

// swiftlint:disable line_length
private extension ExtendedScanHistoryCoordinator where Self: CoordinatorType {
    func showProductAmountScene(viewController: UIViewController, container: DependenciesContainer, input: ProductAmountInput) -> Observable<ModalProductAmountCoordinationResult> {
        let coordinator = ModalProductAmountCoordinator(rootViewController: viewController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    func showScanListScene(viewController: UINavigationController, container: DependenciesContainer) -> Observable<ScanListCoordinationResult> {
        let coordinator = ScanListCoordinator(rootNavigationController: viewController, container: container)
        return coordinate(to: coordinator)
    }

    func showProductInfoScene(navigationController: UINavigationController, container: DependenciesContainer, input: ProductInfoInput) -> Observable<ProductInfoCoordinationResult> {
        let coordinator = ProductInfoCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }
}
// swiftlint:enable line_length

enum ScanHistoryCoordinationResult {
    case popped
    case dismissed
}

final class ScanHistoryCoordinator: Coordinator<ScanHistoryCoordinationResult>, ExtendedScanHistoryCoordinator {

    private let rootNavigationController: UINavigationController

    private let container: DependenciesContainer

    init(rootNavigationController: UINavigationController, container: DependenciesContainer) {
        self.rootNavigationController = rootNavigationController
        self.container = container
    }

    override func start() -> Observable<ScanHistoryCoordinationResult> {
        let viewModel = container.scanHistoryViewModel
        let viewController = ScanHistoryViewController(reactor: viewModel)

        rootNavigationController.pushViewController(viewController, animated: true)

        let popTrigger = viewController.rx.popAction
            .mapTo(ScanHistoryCoordinationResult.popped)

        let dismissTrigger = viewController.rx.showScannerSceneAction
            .mapTo(ScanHistoryCoordinationResult.dismissed)
            .do(onNext: { [weak self] _ in
                self?.rootNavigationController.dismiss(animated: true)
            })

        let isScanListWasBefore = rootNavigationController.viewControllers.last is ScanHistoryViewController
        let isScanListWasBeforeElement = Observable.just(isScanListWasBefore)

        let shouldPopToScanListScene = viewController.rx.showScanListSceneAction
            .withLatestFrom(isScanListWasBeforeElement)

        let popToScanListTrigger = shouldPopToScanListScene
            .filter({ $0 })
            .mapTo(ScanHistoryCoordinationResult.popped)
            .do(onNext: { [weak self] _ in
                self?.rootNavigationController.popViewController(animated: true)
            })

        let dismissedEvent = shouldPopToScanListScene
            .filter({ !$0 })
            .flatMap({ [weak self] _ -> Observable<ScanListCoordinationResult> in
                guard let `self` = self else { return .empty() }
                return self.showScanListScene(viewController: self.rootNavigationController, container: self.container)
            })
            .filter({ coordinationResult -> Bool in
                if case ScanListCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapTo(ScanHistoryCoordinationResult.dismissed)

        viewController.rx.showChangeAmountSceneAction
            .flatMap({ [weak self] product -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: product.domainProduct, amount: nil, mode: .add)
                return self.showProductAmountScene(viewController: viewController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        let dismissedToScannerTrigger = viewController.rx.productSelection
            .asObservable()
            .flatMap({ [weak self] historyProduct -> Observable<ProductInfoCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductInfoInput(product: historyProduct.domainProduct)
                return self.showProductInfoScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                if case ProductInfoCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapTo(ScanHistoryCoordinationResult.dismissed)

        return Observable.merge(popTrigger, dismissTrigger, popToScanListTrigger, dismissedEvent, dismissedToScannerTrigger)
            .take(1)
    }
}

// MARK: - Modal Coordinator

final class ModalScanHistoryCoordinator: Coordinator<Void>, ExtendedScanHistoryCoordinator {

    private let rootViewController: UIViewController

    private let container: DependenciesContainer

    init(rootViewController: UIViewController, container: DependenciesContainer) {
        self.rootViewController = rootViewController
        self.container = container
    }

    override func start() -> Observable<Void> {
        let viewModel = container.scanHistoryViewModel
        let viewController = ScanHistoryViewController(reactor: viewModel)
        let navigationController = BaseNavigationController(rootViewController: viewController)

        rootViewController.present(navigationController, animated: true)

        let cancelTrigger = viewController.rx.dismissAction
            .asObservable()
            .do(onDispose: {
                navigationController.dismiss(animated: true)
            })

        let searchTrigger = viewController.rx.showScannerSceneAction
            .asObservable()
            .do(onDispose: {
                navigationController.dismiss(animated: true)
            })

        let dismissedEvent = viewController.rx.showScanListSceneAction
            .flatMap({ [weak self] _ -> Observable<ScanListCoordinationResult> in
                guard let `self` = self else { return .empty() }
                return self.showScanListScene(viewController: navigationController, container: self.container)
            })
            .filter({ coordinationResult -> Bool in
                if case ScanListCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapToVoid()

        viewController.contentView.productSelectionActionPublisher
            .flatMap({ [weak self] product -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: product.domainProduct, amount: nil, mode: .add)
                return self.showProductAmountScene(viewController: viewController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        let dismissedToScannerTrigger = viewController.rx.productSelection
            .asObservable()
            .flatMap({ [weak self] historyProduct -> Observable<ProductInfoCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductInfoInput(product: historyProduct.domainProduct)
                return self.showProductInfoScene(navigationController: navigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                if case ProductInfoCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapToVoid()

        return Observable.merge(cancelTrigger, searchTrigger, dismissedEvent, dismissedToScannerTrigger)
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .none, inCategory: .history, tag: .none)
            })
            .take(1)
    }
}
