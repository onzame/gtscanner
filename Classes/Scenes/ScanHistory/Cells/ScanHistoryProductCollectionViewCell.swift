//
//  ScanHistoryProductCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import Cosmos

protocol ScanHistoryProductCollectionViewCellDelegate: class {
    func saveProduct(product: HistoryProduct)
}

final class ScanHistoryProductCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let headerView = UIView()

    let containerView = UIView().then {
        $0.backgroundColor = Color.containerViewBackgroundColor
        $0.layer.cornerRadius = CornerRadius.containerViewCornerRadius
        $0.layer.masksToBounds = true
        $0.setupShadow(.lightBlue)
    }

    let imageContainerView = ImageContainerView().then {
        $0.imageView.contentMode = .scaleAspectFit
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let ratingView = RatingView().then {
        $0.settings.textColor = Color.ratingViewLabelTextColor
        $0.settings.textFont = Font.ratingViewLabelFont
        $0.settings.starSize = Metric.ratingViewStarPartSize
        $0.settings.starMargin = Metric.ratingViewStarMaring
        $0.settings.updateOnTouch = false
    }

    let priceLabel = UILabel().then {
        $0.text = "Price"
        $0.font = Font.priceLabelFont
        $0.textColor = Color.priceLabelTextColor
    }

    lazy var actionButton = UIButton(type: .system).then {
        $0.setImage(Image.actionButtonImage, for: .normal)
        $0.setTintColor(Color.actionButtonTintColor)
        $0.setBackgroundColor(Color.actionButtonBackgroundColor)
        $0.addTarget(self, action: #selector(addButtonWasPressed), for: .touchUpInside)
    }

    // MARK: - Delegates

    weak var cellDelegate: ScanHistoryProductCollectionViewCellDelegate?

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        actionButton.setCornerRadius(corners: .layerMinXMinYCorner, radius: CornerRadius.actionButtonCornerRadius)
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        imageContainerView.imageView.kf.prepareForReuse()
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(containerView)

        containerView.addSubview(headerView)
        containerView.addSubview(descriptionStackView)
        containerView.addSubview(ratingView)
        containerView.addSubview(priceLabel)
        containerView.addSubview(actionButton)

        headerView.addSubview(imageContainerView)
    }

    private func setupLayout() {
        containerView.addEdgesConstraints()

        headerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.headerViewLeftOffset)
            make.top.equalToSuperview().offset(Metric.headerViewTopOffset)
            make.right.equalToSuperview().offset(Metric.headerViewRightOffset)
            make.height.equalTo(headerView.snp.width).multipliedBy(Metric.headerViewHeightRatio)
        }

        imageContainerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(Metric.imageContainerViewInset)
        }

        actionButton.snp.makeConstraints { (make) in
            make.right.bottom.equalToSuperview()
            make.width.equalTo(Metric.actionButtonWidth)
            make.height.equalTo(Metric.actionButtonHeight)
        }

        priceLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.priceLabelLeftOffset)
            make.right.equalTo(actionButton.snp.left).offset(Metric.priceLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.priceLabelBottomOffset)
        }

        ratingView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.ratingViewLeftOffset)
            make.bottom.equalTo(actionButton.snp.top).offset(Metric.ratingViewBottomOffset)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.descriptionStackViewRightOffset)
            make.bottom.equalTo(ratingView.snp.top).offset(Metric.descriptionStackViewBottomOffset)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ScanHistoryProductCollectionViewModel) {

        // Output

        reactor.state
            .map({ $0.product.name })
            .bind(to: descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.productionPlace })
            .bind(to: descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.images.first?.url })
            .bind(to: imageContainerView.imageView.rx.image(placeholder: ImagePlaceholderView.product()))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.avgPrice })
            .mapToPrice()
            .bind(to: priceLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.rating })
            .bind(to: ratingView.rx.rating)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.rating })
            .mapToString()
            .bind(to: ratingView.rx.text)
            .disposed(by: disposeBag)

    }

    // MARK: - Actions

    @objc func addButtonWasPressed(sender: UIButton) {
        guard let reactor = reactor else { return }
        cellDelegate?.saveProduct(product: reactor.currentState.product)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanHistoryProductCollectionViewCell {
    fileprivate enum Color {
        static let containerViewBackgroundColor = Style.Color.white
        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitleTextColor = Style.Color.secondaryText
        static let ratingViewLabelTextColor = Style.Color.secondaryText ?? .black
        static let priceLabelTextColor = Style.Color.black
        static let actionButtonTintColor = Style.Color.green
        static let actionButtonBackgroundColor = Style.Color.green?.withAlphaComponent(0.1)
    }

    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 12
        static let actionButtonCornerRadius: CGFloat = 12
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
        static let descriptionStackViewSubtitleLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let ratingViewLabelFont = Style.Font.sanFrancisco(.semibold, size: 12)
        static let priceLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
    }

    fileprivate enum Image {
        static let actionButtonImage = Media.icon(.plus, size: .x18)
    }

    fileprivate enum Metric {
        static let headerViewLeftOffset: CGFloat = 12
        static let headerViewTopOffset: CGFloat = 12
        static let headerViewRightOffset: CGFloat = -12
        static let headerViewHeightRatio: CGFloat = 0.66
        static let imageContainerViewInset: CGFloat = 20
        static let actionButtonWidth: CGFloat = 64
        static let actionButtonHeight: CGFloat = 44
        static let priceLabelLeftOffset: CGFloat = 12
        static let priceLabelRightOffset: CGFloat = -12
        static let priceLabelBottomOffset: CGFloat = -16
        static let ratingViewLeftOffset: CGFloat = 12
        static let ratingViewBottomOffset: CGFloat = -10
        static let ratingViewStarPartSize: Double = 12
        static let ratingViewStarMaring: Double = 2
        static let descriptionStackViewLeftOffset: CGFloat = 12
        static let descriptionStackViewRightOffset: CGFloat = -12
        static let descriptionStackViewBottomOffset: CGFloat = -16
        static let descriptionStackViewSpacing: CGFloat = 2
    }
}
