//
//  ScanHistoryDateHeaderCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ScanHistoryDateHeaderCollectionViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(date: Date) {
        initialState = State(date: date)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScanHistoryDateHeaderCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ScanHistoryDateHeaderCollectionViewModel {
    struct State {
        let date: Date
    }
}
