//
//  ScanHistoryDateHeaderCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

final class ScanHistoryDateHeaderCollectionViewCell: BaseCollectionViewCell, View {

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    let subtitleLabel = UILabel().then {
        $0.font = Font.subtitleLabelFont
        $0.textColor = Color.subtitleLabelTextColor
        $0.textAlignment = .right
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
        addSubview(subtitleLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.bottom.equalToSuperview().offset(Metric.informationBottomOffset)
        }

        subtitleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleLabel.snp.right).offset(Metric.subtitleLabelLeftOffset)
            make.right.equalToSuperview().offset(Metric.subtitleLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.informationBottomOffset)
            make.width.equalTo(titleLabel)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ScanHistoryDateHeaderCollectionViewModel) {

        // Output

        reactor.state
            .map({ $0.date.stringBy(dateFormat: "d MMMM") })
            .map({ $0.lowercased() })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.date.stringBy(dateFormat: "EEEE")})
            .bind(to: subtitleLabel.rx.text)
            .disposed(by: disposeBag)

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanHistoryDateHeaderCollectionViewCell {

    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
        static let subtitleLabelTextColor = Style.Color.newGray
    }

    fileprivate enum CornerRadius {

    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 16)
        static let subtitleLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Image {

    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 20
        static let subtitleLabelRightOffset: CGFloat = -20
        static let subtitleLabelLeftOffset: CGFloat = 8
        static let informationBottomOffset: CGFloat = -12
    }

    fileprivate enum Reusable {

    }

}
