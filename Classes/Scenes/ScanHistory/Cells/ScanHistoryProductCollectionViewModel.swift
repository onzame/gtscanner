//
//  ScanHistoryProductCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ScanHistoryProductCollectionViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(scannedProduct: HistoryProductContainer) {
        initialState = State(product: scannedProduct.product)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScanHistoryProductCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ScanHistoryProductCollectionViewModel {
    struct State {
        let product: HistoryProduct
    }
}

// MARK: - Equatable

extension ScanHistoryProductCollectionViewModel: Equatable {
    static func == (lhs: ScanHistoryProductCollectionViewModel, rhs: ScanHistoryProductCollectionViewModel) -> Bool {
        return lhs.currentState == rhs.currentState
    }
}

// MARK: - State Equatable

extension ScanHistoryProductCollectionViewModel.State: Equatable {
    static func == (
        lhs: ScanHistoryProductCollectionViewModel.State,
        rhs: ScanHistoryProductCollectionViewModel.State
    ) -> Bool {
        return lhs.product == rhs.product
    }
}
