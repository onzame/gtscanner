//
//  ScanHistoryView.swift
//  Gorod
//
//  Created by Sergei Fabian on 03.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class ScanHistoryView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let refreshControl = UIRefreshControl()

    let savedProductsDetailsView = ProductsDetailsView(mode: .bright).then {
        $0.isUserInteractionEnabled = true
        $0.setCornerRadius(CornerRadius.savedProductsDetailsViewCornerRadius)
    }

    let collectionViewLayout = UICollectionViewFlowLayout().then {
        $0.minimumInteritemSpacing = Metric.collectionViewItemSpacing
        $0.minimumLineSpacing = Metric.collectionViewItemSpacing
        $0.sectionInset = UIEdgeInsets(
            top: Metric.collectionViewSectionVerticalSpacing,
            left: Metric.collectionViewSectionHorizontalSpacing,
            bottom: Metric.collectionViewSectionBottomSpacing,
            right: Metric.collectionViewSectionHorizontalSpacing
        )
    }

    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout).then {
        $0.alwaysBounceVertical = true
        $0.setBackgroundColor(Color.collectionViewBackgroundColor)
        $0.register(Reusable.dateHeaderView, kind: .header)
        $0.register(Reusable.emptyView, kind: "empty")
        $0.register(Reusable.scannedProductCell)
    }

    // MARK: - DataSource

    lazy var dataSource = RxCollectionViewSectionedReloadDataSource<ScanHistorySectionModel>(
        configureCell: { [weak self] (dataSource, collectionView, indexPath, itemModel) -> UICollectionViewCell in
            switch itemModel {
            case .product(let viewModel):
                let cell = collectionView.dequeue(Reusable.scannedProductCell, for: indexPath)
                cell.reactor = viewModel
                cell.cellDelegate = self
                return cell
            }
        },
        configureSupplementaryView: { (dataSource, collectionView, kind, indexPath) -> UICollectionReusableView in
            switch (dataSource[indexPath.section], kind) {
            case (.section(let viewModel, _), SupplementaryViewKind.header.rawValue):
                let cell = collectionView.dequeue(Reusable.dateHeaderView, kind: kind, for: indexPath)
                cell.reactor = viewModel
                return cell
            default:
                return collectionView.dequeue(Reusable.emptyView, kind: "empty", for: indexPath)
            }
    })

    // MARK: - Publishers

    let productSelectionActionPublisher = PublishSubject<HistoryProduct>()

    // MARK: - Gestures

    fileprivate let savedProductsDetailsTapGesture = UITapGestureRecognizer()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        addSubview(collectionView)
    }

    private func setupLayout() {
        collectionView.addEdgesConstraints()
    }

    private func setupView() {
        collectionView.refreshControl = refreshControl
        savedProductsDetailsView.addGestureRecognizer(savedProductsDetailsTapGesture)
    }

    private func setupBinding() {
        collectionView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

    // MARK: - SavedProductsDetailsView showing

    fileprivate func showSavedProductsDetailsView() {
        if savedProductsDetailsView.superview == nil {
            setupSavedProductsDetailsViewHierarchy()
            setupSavedProductsDetailsViewLayout()
            setupCollectionViewInsets()
        }
    }

    fileprivate func hideSavedProductsDetailsView() {
        if savedProductsDetailsView.superview != nil {
            savedProductsDetailsView.removeFromSuperview()
            collectionView.setBottomInset(.zero)
        }
    }

    fileprivate func setupSavedProductsDetailsViewHierarchy() {
        addSubview(savedProductsDetailsView)
    }

    fileprivate func setupSavedProductsDetailsViewLayout() {
        savedProductsDetailsView.snp.makeConstraints { (make) in
            make.left.equalTo(safeLeftConstraintItem).offset(Metric.savedProductsDetailsViewLeftOffset)
            make.right.equalTo(safeRightConstraintItem).offset(Metric.savedProductsDetailsViewRightOffset)
            make.bottom.equalTo(safeBottomConstraintItem).offset(Metric.savedProductsDetailsViewBottomOffset)
            make.height.equalTo(Metric.savedProductsDetailsViewHeight)
        }

        layoutIfNeeded()
    }

    fileprivate func setupCollectionViewInsets() {
        let collectionViewBottomInset = frame.maxY - savedProductsDetailsView.frame.minY
        collectionView.setBottomInset(collectionViewBottomInset)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanHistoryView {
    fileprivate enum Color {
        static let collectionViewBackgroundColor = Style.Color.lightBlue
    }

    fileprivate enum CornerRadius {
        static let savedProductsDetailsViewCornerRadius: CGFloat = 18
    }

    fileprivate enum Metric {
        static let collectionViewSectionHorizontalSpacing: CGFloat = 20
        static let collectionViewSectionVerticalSpacing: CGFloat = 0
        static let collectionViewSectionBottomSpacing: CGFloat = 8
        static let collectionViewMinItemWidth: CGFloat = 136
        static let collectionViewItemHeightRatio: CGFloat = 1.5
        static let collectionViewItemSpacing: CGFloat = 8
        static let collectionViewSectionHeaderHeight: CGFloat = 52
        static let collectionViewBottomOffset: CGFloat = 72
        static let savedProductsDetailsViewLeftOffset: CGFloat = 20
        static let savedProductsDetailsViewRightOffset: CGFloat = -20
        static let savedProductsDetailsViewBottomOffset: CGFloat = -20
        static let savedProductsDetailsViewHeight: CGFloat = 64
    }

    fileprivate enum Reusable {
        static let scannedProductCell = ReusableCell<ScanHistoryProductCollectionViewCell>()
        static let dateHeaderView = ReusableView<ScanHistoryDateHeaderCollectionViewCell>()
        static let emptyView = ReusableView<UICollectionReusableView>()
    }

}

// MARK: - UICollectionViewDelegateFlowLayout

extension ScanHistoryView: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let spaceWidth = collectionView.frame.size.width - Metric.collectionViewSectionHorizontalSpacing * 2
        var spaceWidthContainer = spaceWidth - (Metric.collectionViewMinItemWidth * 2 + Metric.collectionViewItemSpacing)
        var columnsAmount: CGFloat = 2

        let nextColumnWidth = Metric.collectionViewMinItemWidth + Metric.collectionViewItemSpacing

        while spaceWidthContainer > nextColumnWidth {
            columnsAmount += 1
            spaceWidthContainer -= nextColumnWidth
        }

        let itemWidth = (spaceWidth - Metric.collectionViewItemSpacing * (columnsAmount - 1)) / columnsAmount
        let itemHeight = itemWidth * Metric.collectionViewItemHeightRatio

        return CGSize(width: itemWidth, height: itemHeight)
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: Metric.collectionViewSectionHeaderHeight)
    }
}

// MARK: - ScanHistoryProductCollectionViewCellDelegate

extension ScanHistoryView: ScanHistoryProductCollectionViewCellDelegate {
    func saveProduct(product: HistoryProduct) {
        productSelectionActionPublisher.onNext(product)
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScanHistoryView {
    var productSelection: ControlEvent<HistoryProduct> {
        let source = base.collectionView.rx.itemSelected(dataSource: base.dataSource)
            .asObservable()
            .mapToHistoryProduct()
            .filterNil()

        return ControlEvent(events: source)
    }

    var showChangeAmountSceneAction: ControlEvent<HistoryProduct> {
        return ControlEvent(events: base.productSelectionActionPublisher)
    }

    var showScanListSceneAction: ControlEvent<Void> {
        return ControlEvent(events: base.savedProductsDetailsTapGesture.rx.event.mapToVoid())
    }

    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }

    var loadNextAction: ControlEvent<Void> {
        return ControlEvent(events: base.collectionView.rx.reachedBottom())
    }

    var showProductsDetailsBinding: Binder<Bool> {
        return Binder(base, binding: { viewController, shouldShow in
            if shouldShow {
                viewController.showSavedProductsDetailsView()
            } else {
                viewController.hideSavedProductsDetailsView()
            }
        })
    }
}
