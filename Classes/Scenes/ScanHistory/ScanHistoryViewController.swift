//
//  ScanHistoryViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

final class ScanHistoryViewController: BaseViewController<ScanHistoryViewModel> {

    // MARK: - UI components

    let contentView = ScanHistoryView()

    // MARK: - Publishers

    fileprivate let showScannerSceneActionPublisher = PublishSubject<Void>()
    fileprivate let retryActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.ScanHistory.title
        view.backgroundColor = Color.backgroundColor
    }

    // MARK: - Bindings

    override func bind(reactor: ScanHistoryViewModel) {

        // Input

        rx.viewDidAppear
            .take(1)
            .mapTo(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .mapTo(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.refreshAction
            .withLatestFrom(reactor.state)
            .filter({ !$0.isRefreshing })
            .mapTo(Reactor.Action.refresh)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.loadNextAction
            .withLatestFrom(reactor.state)
            .filter({ $0.pagination.hasNext && !$0.isLoadingNext })
            .map({ $0.pagination.nextPage })
            .filterNil()
            .map(Reactor.Action.loadNext)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .take(1)
            .mapTo(Reactor.Action.observeScannedProducts)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .take(1)
            .mapTo(Reactor.Action.observeSavedProducts)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.collectionView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ (flag: $0.showErrorState, error: $0.error) })
            .distinctUntilChanged({ $0.flag == $1.flag })
            .filter({ $0.flag })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showLoadingState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showDefaultState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showNoDataState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.savedProductsDetails })
            .filterNil()
            .distinctUntilChanged()
            .bind(to: contentView.savedProductsDetailsView.rx.update)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shouldShowSavedProductsDetails })
            .distinctUntilChanged()
            .bind(to: contentView.rx.showProductsDetailsBinding)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanHistoryViewController {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.lightBlue
    }

    fileprivate enum Image {
        static let noDataStateViewImage = Media.image(.circleError)
    }
}

// MARK: - StateResolvable

extension ScanHistoryViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        return ErrorStateView().then {
            $0.image = Image.noDataStateViewImage
            $0.title = Localization.ScanHistory.errorPlaceholderTitle
            $0.subtitle = Localization.ScanHistory.errorPlaceholderSubtitle
            $0.actionTitle = Localization.ScanHistory.errorPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.noDataStateViewImage
            $0.title = Localization.ScanHistory.noDataPlaceholderTitle
            $0.subtitle = Localization.ScanHistory.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.ScanHistory.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: showScannerSceneActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScanHistoryViewController {
    var productSelection: ControlEvent<HistoryProduct> {
        return base.contentView.rx.productSelection
    }

    var showChangeAmountSceneAction: ControlEvent<HistoryProduct> {
        return ControlEvent(events: base.contentView.rx.showChangeAmountSceneAction)
    }

    var showScanListSceneAction: ControlEvent<Void> {
        return base.contentView.rx.showScanListSceneAction
    }

    var showScannerSceneAction: ControlEvent<Void> {
        return ControlEvent(events: base.showScannerSceneActionPublisher)
    }
}
