//
//  ScanHistorySections.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources
import RxSwift
import RxCocoa

// MARK: - Sections

enum ScanHistorySectionModel {
    case section(viewModel: ScanHistoryDateHeaderCollectionViewModel, items: [ScanHistoryItemModel])
}

extension ScanHistorySectionModel: AnimatableSectionModelType {
    var identity: Date {
        switch self {
        case .section(viewModel: let viewModel, _):
            return viewModel.currentState.date
        }
    }

    var items: [ScanHistoryItemModel] {
        switch self {
        case .section(_, items: let items):
            return items
        }
    }

    init(original: ScanHistorySectionModel, items: [ScanHistoryItemModel]) {
        switch original {
        case .section(viewModel: let viewModel, _):
            self = .section(viewModel: viewModel, items: items)
        }
    }
}

// MARK: - Items

enum ScanHistoryItemModel {
    case product(ScanHistoryProductCollectionViewModel)
}

extension ScanHistoryItemModel: IdentifiableType {
    var identity: String {
        switch self {
        case .product(let viewModel):
            return viewModel.currentState.product.gtin
        }
    }
}

extension ScanHistoryItemModel: Equatable {
    static func == (lhs: ScanHistoryItemModel, rhs: ScanHistoryItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.product(let left), .product(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Observable where Element == ScanHistoryItemModel {
    func mapToHistoryProduct() -> Observable<HistoryProduct?> {
        return map({ model in
            if case ScanHistoryItemModel.product(let viewModel) = model {
                return viewModel.currentState.product
            } else {
                return nil
            }
        })
    }
}
