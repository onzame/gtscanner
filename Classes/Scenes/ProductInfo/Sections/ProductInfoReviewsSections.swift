//
//  ProductInfoReviewsSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources

// MARK: - Sections

enum ProductInfoReviewsSectionModel {
    case section(items: [ProductInfoReviewsItemModel])
}

extension ProductInfoReviewsSectionModel: AnimatableSectionModelType {
    var identity: String {
        return "section"
    }

    var items: [ProductInfoReviewsItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: ProductInfoReviewsSectionModel, items: [ProductInfoReviewsItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum ProductInfoReviewsItemModel {
    case review(ReviewPreviewCollectionViewModel)
    case placeholder
}

extension ProductInfoReviewsItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .review(let viewModel):
            return viewModel.currentState.review.id
        case .placeholder:
            return -1
        }
    }
}

extension ProductInfoReviewsItemModel: Equatable {
    static func == (lhs: ProductInfoReviewsItemModel, rhs: ProductInfoReviewsItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.review(let left), .review(let right)):
            return left.currentState == right.currentState
        case (.placeholder, .placeholder):
            return true
        default:
            return false
        }
    }
}

extension Array where Element == Review {
    func mapToSections() -> [ProductInfoReviewsSectionModel] {
        var items = map({ review -> ProductInfoReviewsItemModel in
            let viewModel = ReviewPreviewCollectionViewModel(review: review)
            let itemModel = ProductInfoReviewsItemModel.review(viewModel)
            return itemModel
        })

        if items.isEmpty {
            items.append(.placeholder)
        }

        return [ProductInfoReviewsSectionModel.section(items: items)]
    }
}
