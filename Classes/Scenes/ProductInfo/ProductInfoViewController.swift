//
//  ProductInfoViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 15.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

final class ProductInfoViewController: BaseViewController<ProductInfoViewModel> {

    // MARK: - UI components

    let contentView = ProductInfoView()

    // MARK: - Publisher

    let currentProductPublisher = BehaviorRelay<Product?>(value: nil)

    // MARK: - Properties

    private let locationSessionController = LocationSessionController.shared

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.addEdgesConstraints()
    }

    private func setupView() {

    }

    // MARK: - Bindings

    override func bind(reactor: ProductInfoViewModel) {

        let locationSessionSequence = locationSessionController.rx.location
            .mapToDomain()

        // Input

        rx.viewWillAppear
            .withLatestFrom(reactor.state)
            .map({ $0.product })
            .map(Reactor.Action.invokeProductFromStorage)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewDidAppear
            .take(1)
            .withLatestFrom(reactor.state)
            .withLatestFrom(locationSessionSequence) { (state, location) -> Reactor.Action in
                .refreshProduct(product: state.product, location: location)
            }
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.compareView.rx.triggerCompareStateAction
            .withLatestFrom(reactor.state)
            .map({ $0.product })
            .map(Reactor.Action.triggerCompareProductState)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewWillAppear
            .asObservable()
            .mapToVoid()
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewWillDisappear
            .asObservable()
            .mapToVoid()
            .bind(to: locationSessionController.rx.disableUpdating)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.product })
            .bind(to: currentProductPublisher)
            .disposed(by: disposeBag)

        let nameSequence = reactor.state
            .map({ $0.product.name })

        let locationSequence = reactor.state
            .map({ $0.product.productionPlace })

        nameSequence
            .mapToControllerTitle()
            .bind(to: rx.title)
            .disposed(by: disposeBag)

        nameSequence
            .bind(to: contentView.descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        locationSequence
            .bind(to: contentView.descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.avgPrice })
            .mapToPrice()
            .bind(to: contentView.priceStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.amounts.prices })
            .map({ Localization.Common.pricesAmount($0) })
            .bind(to: contentView.priceStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.lists.inCompare })
            .bind(to: contentView.compareView.rx.inCompare)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.rating })
            .mapToString()
            .bind(to: contentView.countersView.ratingAmountView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.amounts.rates })
            .mapToString()
            .bind(to: contentView.countersView.ratesCounterView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.amounts.reviews })
            .mapToString()
            .bind(to: contentView.countersView.reviewsCounterView.rx.title)
            .disposed(by: disposeBag)

        nameSequence
            .bind(to: contentView.nameStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.gtin })
            .bind(to: contentView.barcodeStackView.rx.subtitle)
            .disposed(by: disposeBag)

        locationSequence
            .bind(to: contentView.locationStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.source })
            .bind(to: contentView.sourceLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.product.permissions.hasAddedPrice })
            .mapToVoid()
            .bind(to: contentView.rx.enableAddPrice)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.product.permissions.hasAddedPrice })
            .map({ (price: $0.product.avgPrice.orZero, amount: $0.product.amounts.prices) })
            .bind(to: contentView.rx.showPrice)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.reviews.isNotEmpty })
            .distinctUntilChanged()
            .bind(to: contentView.rx.enableShowReviews)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.amounts.reviews - $0.product.reviews.count })
            .map({ Localization.ProductInfo.showReviewsActionTitle($0) })
            .bind(to: contentView.showReviewsButton.rx.title(for: .normal))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.images.map({ $0.url }) })
            .bind(to: contentView.carouselView.rx.images)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .distinctUntilChanged()
            .filter({ !$0 })
            .mapToVoid()
            .bind(to: contentView.carouselView.rx.refresh)
            .disposed(by: disposeBag)

        let collectionView = contentView.reviewsView.collectionView
        let dataSource = contentView.reviewsView.dataSource

        reactor.state
            .map({ $0.sections })
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.sections.first?.items.count ?? 0 })
            .bind(to: contentView.reviewsView.pageIndicator.rx.numberOfPages)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .distinctUntilChanged()
            .filter({ !$0 })
            .mapToVoid()
            .bind(to: contentView.reviewsView.pageIndicator.rx.refresh)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

    }

}

// MARK: - Extensions
// MARK: - Reactive interface

extension Reactive where Base: ProductInfoViewController {
    var showPricesListAction: ControlEvent<Product> {
        let source = base.contentView.rx.showPricesListAction.withLatestFrom(base.currentProductPublisher).filterNil()
        return ControlEvent(events: source)
    }

    var showReviewsListAction: ControlEvent<Product> {
        let source = base.contentView.rx.showReviewsListAction.withLatestFrom(base.currentProductPublisher).filterNil()
        return ControlEvent(events: source)
    }

    var showCompareAction: ControlEvent<Void> {
        return base.contentView.compareView.rx.showCompareSceneAction
    }

    var addPriceAction: ControlEvent<Product> {
        let source = base.contentView.rx.addPriceAction.withLatestFrom(base.currentProductPublisher).filterNil()
        return ControlEvent(events: source)
    }

    var addReviewAction: ControlEvent<Product> {
        let source = base.contentView.rx.addReviewAction.withLatestFrom(base.currentProductPublisher).filterNil()
        return ControlEvent(events: source)
    }

    var showProductsDetailsAction: ControlEvent<Product> {
        let source = base.contentView.rx.showProductDetailsAction.withLatestFrom(base.currentProductPublisher).filterNil()
        return ControlEvent(events: source)
    }

    var saveProductAction: ControlEvent<Product> {
        let source = base.contentView.rx.saveProductAction.withLatestFrom(base.currentProductPublisher).filterNil()
        return ControlEvent(events: source)
    }
}
