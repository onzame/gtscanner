//
//  ReviewPreviewCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ReviewPreviewCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.setBackgroundColor(Color.containerViewBackgroundColor)
        $0.setCornerRadius(CornerRadius.containerViewCornerRadius)
    }

    let avatarContainerView = ImageContainerView().then {
        $0.setCornerRadius(CornerRadius.avatarContainerViewCornerRadius)
    }

    let nameLabel = UILabel().then {
        $0.font = Font.nameLabelFont
        $0.textColor = Color.nameLabelTextColor
    }

    let ratingView = RatingView().then {
        $0.settings.updateOnTouch = false
        $0.settings.filledImage = Image.ratingViewFilledImage
        $0.settings.emptyImage = Image.ratingViewEmptyImage
        $0.settings.starMargin = Metric.ratingViewStarMargin
        $0.settings.starSize = Metric.ratingViewStarSize
    }

    let informationView = UIView()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func prepareForReuse() {
        super.prepareForReuse()

        avatarContainerView.imageView.kf.prepareForReuse()
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(containerView)

        containerView.addSubview(avatarContainerView)
        containerView.addSubview(informationView)

        informationView.addSubview(nameLabel)
        informationView.addSubview(ratingView)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        avatarContainerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.avatarContainerViewLeftOffset)
            make.top.equalToSuperview().offset(Metric.avatarContainerViewTopOffset)
            make.width.height.equalTo(Metric.avatarContainerViewPartSize)
        }

        informationView.snp.makeConstraints { (make) in
            make.left.equalTo(avatarContainerView.snp.right).offset(Metric.informationViewLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.informationViewRightOffset)
            make.centerY.equalTo(avatarContainerView)
        }

        nameLabel.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }

        ratingView.snp.makeConstraints { (make) in
            make.left.bottom.equalToSuperview()
            make.top.equalTo(nameLabel.snp.bottom).offset(Metric.ratingViewTopOffset)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ReviewPreviewCollectionViewModel) {
        let placeholderSize = CGSize(squareSide: Metric.avatarContainerViewPartSize)
        let placeholder = ImagePlaceholderView(image: Image.avatarContainerViewPlaceholder, imageSize: placeholderSize)

        reactor.state
            .map({ $0.review.author.avatarUrl })
            .bind(to: avatarContainerView.imageView.rx.image(placeholder: placeholder))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.author.name.orPlaceholder(Localization.ProductInfo.reviewAuthorNamePlaceholder) })
            .bind(to: nameLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.rating })
            .bind(to: ratingView.rx.rating)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ReviewPreviewCollectionViewCell {
    fileprivate enum Color {
        static let containerViewBackgroundColor = Style.Color.lightBlue
        static let nameLabelTextColor = Style.Color.black
    }

    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 30
        static let avatarContainerViewCornerRadius: CGFloat = 19
    }

    fileprivate enum Font {
        static let nameLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Image {
        static let ratingViewFilledImage = Media.image(.starFatFilled)
        static let ratingViewEmptyImage = Media.image(.starFat)
        static let avatarContainerViewPlaceholder = Media.image(.avatarPlaceholder)
    }

    fileprivate enum Metric {
        static let avatarContainerViewLeftOffset: CGFloat = 20
        static let avatarContainerViewTopOffset: CGFloat = 20
        static let avatarContainerViewPartSize: CGFloat = 38

        static let informationViewLeftOffset: CGFloat = 12
        static let informationViewRightOffset: CGFloat = -12

        static let ratingViewTopOffset: CGFloat = 4
        static let ratingViewStarMargin: Double = 5
        static let ratingViewStarSize: Double = 10
    }
}
