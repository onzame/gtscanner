//
//  ReviewPlaceholderCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ReviewPlaceholderCollectionViewCell: BaseCollectionViewCell {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.setBackgroundColor(Color.containerViewBackgroundColor)
        $0.setCornerRadius(CornerRadius.containerViewCornerRadius)
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.text = Localization.ProductInfo.reviewEmptyTitle
        $0.titleLabel.font = Font.titleLabelFont
        $0.titleLabel.textColor = Color.titleLabelTextColor
        $0.titleLabel.textAlignment = .center
        $0.titleLabel.numberOfLines = 0
        $0.subtitleLabel.text = Localization.ProductInfo.reviewEmptySubtitle
        $0.subtitleLabel.font = Font.subtitleLabelFont
        $0.subtitleLabel.textColor = Color.subtitleLabelTextColor
        $0.subtitleLabel.textAlignment = .center
        $0.subtitleLabel.numberOfLines = 0
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(containerView)

        containerView.addSubview(descriptionStackView)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.left.greaterThanOrEqualToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.descriptionStackViewRightOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ReviewPlaceholderCollectionViewCell {
    fileprivate enum Color {
        static let containerViewBackgroundColor = Style.Color.lightBlue
        static let titleLabelTextColor = Style.Color.black
        static let subtitleLabelTextColor = UIColor(hex: "#706E8B")
    }

    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 30
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 18)
        static let subtitleLabelFont = Style.Font.sanFrancisco(.regular, size: 12)
    }

    fileprivate enum Metric {
        static let descriptionStackViewLeftOffset: CGFloat = 20
        static let descriptionStackViewRightOffset: CGFloat = -20
        static let descriptionStackViewSpacing: CGFloat = 12
    }
}
