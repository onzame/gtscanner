//
//  ReviewSignCollectionView.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.04.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ReviewSignCollectionView: ReviewPreviewCollectionViewCell {

    // MARK: - UI components

    let positiveImageView = UIImageView().then {
        $0.image = Image.positiveImageViewImage
    }

    let positiveLabel = UILabel().then {
        $0.font = Font.positiveLabelFont
        $0.textColor = Color.positiveLabelTextColor
    }

    let negativeImageView = UIImageView().then {
        $0.image = Image.negativeImageViewImage
    }

    let negativeLabel = UILabel().then {
        $0.font = Font.negativeLabelFont
        $0.textColor = Color.negativeLabelTextColor
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Override

    override func prepareForReuse() {
        super.prepareForReuse()
        resetLayout()
    }

    // MARK: - Settings

    private func commonInit() {

    }

    private func resetLayout() {
        positiveImageView.removeFromSuperview()
        positiveLabel.removeFromSuperview()
        negativeImageView.removeFromSuperview()
        negativeLabel.removeFromSuperview()
    }

    fileprivate func setupLayout(for review: Review) {
        resetLayout()

        if let text = review.positiveText, text.isNotEmpty {
            addBodyElement(imageView: positiveImageView, label: positiveLabel, under: avatarContainerView, offset: Metric.headerToBodyOffset)
        }

        if let text = review.negativeText, text.isNotEmpty {
            if positiveLabel.superview != nil {
                addBodyElement(imageView: negativeImageView, label: negativeLabel, under: positiveLabel, offset: Metric.bodyMargin)
            } else {
                addBodyElement(imageView: negativeImageView, label: negativeLabel, under: avatarContainerView, offset: Metric.headerToBodyOffset)
            }
        }
    }

    // MARK: - Bindings

    override func bind(reactor: ReviewPreviewCollectionViewModel) {
        super.bind(reactor: reactor)
        
        reactor.state
            .map({ $0.review.positiveText })
            .bind(to: positiveLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.negativeText })
            .bind(to: negativeLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review })
            .bind(to: rx.constraintsBinding)
            .disposed(by: disposeBag)
    }

    // MARK: - Utils

    private func addBodyElement(imageView: UIImageView, label: UILabel, under view: UIView, offset: CGFloat) {
        addSubview(imageView)
        addSubview(label)

        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.bottom).offset(offset)
            make.centerX.equalTo(avatarContainerView.imageView)
        }

        label.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.bottom).offset(offset)
            make.left.equalTo(informationView)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }
    }
}

// MARK: - Extensions
// MARK: - Constants

extension ReviewSignCollectionView {
    fileprivate enum Color {
        static let positiveLabelTextColor = Style.Color.black
        static let negativeLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let positiveLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let negativeLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Image {
        static let positiveImageViewImage = Media.icon(.plus, size: .x18)?.tint(with: UIColor(hex: "#26CB5A"))
        static let negativeImageViewImage = Media.icon(.minus, size: .x18)?.tint(with: UIColor(hex: "#ED4C34"))
    }

    fileprivate enum Metric {
        static let contentRightOffset: CGFloat = -20
        static let headerToBodyOffset: CGFloat = 20
        static let bodyMargin: CGFloat = 4
    }
}

// MARK: - Reactive extension

extension Reactive where Base: ReviewSignCollectionView {
    var constraintsBinding: Binder<Review> {
        return Binder(base, binding: { view, review in
            view.setupLayout(for: review)
        })
    }
}
