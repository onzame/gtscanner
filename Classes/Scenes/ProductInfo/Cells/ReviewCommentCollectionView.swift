//
//  ReviewCommentCollectionView.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.04.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ReviewCommentCollectionView: ReviewPreviewCollectionViewCell {

    // MARK: - UI components
    
    let textView = UITextView().then {
        $0.font = Font.textViewFont
        $0.textColor = Color.textViewTextColor
        $0.isEditable = false
        $0.isScrollEnabled = false
        $0.setBackgroundColor(Color.textViewBackgroundColor)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        containerView.addSubview(textView)
    }

    private func setupLayout() {
        textView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.textLabelLeftOffset)
            make.top.equalTo(avatarContainerView.snp.bottom).offset(Metric.textLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.textLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.textLabelBottomOffset)
        }
    }

    // MARK: - Bindings

    override func bind(reactor: ReviewPreviewCollectionViewModel) {
        super.bind(reactor: reactor)

        reactor.state
            .map({ $0.review.text })
            .bind(to: textView.rx.text)
            .disposed(by: disposeBag)
    }
}

// MARK: - Extensions
// MARK: - Constants

extension ReviewCommentCollectionView {
    fileprivate enum Color {
        static let textViewTextColor = UIColor(hex: "#989898")
        static let textViewBackgroundColor = UIColor.clear
    }

    fileprivate enum Font {
        static let textViewFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Metric {
        static let textLabelLeftOffset: CGFloat = 20
        static let textLabelTopOffset: CGFloat = 16
        static let textLabelRightOffset: CGFloat = -20
        static let textLabelBottomOffset: CGFloat = -20
    }
}
