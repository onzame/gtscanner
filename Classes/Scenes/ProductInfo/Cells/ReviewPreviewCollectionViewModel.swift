//
//  ReviewPreviewCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 20.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ReviewPreviewCollectionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(review: Review) {
        initialState = State(review: review)
    }
}

// MARK: - Extensions
// MARK: - Actions

extension ReviewPreviewCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ReviewPreviewCollectionViewModel {
    struct State {
        let review: Review
    }
}

// MARK: - Equatable

extension ReviewPreviewCollectionViewModel.State: Equatable {
    static func == (lhs: ReviewPreviewCollectionViewModel.State, rhs: ReviewPreviewCollectionViewModel.State) -> Bool {
        return lhs.review == rhs.review
    }
}
