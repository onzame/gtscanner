//
//  ProductInfoViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 15.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift
import RxCocoa

final class ProductInfoViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let productsService: ProductsServiceType
    private let comparesService: ComparesServiceType
    private let favoritesService: FavoritesServiceType

    // MARK: - Lifecycle

    init(
        productsService: ProductsServiceType,
        comparesService: ComparesServiceType,
        favoritesService: FavoritesServiceType,
        input: ProductInfoInput
    ) {
        self.productsService = productsService
        self.comparesService = comparesService
        self.favoritesService = favoritesService
        self.initialState = State(product: input.product, sections: input.product.reviews.mapToSections())
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .refreshProduct(product: let product, location: let location):
            return productsService.fetchProduct(product: product, location: location)
                .map(Mutation.updateProduct)
                .catchError({ .just(.showError($0)) })
                .startWith(.setLoading(true))

        case .invokeProductFromStorage(product: let product):
            return favoritesService.observeSavedProducts()
                .take(1)
                .map({ Mutation.invokeProductFromStorage(product: product, aggregations: $0) })

        case .updateProduct(let product):
            return productsService.updatedProduct(product: product)
                .andThen(.just(Mutation.updateProduct(product)))

        case .triggerCompareProductState(let product):
            if product.lists.inCompare {
                return comparesService.deleteProduct(product: product)
                    .andThen(.just(Mutation.changeProductState(product: product, inCompare: false)))
                    .catchError({ .just(.showError($0)) })
                    .startWith(.setLoading(true))
            } else {
                return comparesService.addProduct(product: product)
                    .andThen(.just(Mutation.changeProductState(product: product, inCompare: true)))
                    .catchError({ .just(.showError($0)) })
                    .startWith(.setLoading(true))
            }
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .stub:
            break

        case .updateProduct(let product):
            state.product = product
            state.sections = product.reviews.mapToSections()
            state.isLoading = false

        case .setLoading(let isLoading):
            state.isLoading = isLoading

        case .showError(let error):
            state.error = error
            state.isLoading = false
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductInfoViewModel {
    enum Action {
        case refreshProduct(product: Product, location: Location?)
        case invokeProductFromStorage(Product)
        case updateProduct(Product)
        case triggerCompareProductState(Product)
    }
}

// MARK: - Mutations

extension ProductInfoViewModel {
    enum Mutation {
        case stub
        case updateProduct(Product)
        case setLoading(Bool)
        case showError(Error)

        static func changeProductState(product: Product, inCompare: Bool) -> Mutation {
            let updatetLists = ProductLists(inFavorites: product.lists.inFavorites, inCompare: inCompare)
            let updatedProduct = product.update(updates: CompareProductUpdates(id: .zero, lists: updatetLists))
            return .updateProduct(updatedProduct)
        }

        static func invokeProductFromStorage(product: Product, aggregations: [FavoriteProductsAggregation]) -> Mutation {
            let optionalCategory = aggregations.first { aggregation -> Bool in
                aggregation.category.id == product.category.id
            }

            guard let category = optionalCategory else { return .stub }

            let optionalFavoriteProduct = category.products.first { favoriteProduct -> Bool in
                favoriteProduct.product.id == product.id
            }

            guard let favoriteProduct = optionalFavoriteProduct else { return .stub }

            return .updateProduct(favoriteProduct.product)
        }
    }
}

// MARK: - State

extension ProductInfoViewModel {
    struct State {
        var product: Product
        var sections: [ProductInfoReviewsSectionModel]
        var isLoading: Bool = false
        var error: Error?
    }
}
