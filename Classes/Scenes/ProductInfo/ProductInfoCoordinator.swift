//
//  ProductInfoCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 15.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

private protocol ExtendedProductInfoCoordinator { }

// swiftlint:disable line_length
private extension ExtendedProductInfoCoordinator where Self: CoordinatorType {
    func showPricesListScene(navigationController: UINavigationController, container: DependenciesContainer, input: ProductPricesListInput) -> Observable<ProductPricesListCoordinationResult> {
        let coordinator = ProductPricesListCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    func showReviewsListScene(navigationController: UINavigationController, container: DependenciesContainer, input: ProductReviewsListInput) -> Observable<ProductReviewsListCoordinationResult> {
        let coordinator = ProductReviewsListCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    func showProductDetailsScene(navigationController: UINavigationController, container: DependenciesContainer, input: ProductDetailsInput) -> Observable<ProductDetailsCoordinator.CoordinationResult> {
        let coordinator = ProductDetailsCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    func showProductAmountScene(viewController: UIViewController, container: DependenciesContainer, input: ProductAmountInput) -> Observable<ModalProductAmountCoordinationResult> {
        let coordinator = ModalProductAmountCoordinator(rootViewController: viewController, container: container, input: input)
        return coordinate(to: coordinator)
    }
}
// swiftlint:enable line_length

enum ProductInfoCoordinationResult {
    case popped
    case dismissed
}

final class ProductInfoCoordinator: Coordinator<ProductInfoCoordinationResult>, ExtendedProductInfoCoordinator, ProductModificationCoordinatable {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer
    private let input: ProductInfoInput

    private let actionPublisher = PublishSubject<ProductInfoViewModel.Action>()

    init(rootNavigationController: UINavigationController, container: DependenciesContainer, input: ProductInfoInput) {
        self.rootNavigationController = rootNavigationController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<ProductInfoCoordinationResult> {
        let viewModel = container.productInfoViewModel(input: input)
        let viewController = ProductInfoViewController(reactor: viewModel)

        rootNavigationController.pushViewController(viewController, animated: true)

        actionPublisher
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)

        viewController.rx.showPricesListAction
            .flatMap({ [weak self] product -> Observable<ProductPricesListCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductPricesListInput(product: product)
                return self.showPricesListScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .subscribe(onNext: { [weak self] coordinationResult in
                switch coordinationResult {
                case .popped(let product):
                    self?.actionPublisher.onNext(.updateProduct(product))
                }
            })
            .disposed(by: disposeBag)

        viewController.rx.showReviewsListAction
            .flatMap({ [weak self] product -> Observable<ProductReviewsListCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductReviewsListInput(product: product)
                return self.showReviewsListScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .subscribe(onNext: { [weak self] coordinationResult in
                switch coordinationResult {
                case .popped(let product):
                    self?.actionPublisher.onNext(.updateProduct(product))
                }
            })
            .disposed(by: disposeBag)

        viewController.rx.addPriceAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductPriceFlowMetadata(product: product, selectedPrice: nil, selectedShop: nil)

                self.startProductPriceRecursiveFlow(
                    viewController: viewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.addReviewAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductReviewFlowMetadata(product: product, selectedRating: nil, selectedShop: nil, reviewText: .stub)

                self.startProductReviewRecursiveFlow(
                    viewController: viewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.showProductsDetailsAction
            .flatMap({ [weak self] product -> Observable<ProductDetailsCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductDetailsInput(product: product)
                return self.showProductDetailsScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.saveProductAction
            .flatMap({ [weak self] product -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: product, amount: nil, mode: .add)
                return self.showProductAmountScene(viewController: viewController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        // ProductInfoCoordinationResult.dismissed

        let dismissedEvent = viewController.rx.showCompareAction
            .flatMap({ [weak self] _ -> Observable<CompareProductsCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = CompareProductsInput.product(self.input.product)
                return self.showCompareScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                switch coordinationResult {
                case .dismissed:
                    return true
                default:
                    return false
                }
            })
            .mapTo(ProductInfoCoordinationResult.dismissed)

        // ProductInfoCoordinationResult.popped

        let poppedEvent = viewController.rx.popAction
            .mapTo(ProductInfoCoordinationResult.popped)

        return Observable.merge(dismissedEvent, poppedEvent)
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .product, inCategory: .info, tag: .none)
            })
            .take(1)
    }

    private func showCompareScene(
        navigationController: UINavigationController,
        container: DependenciesContainer,
        input: CompareProductsInput
    ) -> Observable<CompareProductsCoordinationResult> {
        let coordinator = CompareProductsCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    // MARK: - ProductModificationCoordinatable

    func updateProduct(type: ProductUpdateResultType) {
        if case ProductUpdateResultType.price(let metadata) = type {
            actionPublisher.onNext(ProductInfoViewModel.Action.updateProduct(metadata.product))
        } else if case ProductUpdateResultType.review(let metadata) = type {
            actionPublisher.onNext(ProductInfoViewModel.Action.updateProduct(metadata.product))
        }
    }

}

final class ModalProductInfoCoordinator: Coordinator<Void>, ExtendedProductInfoCoordinator, ProductModificationCoordinatable {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let input: ProductInfoInput

    private let actionPublisher = PublishSubject<ProductInfoViewModel.Action>()

    init(rootViewController: UIViewController, container: DependenciesContainer, input: ProductInfoInput) {
        self.rootViewController = rootViewController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<Void> {
        let viewModel = container.productInfoViewModel(input: input)
        let viewController = ProductInfoViewController(reactor: viewModel)
        let navigationController = BaseNavigationController(rootViewController: viewController)

        rootViewController.present(navigationController, animated: true)

        actionPublisher
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)

        viewController.rx.showPricesListAction
            .flatMap({ [weak self] product -> Observable<ProductPricesListCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductPricesListInput(product: product)
                return self.showPricesListScene(navigationController: navigationController, container: self.container, input: input)
            })
            .subscribe(onNext: { [weak self] coordinationResult in
                switch coordinationResult {
                case .popped(let product):
                    self?.actionPublisher.onNext(.updateProduct(product))
                }
            })
            .disposed(by: disposeBag)

        viewController.rx.showReviewsListAction
            .flatMap({ [weak self] product -> Observable<ProductReviewsListCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductReviewsListInput(product: product)
                return self.showReviewsListScene(navigationController: navigationController, container: self.container, input: input)
            })
            .subscribe(onNext: { [weak self] coordinationResult in
                switch coordinationResult {
                case .popped(let product):
                    self?.actionPublisher.onNext(.updateProduct(product))
                }
            })
            .disposed(by: disposeBag)

        viewController.rx.addPriceAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductPriceFlowMetadata(product: product, selectedPrice: nil, selectedShop: nil)

                self.startProductPriceRecursiveFlow(
                    viewController: viewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.addReviewAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductReviewFlowMetadata(product: product, selectedRating: nil, selectedShop: nil, reviewText: .stub)

                self.startProductReviewRecursiveFlow(
                    viewController: viewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.showProductsDetailsAction
            .flatMap({ [weak self] product -> Observable<ProductDetailsCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductDetailsInput(product: product)
                return self.showProductDetailsScene(navigationController: navigationController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.saveProductAction
            .flatMap({ [weak self] product -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: product, amount: nil, mode: .add)
                return self.showProductAmountScene(viewController: viewController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        let showScannerEvent = viewController.rx.showCompareAction
            .flatMap({ [weak self] _ -> Observable<CompareProductsCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = CompareProductsInput.product(self.input.product)
                return self.showCompareScene(navigationController: navigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                switch coordinationResult {
                case .dismissed:
                    return true
                default:
                    return false
                }
            })
            .mapToVoid()

        let dismissEvent = viewController.rx.dismissAction
            .do(onNext: {
                navigationController.dismiss(animated: true)
            })

        return Observable.merge(showScannerEvent, dismissEvent)
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .product, inCategory: .info, tag: .none)
            })
            .take(1)
    }

    private func showCompareScene(
        navigationController: UINavigationController,
        container: DependenciesContainer,
        input: CompareProductsInput
    ) -> Observable<CompareProductsCoordinationResult> {
        let coordinator = CompareProductsCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    // MARK: - ProductModificationCoordinatable

    func updateProduct(type: ProductUpdateResultType) {
        if case ProductUpdateResultType.price(let metadata) = type {
            actionPublisher.onNext(ProductInfoViewModel.Action.updateProduct(metadata.product))
        } else if case ProductUpdateResultType.review(let metadata) = type {
            actionPublisher.onNext(ProductInfoViewModel.Action.updateProduct(metadata.product))
        }
    }

}
