//
//  ProductInfoView.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductInfoView: UIView {

    // MARK: - UI components

    let scrollView = UIScrollView().then {
        $0.setBottomInset(Metric.scrollViewBottomInset)
        $0.setBackgroundColor(Color.scrollViewBackgroundColor)
    }

    let saveActionView = CompletionActionComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductInfo.saveProductActionTitle, for: .normal)
    }

    // Scroll view components

    let contentView = UIView().then {
        $0.setBackgroundColor(Color.contentViewBackgroundColor)
    }

    let carouselView = CarouselView().then {
        $0.setBackgroundColor(Color.carouselViewBackgroundColor)
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.titleLabel.numberOfLines = 0
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleTextColor
    }

    let priceStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.priceStackViewTitleFont
        $0.titleLabel.textColor = Color.priceStackViewTitleTextColor
        $0.titleLabel.textAlignment = .right
        $0.subtitleLabel.font = Font.priceStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.priceStackViewSubtitleTextColor
        $0.subtitleLabel.textAlignment = .right
        $0.isUserInteractionEnabled = true
    }

    let showPricesButton = UIButton(type: .system).then {
        $0.setImage(Image.showPricesButtonImage, for: .normal)
        $0.setTintColor(Color.showPricesButtonTintColor)
    }

    let addPriceButton = UIButton(type: .system).then {
        $0.setTitle(Localization.ProductInfo.addPriceActionTitle, for: .normal)
        $0.setTitleFont(Font.addPriceButtonTitleFont)
        $0.setTitleColor(Color.addPriceButtonTitleColor, for: .normal)
        $0.setCornerRadius(CornerRadius.addPriceButtonCornerRadius)
        $0.setBackgroundColor(Color.addPriceButtonBackgroundColor)
        $0.setHorizontalInsets(Metric.addPriceButtonHorizontalInset)
    }

    let compareView = ProductInfoCompareView()

    let countersView = ProductInfoCountersView().then {
        $0.isUserInteractionEnabled = true
    }

    let ratingControlView = RatingTitledControlView().then {
        $0.titleLabel.text = Localization.ProductInfo.ratingEmptyState
        $0.ratingView.settings.updateOnTouch = false
    }

    let headerDividerView = UIView().then {
        $0.setBackgroundColor(Color.contentDividerBackgroundColor)
    }

    let reviewsTitleLabel = UILabel().then {
        $0.text = Localization.ProductInfo.reviewsTitle
        $0.font = Font.contentTitleLabelFont
        $0.textColor = Color.contentTitleLabelTextColor
    }

    let showReviewsButton = UIButton(type: .system).then {
        $0.setTitleFont(Font.contentActionTitleFont)
        $0.setTitleColor(Color.contentActionTitleColor, for: .normal)
        $0.setBackgroundColor(Color.conentActionBackgroundColor)
        $0.setHorizontalInsets(Metric.contentActionHorizontalInset)
        $0.setCornerRadius(CornerRadius.contentActionCornerRadius, masksToBounds: false)
        $0.setupShadow(.longBlue)
    }

    let reviewsView = ProductInfoReviewsView()

    let addReviewButton = UIButton(type: .system).then {
        $0.setTitle(Localization.ProductInfo.addReviewActionTitle, for: .normal)
        $0.setTitleFont(Font.addReviewButtonTitleFont)
        $0.setTitleColor(Color.addReviewButtonTitleColor, for: .normal)
        $0.setCornerRadius(CornerRadius.addReviewButtonCornerRadius)
        $0.setBackgroundColor(Color.addReviewButtonBackgroundColor)
    }

    let bodyDividerView = UIView().then {
        $0.setBackgroundColor(Color.contentDividerBackgroundColor)
    }

    let footerTitleLabel = UILabel().then {
        $0.text = Localization.ProductInfo.aboutTitle
        $0.font = Font.contentTitleLabelFont
        $0.textColor = Color.contentTitleLabelTextColor
    }

    let showDetailsButton = UIButton(type: .system).then {
        $0.setTitle(Localization.ProductInfo.showDetailsActionTitle, for: .normal)
        $0.setTitleFont(Font.contentActionTitleFont)
        $0.setTitleColor(Color.contentActionTitleColor, for: .normal)
        $0.setBackgroundColor(Color.conentActionBackgroundColor)
        $0.setHorizontalInsets(Metric.contentActionHorizontalInset)
        $0.setCornerRadius(CornerRadius.contentActionCornerRadius, masksToBounds: false)
        $0.setupShadow(.longBlue)
    }

    let nameStackView = DescriptionStackView().then {
        $0.titleLabel.text = Localization.ProductInfo.nameTitle
        $0.titleLabel.font = Font.footerTitleFont
        $0.titleLabel.textColor = Color.footerTitleTextColor
        $0.subtitleLabel.font = Font.footerSubtitleFont
        $0.subtitleLabel.textColor = Color.footerSubtitleTextColor
        $0.subtitleLabel.numberOfLines = 0
    }

    let barcodeStackView = DescriptionStackView().then {
        $0.titleLabel.text = Localization.ProductInfo.barcodeTitle
        $0.titleLabel.font = Font.footerTitleFont
        $0.titleLabel.textColor = Color.footerTitleTextColor
        $0.subtitleLabel.font = Font.footerSubtitleFont
        $0.subtitleLabel.textColor = Color.footerSubtitleTextColor
        $0.subtitleLabel.numberOfLines = 0
    }

    let locationStackView = DescriptionStackView().then {
        $0.titleLabel.text = Localization.ProductInfo.locationTitle
        $0.titleLabel.font = Font.footerTitleFont
        $0.titleLabel.textColor = Color.footerTitleTextColor
        $0.subtitleLabel.font = Font.footerSubtitleFont
        $0.subtitleLabel.textColor = Color.footerSubtitleTextColor
        $0.subtitleLabel.numberOfLines = 0
    }

    let sourceLabel = UILabel().then {
        $0.font = Font.sourceLabelFont
        $0.textColor = Color.sourceTextColor
        $0.numberOfLines = 0
    }

    let footerStackView = UIStackView().then {
        $0.axis = .vertical
        $0.spacing = Metric.footerStackViewSpacing
    }

    // MARK: - Gestures

    let priceTapGesture = UITapGestureRecognizer()
    let countersTapGesture = UITapGestureRecognizer()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(scrollView)
        addSubview(saveActionView)

        scrollView.addSubview(contentView)

        contentView.addSubview(carouselView)
        contentView.addSubview(descriptionStackView)
        contentView.addSubview(compareView)
        contentView.addSubview(countersView)
        contentView.addSubview(ratingControlView)
        contentView.addSubview(headerDividerView)
        contentView.addSubview(reviewsTitleLabel)
        contentView.addSubview(reviewsView)
        contentView.addSubview(addReviewButton)
        contentView.addSubview(bodyDividerView)
        contentView.addSubview(footerTitleLabel)
        contentView.addSubview(showDetailsButton)
        contentView.addSubview(footerStackView)
        contentView.addSubview(sourceLabel)

        footerStackView.addArrangedSubview(nameStackView)
        footerStackView.addArrangedSubview(barcodeStackView)
        footerStackView.addArrangedSubview(locationStackView)
    }

    private func setupLayout() {
        scrollView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(safeBottomConstraintItem)
        }

        saveActionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }

        contentView.snp.makeConstraints { (make) in
            make.edges.width.equalToSuperview()
            make.height.equalToSuperview().priority(.init(250))
        }

        carouselView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalToSuperview().offset(Metric.carouselViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
            make.height.equalTo(Metric.carouselViewHeight)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(carouselView.snp.bottom).offset(Metric.descriptionStackViewTopOffset)
            make.height.greaterThanOrEqualTo(Metric.descriptionStackViewMinHeight)
        }

        compareView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(descriptionStackView.snp.bottom).offset(Metric.compareViewTopOffset)
            make.height.equalTo(Metric.compareViewHeight)
        }

        countersView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(compareView.snp.bottom).offset(Metric.countersViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        ratingControlView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(countersView.snp.bottom).offset(Metric.ratingControlViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        headerDividerView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(ratingControlView.snp.bottom).offset(Metric.contentDividerTopOffset)
            make.height.equalTo(Metric.contentDividerHeight)
        }

        reviewsTitleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(headerDividerView.snp.bottom).offset(Metric.reviewsTitleLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        reviewsView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(reviewsTitleLabel.snp.bottom).offset(Metric.reviewsCollectionViewTopOffset)
            make.height.equalTo(Metric.reviewsCollectionViewHeight)
        }

        addReviewButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(reviewsView.snp.bottom).offset(Metric.addReviewButtonTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
            make.height.equalTo(Metric.addReviewButtonHeight)
        }

        bodyDividerView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(addReviewButton.snp.bottom).offset(Metric.contentDividerTopOffset)
            make.height.equalTo(Metric.contentDividerHeight)
        }

        footerTitleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(bodyDividerView.snp.bottom).offset(Metric.footerTitleLabelTopOffset)
        }

        showDetailsButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(footerTitleLabel)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
            make.height.equalTo(Metric.contentActionHeight)
            make.left.greaterThanOrEqualTo(footerTitleLabel.snp.right)
        }

        footerStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(footerTitleLabel.snp.bottom).offset(Metric.footerStackViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        sourceLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(footerStackView.snp.bottom).offset(Metric.sourceLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
            make.bottom.equalToSuperview().offset(Metric.sourceLabelBottomOffset)
        }
    }

    private func setupView() {
        setBackgroundColor(Color.backgroundColor)

        priceStackView.addGestureRecognizer(priceTapGesture)
        countersView.addGestureRecognizer(countersTapGesture)
    }

    // MARK: - Private methods

    fileprivate func enableAddPrice() {
        if addPriceButton.superview == nil {

            showPricesButton.removeFromSuperview()
            priceStackView.removeFromSuperview()

            contentView.addSubview(addPriceButton)

            descriptionStackView.snp.remakeConstraints { (make) in
                make.left.equalToSuperview().offset(Metric.contentLeftOffset)
                make.top.equalTo(carouselView.snp.bottom).offset(Metric.descriptionStackViewTopOffset)
                make.height.greaterThanOrEqualTo(Metric.descriptionStackViewDefaultMinHeight)
            }

            addPriceButton.snp.remakeConstraints { (make) in
                make.top.equalTo(descriptionStackView)
                make.right.equalToSuperview().offset(Metric.contentRightOffset)
                make.height.equalTo(Metric.addPriceButtonHeight)
                make.left.greaterThanOrEqualTo(descriptionStackView.snp.right).offset(Metric.addPriceButtonLeftOffset)
                make.width.equalTo(Metric.addPriceButtonWidth)
            }

            layoutIfNeeded()
        }
    }

    fileprivate func showPrice(price: Double, amount: Int) {
        if priceStackView.superview == nil && showPricesButton.superview == nil {

            addPriceButton.removeFromSuperview()

            contentView.addSubview(priceStackView)
            contentView.addSubview(showPricesButton)

            descriptionStackView.snp.remakeConstraints { (make) in
                make.left.equalToSuperview().offset(Metric.contentLeftOffset)
                make.top.equalTo(carouselView.snp.bottom).offset(Metric.descriptionStackViewTopOffset)
                make.height.greaterThanOrEqualTo(Metric.descriptionStackViewMinHeight)
            }

            showPricesButton.snp.remakeConstraints { (make) in
                make.right.equalToSuperview().offset(Metric.contentRightOffset)
                make.centerY.equalTo(priceStackView.titleLabel)
                make.width.height.equalTo(Metric.showPricesButtonPartSize)
            }

            let priceString = price.mapToPrice
            let amountString = Localization.Common.pricesAmount(amount)

            let priceStringWidth = priceString.boundingWidth(height: .greatestFiniteMagnitude, font: Font.priceStackViewTitleFont)
            let amountStringWidth = amountString.boundingWidth(height: .greatestFiniteMagnitude, font: Font.priceStackViewSubtitleFont)
            let containerWidth = max(priceStringWidth, amountStringWidth)

            priceStackView.snp.remakeConstraints { (make) in
                make.left.equalTo(descriptionStackView.snp.right).offset(Metric.priceStackViewLeftOffset)
                make.top.equalTo(descriptionStackView)
                make.right.equalTo(showPricesButton.snp.left).offset(Metric.priceStackViewRightOffset)
                make.width.equalTo(containerWidth)
            }

            layoutIfNeeded()
        }
    }

    fileprivate func enableShowReviews(isEnabled: Bool) {
        if showReviewsButton.superview == nil && isEnabled {
            contentView.addSubview(showReviewsButton)

            showReviewsButton.snp.makeConstraints { (make) in
                make.centerY.equalTo(reviewsTitleLabel)
                make.right.equalToSuperview().offset(Metric.contentRightOffset)
                make.height.equalTo(Metric.contentActionHeight)
            }
        } else {
            showReviewsButton.removeFromSuperview()
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductInfoView {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.white

        static let scrollViewBackgroundColor = Style.Color.white

        static let contentViewBackgroundColor = Style.Color.white
        static let contentDividerBackgroundColor = UIColor(hex: "#F2F2F2")

        static let carouselViewBackgroundColor = Style.Color.white // UIColor(hex: "#246B27")

        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitleTextColor = Style.Color.secondaryText

        static let priceStackViewTitleTextColor = Style.Color.black
        static let priceStackViewSubtitleTextColor = Style.Color.secondaryText

        static let showPricesButtonTintColor = Style.Color.secondaryText

        static let addPriceButtonBackgroundColor = Style.Color.lightBlue
        static let addPriceButtonTitleColor = Style.Color.brightBlue

        static let contentTitleLabelTextColor = Style.Color.black

        static let addReviewButtonTitleColor = Style.Color.brightBlue
        static let addReviewButtonBackgroundColor = Style.Color.lightBlue

        static let footerTitleTextColor = Style.Color.secondaryText
        static let footerSubtitleTextColor = Style.Color.black

        static let sourceTextColor = Style.Color.secondaryText

        static let contentActionTitleColor = Style.Color.brightBlue
        static let conentActionBackgroundColor = Style.Color.white
    }

    fileprivate enum CornerRadius {
        static let addPriceButtonCornerRadius: CGFloat = 22
        static let contentActionCornerRadius: CGFloat = 16
        static let addReviewButtonCornerRadius: CGFloat = 24
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.medium, size: 14)

        static let priceStackViewTitleFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let priceStackViewSubtitleFont = Style.Font.sanFrancisco(.medium, size: 14)

        static let addPriceButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 14)

        static let contentTitleLabelFont = Style.Font.sanFrancisco(.medium, size: 18)

        static let footerTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let footerSubtitleFont = Style.Font.sanFrancisco(.medium, size: 16)

        static let sourceLabelFont = Style.Font.sanFrancisco(.medium, size: 12)

        static let contentActionTitleFont = Style.Font.sanFrancisco(.regular, size: 12)

        static let addReviewButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Image {
        static let showPricesButtonImage = Media.icon(.rightChevron, size: .x18)
    }

    fileprivate enum Metric {
        static let scrollViewBottomInset: CGFloat = 80

        static let contentLeftOffset: CGFloat = 20
        static let contentRightOffset: CGFloat = -20
        static let contentDividerTopOffset: CGFloat = 28
        static let contentDividerHeight: CGFloat = 1

        static let carouselViewTopOffset: CGFloat = 20
        static let carouselViewHeight: CGFloat = 200

        static let descriptionStackViewTopOffset: CGFloat = 20
        static let descriptionStackViewMinHeight: CGFloat = 41
        static let descriptionStackViewDefaultMinHeight: CGFloat = 52

        static let priceStackViewLeftOffset: CGFloat = 8
        static let priceStackViewRightOffset: CGFloat = -6

        static let showPricesButtonPartSize: CGFloat = 24

        static let addPriceButtonWidth: CGFloat = 132
        static let addPriceButtonHeight: CGFloat = 52
        static let addPriceButtonLeftOffset: CGFloat = 8
        static let addPriceButtonHorizontalInset: CGFloat = 12

        static let compareViewTopOffset: CGFloat = 18
        static let compareViewHeight: CGFloat = 56

        static let countersViewTopOffset: CGFloat = 20

        static let ratingControlViewTopOffset: CGFloat = 12

        static let reviewsTitleLabelTopOffset: CGFloat = 28

        static let reviewsCollectionViewTopOffset: CGFloat = 28
        static let reviewsCollectionViewHeight: CGFloat = 166

        static let addReviewButtonTopOffset: CGFloat = 18
        static let addReviewButtonHeight: CGFloat = 48

        static let footerTitleLabelTopOffset: CGFloat = 28

        static let contentActionHeight: CGFloat = 32
        static let contentActionHorizontalInset: CGFloat = 24

        static let footerStackViewTopOffset: CGFloat = 28
        static let footerStackViewSpacing: CGFloat = 16

        static let sourceLabelTopOffset: CGFloat = 60
        static let sourceLabelBottomOffset: CGFloat = -26
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductInfoView {
    var showPricesListAction: ControlEvent<Void> {
        let pricesTapEvent = base.priceTapGesture.rx.event.mapToVoid()
        let showPricesTapEvent = base.showPricesButton.rx.tap.mapToVoid()
        let source = Observable.merge(pricesTapEvent, showPricesTapEvent)
        return ControlEvent(events: source)
    }

    var showReviewsListAction: ControlEvent<Void> {
        let countersTapEvent = base.countersTapGesture.rx.event.mapToVoid()
        let showReviewsTapEvent = base.showReviewsButton.rx.tap.mapToVoid()
        let source = Observable.merge(countersTapEvent, showReviewsTapEvent)
        return ControlEvent(events: source)
    }

    var addPriceAction: ControlEvent<Void> {
        return ControlEvent(events: base.addPriceButton.rx.tap)
    }

    var addReviewAction: ControlEvent<Void> {
        let ratingViewAction = base.ratingControlView.rx.selectRatingAction.asObservable()
        let reviewButtonAction = base.addReviewButton.rx.tap.asObservable()
        let source = Observable.merge(ratingViewAction, reviewButtonAction)
        return ControlEvent(events: source)
    }

    var showProductDetailsAction: ControlEvent<Void> {
        return ControlEvent(events: base.showDetailsButton.rx.tap)
    }

    var saveProductAction: ControlEvent<Void> {
        return ControlEvent(events: base.saveActionView.primaryButton.rx.tap)
    }

    var enableAddPrice: Binder<Void> {
        return Binder(base, binding: { view, _ in
            view.enableAddPrice()
        })
    }

    var showPrice: Binder<(price: Double, amount: Int)> {
        return Binder(base, binding: { view, input in
            view.showPrice(price: input.price, amount: input.amount)
        })
    }

    var enableShowReviews: Binder<Bool> {
        return Binder(base, binding: { view, isEnabled in
            view.enableShowReviews(isEnabled: isEnabled)
        })
    }
}
