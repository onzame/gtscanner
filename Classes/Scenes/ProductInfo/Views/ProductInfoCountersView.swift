//
//  ProductInfoCountersView.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class ProductInfoCountersView: UIView {

    // MARK: - UI components

    let ratingAmountContainerView = UIView().then {
        $0.setBackgroundColor(Color.containerViewNeutralBackgroundColor)
    }

    let ratingAmountView = TitledImageView().then {
        $0.imageView.image = Image.ratingAmountViewImage
        $0.titleLabel.font = Font.titledViewTitleFont
        $0.titleLabel.textColor = Color.titledViewTitleTextColor
        $0.stackView.spacing = Metric.titledViewStackViewSpacing
    }

    let ratesCounterContainerView = UIView().then {
        $0.setBackgroundColor(Color.containerViewNeutralBackgroundColor)
    }

    let ratesCounterView = TitledImageView().then {
        $0.imageView.image = Image.ratesCounterViewImage
        $0.titleLabel.font = Font.titledViewTitleFont
        $0.titleLabel.textColor = Color.titledViewTitleTextColor
        $0.stackView.spacing = Metric.titledViewStackViewSpacing
    }

    let stackView = UIStackView().then {
        $0.spacing = Metric.stackViewSpacing
        $0.distribution = .fillEqually
    }

    let reviewsCounterContainerView = UIView().then {
        $0.setBackgroundColor(Color.containerViewNeutralBackgroundColor)
        $0.setCornerRadius(CornerRadius.elementsCornerRadius)
    }

    let reviewsCounterView = TitledImageView().then {
        $0.imageView.image = Image.reviewsCounterView
        $0.titleLabel.font = Font.titledViewTitleFont
        $0.titleLabel.textColor = Color.titledViewTitleTextColor
        $0.stackView.spacing = Metric.titledViewStackViewSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        ratingAmountContainerView.setCornerRadius(
            corners: [.layerMinXMinYCorner, .layerMinXMaxYCorner],
            radius: CornerRadius.elementsCornerRadius
        )

        ratesCounterContainerView.setCornerRadius(
            corners: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner],
            radius: CornerRadius.elementsCornerRadius
        )
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(stackView)
        addSubview(reviewsCounterContainerView)

        stackView.addArrangedSubview(ratingAmountContainerView)
        stackView.addArrangedSubview(ratesCounterContainerView)

        ratingAmountContainerView.addSubview(ratingAmountView)

        ratesCounterContainerView.addSubview(ratesCounterView)

        reviewsCounterContainerView.addSubview(reviewsCounterView)
    }

    private func setupLayout() {
        stackView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
        }

        reviewsCounterContainerView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.width.equalTo(Metric.reviewsCounterContainerViewWidth)
            make.height.equalTo(Metric.reviewsCounterContainerViewHeight)
            make.left.equalTo(stackView.snp.right).offset(Metric.reviewsCounterContainerViewLeftOffset)
        }

        [ratingAmountView, ratesCounterView, reviewsCounterView].forEach {
            $0.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.left.greaterThanOrEqualToSuperview()
                make.right.lessThanOrEqualToSuperview()
            }
        }
    }

    private func setupView() {

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductInfoCountersView {
    fileprivate enum Color {
        static let containerViewNeutralBackgroundColor = Style.Color.lightBlue
        static let titledViewTitleTextColor = UIColor(hex: "#262A3C")
    }

    fileprivate enum CornerRadius {
        static let elementsCornerRadius: CGFloat = 26
    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {
        static let titledViewTitleFont = Style.Font.sanFrancisco(.semibold, size: 20)
    }

    fileprivate enum Image {
        static let ratingAmountViewImage = Media.icon(.starFilled, size: .x24)
        static let ratesCounterViewImage = Media.icon(.profile, size: .x24)
        static let reviewsCounterView = Media.icon(.dialogBubble, size: .x24)
    }

    fileprivate enum Metric {
        static let titledViewStackViewSpacing: CGFloat = 4

        static let stackViewSpacing: CGFloat = 1

        static let reviewsCounterContainerViewWidth: CGFloat = 110
        static let reviewsCounterContainerViewHeight: CGFloat = 64
        static let reviewsCounterContainerViewLeftOffset: CGFloat = 8
    }
}
