//
//  ProductInfoReviewsView.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class ProductInfoReviewsView: UIView {

    // MARK: - UI components

    let collectionViewLayout = UICollectionViewFlowLayout().then {
        let horizontalInset = Metric.collectionViewSectionHorizontalInset
        let verticalInset = Metric.collectionViewSectionVerticalInset
        $0.minimumLineSpacing = Metric.collectionViewItemSpacing
        $0.scrollDirection = .horizontal
        $0.sectionInset = UIEdgeInsets(top: verticalInset, left: horizontalInset, bottom: verticalInset, right: horizontalInset)
        $0.itemSize = CGSize(width: Metric.collectionViewItemWidth, height: Metric.collectionViewItemHeight)
    }

    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout).then {
        $0.showsHorizontalScrollIndicator = false
        $0.decelerationRate = .fast
        $0.setBackgroundColor(Color.collectionViewBackgroundColor)
        $0.register(Reusable.reviewSignCell)
        $0.register(Reusable.placeholderCell)
        $0.register(Reusable.reviewCommentCell)
    }

    let pageIndicator = LinePageControl()

    // MARK: - DataSource

    let dataSource = RxCollectionViewSectionedAnimatedDataSource<ProductInfoReviewsSectionModel>(
        configureCell: { (_, collectionView, indexPath, item) -> UICollectionViewCell in
            switch item {
            case .review(let viewModel):
                let review = viewModel.currentState.review

                if review.positiveText.orEmpty.count > 0 || review.negativeText.orEmpty.count > 0 {
                    let cell = collectionView.dequeue(Reusable.reviewSignCell, for: indexPath)
                    cell.reactor = viewModel
                    return cell
                } else {
                    let cell = collectionView.dequeue(Reusable.reviewCommentCell, for: indexPath)
                    cell.reactor = viewModel
                    return cell
                }
            case .placeholder:
                return collectionView.dequeue(Reusable.placeholderCell, for: indexPath)
            }
    })

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(collectionView)
        addSubview(pageIndicator)
    }

    private func setupLayout() {
        pageIndicator.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
            make.width.equalTo(LinePageControl.Metric.width)
            make.height.equalTo(LinePageControl.Metric.height)
        }

        collectionView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(pageIndicator).offset(Metric.collectionViewBottomOffset)
        }
    }

    private func setupView() {
        collectionView.delegate = self
    }
}

// MARK: - Extensions
// MARK: - Constants

extension ProductInfoReviewsView {
    fileprivate enum Color {
        static let collectionViewBackgroundColor = Style.Color.white
    }

    fileprivate enum Metric {
        static let collectionViewSectionHorizontalInset: CGFloat = 20
        static let collectionViewSectionVerticalInset: CGFloat = 0
        static let collectionViewItemSpacing: CGFloat = 8
        static let collectionViewItemHeight: CGFloat = 152
        static let collectionViewItemWidth: CGFloat = min(Screen.width - collectionViewSectionHorizontalInset * 2, 374)
        static let collectionViewBottomOffset: CGFloat = -12
    }

    fileprivate enum Reusable {
        static let reviewSignCell = ReusableCell<ReviewSignCollectionView>()
        static let placeholderCell = ReusableCell<ReviewPlaceholderCollectionViewCell>()
        static let reviewCommentCell = ReusableCell<ReviewCommentCollectionView>()
    }
}

// MARK: - CollectionViewCenterScrollable

extension ProductInfoReviewsView: CollectionViewCenterScrollable { }

// MARK: - UICollectionViewDelegateFlowLayout

extension ProductInfoReviewsView: UICollectionViewDelegateFlowLayout {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        collectionViewWillScrollToVisibleCellCenter { indexPath in
            self.pageIndicator.page = indexPath.item
        }
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate { collectionViewWillScrollToVisibleCellCenter() }
    }
}
