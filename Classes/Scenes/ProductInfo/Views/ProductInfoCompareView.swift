//
//  ProductInfoCompareView.swift
//  Gorod
//
//  Created by Sergei Fabian on 22.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductInfoCompareView: UIView {

    // MARK: - UI components

    let topDivider = UIView().then {
        $0.setBackgroundColor(Color.dividerBackgroundColor)
    }

    let compareButton = UIButton(type: .system).then {
        $0.setRightToLeftSemanticContentAttribute()
        $0.centerTextAndImage(spacing: 6)
    }

    let amountLabel = UILabel().then {
        $0.font = Font.amountLabelFont
        $0.text = "\(Localization.ProductInfo.compareAmountTitle)"
        $0.textColor = Color.amountLabelTextColor
        $0.isUserInteractionEnabled = true
    }

    let showCompareButton = UIButton().then {
        $0.setImage(Image.showCompareButtonImage, for: .normal)
        $0.setTintColor(Color.showCompareButtonTintColor)
    }

    let bottomDivider = UIView().then {
        $0.setBackgroundColor(Color.dividerBackgroundColor)
    }

    // MARK: - Gestures

    let amountLabelTapGesture = UITapGestureRecognizer()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(topDivider)
        addSubview(compareButton)
        addSubview(amountLabel)
        addSubview(showCompareButton)
        addSubview(bottomDivider)
    }

    private func setupLayout() {
        topDivider.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(Metric.dividerHeight)
        }

        compareButton.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.compareButtonLeftOffset)
            make.centerY.equalToSuperview()
        }

        showCompareButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.showCompareButtonRightOffset)
            make.centerY.equalToSuperview()
        }

        amountLabel.snp.makeConstraints { (make) in
            make.left.greaterThanOrEqualTo(compareButton.snp.right).offset(Metric.amountLabelLeftOffset)
            make.right.equalTo(showCompareButton.snp.left).offset(Metric.amountLabelRightOffset)
            make.centerY.equalToSuperview()
        }

        bottomDivider.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(Metric.dividerHeight)
        }
    }

    private func setupView() {
        amountLabel.addGestureRecognizer(amountLabelTapGesture)
    }

    // MARK: - Utils

    fileprivate func changeState(inCompare: Bool) {
        UIView.performWithoutAnimation {
            if inCompare {
                compareButton.setTitle(Localization.ProductInfo.compareButtonDeleteActionTitle, for: .normal)
                compareButton.setTitleColor(Color.compareButtonDeleteTitleColor, for: .normal)
                compareButton.setImage(Image.compareButtonDeleteImage, for: .normal)
                compareButton.tintColor = Color.compareButtonDeleteTitleColor
            } else {
                compareButton.setTitle(Localization.ProductInfo.compareButtonAddActionTitle, for: .normal)
                compareButton.setTitleColor(Color.compareButtonAddTitleColor, for: .normal)
                compareButton.setImage(nil, for: .normal)
                compareButton.tintColor = Color.compareButtonAddTitleColor
            }
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductInfoCompareView {
    fileprivate enum Color {
        static let compareButtonAddTitleColor = Style.Color.brightBlue
        static let compareButtonDeleteTitleColor = Style.Color.secondaryText
        static let showCompareButtonTintColor = Style.Color.secondaryText
        static let amountLabelTextColor = Style.Color.black
        static let dividerBackgroundColor = UIColor(hex: "#F2F2F2")
    }

    fileprivate enum Font {
        static let amountLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Image {
        static let compareButtonDeleteImage = Media.icon(.clear, size: .x18)
        static let showCompareButtonImage = Media.icon(.rightChevron, size: .x18)
    }

    fileprivate enum Metric {
        static let compareButtonLeftOffset: CGFloat = 20

        static let showCompareButtonRightOffset: CGFloat = -20

        static let amountLabelLeftOffset: CGFloat = 8
        static let amountLabelRightOffset: CGFloat = -8

        static let dividerHeight: CGFloat = 1
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductInfoCompareView {
    var triggerCompareStateAction: ControlEvent<Void> {
        return ControlEvent(events: base.compareButton.rx.tap)
    }

    var showCompareSceneAction: ControlEvent<Void> {
        let showAction = base.showCompareButton.rx.tap.asObservable()
        let amountAction = base.amountLabelTapGesture.rx.event.asObservable().mapToVoid()
        return ControlEvent(events: Observable.merge(showAction, amountAction))
    }

    var inCompare: Binder<Bool> {
        return Binder(base, binding: { view, inCompare in
            view.changeState(inCompare: inCompare)
        })
    }

    var changeCompareAmountBinding: Binder<Int> {
        return Binder(base, binding: { view, amount in
            view.amountLabel.text = "\(Localization.ProductInfo.compareAmountTitle) (\(amount))"
        })
    }
}
