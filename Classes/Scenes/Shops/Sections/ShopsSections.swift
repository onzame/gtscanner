//
//  ShopsSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum ShopsSectionModel {
    case section(items: [ShopsItemModel])
}

extension ShopsSectionModel: AnimatableSectionModelType {
    var identity: String {
        switch self {
        case .section:
            return "section"
        }
    }

    var items: [ShopsItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: ShopsSectionModel, items: [ShopsItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum ShopsItemModel {
    case shop(ShopsTableViewModel)
    case noDataPlaceholder
}

extension ShopsItemModel: IdentifiableType {
    var identity: String {
        switch self {
        case .shop(let viewModel):
            return "\(viewModel.identity)"
        case .noDataPlaceholder:
            return "ShopsItemModel.noDataPlaceholder"
        }
    }
}

extension ShopsItemModel: Equatable {
    static func == (lhs: ShopsItemModel, rhs: ShopsItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.shop(let left), .shop(let right)):
            return left.currentState == right.currentState
        case (.noDataPlaceholder, .noDataPlaceholder):
            return true
        default:
            return false
        }
    }
}

extension Array where Element == Shop {
    func mapToSections() -> [ShopsSectionModel] {
        var items = self
            .map({ shop -> ShopsItemModel in
                let viewModel = ShopsTableViewModel(shop: shop)
                let itemModel = ShopsItemModel.shop(viewModel)
                return itemModel
            })

        if items.isEmpty {
            items.append(.noDataPlaceholder)
        }

        return [ShopsSectionModel.section(items: items)]
    }
}

extension Observable where Element == ShopsItemModel {
    func mapToShop() -> Observable<Shop?> {
        return map({ model in
            if case ShopsItemModel.shop(let viewModel) = model {
                return viewModel.currentState.shop
            } else {
                return nil
            }
        })
    }
}
