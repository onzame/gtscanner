//
//  ShopsPreviewSectionModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxDataSources

// MARK: - Sections

enum ShopsPreviewSectionModel {
    case section(items: [ShopsPreviewItemModel])
}

extension ShopsPreviewSectionModel: AnimatableSectionModelType {
    var identity: String {
        switch self {
        case .section:
            return "section"
        }
    }

    var items: [ShopsPreviewItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: ShopsPreviewSectionModel, items: [ShopsPreviewItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum ShopsPreviewItemModel {
    case shop(ShopsPreviewCollectionViewModel)
    case noData
    case noPermission
}

extension ShopsPreviewItemModel: IdentifiableType {
    var identity: String {
        switch self {
        case .shop(let viewModel):
            return "\(viewModel.identity)"
        case .noData:
            return "ShopsPreviewItemModel.noData"
        case .noPermission:
            return "ShopsPreviewItemModel.noPermission"
        }
    }
}

extension ShopsPreviewItemModel: Equatable {
    static func == (lhs: ShopsPreviewItemModel, rhs: ShopsPreviewItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.shop(let left), .shop(let right)):
            return left.currentState == right.currentState
        case (.noData, .noData):
            return true
        case (.noPermission, .noPermission):
            return true
        default:
            return false
        }
    }
}

// MARK: - Array + Shops

extension Optional where Wrapped == [Shop] {
    func mapToSections() -> [ShopsPreviewSectionModel] {
        switch self {
        case .some(let array):
            if array.isNotEmpty {
                return array.mapToPreviewSections()
            } else {
                return [ShopsPreviewSectionModel.section(items: [ShopsPreviewItemModel.noData])]
            }
        case .none:
            return [ShopsPreviewSectionModel.section(items: [ShopsPreviewItemModel.noPermission])]
        }
    }
}

extension Array where Element == Shop {
    func mapToPreviewSections() -> [ShopsPreviewSectionModel] {
        let items = self
            .map({ shop -> ShopsPreviewItemModel in
                let viewModel = ShopsPreviewCollectionViewModel(shop: shop)
                let itemModel = ShopsPreviewItemModel.shop(viewModel)
                return itemModel
            })

        return [ShopsPreviewSectionModel.section(items: items)]
    }
}
