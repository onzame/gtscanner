//
//  ShopsPreviewNoPermissionCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class ShopsPreviewNoPermissionCollectionViewCell: BaseCollectionViewCell {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.setCornerRadius(CornerRadius.containerViewCornerRadius)
        $0.setBackgroundColor(Color.containerViewBackgroundColor)
    }

    let titleLabel = UILabel().then {
        let actionTitle = Localization.ShopsPreview.errorNoLocationActionTitle
        let actionString = NSAttributedString(string: actionTitle, underlineStyle: .single, underlineColor: Color.titleLabelTextColor)
        let resultText = NSMutableAttributedString(string: Localization.ShopsPreview.errorNoLocationTitle)
        resultText.append(.space)
        resultText.append(actionString)

        $0.attributedText = resultText
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.numberOfLines = 0
        $0.textAlignment = .center
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(titleLabel)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.titleLabelTopOffset)
            make.right.equalToSuperview().offset(Metric.titleLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.titleLabelBottomOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ShopsPreviewNoPermissionCollectionViewCell {
    fileprivate enum Color {
        static let containerViewBackgroundColor = UIColor(hex: "#fff2f7")
        static let titleLabelTextColor = Style.Color.pink
    }

    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 8
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.regular, size: 12)
    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 16
        static let titleLabelTopOffset: CGFloat = 8
        static let titleLabelRightOffset: CGFloat = -16
        static let titleLabelBottomOffset: CGFloat = -8
    }
}
