//
//  ShopsNoDataTableViewCell.swift
//  Alamofire
//
//  Created by Sergei Fabian on 06.04.2020.
//

import UIKit

class ShopsNoDataTableViewCell: BaseTableViewCell {

    static let cellHeight: CGFloat = 144

    // MARK: - UI components

    let containerView = UIView().then {
        $0.setCornerRadius(CornerRadius.containerViewCornerRadius)
    }

    let subContainerView = UIView().then {
        $0.setBackgroundColor(Color.subContainerViewBackgroundColor)
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.text = Localization.Shops.filterNoDataPlaceholderTitle
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleColor
        $0.titleLabel.textAlignment = .center
        $0.subtitleLabel.text = Localization.Shops.filterNoDataPlaceholderSubtitle
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleColor
        $0.subtitleLabel.textAlignment = .center
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()
        subContainerView.setCornerRadius(corners: .layerMaxXMaxYCorner, radius: CornerRadius.containerViewAccentCornerRadius)
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        contentView.addSubview(containerView)
        containerView.addSubview(subContainerView)
        subContainerView.addSubview(descriptionStackView)
    }

    private func setupLayout() {
        containerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(Metric.containerViewInset)
        }

        subContainerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.descriptionStackViewRightOffset)
            make.centerY.equalToSuperview()
        }
    }

    private func setupView() {
        setBackgroundColor(Color.backgroundColor)
    }
}

extension ShopsNoDataTableViewCell {
    fileprivate enum Color {
        static let backgroundColor = UIColor.clear
        static let subContainerViewBackgroundColor = Style.Color.white
        static let descriptionStackViewTitleColor = Style.Color.black
        static let descriptionStackViewSubtitleColor = UIColor(hex: "#706E8B")
    }

    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 16
        static let containerViewAccentCornerRadius: CGFloat = 48
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.medium, size: 18)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.regular, size: 12)
    }

    fileprivate enum Metric {
        static let descriptionStackViewSpacing: CGFloat = 8
        static let containerViewInset: CGFloat = 16
        static let descriptionStackViewLeftOffset: CGFloat = 40
        static let descriptionStackViewRightOffset: CGFloat = -40
    }
}
