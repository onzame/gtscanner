//
//  ShopsPreviewCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

final class ShopsPreviewCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let containerView = UIView().then {
        $0.backgroundColor = Color.containerViewBackgroundColor
        $0.layer.cornerRadius = CornerRadius.containerViewCornerRadius
        $0.setupShadow(.shortBlue)
    }

    let imageView = UIImageView()

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.titleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        $0.subtitleLabel.font = Font.distanceLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
        $0.alignment = .center
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func prepareForReuse() {
        super.prepareForReuse()

        imageView.kf.prepareForReuse()
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(containerView)

        containerView.addSubview(imageView)
        containerView.addSubview(descriptionStackView)
    }

    private func setupLayout() {
        containerView.addEdgesConstraints()

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.descriptionStackViewRightOffset)
            make.bottom.equalToSuperview().offset(Metric.descriptionStackViewBottomOffset)
        }

        imageView.snp.makeConstraints { (make) in
            make.bottom.equalTo(descriptionStackView.snp.top).offset(Metric.imageViewBottomOffset)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(Metric.imageViewPartSize)
        }
    }

    private func setupView() {

    }

    // MARK: - Bindings

    func bind(reactor: ShopsPreviewCollectionViewModel) {
        let imageSize = CGSize(squareSide: Metric.imageViewPlaceholderPartSize)

        reactor.state
            .map({ ($0.shop.logo, Media.placeholder(for: $0.shop.branch.category, imageSize: imageSize)) })
            .bind(to: imageView.rx.imageWithPlaceholder)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shop.name })
            .bind(to: descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shop.branch.distance })
            .bind(to: descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isSelected })
            .bind(to: rx.selectionBinding)
            .disposed(by: disposeBag)
    }

    // MARK: - Private methods

    fileprivate func setupSelectedStyle() {
        containerView.backgroundColor = Color.containerViewSelectedBackgroundColor
        descriptionStackView.titleLabel.textColor = Color.descriptionStackViewTitleLabelSelectedTextColor
        descriptionStackView.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelSelectedTextColor
    }

    fileprivate func setupUnselectedStyle() {
        containerView.backgroundColor = Color.containerViewBackgroundColor
        descriptionStackView.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        descriptionStackView.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ShopsPreviewCollectionViewCell {
    fileprivate enum Color {
        static let containerViewBackgroundColor = Style.Color.white
        static let containerViewSelectedBackgroundColor = UIColor(hex: "#26CB5A")
        static let descriptionStackViewTitleLabelTextColor = Style.Color.black
        static let descriptionStackViewTitleLabelSelectedTextColor = Style.Color.white
        static let descriptionStackViewSubtitleLabelTextColor = Style.Color.brightBlue
        static let descriptionStackViewSubtitleLabelSelectedTextColor = Style.Color.white
    }

    fileprivate enum CornerRadius {
        static let containerViewCornerRadius: CGFloat = 12
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.semibold, size: 12)
        static let distanceLabelFont = Style.Font.sanFrancisco(.heavy, size: 12)
    }

    fileprivate enum Metric {
        static let imageViewPartSize: CGFloat = 44
        static let imageViewBottomOffset: CGFloat = -14
        static let imageViewPlaceholderPartSize: CGFloat = 44
        static let descriptionStackViewLeftOffset: CGFloat = 8
        static let descriptionStackViewRightOffset: CGFloat = -8
        static let descriptionStackViewBottomOffset: CGFloat = -12
        static let descriptionStackViewSpacing: CGFloat = 6
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ShopsPreviewCollectionViewCell {
    var selectionBinding: Binder<Bool> {
        return Binder(base, binding: { view, isSelected in
            if isSelected {
                view.setupSelectedStyle()
            } else {
                view.setupUnselectedStyle()
            }
        })
    }
}
