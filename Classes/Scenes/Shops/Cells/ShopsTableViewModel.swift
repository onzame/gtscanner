//
//  ShopsTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxDataSources

final class ShopsTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(shop: Shop) {
        initialState = State(shop: shop)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ShopsTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ShopsTableViewModel {
    struct State {
        let shop: Shop
    }
}

// MARK: - Equatable

extension ShopsTableViewModel.State: Equatable {
    static func == (lhs: ShopsTableViewModel.State, rhs: ShopsTableViewModel.State) -> Bool {
        return lhs.shop == rhs.shop
    }
}

// MARK: - IdentifiableType

extension ShopsTableViewModel: IdentifiableType {
    var identity: Int {
        return currentState.shop.identity
    }
}
