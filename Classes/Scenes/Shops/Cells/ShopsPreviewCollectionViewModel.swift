//
//  ShopsPreviewCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxDataSources

final class ShopsPreviewCollectionViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(shop: Shop) {
        initialState = State(shop: shop, isSelected: false)
    }

    init(shop: Shop, isSelected: Bool) {
        initialState = State(shop: shop, isSelected: isSelected)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ShopsPreviewCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ShopsPreviewCollectionViewModel {
    struct State {
        let shop: Shop
        let isSelected: Bool
    }
}

// MARK: - Equatable

extension ShopsPreviewCollectionViewModel.State: Equatable {
    static func == (lhs: ShopsPreviewCollectionViewModel.State, rhs: ShopsPreviewCollectionViewModel.State) -> Bool {
        return lhs.shop == rhs.shop
            && lhs.isSelected == rhs.isSelected
    }
}

// MARK: - IdentifiableType

extension ShopsPreviewCollectionViewModel: IdentifiableType {
    var identity: Int {
        return currentState.shop.identity
    }
}
