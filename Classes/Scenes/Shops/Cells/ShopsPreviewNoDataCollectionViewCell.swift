//
//  ShopsPreviewNoDataCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class ShopsPreviewNoDataCollectionViewCell: BaseCollectionViewCell {

    // MARK: - UI components

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.text = Localization.ShopsPreview.noDataTitle
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.subtitleLabel.text = Localization.ShopsPreview.noDataSubtitle
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitelTextColor
        $0.subtitleLabel.numberOfLines = 0
        $0.axis = .vertical
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        contentView.addSubview(descriptionStackView)
    }

    private func setupLayout() {
       descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.top.greaterThanOrEqualToSuperview()
            make.right.bottom.lessThanOrEqualToSuperview()
            make.center.equalToSuperview()
        }
    }

    private func setupView() {

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ShopsPreviewNoDataCollectionViewCell {
    fileprivate enum Color {
        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitelTextColor = Style.Color.newGray
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.medium, size: 18)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.regular, size: 12)
    }

    fileprivate enum Metric {
        static let descriptionStackViewSpacing: CGFloat = 4
    }
}
