//
//  ShopsTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ShopsTableViewCell: BaseTableViewCell, View {

    static let cellHeight: CGFloat = 82

    // MARK: - UI components

    let nameLabel = UILabel().then {
        $0.font = Font.nameLabelFont
        $0.textColor = Color.nameLabelTextColor
    }

    let distanceLabel = UILabel().then {
        $0.font = Font.distanceLabelFont
        $0.textColor = Color.distanceLabelTextColor
    }

    let addressLabel = UILabel().then {
        $0.font = Font.addressLabelFont
        $0.textColor = Color.addressLabelTextColor
    }

    let imageContainerView = ImageContainerView()

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func prepareForReuse() {
        super.prepareForReuse()

        imageContainerView.imageView.kf.prepareForReuse()
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(nameLabel)
        addSubview(distanceLabel)
        addSubview(addressLabel)
        addSubview(imageContainerView)
    }

    private func setupLayout() {
        imageContainerView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.imageContainerViewRightOffset)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(Metric.imageContainerViewPartSize)
        }

        nameLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.nameLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.nameLabelTopOffset)
            make.right.equalTo(imageContainerView.snp.left).offset(Metric.nameLabelRightOffset)
        }

        addressLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom).offset(Metric.addressLabelTopOffset)
            make.right.equalTo(imageContainerView.snp.left).offset(Metric.addressLabelRightOffset)
        }

        distanceLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nameLabel)
            make.right.equalTo(addressLabel.snp.left).offset(Metric.distanceLabelRightOffset)
            make.centerY.equalTo(addressLabel)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ShopsTableViewModel) {
        let imageSize = CGSize(squareSide: Metric.imageContainerViewPlaceholderPartSize)

        reactor.state
            .map({ ($0.shop.logo, Media.placeholder(for: $0.shop.branch.category, imageSize: imageSize)) })
            .bind(to: imageContainerView.imageView.rx.imageWithPlaceholder)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shop.name })
            .bind(to: nameLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shop.branch.distance })
            .bind(to: distanceLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shop.branch.address })
            .bind(to: addressLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ShopsTableViewCell {
    fileprivate enum Color {
        static let nameLabelTextColor = Style.Color.black
        static let distanceLabelTextColor = Style.Color.brightBlue
        static let addressLabelTextColor = Style.Color.secondaryText
        static let imageContainerViewPlaceholderBackgroundColor = Style.Color.lightBlue
    }

    fileprivate enum CornerRadius {
        static let imageContainerPlaceholderCornerRadius: CGFloat = 8
    }

    fileprivate enum Font {
        static let nameLabelFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let distanceLabelFont = Style.Font.sanFrancisco(.heavy, size: 12)
        static let addressLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Image {
        static let imageContainerPlaceholderImage = Media.image(.productPlaceholder)?.tint(with: Style.Color.black)
    }

    fileprivate enum Metric {
        static let imageContainerViewRightOffset: CGFloat = -20
        static let imageContainerViewPartSize: CGFloat = 44
        static let imageContainerViewPlaceholderPartSize: CGFloat = 44

        static let nameLabelLeftOffset: CGFloat = 20
        static let nameLabelTopOffset: CGFloat = 20
        static let nameLabelRightOffset: CGFloat = -8

        static let addressLabelTopOffset: CGFloat = 4
        static let addressLabelRightOffset: CGFloat = -8

        static let distanceLabelRightOffset: CGFloat = -12
    }
}
