//
//  ShopsComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class ShopsComponentView: UIView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.text = Localization.ShopsPreview.title
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleTextColor
    }

    let openShopsButton = UIButton(type: .system).then {
        $0.setTitle(Localization.ShopsPreview.openShopsActionTitle, for: .normal)
        $0.setTitleColor(Color.openShopButtonDisabledTitleColor, for: .normal)
        $0.titleLabel?.font = Font.openShopTitleFont
        $0.isEnabled = false
    }

    let collectionViewLayout = UICollectionViewFlowLayout().then {
        $0.scrollDirection = .horizontal
        $0.minimumInteritemSpacing = Metric.collectionViewItemSpacing
        $0.sectionInset = UIEdgeInsets(
            top: Metric.collectionViewSectionVerticalSpacing,
            left: Metric.collectionViewSectionHorizontalSpacing,
            bottom: Metric.collectionViewSectionVerticalSpacing,
            right: Metric.collectionViewSectionHorizontalSpacing
        )
    }

    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout).then {
        $0.showsHorizontalScrollIndicator = false
        $0.setBackgroundColor(Color.collectionViewBackgroundColor)
        $0.register(Reusable.shopCell)
        $0.register(Reusable.noDataCell)
        $0.register(Reusable.noLocationCell)
    }

    // MARK: - DataSource

    let dataSource = RxCollectionViewSectionedAnimatedDataSource<ShopsPreviewSectionModel>(
        configureCell: { (_, collectionView, indexPath, model) -> UICollectionViewCell in
            switch model {
            case .shop(let viewModel):
                let cell = collectionView.dequeue(Reusable.shopCell, for: indexPath)
                cell.reactor = viewModel
                return cell
            case .noData:
                return collectionView.dequeue(Reusable.noDataCell, for: indexPath)
            case .noPermission:
                return collectionView.dequeue(Reusable.noLocationCell, for: indexPath)
            }
    })

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
        addSubview(openShopsButton)
        addSubview(collectionView)
    }

    private func setupLayout() {
        collectionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(Metric.collectionViewHeight)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.titleLabelTopOffset)
            make.bottom.equalTo(collectionView.snp.top).offset(Metric.titleLabelBottomOffset)
        }

        openShopsButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.openShopsButtonRightOffset)
            make.centerY.equalTo(titleLabel)
        }
    }

    private func setupView() {
        collectionView.delegate = self
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ShopsComponentView {
    fileprivate enum Color {
        static let titleTextColor = Style.Color.newGray
        static let openShopButtonTitleColor = Style.Color.brightBlue
        static let openShopButtonDisabledTitleColor = Style.Color.newGray
        static let collectionViewBackgroundColor = Style.Color.white
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let openShopTitleFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 20
        static let titleLabelTopOffset: CGFloat = 24
        static let titleLabelBottomOffset: CGFloat = 0

        static let openShopsButtonRightOffset: CGFloat = -20

        static let collectionViewHeight: CGFloat = 152
        static let collectionViewItemWidth: CGFloat = 104
        static let collectionViewItemHeight: CGFloat = 128
        static let collectionViewItemSpacing: CGFloat = 8
        static let collectionViewSectionVerticalSpacing: CGFloat = 12
        static let collectionViewSectionHorizontalSpacing: CGFloat = 20
    }

    fileprivate enum Reusable {
        static let shopCell = ReusableCell<ShopsPreviewCollectionViewCell>()
        static let noDataCell = ReusableCell<ShopsPreviewNoDataCollectionViewCell>()
        static let noLocationCell = ReusableCell<ShopsPreviewNoPermissionCollectionViewCell>()
    }
}

// MARK: - UICollectionViewDelegate

extension ShopsComponentView: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        switch dataSource[indexPath] {
        case .shop:
            return CGSize(width: Metric.collectionViewItemWidth, height: Metric.collectionViewItemHeight)
        case .noData, .noPermission:
            let contentWidth = collectionView.bounds.width - 2 * Metric.collectionViewSectionHorizontalSpacing
            return CGSize(width: contentWidth, height: Metric.collectionViewItemHeight)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ShopsComponentView {
    var shopSelection: ControlEvent<Shop> {
        let source = base.collectionView.rx.itemSelected(dataSource: base.dataSource)
            .map({ itemModel -> Shop? in
                if case ShopsPreviewItemModel.shop(let viewModel) = itemModel {
                    return viewModel.currentState.shop
                } else {
                    return nil
                }
            })
            .filterNil()

        return ControlEvent(events: source)
    }

    var openAppSettingAction: ControlEvent<Void> {
        let source = base.collectionView.rx.itemSelected(dataSource: base.dataSource)
            .filter({ itemModel -> Bool in
                if case ShopsPreviewItemModel.noPermission = itemModel {
                    return true
                } else {
                    return false
                }
            })
            .mapToVoid()

        return ControlEvent(events: source)
    }

    var allShopsAction: ControlEvent<Void> {
        return ControlEvent(events: base.openShopsButton.rx.tap)
    }

    var enableShopsAction: Binder<Bool> {
        return Binder(base, binding: { view, isEnabled in
            view.openShopsButton.isEnabled = isEnabled

            if isEnabled {
                view.openShopsButton.setTitleColor(ShopsComponentView.Color.openShopButtonTitleColor, for: .normal)
            } else {
                view.openShopsButton.setTitleColor(ShopsComponentView.Color.openShopButtonDisabledTitleColor, for: .normal)
            }
        })
    }
}
