//
//  ShopsViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

final class ShopsViewController: BaseViewController<ShopsViewModel> {

    // MARK: - UI components

    let contentView = ShopsView()

    // MARK: - Publishers

    fileprivate let retryActionPublisher = PublishSubject<Void>()

    // MARK: - Properties

    private let locationSessionController = LocationSessionController.shared

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.Shops.title
    }

    // MARK: - Bindings

    override func bind(reactor: ShopsViewModel) {

        rx.viewDidLoad
            .asObservable()
            .flatMap({ [weak self] _ -> Observable<Void> in
                guard let `self` = self else { return .empty() }
                return self.locationSessionController.prepareSession()
                    .asObservable()
                    .mapToVoid()
            })
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewDidAppear
            .mapToVoid()
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewDidDisappear
            .mapToVoid()
            .bind(to: locationSessionController.rx.disableUpdating)
            .disposed(by: disposeBag)

        // Input

        let locationSequence = locationSessionController.rx.location
            .mapToDomain()

        rx.viewDidAppear
            .take(1)
            .withLatestFrom(locationSequence)
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .withLatestFrom(locationSequence)
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.filterAction
            .distinctUntilChanged()
            .skip(1)
            .debounce(.milliseconds(Duration.filterDelay), scheduler: MainScheduler.instance)
            .withLatestFrom(locationSequence, resultSelector: { (query: $0, location: $1) })
            .withLatestFrom(reactor.state, resultSelector: { (meta, state) -> Reactor.Action? in
                return state.isRefreshing ? nil : .refresh(location: meta.location, query: meta.query)
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.tableView.rx.reachedBottom()
            .withLatestFrom(reactor.state)
            .filter({ !$0.isFilteredMode && !($0.isLoadingNext || $0.isRefreshing) })
            .withLatestFrom(locationSequence, resultSelector: { (state, location) -> Reactor.Action? in
                if let nextPage = state.pagination.nextPage {
                    return .loadNext(location: location, page: nextPage)
                } else {
                    return nil
                }
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.refreshAction
            .withLatestFrom(reactor.state)
            .filter({ !$0.isRefreshing })
            .withLatestFrom(locationSequence)
            .withLatestFrom(contentView.rx.filterAction, resultSelector: { (location, query) -> Reactor.Action in
                return .refresh(location: location, query: query)
            })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.tableView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ (flag: $0.showErrorState, error: $0.error) })
            .distinctUntilChanged({ $0.flag == $1.flag })
            .filter({ $0.flag })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showLoadingState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showDefaultState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showNoDataState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoadingNext })
            .distinctUntilChanged()
            .bind(to: contentView.tableView.rx.showFooterActivityIndicatorView())
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ShopsViewController {
    fileprivate enum Duration {
        static let filterDelay: Int = 500
    }

    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
    }
}

// MARK: - StateResolvable

extension ShopsViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        if let error = error as? NetworkErrorWrapper,
            error.statusCode == NetworkClientStatusCode.badRequest.rawValue {
            return ErrorStateView().then {
                $0.image = Image.errorBackgroundViewImage
                $0.title = Localization.Shops.noPermissionPlaceholderTitle
                $0.subtitle = Localization.Shops.noPermissionPlaceholderSubtitle
                $0.actionTitle = Localization.Shops.noPermissionPlaceholderActionTitle
                $0.actionButton.rx.tap
                    .bind(to: rx.openAppSettings)
                    .disposed(by: $0.disposeBag)
            }
        } else {
            return ErrorStateView().then {
                $0.image = Image.errorBackgroundViewImage
                $0.title = Localization.Shops.errorPlaceholderTitle
                $0.subtitle = Localization.Shops.errorPlaceholderSubtitle
                $0.actionTitle = Localization.Shops.errorPlaceholderActionTitle
                $0.actionButton.rx.tap
                    .bind(to: retryActionPublisher)
                    .disposed(by: $0.disposeBag)
            }
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.Shops.noDataPlaceholderTitle
            $0.actionTitle = Localization.Shops.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ShopsViewController {
    var selectionAction: ControlEvent<Shop> {
        return base.contentView.rx.selectionAction
    }
}
