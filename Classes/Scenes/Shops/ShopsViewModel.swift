//
//  ShopsViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift
import RxCocoa

final class ShopsViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let shopsService: ShopsServiceType

    // MARK: - Lifecycle

    init(shopsService: ShopsServiceType, product: Product?) {
        self.shopsService = shopsService
        self.initialState = State(product: product)
    }

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .fetch(location: let location):
            return shopsService.fetchShops(location: location, page: 1)
                .map(Mutation.refresh)
                .startWith(.setLoaded(false))
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
        case .refresh(location: let location, query: let query):
            if query.isEmpty {
                return shopsService.fetchShops(location: location, page: 1)
                    .map(Mutation.refresh)
                    .startWith(.setRefreshing(true))
                    .catchError({ .just(.showError(error: $0, isLoaded: true)) })
            } else {
                return shopsService.searchShops(location: location, query: query)
                    .map(Mutation.refreshFiltered)
                    .startWith(.setRefreshing(true))
                    .catchError({ .just(.showError(error: $0, isLoaded: true)) })
            }
        case .loadNext(location: let location, page: let page):
            return shopsService.fetchShops(location: location, page: page)
                .map(Mutation.append)
                .startWith(.beginLoadingNext)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        switch mutation {
        case .refreshFiltered(let shops):
            state.isFilteredMode = true
            state.sections = shops.mapToSections()
            state.isLoadingNext = false
            state.isRefreshing = false
            state.error = nil

        case .refresh(let result):
            state.isFilteredMode = false
            state.shops = result.shops
            state.sections = state.shops.mapToSections()
            state.pagination = result.pagination
            state.isLoadingNext = false
            state.isRefreshing = false
            state.isLoaded = true
            state.error = nil

        case .append(let result):
            state.isFilteredMode = false
            state.shops += result.shops
            state.sections = state.shops.mapToSections()
            state.pagination = result.pagination
            state.isLoadingNext = false
            state.isRefreshing = false
            state.isLoaded = true
            state.error = nil

        case .beginLoadingNext:
            state.isLoadingNext = true
            state.error = nil

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing
            state.error = nil

        case .setLoaded(let isLoaded):
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = nil

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = error
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ShopsViewModel {
    enum Action {
        case fetch(location: Location?)
        case refresh(location: Location?, query: String)
        case loadNext(location: Location?, page: Int)
    }
}

// MARK: - Mutations

extension ShopsViewModel {
    enum Mutation {
        case refreshFiltered([Shop])
        case refresh(FetchShopsResult)
        case append(FetchShopsResult)
        case beginLoadingNext
        case setRefreshing(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
    }
}

// MARK: - State

extension ShopsViewModel {
    struct State {
        let product: Product?
        var shops = [Shop]()
        var sections = [ShopsSectionModel]()
        var pagination = Pagination.initial
        var isFilteredMode = false
        var isLoadingNext = false
        var isRefreshing = false
        var isLoaded = false
        var error: Error?

        var showLoadingState: Bool {
            return !isLoaded && error == nil
        }

        var showDefaultState: Bool {
            return shops.isNotEmpty && isLoaded
        }

        var showErrorState: Bool {
            return !isLoaded && error != nil
        }

        var showNoDataState: Bool {
            return shops.isEmpty && isLoaded
        }

        init(product: Product?) {
            self.product = product
        }
    }
}
