//
//  ShopsCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum ShopsCoordinationResult {
    case selected(Shop)
    case canceled
}

final class ShopsCoordinator: Coordinator<ShopsCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let product: Product?

    init(rootViewController: UIViewController, container: DependenciesContainer, product: Product?) {
        self.rootViewController = rootViewController
        self.container = container
        self.product = product
    }

    override func start() -> Observable<ShopsCoordinationResult> {
        let viewModel = container.shopsViewModel(product: product)
        let viewController = ShopsViewController(reactor: viewModel)
        let navigationController = BaseNavigationController(rootViewController: viewController)

        rootViewController.present(navigationController, animated: true)

        let dismissTrigger = viewController.rx.dismissAction
            .asObservable()
            .mapTo(ShopsCoordinationResult.canceled)
            .do(onNext: { _ in navigationController.dismiss(animated: true) })

        let selectionTrigger = viewController.rx.selectionAction
            .asObservable()
            .map(ShopsCoordinationResult.selected)
            .do(onNext: { _ in navigationController.dismiss(animated: true) })

        return Observable.merge(dismissTrigger, selectionTrigger)
            .take(1)
    }

}
