//
//  UpdatedProductReviewMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct UpdatedProductReviewMetadata {
    let product: Product
    let review: Review
}
