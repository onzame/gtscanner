//
//  ProductReviewViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

final class ProductReviewViewController: BaseViewController<ProductReviewViewModel> {

    // MARK: - UI components

    let contentView = ProductReviewView()

    // MARK: - Publishers

    fileprivate let updatedProductActionPublisher = PublishSubject<UpdatedProductReviewMetadata>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.addEdgesConstraints()
    }

    private func setupView() {
        preferredContentSize = CGSize(width: Metric.width, height: Metric.height)
    }

    // MARK: - Bindings

    override func bind(reactor: ProductReviewViewModel) {

        // Settings

        let startSequence = reactor.state.take(1)

        startSequence
            .map({ $0.selectedRating })
            .filterNil()
            .bind(to: contentView.ratingControlView.rx.rating)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.selectedShop })
            .filterNil()
            .map(Reactor.Action.selecteShop)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.filledPositiveText })
            .bind(to: contentView.reviewFormStackView.rx.positiveText)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.filledNegativeText })
            .bind(to: contentView.reviewFormStackView.rx.negativeText)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.filledText })
            .bind(to: contentView.reviewFormStackView.rx.commentText)
            .disposed(by: disposeBag)

        contentView.shopsView.rx.openAppSettingAction
            .bind(to: rx.openAppSettings)
            .disposed(by: disposeBag)

        // Input

        contentView.ratingControlView.rx.changeRatingAction
            .map(Reactor.Action.selectRating)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.shopsView.rx.shopSelection
            .map(Reactor.Action.selecteShop)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.reviewFormStackView.positiveTextField.rx.text
            .map(Reactor.Action.changePositiveText)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.reviewFormStackView.negativeTextField.rx.text
            .map(Reactor.Action.changeNegativeText)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.reviewFormStackView.commentTextField.rx.text
            .map(Reactor.Action.changeText)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        let submitAction = contentView.actionsView.rx.primaryAction.asObservable()
        let completeAction = contentView.reviewFormStackView.rx.completeAction.asObservable()

        Observable.merge(submitAction, completeAction)
            .withLatestFrom(reactor.state, resultSelector: { (_, state) -> Reactor.Action? in
                guard let rating = state.selectedRating else { return nil }
                guard let shop = state.selectedShop else { return nil }
                let reviewText = ReviewText(positiveText: state.filledPositiveText, negativeText: state.filledNegativeText, text: state.filledText)
                return .addReview(product: state.product, rating: rating, shop: shop, reviewText: reviewText)
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        let collectionView = contentView.shopsView.collectionView
        let dataSource = contentView.shopsView.dataSource

        reactor.state
            .map({ $0.sections })
            .filterNil()
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.shops })
            .filterNil()
            .map({ $0.isNotEmpty })
            .bind(to: contentView.shopsView.rx.enableShopsAction)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.selectedRating })
            .filterNil()
            .bind(to: contentView.ratingControlView.rx.rating)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.selectedRating.orZero > 0 && $0.selectedShop != nil })
            .bind(to: contentView.rx.isSubmitEnabled)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.updatedProductReviewMetadata })
            .filterNil()
            .bind(to: updatedProductActionPublisher)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewViewController {
    fileprivate enum Metric {
        static let width: CGFloat = Screen.width
        static let height: CGFloat = Screen.isLowWidthScreen ? 540 : 660
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewViewController {
    var updateProductAction: ControlEvent<UpdatedProductReviewMetadata> {
        return ControlEvent(events: base.updatedProductActionPublisher)
    }

    var shopsAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.shopsView.rx.allShopsAction)
    }

    var cancelAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.actionsView.secondaryButton.rx.tap)
    }
}
