//
//  ProductReviewFormStackView.swift
//  Gorod
//
//  Created by Sergei Fabian on 13.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import RxSwift
import RxCocoa

class ProductReviewFormStackView: UIStackView {

    // MARK: - UI components

    lazy var positiveTextField = SkyFloatingLabelTextFieldWithIcon().then {
        $0.font = Font.textFieldFont
        $0.delegate = self
        $0.textColor = Color.textFieldTextColor
        $0.returnKeyType = .next

        $0.placeholder = Localization.ProductReview.positiveTextFieldPlaceholder
        $0.placeholderFont = Font.textFieldPlaceholderFont
        $0.placeholderColor = Color.textFieldPlaceholderColor

        $0.titleColor = Color.textFieldPlaceholderColor
        $0.titleFormatter = { $0 }
        $0.selectedTitleColor = Color.textFieldPlaceholderColor

        $0.iconType = .image
        $0.iconImage = Image.positiveTextFieldImage
        $0.iconColor = Color.positiveTextFieldIconColor
        $0.iconMarginLeft = Metric.textFieldIconLeftMargin
        $0.iconMarginBottom = Metric.textFieldIconBottomMargin
        $0.selectedIconColor = Color.positiveTextFieldIconColor

        $0.lineColor = Color.textFieldLineColor
        $0.selectedLineColor = Color.textFieldSelectedLineColor

        $0.identifier = TextFieldFormElement.positive.rawValue
    }

    lazy var negativeTextField = SkyFloatingLabelTextFieldWithIcon().then {
        $0.font = Font.textFieldFont
        $0.delegate = self
        $0.textColor = Color.textFieldTextColor
        $0.returnKeyType = .next

        $0.placeholder = Localization.ProductReview.negativeTextFieldPlaceholder
        $0.placeholderFont = Font.textFieldPlaceholderFont
        $0.placeholderColor = Color.textFieldPlaceholderColor

        $0.titleColor = Color.textFieldPlaceholderColor
        $0.titleFormatter = { $0 }
        $0.selectedTitleColor = Color.textFieldPlaceholderColor

        $0.iconType = .image
        $0.iconImage = Image.negativeTextFieldImage
        $0.iconColor = Color.negativeTextFieldIconColor
        $0.iconMarginLeft = Metric.textFieldIconLeftMargin
        $0.iconMarginBottom = Metric.textFieldIconBottomMargin
        $0.selectedIconColor = Color.negativeTextFieldIconColor

        $0.lineColor = Color.textFieldLineColor
        $0.selectedLineColor = Color.textFieldSelectedLineColor

        $0.identifier = TextFieldFormElement.negative.rawValue
    }

    lazy var commentTextField = SkyFloatingLabelTextField().then {
        $0.font = Font.textFieldFont
        $0.delegate = self
        $0.textColor = Color.textFieldTextColor
        $0.returnKeyType = .send

        $0.placeholder = Localization.ProductReview.textFieldPlaceholder
        $0.placeholderFont = Font.textFieldPlaceholderFont
        $0.placeholderColor = Color.textFieldPlaceholderColor

        $0.titleColor = Color.textFieldPlaceholderColor
        $0.titleFormatter = { $0 }
        $0.selectedTitleColor = Color.textFieldPlaceholderColor

        $0.lineColor = Color.textFieldLineColor
        $0.selectedLineColor = Color.textFieldSelectedLineColor

        $0.identifier = TextFieldFormElement.comment.rawValue
    }

    // MARK: - Publishers

    fileprivate let completeActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupView()
    }

    private func setupHierarchy() {
        addArrangedSubview(positiveTextField)
        addArrangedSubview(negativeTextField)
        addArrangedSubview(commentTextField)
    }

    private func setupView() {
        axis = .vertical
        spacing = Metric.spacing
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewFormStackView {
    fileprivate enum Color {
        static let textFieldTextColor = Style.Color.black
        static let textFieldLineColor = Style.Color.grayBlue ?? .black
        static let textFieldSelectedLineColor = Style.Color.brightYellow ?? .black
        static let textFieldPlaceholderColor = UIColor(hex: "#9B9AA9") ?? .black
        static let positiveTextFieldIconColor = UIColor(hex: "#26CB5A") ?? .black
        static let negativeTextFieldIconColor = UIColor(hex: "#ED4C34") ?? .black
    }

    fileprivate enum Font {
        static let textFieldFont = Style.Font.sanFrancisco(.medium, size: 16)
        static let textFieldPlaceholderFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Image {
        static let positiveTextFieldImage = Media.icon(.plus, size: .x18)
        static let negativeTextFieldImage = Media.icon(.minus, size: .x18)
    }

    fileprivate enum Metric {
        static let spacing: CGFloat = Screen.isLowWidthScreen ? 12 : 32
        static let textFieldIconBottomMargin: CGFloat = 0
        static let textFieldIconLeftMargin: CGFloat = 8
    }

    fileprivate enum TextFieldFormElement: String {
        case positive
        case negative
        case comment
    }
}

// MARK: - UITextFieldDelegate

extension ProductReviewFormStackView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.identifier {
        case TextFieldFormElement.positive.rawValue:
            negativeTextField.becomeFirstResponder()
        case TextFieldFormElement.negative.rawValue:
            commentTextField.becomeFirstResponder()
        case TextFieldFormElement.comment.rawValue:
            commentTextField.resignFirstResponder()
            completeActionPublisher.onNext(())
            return true
        default:
            break
        }

        return false
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewFormStackView {
    var completeAction: ControlEvent<Void> {
        return ControlEvent(events: base.completeActionPublisher)
    }

    var positiveText: ControlProperty<String?> {
        return base.positiveTextField.rx.text
    }

    var negativeText: ControlProperty<String?> {
        return base.negativeTextField.rx.text
    }

    var commentText: ControlProperty<String?> {
        return base.commentTextField.rx.text
    }
}
