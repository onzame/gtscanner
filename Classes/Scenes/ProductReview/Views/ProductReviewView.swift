//
//  ProductReviewView.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductReviewView: ModalContentView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.text = Localization.ProductReview.title
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    let ratingControlView = RatingControlView()

    let shopsView = ShopsComponentView()

    let reviewFormStackView = ProductReviewFormStackView()

    let actionsView = ActionsComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductReview.submitActionTitle, for: .normal)
        $0.secondaryButton.setTitle(Localization.ProductReview.cancelActionTitle, for: .normal)
        $0.primaryContainerView.backgroundColor = Color.primaryActionDisabledBackgoundColor
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        bodyView.addSubview(titleLabel)
        bodyView.addSubview(ratingControlView)
        bodyView.addSubview(shopsView)
        bodyView.addSubview(reviewFormStackView)
        bodyView.addSubview(actionsView)
    }

    private func setupLayout() {
        actionsView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }

        reviewFormStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.reviewFormStackViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.reviewFormStackViewRightOffset)
            make.bottom.equalTo(actionsView.snp.top).offset(Metric.reviewFormStackViewBottomOffset)
        }

        shopsView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(reviewFormStackView.snp.top).offset(Metric.shopsViewBottomOffset)
        }

        ratingControlView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.ratingControlViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.ratingControlViewRightOffset)
            make.bottom.equalTo(shopsView.snp.top).offset(Metric.ratingControlViewBottomOffset)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.bottom.equalTo(ratingControlView.snp.top).offset(Metric.titleLabelBottomOffset)
            make.centerX.equalToSuperview()
        }
    }

    private func setupView() {
        setHideKeyboardOnTap()
    }

    // MARK: - Private methods

    fileprivate func enableSubmit(isEnabled: Bool) {
        actionsView.primaryButton.isEnabled = isEnabled

        if isEnabled {
            actionsView.primaryContainerView.backgroundColor = Color.primaryActionEnabledBackgoundColor
        } else {
            actionsView.primaryContainerView.backgroundColor = Color.primaryActionDisabledBackgoundColor
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewView {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
        static let primaryActionEnabledBackgoundColor = Style.Color.brightYellow
        static let primaryActionDisabledBackgoundColor = Style.Color.lightBlue
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.semibold, size: 20)
    }

    fileprivate enum Metric {
        static let reviewFormStackViewLeftOffset: CGFloat = 20
        static let reviewFormStackViewRightOffset: CGFloat = -20
        static let reviewFormStackViewBottomOffset: CGFloat = Screen.isLowWidthScreen ? -12 : -24

        static let shopsViewBottomOffset: CGFloat = Screen.isLowWidthScreen ? 0 : -24

        static let ratingControlViewLeftOffset: CGFloat = 20
        static let ratingControlViewRightOffset: CGFloat = -20
        static let ratingControlViewBottomOffset: CGFloat = Screen.isLowWidthScreen ? 0 : -12

        static let titleLabelBottomOffset: CGFloat = Screen.isLowWidthScreen ? -8 : -20
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewView {
    var isSubmitEnabled: Binder<Bool> {
        return Binder(base, binding: { view, isEnabled in
            view.enableSubmit(isEnabled: isEnabled)
        })
    }
}
