//
//  ProductReviewCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum ProductReviewCoordinationResult {
    case requestedShopSelection(ProductReviewFlowMetadata)
    case updatedProduct(UpdatedProductReviewMetadata)
    case canceled
}

final class ProductReviewCoordinator: Coordinator<ProductReviewCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let metadata: ProductReviewFlowMetadata

    init(rootViewController: UIViewController, container: DependenciesContainer, metadata: ProductReviewFlowMetadata) {
        self.rootViewController = rootViewController
        self.container = container
        self.metadata = metadata
    }

    override func start() -> Observable<ProductReviewCoordinationResult> {
        let viewModel = container.productReviewViewModel(metadata: metadata)
        let viewController = ProductReviewViewController(reactor: viewModel)
        let bottomSheetController = BottomSheetController(contentViewController: viewController)

        rootViewController.present(bottomSheetController, animated: true)

        let dismissingTrigger = bottomSheetController.rx.isDismissing
            .asObservable()

        // ProductReviewCoordinationResult.canceled

        let cancelTrigger = viewController.rx.cancelAction
            .asObservable()
            .do(onNext: { bottomSheetController.dismiss(animated: true) })

        let dismissTrigger = bottomSheetController.rx.dismissAction
            .asObservable()

        let canceledEvent = Observable.merge(cancelTrigger, dismissTrigger)
            .mapTo(ProductReviewCoordinationResult.canceled)

        // ProductReviewCoordinationResult.updatedProduct

        let updateProductTrigger = viewController.rx.updateProductAction
            .asObservable()
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let updatedProductEvent = Observable.combineLatest(dismissingTrigger, updateProductTrigger)
            .map({ ProductReviewCoordinationResult.updatedProduct($0.1) })

        // ProductRatingCoordinationResult.requestedShop

        let shopsTrigger = viewController.rx.shopsAction
            .asObservable()
            .do(onNext: { bottomSheetController.dismiss(animated: true) })

        let requestedShopEvent = Observable.combineLatest(dismissingTrigger, shopsTrigger)
            .withLatestFrom(viewModel.state)
            .map({ state -> ProductReviewCoordinationResult in
                let product = state.product
                let rating = state.selectedRating
                let shop = state.selectedShop
                let reviewText = ReviewText(positiveText: state.filledPositiveText, negativeText: state.filledNegativeText, text: state.filledText)
                let metadata = ProductReviewFlowMetadata(product: product, selectedRating: rating, selectedShop: shop, reviewText: reviewText)
                return ProductReviewCoordinationResult.requestedShopSelection(metadata)
            })

        return Observable.merge(canceledEvent, updatedProductEvent, requestedShopEvent)
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .rate, inCategory: .rate, tag: .evaluation)
            })
            .take(1)
    }

}
