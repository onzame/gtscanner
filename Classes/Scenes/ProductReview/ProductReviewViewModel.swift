//
//  ProductReviewViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 11.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift
import RxCocoa

final class ProductReviewViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let reviewsService: ReviewsServiceType

    // MARK: - Lifecycle

    init(reviewsService: ReviewsServiceType, metadata: ProductReviewFlowMetadata) {
        self.reviewsService = reviewsService
        self.initialState = State(metadata: metadata)
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .selectRating(let selectedRating):
            return .just(.selectRating(selectedRating))

        case .selecteShop(let selectedShop):
            return .just(.selectShop(selectedShop))

        case .changePositiveText(let text):
            return .just(.updatePositiveText(text))

        case .changeNegativeText(let text):
            return .just(.updateNegativeText(text))

        case .changeText(let text):
            return .just(.updateText(text))

        case .addReview(product: let product, rating: let rating, shop: let shop, reviewText: let reviewText):
            return reviewsService.addReview(product: product, rating: rating, shop: shop, reviewText: reviewText)
                .map({ UpdatedProductReviewMetadata(product: product.update(updates: $0.productUpdates), review: $0.productReview) })
                .map(Mutation.updatedProductMetadata)
                .catchError({ .just(.showError($0)) })
                .startWith(.setLoading(true))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .selectRating(let selectedRating):
            state.selectedRating = selectedRating

        case .selectShop(let selectedShop):
            state.selectedShop = selectedShop

            state.sections = state.sections?.map({ section -> ShopsPreviewSectionModel in
                if case ShopsPreviewSectionModel.section(items: let items) = section {
                    return .section(
                        items: items.map({ item -> ShopsPreviewItemModel in
                            if case ShopsPreviewItemModel.shop(let viewModel) = item {
                                let shop = viewModel.currentState.shop
                                let isSelected = viewModel.currentState.shop.identity == selectedShop.identity
                                let updatedViewModel = ShopsPreviewCollectionViewModel(shop: shop, isSelected: isSelected)
                                return .shop(updatedViewModel)
                            } else {
                                return item
                            }
                        })
                    )
                } else {
                    return section
                }
            })

        case .updatePositiveText(let text):
            state.filledPositiveText = text

        case .updateNegativeText(let text):
            state.filledNegativeText = text

        case .updateText(let text):
            state.filledText = text

        case .setLoading(let isLoading):
            state.isLoading = isLoading

        case .updatedProductMetadata(let metadata):
            state.updatedProductReviewMetadata = metadata
            state.isLoading = false

        case .showError(let error):
            state.error = error
            state.isLoading = false

        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductReviewViewModel {
    enum Action {
        case selectRating(Double)
        case selecteShop(Shop)
        case changePositiveText(String?)
        case changeNegativeText(String?)
        case changeText(String?)
        case addReview(product: Product, rating: Double, shop: Shop, reviewText: ReviewText)
    }
}

// MARK: - Mutations

extension ProductReviewViewModel {
    enum Mutation {
        case selectRating(Double)
        case selectShop(Shop)
        case updatePositiveText(String?)
        case updateNegativeText(String?)
        case updateText(String?)
        case setLoading(Bool)
        case updatedProductMetadata(UpdatedProductReviewMetadata)
        case showError(Error?)
    }
}

// MARK: - State

extension ProductReviewViewModel {
    struct State {
        let product: Product
        var sections: [ShopsPreviewSectionModel]?
        var selectedRating: Double?
        var selectedShop: Shop?
        var filledPositiveText: String?
        var filledNegativeText: String?
        var filledText: String?
        var isLoading: Bool = false
        var error: Error?
        var updatedProductReviewMetadata: UpdatedProductReviewMetadata?

        init(metadata: ProductReviewFlowMetadata) {
            self.product = metadata.product
            self.sections = metadata.product.shops.mapToSections()
            self.selectedRating = metadata.selectedRating
            self.selectedShop = metadata.selectedShop
            self.filledPositiveText = metadata.reviewText.positiveText
            self.filledNegativeText = metadata.reviewText.negativeText
            self.filledText = metadata.reviewText.text
        }
    }
}
