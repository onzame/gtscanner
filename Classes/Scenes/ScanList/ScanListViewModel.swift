//
//  ScanListViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

final class ScanListViewModel: Reactor {

    // MARK: - Properties

    var initialState = ScanListViewModel.State()

    // MARK: - Dependencies

    private let favoritesService: FavoritesServiceType

    // MARK: - Lifecycle

    init(favoritesService: FavoritesServiceType) {
        self.favoritesService = favoritesService
    }

    // MARK: - Transformations

    func mutate(action: ScanListViewModel.Action) -> Observable<ScanListViewModel.Mutation> {
        switch action {
        case .bindUpdates:
            return favoritesService.observeSavedProducts()
                .map(Mutation.mutateFavoriteProductsAggreagtions)

        case .fetch(location: let location):
            return favoritesService.refresh(location: location)
                .andThen(.just(.setLoaded(true)))
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh(location: let location):
            return favoritesService.refresh(location: location)
                .andThen(.just(.setRefreshing(false)))
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))

        case .deleteProduct(product: let favoriteProduct):
            return favoritesService.delete(product: favoriteProduct.product)
                .andThen(.just(.setDeleting(false)))
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setDeleting(true))
        }
    }

    func reduce(state: ScanListViewModel.State, mutation: ScanListViewModel.Mutation) -> ScanListViewModel.State {
        var state = state

        state.error = nil

        switch mutation {
        case .updateSavedProductsDetails(sections: let sections, details: let details):
            state.sections = sections
            state.savedProductsDetails = details
            state.shouldShowSavedProductsDetails = details != nil

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing

        case .setDeleting(let isDeleting):
            state.isDeleting = isDeleting

        case .setLoaded(let isLoadded):
            state.isLoaded = isLoadded

        case .showError(error: let error, isLoaded: let isLoaded):
            state.error = error
            state.isRefreshing = false
            state.isDeleting = false
            state.isLoaded = isLoaded
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScanListViewModel {
    enum Action {
        case bindUpdates
        case fetch(location: Location?)
        case refresh(location: Location?)
        case deleteProduct(FavoriteProduct)
    }
}

// MARK: - Mutations

extension ScanListViewModel {
    enum Mutation {
        static func mutateFavoriteProductsAggreagtions(aggregations: [FavoriteProductsAggregation]) -> Mutation {
            if aggregations.isNotEmpty {
                let sections = aggregations.mapToSections()
                let products = aggregations.map({ $0.products }).reduce([], +)
                let details = FavoriteProductsDetails(products: products)
                return .updateSavedProductsDetails(sections: sections, details: details)
            } else {
                let sections = aggregations.mapToSections()
                return .updateSavedProductsDetails(sections: sections, details: nil)
            }
        }

        case updateSavedProductsDetails(sections: [ScanListSectionModel], details: FavoriteProductsDetails?)
        case setRefreshing(Bool)
        case setDeleting(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
    }
}

// MARK: - State

extension ScanListViewModel {
    struct State {
        var sections: [ScanListSectionModel] = []
        var savedProductsDetails: FavoriteProductsDetails?
        var shouldShowSavedProductsDetails: Bool = false
        var isRefreshing: Bool = false
        var isDeleting: Bool = false
        var isLoaded: Bool = false
        var error: Error?

        var showLoadingState: Bool {
            return !isLoaded && error == nil
        }

        var showDefaultState: Bool {
            return sections.isNotEmpty && isLoaded
        }

        var showErrorState: Bool {
            return !isLoaded && error != nil
        }

        var showNoDataState: Bool {
            return sections.isEmpty && isLoaded
        }
    }
}
