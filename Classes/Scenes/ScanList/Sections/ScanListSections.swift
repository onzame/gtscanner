//
//  ScanListSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 27.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum ScanListSectionModel {
    case section(viewModel: ScanListCategoryHeaderTableViewModel, items: [ScanListItemModel])
}

extension ScanListSectionModel: AnimatableSectionModelType {
    var identity: Int {
        switch self {
        case .section(viewModel: let viewModel, _):
            return viewModel.currentState.category.id
        }
    }

    var items: [ScanListItemModel] {
        switch self {
        case .section(_, items: let items):
            return items
        }
    }

    init(original: ScanListSectionModel, items: [ScanListItemModel]) {
        switch original {
        case .section(viewModel: let viewModel, _):
            self = .section(viewModel: viewModel, items: items)
        }
    }
}

// MARK: - Items

enum ScanListItemModel {
    static func parseProduct(model: ScanListItemModel) -> ScanListSavedProductTableViewModel? {
        if case ScanListItemModel.product(let viewModel) = model {
            return viewModel
        } else {
            return nil
        }
    }

    case product(ScanListSavedProductTableViewModel)
}

extension ScanListItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .product(let viewModel):
            return viewModel.currentState.product.product.id
        }
    }
}

extension ScanListItemModel: Equatable {
    static func == (lhs: ScanListItemModel, rhs: ScanListItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.product(let left), .product(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Observable where Element == ScanListItemModel {
    func mapToFavoriteProduct() -> Observable<FavoriteProduct?> {
        return map({ model in
            if case ScanListItemModel.product(let viewModel) = model {
                return viewModel.currentState.product
            } else {
                return nil
            }
        })
    }
}

extension Array where Element == FavoriteProductsAggregation {
    func mapToSections() -> [ScanListSectionModel] {
        return map({ aggregation -> ScanListSectionModel in
            .section(
                viewModel: ScanListCategoryHeaderTableViewModel(category: aggregation.category),
                items: aggregation.products.map({ .product(ScanListSavedProductTableViewModel(product: $0)) })
            )
        })
        .sorted(by: { (left, right) -> Bool in
            left.identity < right.identity
        })
    }
}
