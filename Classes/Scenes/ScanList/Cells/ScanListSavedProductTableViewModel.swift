//
//  ScanListSavedProductTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ScanListSavedProductTableViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(product: FavoriteProduct) {
        initialState = State(product: product)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScanListSavedProductTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ScanListSavedProductTableViewModel {
    struct State {
        let product: FavoriteProduct
    }
}

// MARK: - Equatable

extension ScanListSavedProductTableViewModel: Equatable {
    static func == (lhs: ScanListSavedProductTableViewModel, rhs: ScanListSavedProductTableViewModel) -> Bool {
        return lhs.currentState == rhs.currentState
    }
}

// MARK: - State Equatable

extension ScanListSavedProductTableViewModel.State: Equatable {
    static func == (lhs: ScanListSavedProductTableViewModel.State, rhs: ScanListSavedProductTableViewModel.State) -> Bool {
        return lhs.product == rhs.product
    }
}
