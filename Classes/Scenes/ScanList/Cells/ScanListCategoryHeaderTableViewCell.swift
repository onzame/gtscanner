//
//  ScanListCategoryHeaderTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

final class ScanListCategoryHeaderTableViewCell: BaseTableViewHeaderFooterView, View {

    static let cellHeight: CGFloat = 64

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.text = "Title"
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
    }

    // MARK: - Lifecycle

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(titleLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.right.equalToSuperview().offset(Metric.titleLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.titleLabelBottomOffset)
        }
    }

    private func setupView() {
        contentView.backgroundColor = Color.contentViewBackgroundColor
    }

    // MARK: - Bindings

    func bind(reactor: ScanListCategoryHeaderTableViewModel) {
        reactor.state
            .map({ $0.category.name })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanListCategoryHeaderTableViewCell {
    fileprivate enum Color {
        static let contentViewBackgroundColor = Style.Color.white
        static let titleLabelTextColor = Style.Color.secondaryText
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.medium, size: 16)
    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 20
        static let titleLabelRightOffset: CGFloat = -20
        static let titleLabelBottomOffset: CGFloat = -4
    }
}
