//
//  ScanListSavedProductTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 18.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

final class ScanListSavedProductTableViewCell: BaseTableViewCell, View {

    static let cellHeight: CGFloat = 112

    // MARK: - UI components

    let imageContainerView = ImageContainerView().then {
//        $0.backgroundColor = Color.imageContainerViewBackgroundColor
        $0.layer.cornerRadius = CornerRadius.imageContainerViewCornerRadius
        $0.layer.masksToBounds = true
        $0.imageView.contentMode = .scaleAspectFit
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let priceLabel = UILabel().then {
        $0.text = "999 999 ₽"
        $0.font = Font.priceLabelFont
        $0.textColor = Color.priceLabelTextColor
    }

    let ratingView = RatingView().then {
        $0.settings.textFont = Font.informationLabelsFont
        $0.settings.textColor = Color.informationLabelsTextColor
        $0.settings.starSize = Metric.ratingViewStarPartSize
        $0.settings.starMargin = Metric.ratingViewStarMaring
        $0.settings.updateOnTouch = false
    }

    let ratesCounterView = TitledImageView().then {
        $0.titleLabel.font = Font.informationLabelsFont
        $0.titleLabel.textColor = Color.informationLabelsTextColor
        $0.imageView.image = Image.ratesCounterViewImage
        $0.imageView.alpha = 0.6
    }

    let reviewsCounterView = TitledImageView().then {
        $0.titleLabel.font = Font.informationLabelsFont
        $0.titleLabel.textColor = Color.informationLabelsTextColor
        $0.imageView.image = Image.reviewsCounterViewImage
        $0.imageView.alpha = 0.6
    }

    let informationStackView = UIStackView().then {
        $0.spacing = Metric.informationStackViewSpacing
        $0.alignment = .center
    }

    let amountView = TextContainerView().then {
        $0.textLabel.font = Font.amountViewTextLabelFont
        $0.textLabel.textColor = Color.amountViewTextLabelTextColor
        $0.backgroundColor = Color.amountViewBackgroundColor
        $0.layer.cornerRadius = CornerRadius.amountViewCornerRadius
        $0.layer.masksToBounds = true
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func prepareForReuse() {
        super.prepareForReuse()

        imageContainerView.imageView.kf.prepareForReuse()
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(imageContainerView)
        addSubview(descriptionStackView)
        addSubview(priceLabel)
        addSubview(informationStackView)
        addSubview(amountView)

        informationStackView.addArrangedSubview(ratingView)
        informationStackView.addArrangedSubview(ratesCounterView)
        informationStackView.addArrangedSubview(reviewsCounterView)
    }

    private func setupLayout() {
        imageContainerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.imageContainerViewLeftOffset)
            make.top.equalToSuperview().offset(Metric.imageContainerViewTopOffset)
            make.width.equalTo(Metric.imageContainerViewWidth)
            make.height.equalTo(Metric.imageContainerViewHeight)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalTo(imageContainerView.snp.right).offset(Metric.descriptionStackViewLeftOffset)
            make.centerY.equalTo(imageContainerView)
        }

        amountView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.amountViewRightOffset)
            make.bottom.equalToSuperview().offset(Metric.amountViewBottomOffset)
        }

        informationStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.informationStackViewLeftOffset)
            make.centerY.equalTo(amountView)
        }
    }

    private func setupView() {

    }

    // MARK: - Bindings

    func bind(reactor: ScanListSavedProductTableViewModel) {

        let placeholderSize = CGSize(width: Metric.imagePlaceholderWidth, height: Metric.imagePlaceholderHeight)
        let placeholder = ImagePlaceholderView.product(imageSize: placeholderSize)

        reactor.state
            .map({ $0.product.product.images.first?.url })
            .bind(to: imageContainerView.imageView.rx.image(placeholder: placeholder))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.name })
            .bind(to: descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.description })
            .bind(to: descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ "\($0.product.count) шт" })
            .bind(to: amountView.textLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.rating })
            .bind(to: ratingView.rx.rating)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.rating })
            .mapToString()
            .bind(to: ratingView.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.avgPrice })
            .mapToPrice()
            .bind(to: priceLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ ($0.product.product.avgPrice?.mapToPrice) ?? "~" })
            .bind(to: rx.updatePriceWidth)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.amounts.reviews })
            .mapToString()
            .bind(to: reviewsCounterView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.product.amounts.rates })
            .mapToString()
            .bind(to: ratesCounterView.rx.title)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanListSavedProductTableViewCell {
    fileprivate enum Color {
        static let imageContainerViewBackgroundColor = UIColor(hex: "#F4F4F4")
        static let descriptionStackViewTitleLabelTextColor = Style.Color.black
        static let descriptionStackViewSubtitleLabelTextColor = Style.Color.secondaryText
        static let informationLabelsTextColor = Style.Color.secondaryText ?? .black
        static let priceLabelTextColor = Style.Color.black
        static let amountViewTextLabelTextColor = Style.Color.green
        static let amountViewBackgroundColor = Style.Color.aquamarine?.withAlphaComponent(0.1)
    }

    fileprivate enum CornerRadius {
        static let imageContainerViewCornerRadius: CGFloat = 8
        static let amountViewCornerRadius: CGFloat = 12
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
        static let descriptionStackViewSubtitleLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let informationLabelsFont = Style.Font.sanFrancisco(.semibold, size: 12)
        static let priceLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
        static let amountViewTextLabelFont = Style.Font.sanFrancisco(.semibold, size: 12)
    }

    fileprivate enum Image {
        static let ratesCounterViewImage = Media.icon(.profile, size: .x18)
        static let reviewsCounterViewImage = Media.icon(.dialogBubble, size: .x18)
    }

    fileprivate enum Metric {
        static let imageContainerViewLeftOffset: CGFloat = 20
        static let imageContainerViewTopOffset: CGFloat = 20
        static let imageContainerViewWidth: CGFloat = 40
        static let imageContainerViewHeight: CGFloat = 36
        static let descriptionStackViewLeftOffset: CGFloat = 12
        static let descriptionStackViewSpacing: CGFloat = 2
        static let informationStackViewLeftOffset: CGFloat = 20
        static let informationStackViewTopOffset: CGFloat = 12
        static let informationStackViewSpacing: CGFloat = 20
        static let priceLabelTopOffset: CGFloat = 20
        static let priceLabelRightOffset: CGFloat = -20
        static let priceLabelLeftOffset: CGFloat = 8
        static let priceLabelHeight: CGFloat = 20
        static let amountViewRightOffset: CGFloat = -18
        static let amountViewBottomOffset: CGFloat = -14
        static let ratingViewStarPartSize: Double = 12
        static let ratingViewStarMaring: Double = 2
        static let imagePlaceholderWidth: CGFloat = 30
        static let imagePlaceholderHeight: CGFloat = 20
    }
}

extension Reactive where Base: ScanListSavedProductTableViewCell {
    var updatePriceWidth: Binder<String> {
        return Binder(base, binding: { view, priceString in
            let font = ScanListSavedProductTableViewCell.Font.self
            let metric = ScanListSavedProductTableViewCell.Metric.self
            let size = priceString.boundingSize(size: .greatestFiniteMagnitude, font: font.priceLabelFont)

            view.priceLabel.snp.remakeConstraints { (make) in
                make.top.equalToSuperview().offset(metric.priceLabelTopOffset)
                make.right.equalToSuperview().offset(metric.priceLabelRightOffset)
                make.left.equalTo(view.descriptionStackView.snp.right).offset(metric.priceLabelLeftOffset)
                make.width.equalTo(size.width)
                make.height.equalTo(size.height)
            }
        })
    }
}
