//
//  ScanListCategoryHeaderTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ScanListCategoryHeaderTableViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(category: Category) {
        initialState = State(category: category)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScanListCategoryHeaderTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ScanListCategoryHeaderTableViewModel {
    struct State {
        let category: Category
    }
}
