//
//  ScanListCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

private protocol ExtendedScanListCoordinator { }

// swiftlint:disable line_length
private extension ExtendedScanListCoordinator where Self: CoordinatorType {
    func showScanHistoryScene(navigationController: UINavigationController, container: DependenciesContainer) -> Observable<ScanHistoryCoordinator.CoordinationResult> {
        let coordinator = ScanHistoryCoordinator(rootNavigationController: navigationController, container: container)
        return coordinate(to: coordinator)
    }

    func showProductAmountScene(viewController: UIViewController, container: DependenciesContainer, input: ProductAmountInput) -> Observable<ModalProductAmountCoordinationResult> {
        let coordinator = ModalProductAmountCoordinator(rootViewController: viewController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    func showProductInfoScene(navigationController: UINavigationController, container: DependenciesContainer, input: ProductInfoInput) -> Observable<ProductInfoCoordinationResult> {
        let coordinator = ProductInfoCoordinator(rootNavigationController: navigationController, container: container, input: input)
        return coordinate(to: coordinator)
    }
}
// swiftlint:enable line_length

enum ScanListCoordinationResult {
    case popped
    case dismissed
}

final class ScanListCoordinator: Coordinator<ScanListCoordinationResult>, ExtendedScanListCoordinator {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer

    init(rootNavigationController: UINavigationController, container: DependenciesContainer) {
        self.rootNavigationController = rootNavigationController
        self.container = container
    }

    override func start() -> Observable<ScanListCoordinationResult> {
        let viewModel = container.scanListViewModel
        let viewController = ScanListViewController(reactor: viewModel)

        rootNavigationController.pushViewController(viewController, animated: true)

        let popTrigger = viewController.rx.popAction
            .mapTo(ScanListCoordinationResult.popped)

        let isScanHistoryWasBefore = rootNavigationController.viewControllers.last is ScanHistoryViewController
        let isScanHistoryWasBeforeElement = Observable.just(isScanHistoryWasBefore)

        let shouldPopToScanHistoryScene = viewController.rx.showHistorySceneAction
            .withLatestFrom(isScanHistoryWasBeforeElement)

        let popToScanHistoryTrigger = shouldPopToScanHistoryScene
            .filter({ $0 })
            .mapTo(ScanListCoordinationResult.popped)
            .do(onNext: { [weak self] _ in
                self?.rootNavigationController.popViewController(animated: true)
            })

        shouldPopToScanHistoryScene
            .filter({ !$0 })
            .flatMap({ [weak self] _ -> Observable<ScanHistoryCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                return self.showScanHistoryScene(navigationController: self.rootNavigationController, container: self.container)
            })
            .subscribe()
            .disposed(by: disposeBag)

        viewController.rx.showChangeAmountSceneAction
            .asObservable()
            .flatMap({ [weak self] savedProduct -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: savedProduct.product, amount: savedProduct.count, mode: .common)
                return self.showProductAmountScene(viewController: viewController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        let dismissedToScannerTrigger = viewController.rx.productSelection
            .asObservable()
            .flatMap({ [weak self] favoriteProduct -> Observable<ProductInfoCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductInfoInput(product: favoriteProduct.product)
                return self.showProductInfoScene(navigationController: self.rootNavigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                if case ProductInfoCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapTo(ScanListCoordinationResult.dismissed)

        return Observable.merge(popTrigger, popToScanHistoryTrigger, dismissedToScannerTrigger)
            .take(1)
    }
}

final class ModalScanListCoordinator: Coordinator<Void>, ExtendedScanListCoordinator {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer

    init(rootViewController: UIViewController, container: DependenciesContainer) {
        self.rootViewController = rootViewController
        self.container = container
    }

    override func start() -> Observable<Void> {
        let viewModel = container.scanListViewModel
        let viewController = ScanListViewController(reactor: viewModel)
        let navigationController = BaseNavigationController(rootViewController: viewController)

        rootViewController.present(navigationController, animated: true)

        let cancelTrigger = viewController.rx.dismissAction
            .asObservable()
            .do(onNext: { navigationController.dismiss(animated: true) })

        let historyTrigger = viewController.rx.showHistorySceneAction
            .asObservable()
            .flatMap({ [weak self] _ -> Observable<ScanHistoryCoordinationResult> in
                guard let `self` = self else { return .empty() }
                return self.showScanHistoryScene(navigationController: navigationController, container: self.container)
            })
            .filter({ $0 == .dismissed })
            .mapToVoid()

        viewController.rx.showChangeAmountSceneAction
            .asObservable()
            .flatMap({ [weak self] savedProduct -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: savedProduct.product, amount: savedProduct.count, mode: .common)
                return self.showProductAmountScene(viewController: viewController, container: self.container, input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        let dismissedToScannerTrigger = viewController.rx.productSelection
            .asObservable()
            .flatMap({ [weak self] favoriteProduct -> Observable<ProductInfoCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductInfoInput(product: favoriteProduct.product)
                return self.showProductInfoScene(navigationController: navigationController, container: self.container, input: input)
            })
            .filter({ coordinationResult -> Bool in
                if case ProductInfoCoordinationResult.dismissed = coordinationResult {
                    return true
                } else {
                    return false
                }
            })
            .mapToVoid()

        return Observable.merge(cancelTrigger, historyTrigger, dismissedToScannerTrigger)
            .take(1)
    }
}
