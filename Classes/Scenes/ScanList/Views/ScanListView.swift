//
//  ScanListView.swift
//  Gorod
//
//  Created by Sergei Fabian on 03.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit
import SwipeCellKit

class ScanListView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let savedProductsDetailsView = ProductsDetailsView(mode: .dark).then {
        $0.layer.zPosition = 1
        $0.setCornerRadius(CornerRadius.savedProductsDetailsViewCornerRadius)
    }

    let refreshControl = UIRefreshControl()

    let tableView = UITableView().then {
        $0.register(Reusable.savedProductCell)
        $0.register(Reusable.categoryHeaderView)
        $0.separatorColor = Color.tableViewSeparatorColor
        $0.separatorInset = UIEdgeInsets.zero
        $0.tableFooterView = UIView()
    }

    // MARK: - DataSource

    lazy var dataSource = RxTableViewSectionedAnimatedDataSource<ScanListSectionModel>(
        configureCell: { [weak self] (_, tableView, indexPath, item) -> UITableViewCell in
            switch item {
            case .product(let viewModel):
                let cell = tableView.dequeue(Reusable.savedProductCell, for: indexPath)
                cell.reactor = viewModel
                cell.delegate = self
                return cell
            }
        }
    )

    // MARK: - Publishers

    fileprivate let deleteActionPublisher = PublishSubject<FavoriteProduct>()
    fileprivate let itemChangePublisher = PublishSubject<FavoriteProduct>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        addSubview(tableView)
    }

    private func setupLayout() {
        tableView.addEdgesConstraints()
    }

    private func setupView() {
        tableView.refreshControl = refreshControl
    }

    private func setupBinding() {
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

    // MARK: - SavedProductsDetailsView showing

    fileprivate func showSavedProductsDetailsView() {
        if savedProductsDetailsView.superview == nil {
            setupSavedProductsDetailsViewHierarchy()
            setupSavedProductsDetailsViewLayout()
            setupTableViewInsets()
        }
    }

    fileprivate func hideSavedProductsDetailsView() {
        if savedProductsDetailsView.superview != nil {
            savedProductsDetailsView.removeFromSuperview()
            tableView.setBottomInset(.zero)
        }
    }

    fileprivate func setupSavedProductsDetailsViewHierarchy() {
        addSubview(savedProductsDetailsView)
    }

    fileprivate func setupSavedProductsDetailsViewLayout() {
        savedProductsDetailsView.snp.makeConstraints { (make) in
            make.left.equalTo(safeLeftConstraintItem).offset(Metric.savedProductsDetailsViewLeftOffset)
            make.right.equalTo(safeRightConstraintItem).offset(Metric.savedProductsDetailsViewRightOffset)
            make.bottom.equalTo(safeBottomConstraintItem).offset(Metric.savedProductsDetailsViewBottomOffset)
            make.height.equalTo(Metric.savedProductsDetailsViewHeight)
        }

        layoutIfNeeded()
    }

    fileprivate func setupTableViewInsets() {
        tableView.setBottomInset(Metric.tableViewBottomInset)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanListView {
    fileprivate enum Color {
        static let tableViewSeparatorColor = Style.Color.grayBlue
        static let deleteActionBackgroundColor = Style.Color.pink
        static let changeActionBackgroundColor = UIColor(hex: "FFD41A")
    }

    fileprivate enum CornerRadius {
        static let savedProductsDetailsViewCornerRadius: CGFloat = 18
    }

    fileprivate enum Image {
        static let deleteActionImage = Media.icon(.trash, size: .x24)
        static let changeActionImage = Media.icon(.plus, size: .x24)
    }

    fileprivate enum Metric {
        static let savedProductsDetailsViewLeftOffset: CGFloat = 20
        static let savedProductsDetailsViewRightOffset: CGFloat = -20
        static let savedProductsDetailsViewBottomOffset: CGFloat = -20
        static let savedProductsDetailsViewHeight: CGFloat = 64
        static let tableViewBottomInset: CGFloat = 92
    }

    fileprivate enum Reusable {
        static let savedProductCell = ReusableCell<ScanListSavedProductTableViewCell>()
        static let categoryHeaderView = ReusableView<ScanListCategoryHeaderTableViewCell>()
    }
}

// MARK: - UITableViewDelegate

extension ScanListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch dataSource[section] {
        case .section(viewModel: let viewModel, _):
            let cell = tableView.dequeue(Reusable.categoryHeaderView)
            cell?.reactor = viewModel
            return cell
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch dataSource[section] {
        case .section:
            return Reusable.categoryHeaderView.class.cellHeight
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSource[indexPath] {
        case .product:
            return Reusable.savedProductCell.class.cellHeight
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - SwipeTableViewCellDelegate

extension ScanListView: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        switch dataSource[indexPath] {
        case .product(let viewModel):
            let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] (_, _) in
                self?.deleteActionPublisher.onNext(viewModel.currentState.product)
            }.then {
                $0.backgroundColor = Color.deleteActionBackgroundColor
                $0.image = Image.deleteActionImage?.tint(with: .white)
            }

            let changeAction = SwipeAction(style: .default, title: nil) { [weak self] (_, _) in
                let cell = self?.tableView.cellForRow(at: indexPath) as? SwipeTableViewCell
                cell?.hideSwipe(animated: true)
                self?.itemChangePublisher.onNext(viewModel.currentState.product)
            }.then {
                $0.backgroundColor = Color.changeActionBackgroundColor
                $0.image = Image.changeActionImage
            }

            return [deleteAction, changeAction]
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScanListView {
    var productSelection: ControlEvent<FavoriteProduct> {
        let source = base.tableView.rx.itemSelected(dataSource: base.dataSource)
            .asObservable()
            .mapToFavoriteProduct()
            .filterNil()

        return ControlEvent(events: source)
    }

    var productDeletion: ControlEvent<FavoriteProduct> {
        return ControlEvent(events: base.deleteActionPublisher)
    }

    var showChangeAmountSceneAction: ControlEvent<FavoriteProduct> {
        return ControlEvent(events: base.itemChangePublisher)
    }

    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }

    var showProductsDetailsBinding: Binder<Bool> {
        return Binder(base, binding: { view, shouldShow in
            if shouldShow {
                view.showSavedProductsDetailsView()
            } else {
                view.hideSavedProductsDetailsView()
            }
        })
    }
}
