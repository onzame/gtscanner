//
//  ScanListViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa
import RxDataSources
import RxSwiftExt
import ReusableKit
import SwipeCellKit

class ScanListViewController: BaseViewController<ScanListViewModel> {

    // MARK: - UI components

    let contentView = ScanListView()

    // MARK: - Publishers

    fileprivate let showHistoryScenePublisher = PublishSubject<Void>()
    fileprivate let retryActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.ScanList.title
        view.backgroundColor = Color.backgroundColor
    }

    // MARK: - Bindings

    override func bind(reactor: ScanListViewModel) {

        rx.viewWillAppear
            .asObservable()
            .mapToVoid()
            .bind(to: LocationSessionController.shared.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewWillDisappear
            .asObservable()
            .mapToVoid()
            .bind(to: LocationSessionController.shared.rx.disableUpdating)
            .disposed(by: disposeBag)

        // Input

        rx.viewDidLoad
            .mapTo(Reactor.Action.bindUpdates)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewWillAppear
            .take(1)
            .withLatestFrom(LocationSessionController.shared.rx.location)
            .mapToDomain()
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .debounce(.seconds(1), scheduler: MainScheduler.instance)
            .withLatestFrom(LocationSessionController.shared.rx.location)
            .mapToDomain()
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.productDeletion
            .map(Reactor.Action.deleteProduct)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.refreshAction
            .withLatestFrom(LocationSessionController.shared.rx.location)
            .mapToDomain()
            .map(Reactor.Action.refresh)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.tableView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .distinctUntilChanged()
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ (flag: $0.showErrorState, error: $0.error) })
            .distinctUntilChanged({ $0.flag == $1.flag })
            .filter({ $0.flag })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showLoadingState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showDefaultState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showNoDataState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.savedProductsDetails })
            .filterNil()
            .distinctUntilChanged()
            .bind(to: contentView.savedProductsDetailsView.rx.update)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.shouldShowSavedProductsDetails })
            .distinctUntilChanged()
            .bind(to: contentView.rx.showProductsDetailsBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isDeleting })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScanListViewController {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.lightBlue
    }

    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
    }
}

// MARK: - StateResolvable

extension ScanListViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        if case LocationSessionError.accessDenied = error {
            return buildLocationErrorStateView().then {
                $0.image = Image.errorBackgroundViewImage
                $0.actionButton.rx.tap
                    .flatMap({ _ in UIApplication.shared.rx.openSettingsAction() })
                    .flatMap({ _ in UIApplication.shared.rx.didBecomeActiveObservable().take(1) })
                    .do(onNext: { [weak self] in self?.retryActionPublisher.onNext(()) })
                    .subscribe()
                    .disposed(by: disposeBag)
            }
        } else {
            return ErrorStateView().then {
                $0.image = Image.errorBackgroundViewImage
                $0.title = Localization.ProductPricesList.errorPlaceholderTitle
                $0.subtitle = Localization.ProductPricesList.errorPlaceholderSubtitle
                $0.actionTitle = Localization.ProductPricesList.errorPlaceholderActionTitle
                $0.actionButton.rx.tap
                    .bind(to: retryActionPublisher)
                    .disposed(by: $0.disposeBag)
            }
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.ScanList.noDataPlaceholderTitle
            $0.subtitle = Localization.ScanList.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.ScanList.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: showHistoryScenePublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScanListViewController {
    var productSelection: ControlEvent<FavoriteProduct> {
        return base.contentView.rx.productSelection
    }

    var productDeletion: ControlEvent<FavoriteProduct> {
        return base.contentView.rx.productDeletion
    }

    var showChangeAmountSceneAction: ControlEvent<FavoriteProduct> {
        return base.contentView.rx.showChangeAmountSceneAction
    }

    var showHistorySceneAction: ControlEvent<Void> {
        return ControlEvent(events: base.showHistoryScenePublisher)
    }
}
