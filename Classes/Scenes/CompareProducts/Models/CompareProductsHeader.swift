//
//  CompareProductsHeader.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareProductsHeader {
    let id: String
    let title: String
    let type: CompareProductsHeaderType
}

// MARK: - Extensions
// MARK: - Equatable

extension CompareProductsHeader: Equatable {
    static func == (lhs: CompareProductsHeader, rhs: CompareProductsHeader) -> Bool {
        return lhs.id == rhs.id
            && lhs.title == rhs.title
            && lhs.type == rhs.type
    }
}
