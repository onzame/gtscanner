//
//  CompareProductsInput.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum CompareProductsInput {
    case compareCategory(CompareCategory)
    case product(Product)

    var product: Product? {
        if case CompareProductsInput.product(let product) = self {
            return product
        } else {
            return nil
        }
    }
}
