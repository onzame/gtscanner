//
//  CompareProductsHeaderType.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum CompareProductsHeaderType {
    case primary
    case secondary
}
