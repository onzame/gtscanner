//
//  СompareProductsSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum CompareProductsSectionModel {
    case products(items: [CompareProductsItemModel])
    case properties(viewModel: CompareProductsHeaderCollectionViewModel, items: [CompareProductsItemModel])
}

extension CompareProductsSectionModel: AnimatableSectionModelType {
    var identity: String {
        switch self {
        case .products:
            return "СompareProductsSectionModel.products"
        case .properties(viewModel: let viewModel, _):
            return "СompareProductsSectionModel.properties.\(viewModel.currentState.header.id).\(viewModel.currentState.header.type)"
        }
    }

    var items: [CompareProductsItemModel] {
        switch self {
        case .products(items: let items):
            return items
        case .properties(_, items: let items):
            return items
        }
    }

    init(original: CompareProductsSectionModel, items: [CompareProductsItemModel]) {
        switch original {
        case .products:
            self = .products(items: items)
        case .properties(viewModel: let viewModel, _):
            self = .properties(viewModel: viewModel, items: items)
        }
    }
}

// MARK: - Items

enum CompareProductsItemModel {
    case product(CompareProductsCollectionViewModel)
    case amounts(CompareProductsAmountsCollectionViewModel)
    case property(CompareProductsPropertyCollectionViewModel)
}

extension CompareProductsItemModel: IdentifiableType {
    var identity: String {
        switch self {
        case .product(let viewModel):
            return "СompareProductsItemModel.product.\(viewModel.currentState.product.id)"
        case .amounts(let viewModel):
            return "СompareProductsItemModel.amounts.\(viewModel.currentState.product.id).\(viewModel.currentState.type)"
        case .property(let viewModel):
            return "СompareProductsItemModel.property.\(viewModel.currentState.propertyValue.id)"
        }
    }
}

extension CompareProductsItemModel: Equatable {
    static func == (lhs: CompareProductsItemModel, rhs: CompareProductsItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.product(let left), .product(let right)):
            return left.currentState == right.currentState
        case (.amounts(let left), .amounts(let right)):
            return left.currentState == right.currentState
        case (.property(let left), .property(let right)):
            return left.currentState == right.currentState
        default:
            return false
        }
    }
}

extension CompareComposition {
    func mapToSections() -> [CompareProductsSectionModel] {
        var productsSectionItems = [CompareProductsItemModel]()
        var ratingSectionItems = [CompareProductsItemModel]()
        var reviewsSectionItems = [CompareProductsItemModel]()

        products.forEach({ compareProduct in
            productsSectionItems.append(.product(CompareProductsCollectionViewModel(product: compareProduct)))
            ratingSectionItems.append(.amounts(CompareProductsAmountsCollectionViewModel(product: compareProduct, type: .rating)))
            reviewsSectionItems.append(.amounts(CompareProductsAmountsCollectionViewModel(product: compareProduct, type: .reviews)))
        })

        var sections = [CompareProductsSectionModel]()

        if productsSectionItems.isEmpty {
            return sections
        }

        sections.append(.products(items: productsSectionItems))

        let ratingTitle = Localization.CompareProducts.ratingHeaderTitle
        let ratingHeader = CompareProductsHeader(id: ratingTitle, title: ratingTitle, type: .primary)
        let ratingSectionViewModel = CompareProductsHeaderCollectionViewModel(header: ratingHeader)

        if ratingSectionItems.isNotEmpty {
            sections.append(.properties(viewModel: ratingSectionViewModel, items: ratingSectionItems))
        }

        let reviewsTitle = Localization.CompareProducts.reviewsHeaderTitle
        let reviewsHeader = CompareProductsHeader(id: reviewsTitle, title: reviewsTitle, type: .primary)
        let reviewsSectionViewModel = CompareProductsHeaderCollectionViewModel(header: reviewsHeader)

        if reviewsSectionItems.isNotEmpty {
            sections.append(.properties(viewModel: reviewsSectionViewModel, items: reviewsSectionItems))
        }

        details.forEach({ compareProductDetails in
            let primarySectionHeader = CompareProductsHeader(id: compareProductDetails.title, title: compareProductDetails.title, type: .primary)
            let primarySectionViewModel = CompareProductsHeaderCollectionViewModel(header: primarySectionHeader)

            let propertiesSections = compareProductDetails.properties.map({ property -> CompareProductsSectionModel in
                let sectionHeader = CompareProductsHeader(id: "\(property.id)", title: property.title, type: .secondary)
                let sectionViewModel = CompareProductsHeaderCollectionViewModel(header: sectionHeader)

                let sectionItems = property.values.map({ propertyValue -> CompareProductsItemModel in
                    let viewModel = CompareProductsPropertyCollectionViewModel(propertyValue: propertyValue)
                    return .property(viewModel)
                })

                return .properties(viewModel: sectionViewModel, items: sectionItems)
            })

            if propertiesSections.isNotEmpty {
                sections.append(.properties(viewModel: primarySectionViewModel, items: []))
                sections.append(contentsOf: propertiesSections)
            }
        })

        return sections
    }
}
