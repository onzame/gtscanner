//
//  CompareProductsCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class CompareProductsCollectionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(product: CompareProduct) {
        initialState = State(product: product)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareProductsCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension CompareProductsCollectionViewModel {
    struct State {
        let product: CompareProduct
    }
}

// MARK: - Equatable

extension CompareProductsCollectionViewModel.State: Equatable {
    static func == (lhs: CompareProductsCollectionViewModel.State, rhs: CompareProductsCollectionViewModel.State) -> Bool {
        return lhs.product == rhs.product
    }
}
