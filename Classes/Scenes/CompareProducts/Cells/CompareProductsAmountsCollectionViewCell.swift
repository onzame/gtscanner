//
//  CompareProductsAmountsCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class CompareProductsAmountsCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let titledImageView = TitledImageView().then {
        $0.imageView.image = Image.ratingAmountViewImage
        $0.titleLabel.font = Font.titledImageViewTitleFont
        $0.titleLabel.textColor = Color.titledImageViewTitleTextColor
        $0.stackView.spacing = Metric.titledImageViewStackViewSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        contentView.addSubview(titledImageView)
    }

    private func setupLayout() {
        titledImageView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.centerY.equalToSuperview()
            make.right.lessThanOrEqualToSuperview()
        }
    }

    // MARK: - Bindings

    func bind(reactor: CompareProductsAmountsCollectionViewModel) {
        reactor.state
            .map({ $0.value })
            .bind(to: titledImageView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.type })
            .bind(to: rx.type)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareProductsAmountsCollectionViewCell {
    fileprivate enum Color {
        static let titledImageViewTitleTextColor = Style.Color.black
    }

    fileprivate enum CornerRadius {
        static let contentViewCornerRadius: CGFloat = 10
    }

    fileprivate enum Font {
        static let titledImageViewTitleFont = Style.Font.sanFrancisco(.semibold, size: 20)
    }

    fileprivate enum Image {
        static let ratingAmountViewImage = Media.icon(.starFilled, size: .x24)
        static let reviewsAmountViewImage = Media.icon(.dialogBubble, size: .x24)
    }

    fileprivate enum Metric {
        static let titledImageViewStackViewSpacing: CGFloat = 4
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CompareProductsAmountsCollectionViewCell {
    var type: Binder<CompareProductsAmountsValueType> {
        return Binder(base, binding: { view, type in
            switch type {
            case .rating:
                view.titledImageView.imageView.image = CompareProductsAmountsCollectionViewCell.Image.ratingAmountViewImage
            case .reviews:
                view.titledImageView.imageView.image = CompareProductsAmountsCollectionViewCell.Image.reviewsAmountViewImage
            }
        })
    }
}
