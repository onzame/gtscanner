//
//  CompareProductsAmountsCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 23.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class CompareProductsAmountsCollectionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(product: CompareProduct, type: CompareProductsAmountsValueType) {
        initialState = State(product: product, type: type)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareProductsAmountsCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension CompareProductsAmountsCollectionViewModel {
    struct State {
        let product: CompareProduct
        let type: CompareProductsAmountsValueType

        var value: String {
            switch type {
            case .rating:
                return product.rating.clean
            case .reviews:
                return "\(product.amounts.reviews)"
            }
        }
    }
}

// MARK: - Equatable

extension CompareProductsAmountsCollectionViewModel.State: Equatable {
    static func == (lhs: CompareProductsAmountsCollectionViewModel.State, rhs: CompareProductsAmountsCollectionViewModel.State) -> Bool {
        return lhs.product == rhs.product
            && lhs.type == rhs.type
    }
}
