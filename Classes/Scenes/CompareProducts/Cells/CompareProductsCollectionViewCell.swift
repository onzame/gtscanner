//
//  CompareProductsCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class CompareProductsCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let deleteButton = UIButton(type: .system).then {
        $0.setImage(Image.deleteButtonImage, for: .normal)
        $0.tintColor = Color.deleteButtonTintColor
    }

    let photoImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let priceLabel = UILabel().then {
        $0.font = Font.priceLabelFont
        $0.textColor = Color.priceLabelTextColor
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrodes

    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.kf.prepareForReuse()
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        contentView.addSubview(deleteButton)
        contentView.addSubview(photoImageView)
        contentView.addSubview(descriptionStackView)
        contentView.addSubview(priceLabel)
    }

    private func setupLayout() {
        deleteButton.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Metric.deleteButtonTopOffset)
            make.right.equalToSuperview().offset(Metric.deleteButtonRightOffset)
            make.width.height.equalTo(Metric.deleteButtonPartSize)
        }

        photoImageView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.photoImageViewLeftOffset)
            make.top.equalToSuperview().offset(Metric.photoImageViewTopOffset)
            make.right.equalToSuperview().offset(Metric.photoImageViewRightOffset)
            make.height.equalTo(photoImageView.snp.width)
        }

        priceLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.priceLabelLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.priceLabelRightOffset)
            make.bottom.equalToSuperview().offset(Metric.priceLabelBottomOffset)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.descriptionStackViewRightOffset)
            make.bottom.equalTo(priceLabel.snp.top).offset(Metric.descriptionStackViewBottomOffset)
        }
    }

    private func setupView() {
        contentView.setBackgroundColor(Color.contentViewBackgroundColor)
        contentView.setCornerRadius(CornerRadius.contentViewCornerRadius)
    }

    // MARK: - Bindings

    func bind(reactor: CompareProductsCollectionViewModel) {
        let placeholder = ImagePlaceholderView(image: Image.photoImageViewPlaceholderImage)

        reactor.state
            .map({ $0.product.image?.url })
            .bind(to: photoImageView.rx.image(placeholder: placeholder))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.name })
            .bind(to: descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.productionPlace })
            .bind(to: descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.avgPrice })
            .mapToPrice()
            .bind(to: priceLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareProductsCollectionViewCell {
    fileprivate enum Color {
        static let contentViewBackgroundColor = Style.Color.white
        static let deleteButtonTintColor = Style.Color.black
        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitleTextColor = Style.Color.secondaryText
        static let priceLabelTextColor = Style.Color.black

    }

    fileprivate enum CornerRadius {
        static let contentViewCornerRadius: CGFloat = 10
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.semibold, size: 16)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let priceLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
    }

    fileprivate enum Image {
        static let photoImageViewPlaceholderImage = Media.image(.productPlaceholder)
        static let deleteButtonImage = Media.icon(.clear, size: .x18)
    }

    fileprivate enum Metric {
        static let deleteButtonTopOffset: CGFloat = 12
        static let deleteButtonRightOffset: CGFloat = -12
        static let deleteButtonPartSize: CGFloat = 18

        static let photoImageViewLeftOffset: CGFloat = 40
        static let photoImageViewTopOffset: CGFloat = 32
        static let photoImageViewRightOffset: CGFloat = -40

        static let priceLabelLeftOffset: CGFloat = 16
        static let priceLabelRightOffset: CGFloat = -16
        static let priceLabelBottomOffset: CGFloat = -16

        static let descriptionStackViewLeftOffset: CGFloat = 16
        static let descriptionStackViewRightOffset: CGFloat = -16
        static let descriptionStackViewBottomOffset: CGFloat = -16
        static let descriptionStackViewSpacing: CGFloat = 2
    }
}
