//
//  CompareProductsPropertyCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class CompareProductsPropertyCollectionViewCell: BaseCollectionViewCell, View {

    static func cellSize(insideOf contentWidth: CGFloat, viewModel: CompareProductsPropertyCollectionViewModel) -> CGSize {
        let height = viewModel.currentState.propertyValue.value.boundingHeight(width: contentWidth, font: Font.titleLabelFont)
        return CGSize(width: contentWidth, height: height + Metrics.additionalBottomSpace)
    }

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.numberOfLines = 0
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        contentView.addSubview(titleLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }
    }

    // MARK: - Bindings

    func bind(reactor: CompareProductsPropertyCollectionViewModel) {
        reactor.state
            .map({ $0.propertyValue.value })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareProductsPropertyCollectionViewCell {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.regular, size: 14)
    }

    fileprivate enum Metrics {
        static let additionalBottomSpace: CGFloat = 4
    }
}
