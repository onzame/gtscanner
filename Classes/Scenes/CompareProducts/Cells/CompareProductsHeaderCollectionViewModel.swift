//
//  CompareProductsHeaderCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class CompareProductsHeaderCollectionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(header: CompareProductsHeader) {
        initialState = State(header: header)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareProductsHeaderCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension CompareProductsHeaderCollectionViewModel {
    struct State {
        let header: CompareProductsHeader
    }
}

// MARK: - Equatable

extension CompareProductsHeaderCollectionViewModel.State: Equatable {
    static func == (lhs: CompareProductsHeaderCollectionViewModel.State, rhs: CompareProductsHeaderCollectionViewModel.State) -> Bool {
        return lhs.header == rhs.header
    }
}
