//
//  CompareProductsPropertyCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class CompareProductsPropertyCollectionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(propertyValue: CompareProductPropertyValue) {
        initialState = State(propertyValue: propertyValue)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareProductsPropertyCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension CompareProductsPropertyCollectionViewModel {
    struct State {
        let propertyValue: CompareProductPropertyValue
    }
}

// MARK: - Equatable

extension CompareProductsPropertyCollectionViewModel.State: Equatable {
    static func == (lhs: CompareProductsPropertyCollectionViewModel.State, rhs: CompareProductsPropertyCollectionViewModel.State) -> Bool {
        return lhs.propertyValue == rhs.propertyValue
    }
}
