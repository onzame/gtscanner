//
//  CompareProductsHeaderCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class CompareProductsHeaderCollectionViewCell: BaseCollectionViewCell, View {

    static func cellSize(insideOf collectionView: UICollectionView, viewModel: CompareProductsHeaderCollectionViewModel) -> CGSize {
        let model = viewModel.currentState.header
        let width = collectionView.bounds.width
        let contentWidth = width - (abs(Metric.titleLabelLeftOffset) + abs(Metric.titleLabelRightOffset))

        let font: UIFont

        switch model.type {
        case .primary:
            font = Font.titleLabelPrimaryFont
        case .secondary:
            font = Font.titleLabelSecondaryFont
        }

        return CGSize(width: width, height: model.title.boundingHeight(width: contentWidth, font: font))
    }

    // MARK: - UI components

    let titleLabel = UILabel()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        contentView.addSubview(titleLabel)
    }

    private func setupLayout() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.titleLabelLeftOffset)
            make.right.equalToSuperview().offset(Metric.titleLabelRightOffset)
            make.centerY.equalToSuperview()
        }
    }

    // MARK: - Bindings

    func bind(reactor: CompareProductsHeaderCollectionViewModel) {
        reactor.state
            .map({ $0.header.title })
            .bind(to: titleLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.header.type })
            .bind(to: rx.type)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareProductsHeaderCollectionViewCell {
    fileprivate enum Color {
        static let titleLabelPrimaryTextColor = Style.Color.newGray
        static let titleLabelSecondaryTextColor = Style.Color.secondaryText
    }

    fileprivate enum Font {
        static let titleLabelPrimaryFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let titleLabelSecondaryFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Metric {
        static let titleLabelLeftOffset: CGFloat = 20
        static let titleLabelRightOffset: CGFloat = -20
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CompareProductsHeaderCollectionViewCell {
    var type: Binder<CompareProductsHeaderType> {
        return Binder(base, binding: { view, type in
            switch type {
            case .primary:
                view.titleLabel.font = CompareProductsHeaderCollectionViewCell.Font.titleLabelPrimaryFont
                view.titleLabel.textColor = CompareProductsHeaderCollectionViewCell.Color.titleLabelPrimaryTextColor
            case .secondary:
                view.titleLabel.font = CompareProductsHeaderCollectionViewCell.Font.titleLabelSecondaryFont
                view.titleLabel.textColor = CompareProductsHeaderCollectionViewCell.Color.titleLabelSecondaryTextColor
            }
        })
    }
}
