//
//  CompareProductsView.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class CompareProductsView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let collectionViewLayout = SingleRowCollectionViewFlowLayout().then {
        $0.sectionInset = UIEdgeInsets(
            top: Metric.collectionViewTopInset,
            left: Metric.collectionViewInset,
            bottom: Metric.collectionViewBottomInset,
            right: Metric.collectionViewInset
        )
        $0.minimumInteritemSpacing = Metric.collectionViewInteritemSpacing
        $0.minimumLineSpacing = Metric.collectionViewLineSpacing
    }

    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewLayout).then {
        $0.setBackgroundColor(Color.collectionViewBackgroundColor)
        $0.register(Reusable.productCell)
        $0.register(Reusable.amountsCell)
        $0.register(Reusable.propertyCell)
        $0.register(Reusable.headerView, kind: .header)
        $0.register(Reusable.emptyView, kind: .header)
        $0.bounces = false
    }

    // MARK: - DataSource

    lazy var dataSource = RxCollectionViewSectionedAnimatedDataSource<CompareProductsSectionModel>(
        configureCell: { [unowned deleteProductActionPublisher] (dataSource, collectionView, indexPath, model) -> UICollectionViewCell in
            switch model {
            case .product(let viewModel):
                let cell = collectionView.dequeue(Reusable.productCell, for: indexPath)
                cell.reactor = viewModel
                cell.deleteButton.rx.tap
                    .withLatestFrom(viewModel.state)
                    .map({ $0.product })
                    .bind(to: deleteProductActionPublisher)
                    .disposed(by: cell.disposeBag)

                return cell
            case .amounts(let viewModel):
                let cell = collectionView.dequeue(Reusable.amountsCell, for: indexPath)
                cell.reactor = viewModel
                return cell
            case .property(let viewModel):
                let cell = collectionView.dequeue(Reusable.propertyCell, for: indexPath)
                cell.reactor = viewModel
                return cell
            }
    },
        configureSupplementaryView: { (dataSource, collectionView, kind, indexPath) -> UICollectionReusableView in
            switch (dataSource[indexPath.section], kind) {
            case (.products, SupplementaryViewKind.header.rawValue):
                return collectionView.dequeue(Reusable.emptyView, kind: kind, for: indexPath)
            case (.properties(viewModel: let viewModel, _), SupplementaryViewKind.header.rawValue):
                let view = collectionView.dequeue(Reusable.headerView, kind: kind, for: indexPath)
                view.reactor = viewModel
                return view
            default:
                return collectionView.dequeue(Reusable.emptyView, kind: kind, for: indexPath)
            }
    })

    // MARK: - Publishers

    fileprivate let deleteProductActionPublisher = PublishSubject<CompareProduct>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupBinding()
    }

    private func setupHierarchy() {
        addSubview(collectionView)
    }

    private func setupLayout() {
        collectionView.addEdgesConstraints()
    }

    private func setupBinding() {
        collectionView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }
}

// MARK: - Extensions
// MARK: - Constants

extension CompareProductsView {
    fileprivate enum Color {
        static let collectionViewBackgroundColor = Style.Color.lightBlue
    }

    fileprivate enum CornerRadius {

    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {

    }

    fileprivate enum Image {

    }

    fileprivate enum Metric {
        static let collectionViewTopInset: CGFloat = 4
        static let collectionViewBottomInset: CGFloat = 24
        static let collectionViewInset: CGFloat = 20
        static let collectionViewInteritemSpacing: CGFloat = 12
        static let collectionViewLineSpacing: CGFloat = 24
        static let productCellHeight: CGFloat = 220
        static let amountsCellHeight: CGFloat = 24

    }

    fileprivate enum Reusable {
        static let headerView = ReusableView<CompareProductsHeaderCollectionViewCell>()
        static let productCell = ReusableCell<CompareProductsCollectionViewCell>()
        static let amountsCell = ReusableCell<CompareProductsAmountsCollectionViewCell>()
        static let propertyCell = ReusableCell<CompareProductsPropertyCollectionViewCell>()
        static let emptyView = ReusableView<UICollectionReusableView>()
    }
}

// MARK: - UICollectionViewDelegate

extension CompareProductsView: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        let contentWidth = (collectionView.bounds.width - Metric.collectionViewInset * 2 - Metric.collectionViewInteritemSpacing) / 2

        switch dataSource[indexPath] {
        case .product:
            return CGSize(width: contentWidth, height: Metric.productCellHeight)
        case .amounts:
            return CGSize(width: contentWidth, height: Metric.amountsCellHeight)
        case .property(let viewModel):
            return Reusable.propertyCell.class.cellSize(insideOf: contentWidth, viewModel: viewModel)
        }
    }

    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        switch dataSource[section] {
        case .products:
            return .zero
        case .properties(viewModel: let viewModel, _):
            return Reusable.headerView.class.cellSize(insideOf: collectionView, viewModel: viewModel)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CompareProductsView {
    var productDeletion: ControlEvent<CompareProduct> {
        return ControlEvent(events: base.deleteProductActionPublisher)
    }
}
