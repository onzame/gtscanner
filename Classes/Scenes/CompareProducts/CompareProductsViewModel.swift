//
//  CompareProductsViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

class CompareProductsViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let comparesService: ComparesServiceType
    private let input: CompareProductsInput

    // MARK: - Lifecycle

    init(comparesService: ComparesServiceType, input: CompareProductsInput) {
        self.comparesService = comparesService
        self.input = input

        switch input {
        case .compareCategory(let compareCategory):
            self.initialState = State(compareCategory: compareCategory)
        case .product(let product):
            self.initialState = State(product: product)
        }
    }

    deinit {
        log.info("deinit CompareProductsViewModel")
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .fetch(category: let category):
            return comparesService.fetchProducts(category: category, targetProduct: input.product)
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .delete(product: let product):
            return comparesService.deleteProduct(product: product)
                .map(Mutation.update)
                .catchError({ .just(.showError(error: $0, isLoaded: true)) })
                .startWith(.setDeleting(true))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .update(let compareComposition):
            state.sections = compareComposition.mapToSections()
            state.isDeleting = false
            state.isLoaded = true

        case .setLoaded(let isLoaded):
            state.isLoaded = isLoaded

        case .setDeleting(let isDeleting):
            state.isDeleting = isDeleting

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isDeleting = false
            state.isLoaded = isLoaded
            state.error = error
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension CompareProductsViewModel {
    enum Action {
        case fetch(category: CompareCategory)
        case delete(product: CompareProduct)
    }
}

// MARK: - Mutations

extension CompareProductsViewModel {
    enum Mutation {
        case update(CompareComposition)
        case setDeleting(Bool)
        case setLoaded(Bool)
        case showError(error: Error, isLoaded: Bool)
    }
}

// MARK: - State

extension CompareProductsViewModel {
    struct State {
        let compareCategory: CompareCategory
        var sections: [CompareProductsSectionModel] = []
        var isDeleting: Bool = false
        var isLoaded: Bool = false
        var error: Error?

        var showLoadingState: Bool {
            return !isLoaded && error == nil
        }

        var showDefaultState: Bool {
            return sections.isNotEmpty && isLoaded
        }

        var showErrorState: Bool {
            return !isLoaded && error != nil
        }

        var showNoDataState: Bool {
            return sections.isEmpty && isLoaded
        }

        init(compareCategory: CompareCategory) {
            self.compareCategory = compareCategory
        }

        init(product: Product) {
            self.compareCategory = CompareCategory(categoryId: product.category.id, name: product.category.name, count: 0)
        }
    }
}
