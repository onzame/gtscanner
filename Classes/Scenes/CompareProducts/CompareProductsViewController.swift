//
//  CompareProductsViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 24.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class CompareProductsViewController: BaseViewController<CompareProductsViewModel> {

    // MARK: - UI components

    let contentView = CompareProductsView()

    // MARK: - Publishers

    fileprivate let retryActionPublisher = PublishSubject<Void>()
    fileprivate let showScannerActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {

    }

    private func setupLayout() {

    }

    private func setupView() {

    }

    // MARK: - Bindings

    override func bind(reactor: CompareProductsViewModel) {

        // Input

        rx.viewDidLoad
            .take(1)
            .withLatestFrom(reactor.state)
            .map({ $0.compareCategory })
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .withLatestFrom(reactor.state)
            .map({ $0.compareCategory })
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.productDeletion
            .map(Reactor.Action.delete)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.collectionView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ (flag: $0.showErrorState, error: $0.error) })
            .distinctUntilChanged({ $0.flag == $1.flag })
            .filter({ $0.flag })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showLoadingState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showDefaultState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.showNoDataState })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isDeleting })
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.compareCategory.name })
            .distinctUntilChanged()
            .mapToControllerTitle()
            .bind(to: rx.title)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension CompareProductsViewController {
    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
    }
}

// MARK: - StateResolvable

extension CompareProductsViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.CompareProducts.errorPlaceholderTitle
            $0.subtitle = Localization.CompareProducts.errorPlaceholderSubtitle
            $0.actionTitle = Localization.CompareProducts.errorPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.CompareProducts.noDataPlaceholderTitle
            $0.subtitle = Localization.CompareProducts.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.CompareProducts.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: showScannerActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: CompareProductsViewController {
    var showScannerAction: ControlEvent<Void> {
        return ControlEvent(events: base.showScannerActionPublisher)
    }
}
