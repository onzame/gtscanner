//
//  CompareProductsCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum CompareProductsCoordinationResult {
    case popped
    case dismissed
}

class CompareProductsCoordinator: Coordinator<CompareProductsCoordinationResult> {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer
    private let input: CompareProductsInput

    init(rootNavigationController: UINavigationController, container: DependenciesContainer, input: CompareProductsInput) {
        self.rootNavigationController = rootNavigationController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<CompareProductsCoordinationResult> {
        let viewModel = container.compareProductsViewModel(input: input)
        let viewController = CompareProductsViewController(reactor: viewModel)

        rootNavigationController.pushViewController(viewController, animated: true)

        // CompareProductsCoordinationResult.popped

        let poppedEvent = viewController.rx.popAction
            .mapTo(CompareProductsCoordinationResult.popped)

        // CompareProductsCoordinationResult.dismissed

        let dismissedEvent = viewController.rx.showScannerAction
            .mapTo(CompareProductsCoordinationResult.dismissed)
            .do(onNext: { [weak self] _ in
                self?.rootNavigationController.dismiss(animated: true)
            })

        return Observable.merge(poppedEvent, dismissedEvent)
            .take(1)
    }

}
