//
//  ProductReviewCompletionInput.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductReviewCompletionInput {
    let product: Product
    let review: Review
}
