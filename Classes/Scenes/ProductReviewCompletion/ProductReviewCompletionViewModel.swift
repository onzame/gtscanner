//
//  ProductReviewCompletionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ProductReviewCompletionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(input: ProductReviewCompletionInput) {
        initialState = State(product: input.product, review: input.review)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductReviewCompletionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ProductReviewCompletionViewModel {
    struct State {
        let product: Product
        let review: Review
    }
}
