//
//  ProductReviewCompletionView.swift
//  Gorod
//
//  Created by Sergei Fabian on 02.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductReviewCompletionView: ModalContentView {

    // MARK: - UI components

    let descriptionView = CompletionDescriptionComponentView().then {
        $0.iconImageView.image = Media.image(.handLike)
        $0.titleLabel.text = Localization.ProductRatingCompletion.title
    }

    let actionView = CompletionActionComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductPriceCompletion.actionTitle, for: .normal)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        bodyView.addSubview(descriptionView)
        bodyView.addSubview(actionView)
    }

    private func setupLayout() {
        actionView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }

        descriptionView.snp.makeConstraints { (make) in
            make.left.equalTo(safeLeftConstraintItem).offset(Metric.descriptionViewLeftOffset)
            make.right.equalTo(safeRightConstraintItem).offset(Metric.descriptionViewRightOffset)
            make.bottom.equalTo(actionView.snp.top).offset(Metric.descriptionViewBottomOffset)
            make.top.equalToSuperview().offset(10)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewCompletionView {
    fileprivate enum Metric {
        static let descriptionViewBottomOffset: CGFloat = -24
        static let descriptionViewLeftOffset: CGFloat = 24
        static let descriptionViewRightOffset: CGFloat = -24
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewCompletionView {
    var subtitle: Binder<Int> {
        return Binder(base, binding: { view, rating in
            let subtitle = Localization.ProductRatingCompletion.subtitle(rating, Rating.totalStars)
            view.descriptionView.subtitleLabel.text = subtitle
        })
    }
}
