//
//  ProductReviewCompletionCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 10.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum ProductReviewCompletionCoordinationResult {
    case canceled
}

final class ProductReviewCompletionCoordinator: Coordinator<ProductReviewCompletionCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let input: ProductReviewCompletionInput

    init(rootViewController: UIViewController, container: DependenciesContainer, input: ProductReviewCompletionInput) {
        self.rootViewController = rootViewController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<ProductReviewCompletionCoordinationResult> {
        let viewModel = container.productReviewCompletionViewModel(input: input)
        let viewController = ProductReviewCompletionViewController(reactor: viewModel)
        let bottomSheetController = BottomSheetController(contentViewController: viewController)

        rootViewController.present(bottomSheetController, animated: true)

        // ProductReviewCompletionCoordinationResult.canceled

        let cancelAction = viewController.rx.cancelAction
            .asObservable()
            .mapTo(ProductReviewCompletionCoordinationResult.canceled)
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let dismissAction = viewController.rx.dismissAction
            .asObservable()
            .mapTo(ProductReviewCompletionCoordinationResult.canceled)

        return Observable.merge(cancelAction, dismissAction)
            .take(1)
    }

}
