//
//  ProductReviewCompletionViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

final class ProductReviewCompletionViewController: BaseViewController<ProductReviewCompletionViewModel> {

    // MARK: - UI components

    let contentView = ProductReviewCompletionView()

    // MARK: - Lifecycle

    override init(reactor: ProductReviewCompletionViewModel) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.addEdgesConstraints()
    }

    private func setupView() {
        preferredContentSize = CGSize(width: Metric.width, height: Metric.height)
    }

    // MARK: - Bindings

    override func bind(reactor: ProductReviewCompletionViewModel) {
        reactor.state
            .map({ $0.review.rating })
            .mapToInt()
            .bind(to: contentView.rx.subtitle)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewCompletionViewController {
    fileprivate enum Metric {
        static let width: CGFloat = Screen.width
        static let height: CGFloat = 276
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewCompletionViewController {
    var cancelAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.actionView.primaryButton.rx.tap)
    }
}
