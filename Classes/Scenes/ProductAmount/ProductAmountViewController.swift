//
//  ProductAmountViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ProductAmountViewController: BaseViewController<ProductAmountViewModel> {

    // MARK: - UI components

    let contentView = ProductAmountView()

    // MARK: - Properties

    let mode: Mode

    // MARK: - Publishers

    fileprivate let commitChangeActionPublisher = PublishSubject<Void>()

    // MARK: - Lifecycle

    init(reactor: Reactor, mode: Mode) {
        self.mode = mode
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.addEdgesConstraints()
    }

    private func setupView() {
        preferredContentSize = CGSize(width: Metric.width, height: Metric.height)
    }

    // MARK: - Bindings

    override func bind(reactor: ProductAmountViewModel) {

        // Settings

        let modeSequence = Observable.just(mode)

        contentView.amountTextField.rx.text.orEmpty
            .mapToInt(placeholder: 0)
            .withLatestFrom(modeSequence, resultSelector: { (amount: $0, mode: $1) })
            .filter({ $0.mode == .add })
            .map({ $0.amount > 0 })
            .bind(to: contentView.rx.isSubmitEnabled)
            .disposed(by: disposeBag)

        contentView.amountTextField.rx.text.orEmpty
            .mapToInt(placeholder: 0)
            .withLatestFrom(modeSequence, resultSelector: { (amount: $0, mode: $1) })
            .filter({ $0.mode == .common })
            .map({ $0.amount > 0 })
            .bind(to: contentView.rx.isPositiveModeEnabled)
            .disposed(by: disposeBag)

        // Input

        contentView.actionsView.primaryButton.rx.tap
            .withLatestFrom(contentView.amountTextField.rx.text.orEmpty)
            .mapToInt()
            .filterNil()
            .map(Reactor.Action.updateAmount)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.actionsView.primaryButton.rx.tap
            .withLatestFrom(reactor.state)
            .withLatestFrom(modeSequence, resultSelector: { (state, mode) -> Reactor.Action? in
                if state.amount > 0 {
                    switch mode {
                    case .add:
                        return .append(product: state.product, amount: state.amount)
                    case .common:
                        return .update(product: state.product, amount: state.amount)
                    }
                } else {
                    return nil
                }
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.product.name })
            .bind(to: contentView.descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.productionPlace })
            .bind(to: contentView.descriptionStackView.rx.subtitle )
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.avgPrice })
            .mapToPrice()
            .bind(to: contentView.priceLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .take(1)
            .map({ $0.amount })
            .mapToString()
            .bind(to: contentView.amountTextField.rx.updateText)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isUpdating })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.productAmountChanged })
            .mapToVoid()
            .bind(to: commitChangeActionPublisher)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductAmountViewController {
    fileprivate enum Metric {
        static var width: CGFloat = Screen.width
        static let height: CGFloat = 256
    }
}

// MARK: - Mode

extension ProductAmountViewController {
    enum Mode {
        case add
        case common
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductAmountViewController {
    var commitChangeAction: ControlEvent<Void> {
        return ControlEvent(events: base.commitChangeActionPublisher)
    }

    var cancelAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.actionsView.secondaryButton.rx.tap)
    }
}
