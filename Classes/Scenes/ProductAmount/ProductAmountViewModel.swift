//
//  ProductAmountViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

final class ProductAmountViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Dependencies

    private let favoritesService: FavoritesServiceType

    // MARK: - Lifecycle

    init(favoritesService: FavoritesServiceType, input: ProductAmountInput) {
        self.favoritesService = favoritesService
        self.initialState = State(product: input.product, amount: input.amount)
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .append(product: let product, amount: let amount):
            return favoritesService.append(product: product, amount: amount)
                .andThen(.just(.commitUpdating))
                .catchError({ .just(.showError($0)) })
                .startWith(.beginUpdating)

        case .update(product: let product, amount: let amount):
            return favoritesService.update(product: product, amount: amount)
                .andThen(.just(.commitUpdating))
                .catchError({ .just(.showError($0)) })
                .startWith(.beginUpdating)

        case .updateAmount(let amount):
            return .just(.updateAmount(amount))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .beginUpdating:
            state.isUpdating = true

        case .commitUpdating:
            state.isUpdating = false
            state.productAmountChanged = true

        case .showError(let error):
            state.isUpdating = false
            state.error = error

        case .updateAmount(let amount):
            state.amount = amount
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductAmountViewModel {
    enum Action {
        case append(product: Product, amount: Int)
        case update(product: Product, amount: Int)
        case updateAmount(Int)
    }
}

// MARK: - Mutations

extension ProductAmountViewModel {
    enum Mutation {
        case beginUpdating
        case commitUpdating
        case showError(Error)
        case updateAmount(Int)
    }
}

// MARK: - State

extension ProductAmountViewModel {
    struct State {
        let product: Product
        var amount: Int
        var isUpdating = false
        var productAmountChanged = false
        var error: Error?

        init(product: Product, amount: Int?) {
            self.product = product
            self.amount = amount ?? 1
        }
    }
}
