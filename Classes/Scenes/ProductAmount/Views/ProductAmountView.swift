//
//  ProductAmountView.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ProductAmountView: ModalContentView {

    // MARK: - UI components

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let priceLabel = UILabel().then {
        $0.text = "999 999 ₽"
        $0.font = Font.priceLabelFont
        $0.textColor = Color.priceLabelTextColor
        $0.textAlignment = .right
    }

    let amountTextField = AmountTextField()

    let actionsView = ActionsComponentView().then {
        $0.primaryContainerView.backgroundColor = Color.primaryActionAddBackgoundColor
        $0.primaryButton.setTitle(Localization.ProductAmount.addActionTitle, for: .normal)
        $0.primaryButton.setTitleColor(Color.primaryActionAddTitleColor, for: .normal)
        $0.primaryButton.titleLabel?.font = Font.actionTitleFont
        $0.secondaryButton.setTitle(Localization.ProductAmount.cancelActionTitle, for: .normal)
        $0.secondaryButton.setTitleColor(Color.secondaryButtonTitleColor, for: .normal)
        $0.secondaryButton.titleLabel?.font = Font.actionTitleFont
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        actionsView.primaryContainerView.setCornerRadius(
            corners: .layerMinXMinYCorner,
            radius: CornerRadius.primaryActionCornerRadius
        )
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        bodyView.addSubview(descriptionStackView)
        bodyView.addSubview(priceLabel)
        bodyView.addSubview(amountTextField)
        bodyView.addSubview(actionsView)
    }

    private func setupLayout() {
        actionsView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }

        amountTextField.snp.makeConstraints { (make) in
            make.left.equalTo(safeLeftConstraintItem).offset(Metric.amountTextFieldLeftOffset)
            make.right.equalTo(safeRightConstraintItem).offset(Metric.amountTextFieldRightOffset)
            make.bottom.equalTo(actionsView.snp.top).offset(Metric.amountTextFieldBottomOffset)
            make.height.equalTo(Metric.amountTextFieldHeight)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalTo(safeLeftConstraintItem).offset(Metric.descriptionStackViewLeftOffset)
            make.bottom.equalTo(amountTextField.snp.top).offset(Metric.descriptionStackViewBottomOffset)
        }

        priceLabel.snp.makeConstraints { (make) in
            make.top.equalTo(descriptionStackView)
            make.left.greaterThanOrEqualTo(descriptionStackView.snp.right).offset(Metric.priceLabelLeftOffset)
            make.right.equalTo(safeRightConstraintItem).offset(Metric.priceLabelRightOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductAmountView {
    fileprivate enum Color {
        static let primaryActionAddBackgoundColor = Style.Color.brightYellow
        static let primaryActionAddTitleColor = Style.Color.black
        static let primaryActionDeleteBackgroundColor = Style.Color.pink
        static let primaryAcrionDeleteTitleColor = Style.Color.white
        static let secondaryButtonTitleColor = Style.Color.newGray
        static let amountTextFieldBorderColor = UIColor(hex: "#EFEFEF")
        static let descriptionStackViewTitleLabelTextColor = Style.Color.black
        static let descriptionStackViewSubtitleLabelTextColor = Style.Color.secondaryText
        static let priceLabelTextColor = Style.Color.black
    }

    fileprivate enum CornerRadius {
        static let primaryActionCornerRadius: CGFloat = 14
        static let amountTextFieldBorderCornerRadius: CGFloat = 8
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleLabelFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let descriptionStackViewSubtitleLabelFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let priceLabelFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let actionTitleFont = Style.Font.sanFrancisco(.medium, size: 18)
    }

    fileprivate enum Metric {
        static let width: CGFloat = Screen.width
        static let height: CGFloat = 280
        static let amountTextFieldLeftOffset: CGFloat = 25
        static let amountTextFieldRightOffset: CGFloat = -25
        static let amountTextFieldBottomOffset: CGFloat = -20
        static let amountTextFieldHeight: CGFloat = 64
        static let descriptionStackViewLeftOffset: CGFloat = 24
        static let descriptionStackViewBottomOffset: CGFloat = -20
        static let descriptionStackViewSpacing: CGFloat = 4
        static let priceLabelRightOffset: CGFloat = -24
        static let priceLabelLeftOffset: CGFloat = 8
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductAmountView {
    var isSubmitEnabled: Binder<Bool> {
        return Binder(base, binding: { view, isEnabled in
            view.actionsView.primaryButton.isEnabled = isEnabled
        })
    }

    var isPositiveModeEnabled: Binder<Bool> {
        return Binder(base, binding: { view, isModeEnabled in
            UIView.performWithoutAnimation {
                let primaryContainerView = view.actionsView.primaryContainerView
                let primaryButton = view.actionsView.primaryButton

                if isModeEnabled {
                    primaryContainerView.backgroundColor = ProductAmountView.Color.primaryActionAddBackgoundColor
                    primaryButton.setTitle(Localization.ProductAmount.addActionTitle, for: .normal)
                    primaryButton.setTitleColor(ProductAmountView.Color.primaryActionAddTitleColor, for: .normal)
                } else {
                    primaryContainerView.backgroundColor = ProductAmountView.Color.primaryActionDeleteBackgroundColor
                    primaryButton.setTitle(Localization.ProductAmount.deleteActionTitle, for: .normal)
                    primaryButton.setTitleColor(ProductAmountView.Color.primaryAcrionDeleteTitleColor, for: .normal)
                }

                primaryButton.layoutIfNeeded()
            }
        })
    }
}
