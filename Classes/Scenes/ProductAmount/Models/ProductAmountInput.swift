//
//  ProductAmountInput.swift
//  Gorod
//
//  Created by Sergei Fabian on 14.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductAmountInput {
    let product: Product
    let amount: Int?
    let mode: ProductAmountViewController.Mode
}
