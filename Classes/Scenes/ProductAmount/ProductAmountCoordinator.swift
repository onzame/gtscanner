//
//  ProductAmountCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 28.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum ModalProductAmountCoordinationResult {
    case changed
    case canceled
}

final class ModalProductAmountCoordinator: Coordinator<ModalProductAmountCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let input: ProductAmountInput

    init(rootViewController: UIViewController, container: DependenciesContainer, input: ProductAmountInput) {
        self.rootViewController = rootViewController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<ModalProductAmountCoordinationResult> {
        let viewModel = container.productAmountViewModel(input: input)
        let viewController = ProductAmountViewController(reactor: viewModel, mode: input.mode)
        let bottomSheetController = BottomSheetController(contentViewController: viewController)

        rootViewController.present(bottomSheetController, animated: true)

        let cancelAction = viewController.rx.cancelAction
            .asObservable()
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let dismissAction = bottomSheetController.rx.dismissAction
            .asObservable()

        let cancelTrigger = Observable.merge(cancelAction, dismissAction)
            .mapTo(ModalProductAmountCoordinationResult.canceled)

        let changeTrigger = viewController.rx.commitChangeAction
            .asObservable()
            .mapTo(ModalProductAmountCoordinationResult.changed)
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        return Observable.merge(changeTrigger, cancelTrigger)
            .take(1)
    }

}
