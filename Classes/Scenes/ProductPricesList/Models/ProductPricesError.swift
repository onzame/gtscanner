//
//  ProductPricesError.swift
//  Gorod
//
//  Created by Sergei Fabian on 30.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

enum ProductPricesError: Error {
    case noData
    case noResponse
}
