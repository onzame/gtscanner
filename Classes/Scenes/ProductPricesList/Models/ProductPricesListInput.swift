//
//  ProductPricesListInput.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct ProductPricesListInput {
    let product: Product
}
