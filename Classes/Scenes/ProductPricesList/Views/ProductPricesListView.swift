//
//  ProductPricesListView.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit
import SwipeCellKit

class ProductPricesListView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let addPriceView = CompletionActionComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductPricesList.addPriceActionTitle, for: .normal)
    }

    let refreshControl = UIRefreshControl()

    let tableView = UITableView().then {
        $0.setBackgroundColor(Color.tableViewBackgroundColor)
        $0.setBottomInset(Metric.tableViewBottomInset)
        $0.register(Reusable.priceCell)
        $0.allowsSelection = false
        $0.separatorInset = .zero
        $0.separatorColor = Color.tableViewSeparatorColor
    }

    // MARK: - DataSource

    lazy var dataSource = RxTableViewSectionedAnimatedDataSource<ProductPricesSectionModel>(
        configureCell: { [weak self] (_, tableView, indexPath, model) -> UITableViewCell in
            switch model {
            case .price(let viewModel):
                let cell = tableView.dequeue(Reusable.priceCell, for: indexPath)
                cell.reactor = viewModel
                cell.delegate = self
                return cell
            }
        },
        canEditRowAtIndexPath: { (dataSource, indexPath) -> Bool in
            switch dataSource[indexPath] {
            case .price(let viewModel):
                return viewModel.currentState.price.permissions.canDelete
            }
        }
    )

    // MARK: - Publishers

    fileprivate let deletePriceActionPublisher = PublishSubject<Price>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        addSubview(tableView)
        addSubview(addPriceView)
    }

    private func setupLayout() {
        tableView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(safeBottomConstraintItem)
        }

        addPriceView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }
    }

    private func setupView() {
        tableView.refreshControl = refreshControl
    }

    private func setupBinding() {
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductPricesListView {
    fileprivate enum Color {
        static let tableViewBackgroundColor = Style.Color.white
        static let tableViewSeparatorColor = UIColor(hex: "#F2F2F2")
        static let deleteActionBackgroundColor = Style.Color.pink
    }

    fileprivate enum Image {
        static let deleteActionImage = Media.icon(.trash, size: .x24)
    }

    fileprivate enum Metric {
        static let tableViewBottomInset: CGFloat = 80
    }

    fileprivate enum Reusable {
        static let priceCell = ReusableCell<ProductPriceTableViewCell>()
    }
}

// MARK: - UITableViewDelegate

extension ProductPricesListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSource[indexPath] {
        case .price:
            return ProductPriceTableViewCell.cellHeight
        }
    }
}

// MARK: - SwipeTableViewCellDelegate

extension ProductPricesListView: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        switch dataSource[indexPath] {
        case .price(let viewModel):
            if viewModel.currentState.price.permissions.canDelete {
                let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] (_, _) in
                    self?.deletePriceActionPublisher.onNext(viewModel.currentState.price)
                }.then {
                    $0.backgroundColor = Color.deleteActionBackgroundColor
                    $0.image = Image.deleteActionImage?.tint(with: .white)
                }

                return [deleteAction]
            } else {
                return nil
            }
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductPricesListView {
    var priceDeletion: ControlEvent<Price> {
        return ControlEvent(events: base.deletePriceActionPublisher)
    }

    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }

    var addPriceAction: ControlEvent<Void> {
        return ControlEvent(events: base.addPriceView.primaryButton.rx.tap)
    }
}
