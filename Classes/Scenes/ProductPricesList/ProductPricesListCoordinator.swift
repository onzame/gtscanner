//
//  ProductPricesListCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum ProductPricesListCoordinationResult {
    case popped(Product)
}

final class ProductPricesListCoordinator: Coordinator<ProductPricesListCoordinationResult>, ProductModificationCoordinatable {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer
    private let input: ProductPricesListInput

    private let actionPublisher = PublishSubject<ProductPricesListViewModel.Action>()

    init(rootNavigationController: UINavigationController, container: DependenciesContainer, input: ProductPricesListInput) {
        self.rootNavigationController = rootNavigationController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<ProductPricesListCoordinationResult> {
        let viewModel = container.productPricesListViewModel(input: input)
        let viewController = ProductPricesListViewController(reactor: viewModel)
        rootNavigationController.pushViewController(viewController, animated: true)

        let popAction = viewController.rx.popAction
            .withLatestFrom(viewModel.state)
            .map({ ProductPricesListCoordinationResult.popped($0.product) })

        actionPublisher
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)

        viewController.rx.addPriceAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductPriceFlowMetadata(product: product, selectedPrice: nil, selectedShop: nil)

                self.startProductPriceRecursiveFlow(
                    viewController: viewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)

        return popAction
            .take(1)
    }

    // MARK: - ProductModificationCoordinatable

    func updateProduct(type: ProductUpdateResultType) {
        if case ProductUpdateResultType.price(let metadata) = type {
            actionPublisher.onNext(.insertAddedPrice(price: metadata.price))
        }
    }

}
