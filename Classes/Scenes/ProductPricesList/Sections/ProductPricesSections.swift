//
//  ProductPricesSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum ProductPricesSectionModel {
    case section(items: [ProductPricesItemModel])
}

extension ProductPricesSectionModel: AnimatableSectionModelType {
    var identity: String {
        return "section"
    }

    var items: [ProductPricesItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: ProductPricesSectionModel, items: [ProductPricesItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum ProductPricesItemModel {
    case price(ProductPriceTableViewModel)
}

extension ProductPricesItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .price(let viewModel):
            return viewModel.currentState.price.id
        }
    }
}

extension ProductPricesItemModel: Equatable {
    static func == (lhs: ProductPricesItemModel, rhs: ProductPricesItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.price(let left), .price(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Array where Element == Price {
    func mapToSections() -> [ProductPricesSectionModel] {
        let items = map({ price -> ProductPricesItemModel in
                let viewModel = ProductPriceTableViewModel(price: price)
                let itemModel = ProductPricesItemModel.price(viewModel)
                return itemModel
            })

        return items.isEmpty ? [] : [ProductPricesSectionModel.section(items: items)]
    }
}

extension Observable where Element == ProductPricesItemModel {
    func mapToProductPrice() -> Observable<Price?> {
        return map({ model in
            if case ProductPricesItemModel.price(let viewModel) = model {
                return viewModel.currentState.price
            } else {
                return nil
            }
        })
    }
}
