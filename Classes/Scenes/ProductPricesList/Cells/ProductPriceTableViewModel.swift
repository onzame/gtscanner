//
//  ProductPriceTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class ProductPriceTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(price: Price) {
        initialState = State(price: price)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductPriceTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ProductPriceTableViewModel {
    struct State {
        let price: Price
    }
}

// MARK: - Equatable

extension ProductPriceTableViewModel.State: Equatable {
    static func == (lhs: ProductPriceTableViewModel.State, rhs: ProductPriceTableViewModel.State) -> Bool {
        return lhs.price == rhs.price
    }
}
