//
//  ProductPriceTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ProductPriceTableViewCell: BaseTableViewCell, View {

    static let cellHeight: CGFloat = 118

    // MARK: - UI components

    let priceLabel = UILabel().then {
        $0.font = Font.priceLabelFont
        $0.textColor = Color.priceLabelTextColor
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let logoImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
    }

    let distanceLabel = UILabel().then {
        $0.font = Font.distanceLabelFont
        $0.textColor = Color.distanceLabelTextColor
        $0.textAlignment = .right
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(priceLabel)
        addSubview(descriptionStackView)
        addSubview(logoImageView)
        addSubview(distanceLabel)
    }

    private func setupLayout() {
        logoImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(Metric.logoImageViewTopOffset)
            make.right.equalToSuperview().offset(Metric.logoImageViewRightOffset)
            make.width.height.equalTo(Metric.logoImageViewPartSize)
        }

        priceLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.priceLabelLeftOffset)
            make.top.equalToSuperview().offset(Metric.priceLabelTopOffset)
            make.right.equalTo(logoImageView.snp.left).offset(Metric.priceLabelRightOffset)
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.right.equalTo(distanceLabel.snp.left).offset(Metric.descriptionStackViewRightOffset)
            make.bottom.equalToSuperview().offset(Metric.descriptionStackViewBottomOffset)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ProductPriceTableViewModel) {
        let placeholderSize = CGSize(squareSide: Metric.logoImageViewPartSize)

        reactor.state
            .map({ ($0.price.shopLogoUrl, Media.placeholder(for: $0.price.branch.category, imageSize: placeholderSize)) })
            .bind(to: logoImageView.rx.imageWithPlaceholder)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.price.price })
            .mapToPrice()
            .bind(to: priceLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.price.shopName })
            .bind(to: descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        let distanceStringSequence = reactor.state
            .map({ $0.price.branch.distance })

        distanceStringSequence
            .bind(to: distanceLabel.rx.text)
            .disposed(by: disposeBag)

        distanceStringSequence
            .bind(to: rx.updateDistanceLabelLayout)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.price.branch.address })
            .bind(to: descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductPriceTableViewCell {
    fileprivate enum Color {
        static let priceLabelTextColor = Style.Color.black
        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitleTextColor = Style.Color.secondaryText
        static let distanceLabelTextColor = Style.Color.brightBlue
    }

    fileprivate enum Font {
        static let priceLabelFont = Style.Font.sanFrancisco(.bold, size: 20)
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let distanceLabelFont = Style.Font.sanFrancisco(.bold, size: 12)
    }

    fileprivate enum Image {
        static let logoImageViewPlaceholderImage = Media.image(.productPlaceholder)
    }

    fileprivate enum Metric {
        static let priceLabelLeftOffset: CGFloat = 20
        static let priceLabelTopOffset: CGFloat = 26
        static let priceLabelRightOffset: CGFloat = -8

        static let descriptionStackViewLeftOffset: CGFloat = 20
        static let descriptionStackViewRightOffset: CGFloat = -8
        static let descriptionStackViewBottomOffset: CGFloat = -20
        static let descriptionStackViewSpacing: CGFloat = 4

        static let logoImageViewTopOffset: CGFloat = 20
        static let logoImageViewRightOffset: CGFloat = -20
        static let logoImageViewPartSize: CGFloat = 44

        static let distanceLabelRightOffset: CGFloat = -20
        static let distanceLabelBottomOffset: CGFloat = -20
    }
}

extension Reactive where Base: ProductPriceTableViewCell {
    var updateDistanceLabelLayout: Binder<String> {
        return Binder(base, binding: { view, distanceString in
            let metric = ProductPriceTableViewCell.Metric.self
            let font = ProductPriceTableViewCell.Font.self
            let width = distanceString.boundingWidth(height: .greatestFiniteMagnitude, font: font.distanceLabelFont)

            view.distanceLabel.snp.remakeConstraints({ (make) in
                make.right.lessThanOrEqualToSuperview().offset(metric.distanceLabelRightOffset)
                make.bottom.equalToSuperview().offset(metric.distanceLabelBottomOffset)
                make.width.equalTo(width)

                if metric.logoImageViewPartSize > width {
                    make.centerX.equalTo(view.logoImageView)
                } else {
                    make.right.equalToSuperview().offset(metric.distanceLabelRightOffset)
                }
            })
        })
    }
}
