//
//  ProductPricesListViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

final class ProductPricesListViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let pricesService: PricesServiceType

    // MARK: - Lifecycle

    init(pricesService: PricesServiceType, input: ProductPricesListInput) {
        self.pricesService = pricesService
        initialState = State(product: input.product)
    }

    deinit {
        log.info("deinit ProductPricesListViewModel")
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .bindUpdates:
            return pricesService.observePricesWithReset()
                .map(Mutation.update)

        case .fetch(product: let product, location: let location):
            return pricesService.refreshPrices(product: product, location: location)
                .map(Mutation.updateChanges)
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh(product: let product, location: let location):
            return pricesService.refreshPrices(product: product, location: location)
                .map(Mutation.updateChanges)
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))

        case .loadNext(product: let product, location: let location, page: let page):
            return pricesService.loadPrices(product: product, location: location, page: page)
                .map(Mutation.updateChanges)
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: true)) })
                .startWith(.beginLoadingNext)

        case .delete(product: let product, price: let price):
            return pricesService.deletePrice(product: product, price: price)
                .map({ Mutation.commitDeleting($0.productUpdates)})
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: true)) })
                .startWith(.beginDeleting)

        case .insertAddedPrice(let price):
            return pricesService.addPrice(price: price)
                .andThen(.just(.stub))

        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        switch mutation {
        case .update(let prices):
            state.sections = prices.mapToSections()
            state.error = nil

        case .updateChanges(let pagination):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.pagination = pagination
            state.isLoaded = true
            state.error = nil

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing
            state.error = nil

        case .setLoaded(let isLoaded):
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = nil

        case .beginDeleting:
            state.isDeleting = true

        case .commitDeleting(let productUpdates):
            state.isDeleting = false
            state.product = state.product.update(updates: productUpdates)

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.isDeleting = false
            state.isLoaded = isLoaded
            state.error = error

        case .beginLoadingNext:
            state.isLoadingNext = true

        case .stub:
            break
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductPricesListViewModel {
    enum Action {
        case bindUpdates
        case fetch(product: Product, location: Location?)
        case refresh(product: Product, location: Location?)
        case loadNext(product: Product, location: Location?, page: Int)
        case delete(product: Product, price: Price)
        case insertAddedPrice(price: Price)
    }
}

// MARK: - Mutations

extension ProductPricesListViewModel {
    enum Mutation {
        case stub
        case update([Price])
        case updateChanges(Pagination)
        case setRefreshing(Bool)
        case setLoaded(Bool)
        case beginDeleting
        case commitDeleting(ProductUpdates)
        case showError(error: Error, isLoaded: Bool)
        case beginLoadingNext
    }
}

// MARK: - State

extension ProductPricesListViewModel {
    struct State {
        var product: Product
        var sections: [ProductPricesSectionModel] = []
        var isLoadingNext: Bool = false
        var isRefreshing: Bool = false
        var isDeleting: Bool = false
        var isLoaded: Bool = false
        var pagination = Pagination.initial
        var error: Error?
    }
}
