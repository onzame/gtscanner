//
//  ProductPricesListViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 29.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa
import RxDataSources

final class ProductPricesListViewController: BaseViewController<ProductPricesListViewModel> {

    // MARK: - UI components

    let contentView = ProductPricesListView()

    // MARK: - Controllers

    let locationSessionController = LocationSessionController.shared

    // MARK: - Publishers

    fileprivate let retryActionPublisher = PublishSubject<Void>()
    fileprivate let addPriceActionPublisher = PublishSubject<Void>()

    fileprivate let currentProductPublisher = BehaviorRelay<Product?>(value: nil)

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {

    }

    private func setupLayout() {

    }

    private func setupView() {
        title = Localization.ProductPricesList.title
    }

    // MARK: - Bindings

    override func bind(reactor: ProductPricesListViewModel) {

        // Input

        let locationSequence = locationSessionController.rx.location
            .mapToDomain()

        rx.viewWillAppear
            .asObservable()
            .mapToVoid()
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewWillDisappear
            .asObservable()
            .mapToVoid()
            .bind(to: locationSessionController.rx.disableUpdating)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .mapTo(Reactor.Action.bindUpdates)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewWillAppear
            .take(1)
            .withLatestFrom(reactor.state)
            .withLatestFrom(locationSequence, resultSelector: { (state, location) -> Reactor.Action in
                .fetch(product: state.product, location: location)
            })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.refreshControl.rx.controlEvent(.valueChanged)
            .withLatestFrom(reactor.state)
            .withLatestFrom(locationSequence, resultSelector: { (state, location) -> Reactor.Action? in
                if state.isRefreshing {
                    return nil
                } else {
                    return .refresh(product: state.product, location: location)
                }
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.tableView.rx.reachedBottom()
            .withLatestFrom(reactor.state)
            .filter({ $0.pagination.hasNext && !$0.isLoadingNext })
            .withLatestFrom(locationSequence, resultSelector: { (state, location) -> Reactor.Action in
                .loadNext(product: state.product, location: location, page: state.pagination.nextPage.orZero)
            })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.priceDeletion
            .withLatestFrom(reactor.state, resultSelector: { (price, state) -> Reactor.Action? in
                if state.isDeleting {
                    return nil
                } else {
                    return .delete(product: state.product, price: price)
                }
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .withLatestFrom(locationSequence)
            .withLatestFrom(reactor.state, resultSelector: { (location, state) -> Reactor.Action in
                .fetch(product: state.product, location: location)
            })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.tableView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.isLoaded && $0.error == nil })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.isLoaded && $0.error != nil })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.sections.isNotEmpty && $0.isLoaded })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.sections.isEmpty && $0.isLoaded })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isDeleting })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.pagination.hasNext })
            .bind(to: contentView.tableView.rx.showFooterActivityIndicatorView())
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product })
            .bind(to: currentProductPublisher)
            .disposed(by: disposeBag)

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductPricesListViewController {
    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
        static let noDataBackgroundViewImage = Media.image(.handsHeart)
    }
}

// MARK: - StateResolvable

extension ProductPricesListViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        if case LocationSessionError.accessDenied = error {
            return buildLocationErrorStateView().then {
                $0.image = Image.errorBackgroundViewImage
                $0.actionButton.rx.tap
                    .flatMap({ _ in UIApplication.shared.rx.openSettingsAction() })
                    .flatMap({ _ in UIApplication.shared.rx.didBecomeActiveObservable().take(1) })
                    .do(onNext: { [weak self] in self?.retryActionPublisher.onNext(()) })
                    .subscribe()
                    .disposed(by: disposeBag)
            }
        } else {
            return ErrorStateView().then {
                $0.image = Image.errorBackgroundViewImage
                $0.title = Localization.ProductPricesList.errorPlaceholderTitle
                $0.subtitle = Localization.ProductPricesList.errorPlaceholderSubtitle
                $0.actionTitle = Localization.ProductPricesList.errorPlaceholderActionTitle
                $0.actionButton.rx.tap
                    .bind(to: retryActionPublisher)
                    .disposed(by: $0.disposeBag)
            }
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.noDataBackgroundViewImage
            $0.title = Localization.ProductPricesList.noDataPlaceholderTitle
            $0.subtitle = Localization.ProductPricesList.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.ProductPricesList.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: addPriceActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductPricesListViewController {
    var addPriceAction: ControlEvent<Product> {
        let productSequence = base.currentProductPublisher.filterNil()
        let placeholderAction = base.addPriceActionPublisher.asObservable()
        let defaultAction = base.contentView.addPriceView.primaryButton.rx.tap.asObservable()
        let actionsSequence = Observable.merge(placeholderAction, defaultAction)
        let source = actionsSequence.withLatestFrom(productSequence)
        return ControlEvent(events: source)
    }
}
