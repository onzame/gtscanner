//
//  ProductAddViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa
import SnapKit

class ProductAddViewController: BaseViewController<ProductAddViewModel> {

    // MARK: - UI components

    let contentView = ProductAddView()

    var contentTopConstraint: Constraint?

    // MARK: - Publishers

    fileprivate let addedProductMetadataPublisher = PublishSubject<AddedProductMetadata>()

    // MARK: - Properties

    private let locationSessionController = LocationSessionController.shared

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        removeKeyboardObservers(from: .default)
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            self.contentTopConstraint = make.top.equalToSuperview().constraint
        }
    }

    private func setupView() {
        preferredContentSize = CGSize(width: Metric.width, height: Metric.height)
        addKeyboardObservers(to: .default)
    }

    // MARK: - Bindign

    override func bind(reactor: ProductAddViewModel) {

        // Settings

        let startSequence = reactor.state.take(1)

        startSequence
            .map({ $0.name })
            .filterNil()
            .bind(to: contentView.nameTextField.rx.text)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.gtin })
            .filterNil()
            .bind(to: contentView.barcodeTextField.rx.text)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.price })
            .filterNil()
            .mapToString()
            .bind(to: contentView.priceTextField.rx.text)
            .disposed(by: disposeBag)

        startSequence
            .map({ $0.rating })
            .filterNil()
            .bind(to: contentView.ratingView.rx.rating)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .asObservable()
            .flatMap({ [weak self] _ -> Observable<Void> in
                guard let `self` = self else { return .empty() }
                return self.locationSessionController.prepareSession()
                    .asObservable()
                    .mapToVoid()
            })
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewWillAppear
            .mapToVoid()
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        rx.viewWillDisappear
            .mapToVoid()
            .bind(to: locationSessionController.rx.disableUpdating)
            .disposed(by: disposeBag)

        // Input

        contentView.nameTextField.rx.text
            .map(Reactor.Action.updateName)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.priceTextField.rx.text.orEmpty
            .mapToDouble()
            .map(Reactor.Action.updatePrice)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.ratingView.rx.changeRatingAction
            .map(Reactor.Action.updateRating)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.photosView.rx.deletePhotoAction
            .map(Reactor.Action.deletePhoto)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        let locationSequence = locationSessionController.rx.location
            .mapToDomain()

        contentView.actionsView.primaryButton.rx.tap
            .withLatestFrom(reactor.state)
            .withLatestFrom(locationSequence, resultSelector: { (state, location) -> Reactor.Action? in
                if let parameters = AddProductParameters(state: state) {
                    return .addProduct(parameters: parameters, location: location)
                } else {
                    return nil
                }
            })
            .filterNil()
            .do(onNext: { [weak self] _ in self?.view.endEditing(true) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.shop?.name })
            .filterNil()
            .bind(to: contentView.shopView.rx.selectionBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.category?.name })
            .filterNil()
            .bind(to: contentView.categoryView.rx.selectionBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.photosSections })
            .bind(to: contentView.photosView.collectionView.rx.items(dataSource: contentView.photosView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isReadyToSend })
            .distinctUntilChanged()
            .bind(to: contentView.rx.isSubmitEnabled)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.metadata })
            .filterNil()
            .bind(to: addedProductMetadataPublisher)
            .disposed(by: disposeBag)

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductAddViewController {
    fileprivate enum Color {

    }

    fileprivate enum CornerRadius {

    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {

    }

    fileprivate enum Image {

    }

    fileprivate enum Metric {
        static let width: CGFloat = Screen.width
        static let height: CGFloat = Screen.height - Screen.topSafeInset
    }

    fileprivate enum Reusable {

    }
}

// MARK: - ResizeKeyboardObservable

extension ProductAddViewController: ResizeKeyboardObservable {
    var constraint: Constraint? {
        return contentTopConstraint
    }

    var contentContainer: UIView {
        return view
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductAddViewController {
    var showShopsAction: ControlEvent<Void> {
        return base.contentView.rx.showShopsAction
    }

    var showCategoriesAction: ControlEvent<Void> {
        return base.contentView.rx.showCategoriesAction
    }

    var showPhotosAction: ControlEvent<Void> {
        return base.contentView.rx.showPhotosAction
    }

    var addProductAction: ControlEvent<AddedProductMetadata> {
        return ControlEvent(events: base.addedProductMetadataPublisher)
    }

    var cancelAction: ControlEvent<Void> {
        return base.contentView.actionsView.secondaryButton.rx.tap
    }
}
