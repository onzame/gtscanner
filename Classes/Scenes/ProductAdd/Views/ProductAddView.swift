//
//  ProductAddView.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SkyFloatingLabelTextField

class ProductAddView: ModalContentView {

    // MARK: - UI components

    let titleLabel = UILabel().then {
        $0.font = Font.titleLabelFont
        $0.text = Localization.ProductAdd.title
        $0.textColor = Color.titleLabelTextColor
    }

    let nameTextField = FormTextField().then {
        $0.font = Font.textFieldTextFont
        $0.title = Localization.ProductAdd.nameTextFieldTitle
        $0.titleFont = Font.textFieldTitleFont
        $0.setTitleVisible(true)
    }

    let barcodeTextField = FormTextField().then {
        $0.font = Font.textFieldTextFont
        $0.title = Localization.ProductAdd.barcodeTextFieldTitle
        $0.titleFont = Font.textFieldTitleFont
        $0.setTitleVisible(true)
        $0.isEnabled = false
    }

    let priceTextField = PriceTextField().then {
        $0.font = Font.textFieldTextFont
        $0.title = Localization.ProductAdd.priceTextFieldTitle
        $0.titleFont = Font.textFieldTitleFont
        $0.placeholder = ""
        $0.setTitleVisible(true)
    }

    let shopView = SelectionControlView().then {
        $0.titleLabel.text = Localization.ProductAdd.shopTitle
        $0.actionButton.setTitle(Localization.ProductAdd.shopPlaceholder, for: .normal)
        $0.changeButton.setTitle(Localization.ProductAdd.changeActionTitle, for: .normal)
    }

    let categoryView = SelectionControlView().then {
        $0.titleLabel.text = Localization.ProductAdd.categoryTitle
        $0.actionButton.setTitle(Localization.ProductAdd.categoryPlaceholder, for: .normal)
        $0.changeButton.setTitle(Localization.ProductAdd.changeActionTitle, for: .normal)
    }

    let photosView = PhotosComponentView()

    let ratingView = RatingTitledControlView().then {
        $0.titleLabel.text = Localization.ProductAdd.ratingTitle
    }

    let scrollView = UIScrollView()

    let scrollContentView = UIView()

    let actionsView = ActionsComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductAdd.submitActionTitle, for: .normal)
        $0.secondaryButton.setTitle(Localization.ProductAdd.cancelActionTitle, for: .normal)
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        bodyView.addSubview(actionsView)
        bodyView.addSubview(scrollView)

        scrollView.addSubview(scrollContentView)

        scrollContentView.addSubview(titleLabel)
        scrollContentView.addSubview(nameTextField)
        scrollContentView.addSubview(barcodeTextField)
        scrollContentView.addSubview(priceTextField)
        scrollContentView.addSubview(shopView)
        scrollContentView.addSubview(categoryView)
        scrollContentView.addSubview(photosView)
        scrollContentView.addSubview(ratingView)
    }

    private func setupLayout() {
        actionsView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }

        scrollView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.bottom.equalTo(actionsView.snp.top)
        }

        scrollContentView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.width.equalTo(bodyView)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.left.greaterThanOrEqualToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalToSuperview().offset(Metric.titleLabelTopOffset)
            make.right.lessThanOrEqualToSuperview().offset(Metric.contentRightOffset)
            make.centerX.equalToSuperview()
        }

        nameTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(titleLabel.snp.bottom).offset(Metric.nameTextFieldTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        barcodeTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(nameTextField.snp.bottom).offset(Metric.barcodeTextFieldTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        priceTextField.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(barcodeTextField.snp.bottom).offset(Metric.priceTextFieldTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        shopView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(priceTextField.snp.bottom).offset(Metric.shopViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        categoryView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(shopView.snp.bottom).offset(Metric.categoryViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }

        photosView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(categoryView.snp.bottom).offset(Metric.photosViewTopOffset)
        }

        ratingView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(photosView.snp.bottom).offset(Metric.ratingViewTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
            make.bottom.lessThanOrEqualToSuperview().offset(Metric.ratingViewBottomOffset)
        }
    }

    private func setupView() {

    }

    fileprivate func enableSubmit(isEnabled: Bool) {
        actionsView.primaryButton.isEnabled = isEnabled

        if isEnabled {
            actionsView.primaryContainerView.backgroundColor = Color.primaryActionEnabledBackgoundColor
        } else {
            actionsView.primaryContainerView.backgroundColor = Color.primaryActionDisabledBackgoundColor
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductAddView {
    fileprivate enum Color {
        static let titleLabelTextColor = Style.Color.black
        static let primaryActionEnabledBackgoundColor = Style.Color.brightYellow
        static let primaryActionDisabledBackgoundColor = Style.Color.lightBlue
    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.semibold, size: 20)
        static let textFieldTextFont = Style.Font.sanFrancisco(.regular, size: 18)
        static let textFieldTitleFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let textFieldPlaceholderFont = Style.Font.sanFrancisco(.regular, size: 18)
    }

    fileprivate enum Metric {
        static let contentLeftOffset: CGFloat = 20
        static let contentRightOffset: CGFloat = -20

        static let titleLabelTopOffset: CGFloat = 12

        static let nameTextFieldTopOffset: CGFloat = 26
        static let barcodeTextFieldTopOffset: CGFloat = 16
        static let priceTextFieldTopOffset: CGFloat = 16
        static let shopViewTopOffset: CGFloat = 16
        static let categoryViewTopOffset: CGFloat = 16
        static let photosViewTopOffset: CGFloat = 24
        static let ratingViewTopOffset: CGFloat = 24
        static let ratingViewBottomOffset: CGFloat = -30
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductAddView {
    var showShopsAction: ControlEvent<Void> {
        return base.shopView.rx.selectionAction
    }

    var showPhotosAction: ControlEvent<Void> {
        return base.photosView.rx.addNewAction
    }

    var deletePhotoAction: ControlEvent<Photo> {
        return base.photosView.rx.deletePhotoAction
    }

    var showCategoriesAction: ControlEvent<Void> {
        return base.categoryView.rx.selectionAction
    }

    var isSubmitEnabled: Binder<Bool> {
        return Binder(base, binding: { view, isEnabled in
            view.enableSubmit(isEnabled: isEnabled)
        })
    }
}
