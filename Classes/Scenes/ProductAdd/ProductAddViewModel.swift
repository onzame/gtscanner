//
//  ProductAddViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

class ProductAddViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let productService: ProductsServiceType

    // MARK: - Lifecycle

    init(productService: ProductsServiceType, metadata: ProductAddFlowMetadata) {
        self.productService = productService
        self.initialState = State(metadata: metadata)
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .updateName(let name):
            return .just(.updateName(name))
        case .updatePrice(let price):
            return .just(.updatePrice(price))
        case .updateRating(let rating):
            return .just(.updateRating(rating))
        case .addPhoto(let photo):
            return .just(.addPhoto(photo))
        case .deletePhoto(let photo):
            return .just(.deletePhoto(photo))
        case .addProduct(parameters: let parameters, location: let location):
            return productService.addProduct(parameters: parameters, location: location)
                .map(AddedProductMetadata.init)
                .map(Mutation.addedProductMetadata)
                .catchError({ .just(.showError($0)) })
                .startWith(.setLoading(true))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.error = nil

        switch mutation {
        case .updateName(let name):
            state.name = name

        case .updatePrice(let price):
            state.price = price

        case .updateRating(let rating):
            state.rating = rating

        case .addPhoto(let photo):
            state.photos = [photo] + state.photos
            state.photosSections = state.photos.mapToSections()

        case .deletePhoto(let photo):
            state.photos = state.photos.filter({ $0 != photo })
            state.photosSections = state.photos.mapToSections()

        case .addedProductMetadata(let metadata):
            state.metadata = metadata
            state.isLoading = false

        case .setLoading(let isLoading):
            state.isLoading = isLoading

        case .showError(let error):
            state.error = error
            state.isLoading = false
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductAddViewModel {
    enum Action {
        case updateName(String?)
        case updatePrice(Double?)
        case updateRating(Double?)
        case addPhoto(Photo)
        case deletePhoto(Photo)
        case addProduct(parameters: AddProductParameters, location: Location?)
    }
}

// MARK: - Mutations

extension ProductAddViewModel {
    enum Mutation {
        case updateName(String?)
        case updatePrice(Double?)
        case updateRating(Double?)
        case addPhoto(Photo)
        case deletePhoto(Photo)
        case setLoading(Bool)
        case addedProductMetadata(AddedProductMetadata)
        case showError(Error)
    }
}

// MARK: - State

extension ProductAddViewModel {
    struct State {
        var name: String?
        var gtin: String?
        var price: Double?
        var shop: Shop?
        var category: Category?
        var photos: [Photo] = []
        var rating: Double?
        var photosSections: [PhotosComponentSectionModel]
        var metadata: AddedProductMetadata?
        var isLoading: Bool = false
        var error: Error?

        var isReadyToSend: Bool {
            return name.isNotEmpty
                && gtin.isNotEmpty
                && price.isNotZero
                && shop != nil
                && category != nil
                && rating.isNotZero
        }

        init(metadata: ProductAddFlowMetadata) {
            self.name = metadata.name
            self.gtin = metadata.gtin
            self.price = metadata.price
            self.shop = metadata.selectedShop
            self.category = metadata.selectedCategory
            self.photos = metadata.photos
            self.rating = metadata.rating
            self.photosSections = metadata.photos.mapToSections()
        }
    }
}
