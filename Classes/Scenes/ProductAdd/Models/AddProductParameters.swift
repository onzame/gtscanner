//
//  AddProductParameters.swift
//  Gorod
//
//  Created by Sergei Fabian on 19.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct AddProductParameters {
    let name: String
    let gtin: String
    let price: Double
    let rating: Double
    let shop: Shop
    let category: Category
    let photos: [Photo]

    init?(state: ProductAddViewModel.State) {
        guard let name = state.name, let gtin = state.gtin, let price = state.price, let rating = state.rating else { return nil }
        guard let shop = state.shop, let category = state.category else { return nil }
        self.name = name
        self.gtin = gtin
        self.price = price
        self.rating = rating
        self.shop = shop
        self.category = category
        self.photos = state.photos
    }
}
