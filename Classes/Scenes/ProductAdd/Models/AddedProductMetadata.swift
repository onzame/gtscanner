//
//  AddedProductMetadata.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct AddedProductMetadata {
    let product: Product
}
