//
//  ProductAddCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 17.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum ProductAddCoordinationResult {
    case requestedCategorySelection(ProductAddFlowMetadata)
    case requestedShopSelection(ProductAddFlowMetadata)
    case added(AddedProductMetadata)
    case canceled
}

class ProductAddCoordinator: Coordinator<ProductAddCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let metadata: ProductAddFlowMetadata

    init(rootViewController: UIViewController, container: DependenciesContainer, metadata: ProductAddFlowMetadata) {
        self.rootViewController = rootViewController
        self.container = container
        self.metadata = metadata
    }

    override func start() -> Observable<ProductAddCoordinationResult> {
        let viewModel = container.productAddViewModel(metadata: metadata)
        let viewController = ProductAddViewController(reactor: viewModel)
        let bottomSheetController = BottomSheetController(contentViewController: viewController)

        rootViewController.present(bottomSheetController, animated: true)

        viewController.rx.showPhotosAction
            .flatMap({ [weak self] _ -> Observable<PhotosCoordinationResult> in
                guard let `self` = self else { return .empty() }
                return self.showPhotosScene(viewController: viewController)
            })
            .subscribe(onNext: { coordinationResult in
                if case PhotosCoordinationResult.taked(let photo) = coordinationResult {
                    viewModel.action.onNext(.addPhoto(photo))
                }
            })
            .disposed(by: disposeBag)

        let dismissingAction = bottomSheetController.rx.isDismissing
            .asObservable()

        // ProductAddCoordinationResult.requestedCategorySelection

        let categoriesAction = viewController.rx.showCategoriesAction
            .asObservable()
            .do(onNext: { bottomSheetController.dismiss(animated: true) })

        let requestedCategorySelectionEvent = Observable.combineLatest(dismissingAction, categoriesAction)
            .withLatestFrom(viewModel.state)
            .map({ state -> ProductAddCoordinationResult in
                let metadata = ProductAddFlowMetadata(state: state)
                return ProductAddCoordinationResult.requestedCategorySelection(metadata)
            })

        // ProductAddCoordinationResult.requestedShopSelection

        let shopsAction = viewController.rx.showShopsAction
            .asObservable()
            .do(onNext: { bottomSheetController.dismiss(animated: true) })

        let requestedShopSelectionEvent = Observable.combineLatest(dismissingAction, shopsAction)
            .withLatestFrom(viewModel.state)
            .map({ state -> ProductAddCoordinationResult in
                let metadata = ProductAddFlowMetadata(state: state)
                return ProductAddCoordinationResult.requestedShopSelection(metadata)
            })

        // ProductAddCoordinationResult.canceled

        let cancelAction = viewController.rx.cancelAction
            .asObservable()
            .do(onNext: { bottomSheetController.dismiss(animated: true) })

        let dismissAction = bottomSheetController.rx.dismissAction
            .asObservable()

        let canceledEvent = Observable.merge(cancelAction, dismissAction)
            .mapTo(ProductAddCoordinationResult.canceled)

        // ProductAddCoordinationResult.added

        let addAction = viewController.rx.addProductAction
            .map(ProductAddCoordinationResult.added)
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let addedEvent = Observable.combineLatest(addAction, dismissingAction)
            .map({ $0.0 })

        return Observable.merge(requestedCategorySelectionEvent, requestedShopSelectionEvent, canceledEvent, addedEvent)
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .productAdd, inCategory: .add, tag: .productId(id: 0))
            })
            .take(1)
    }

    private func showPhotosScene(viewController: UIViewController) -> Observable<PhotosCoordinationResult> {
        let coordinator = PhotosCoordinator(rootViewController: viewController)
        return coordinate(to: coordinator)
    }

}
