//
//  ProductReviewsListViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 05.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ProductReviewsListViewController: BaseViewController<ProductReviewsListViewModel> {

    // MARK: - UI components

    let contentView = ProductReviewsListView()

    // MARK: - Publishers

    fileprivate let retryActionPublisher = PublishSubject<Void>()
    fileprivate let addReviewActionPublisher = PublishSubject<Void>()

    fileprivate let currentProductPublisher = BehaviorRelay<Product?>(value: nil)

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.ProductReviewsList.title
    }

    // MARK: - Bindings

    override func bind(reactor: ProductReviewsListViewModel) {

        // Input

        rx.viewDidLoad
            .take(1)
            .mapTo(Reactor.Action.bindUpdates)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewDidAppear
            .take(1)
            .withLatestFrom(reactor.state)
            .map({ $0.product })
            .map(Reactor.Action.fetch)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.tableView.rx.reachedBottom()
            .withLatestFrom(reactor.state)
            .filter({ $0.pagination.hasNext && !$0.isLoadingNext })
            .map({ Reactor.Action.loadNext(product: $0.product, page: $0.pagination.nextPage.orZero) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.refreshControl.rx.controlEvent(.valueChanged)
            .withLatestFrom(reactor.state)
            .filter({ !$0.isRefreshing })
            .map({ Reactor.Action.refresh(product: $0.product) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        contentView.rx.reviewDeletion
            .withLatestFrom(reactor.state, resultSelector: { (review, state) -> Reactor.Action? in
                if state.isDeleting {
                    return nil
                } else {
                    return .delete(product: state.product, review: review)
                }
            })
            .filterNil()
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        retryActionPublisher
            .withLatestFrom(reactor.state)
            .map({ Reactor.Action.fetch(product: $0.product) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.sections })
            .bind(to: contentView.tableView.rx.items(dataSource: contentView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isRefreshing })
            .bind(to: contentView.refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.isLoaded })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.isLoaded && $0.error == nil })
            .mapToVoid()
            .bind(to: rx.loadingStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.isLoaded && $0.error != nil })
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.sections.isNotEmpty && $0.isLoaded })
            .mapToVoid()
            .bind(to: rx.defaultStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.sections.isEmpty && $0.isLoaded })
            .mapToVoid()
            .bind(to: rx.noDataStateBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isDeleting })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.pagination.hasNext })
            .bind(to: contentView.tableView.rx.showFooterActivityIndicatorView())
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product })
            .bind(to: currentProductPublisher)
            .disposed(by: disposeBag)

    }
}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewsListViewController {
    fileprivate enum Image {
        static let errorBackgroundViewImage = Media.image(.circleError)
        static let noDataBackgroundViewImage = Media.image(.handsHeart)
    }
}

// MARK: - StateResolvable

extension ProductReviewsListViewController: StateResolvable {
    func buildDefaultStateView() -> UIView {
        return contentView
    }

    func buildErrorStateView(error: Error) -> UIView {
        return ErrorStateView().then {
            $0.image = Image.errorBackgroundViewImage
            $0.title = Localization.ProductReviewsList.errorPlaceholderTitle
            $0.subtitle = Localization.ProductReviewsList.errorPlaceholderSubtitle
            $0.actionTitle = Localization.ProductReviewsList.errorPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: retryActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }

    func buildNoDataStateView() -> UIView {
        return ErrorStateView().then {
            $0.image = Image.noDataBackgroundViewImage
            $0.title = Localization.ProductReviewsList.noDataPlaceholderTitle
            $0.subtitle = Localization.ProductReviewsList.noDataPlaceholderSubtitle
            $0.actionTitle = Localization.ProductReviewsList.noDataPlaceholderActionTitle
            $0.actionButton.rx.tap
                .bind(to: addReviewActionPublisher)
                .disposed(by: $0.disposeBag)
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewsListViewController {
    var addReviewAction: ControlEvent<Product> {
        let productSequence = base.currentProductPublisher.filterNil()
        let placeholderAction = base.addReviewActionPublisher.asObservable()
        let defaultAction = base.contentView.addReviewView.primaryButton.rx.tap.asObservable()
        let actionsSequence = Observable.merge(placeholderAction, defaultAction)
        let source = actionsSequence.withLatestFrom(productSequence)
        return ControlEvent(events: source)
    }
}
