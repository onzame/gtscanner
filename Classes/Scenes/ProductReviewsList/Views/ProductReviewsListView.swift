//
//  ProductReviewsListView.swift
//  Gorod
//
//  Created by Sergei Fabian on 05.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit
import SwipeCellKit

class ProductReviewsListView: UIView {

    let disposeBag = DisposeBag()

    // MARK: - UI components

    let addReviewView = CompletionActionComponentView().then {
        $0.primaryButton.setTitle(Localization.ProductReviewsList.addReviewActionTitle, for: .normal)
    }

    let refreshControl = UIRefreshControl()

    let tableView = UITableView().then {
        $0.setBackgroundColor(Color.tableViewBackgroundColor)
        $0.setBottomInset(Metric.tableViewBottomInset)
        $0.register(Reusable.reviewCell)
        $0.allowsSelection = false
        $0.separatorInset = .zero
        $0.separatorColor = Color.tableViewSeparatorColor
    }

    // MARK: - DataSource

    let dataSource = RxTableViewSectionedAnimatedDataSource<ProductReviewsSectionModel>(
        configureCell: { (_, tableView, indexPath, model) -> UITableViewCell in
            switch model {
            case .review(let viewModel):
                let cell = tableView.dequeue(Reusable.reviewCell, for: indexPath)
                cell.reactor = viewModel
                return cell
            }
        }
    )

    // MARK: - Publishers

    fileprivate let deleteActionPublisher = PublishSubject<IndexPath>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
        setupBinding()
    }

    private func setupHierarchy() {
        addSubview(tableView)
        addSubview(addReviewView)
    }

    private func setupLayout() {
        tableView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
            make.bottom.equalTo(safeBottomConstraintItem)
        }

        addReviewView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
        }
    }

    private func setupView() {
        tableView.refreshControl = refreshControl
    }

    private func setupBinding() {
        tableView.rx.setDelegate(self)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewsListView {
    fileprivate enum Color {
        static let tableViewBackgroundColor = Style.Color.white
        static let tableViewSeparatorColor = UIColor(hex: "#F2F2F2")
        static let deleteActionBackgroundColor = Style.Color.pink
    }

    fileprivate enum Image {
        static let deleteActionImage = Media.icon(.trash, size: .x24)
    }

    fileprivate enum Metric {
        static let tableViewBottomInset: CGFloat = 80
    }

    fileprivate enum Reusable {
        static let reviewCell = ReusableCell<ProductReviewTableViewCell>()
    }
}

// MARK: - UITableViewDelegate

extension ProductReviewsListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch dataSource[indexPath] {
        case .review(let viewModel):
            return ProductReviewTableViewCell.cellHeight(insideOf: tableView, review: viewModel.currentState.review)
        }
    }
}

// MARK: - SwipeTableViewCellDelegate

extension ProductReviewsListView: SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        switch dataSource[indexPath] {
        case .review(let viewModel):
            if viewModel.currentState.review.permissions.canDelete {
                let deleteAction = SwipeAction(style: .destructive, title: nil) { [weak self] (_, indexPath) in
                    self?.deleteActionPublisher.onNext(indexPath)
                }.then {
                    $0.backgroundColor = Color.deleteActionBackgroundColor
                    $0.image = Image.deleteActionImage?.tint(with: .white)
                }

                return [deleteAction]
            } else {
                return nil
            }
        }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewsListView {
    var reviewDeletion: ControlEvent<Review> {
        let source = base.deleteActionPublisher
            .map({ [weak base] indexPath in base?.dataSource[indexPath] })
            .filterNil()
            .mapToProductReview()
            .map({ $0 })
            .filterNil()

        return ControlEvent(events: source)
    }

    var refreshAction: ControlEvent<Void> {
        return ControlEvent(events: base.refreshControl.rx.controlEvent(.valueChanged))
    }

    var addReviewAction: ControlEvent<Void> {
        return ControlEvent(events: base.addReviewView.primaryButton.rx.tap)
    }
}
