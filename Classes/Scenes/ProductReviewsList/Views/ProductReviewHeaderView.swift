//
//  ProductReviewHeaderView.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit

class ProductReviewHeaderView: UIView {

    // MARK: - UI components

    let avatarView = ImageContainerView().then {
        $0.setCornerRadius(CornerRadius.avatarViewCornerRadius)
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleTextColor
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleTextColor
        $0.spacing = Metric.descriptionStackViewSpacing
    }

    let ratingView = TitledImageView(imageViewSize: CGSize(squareSide: Metric.ratingViewImageViewPartSize)).then {
        $0.imageView.image = Image.ratingViewImage
        $0.titleLabel.font = Font.ratingViewTitleFont
        $0.titleLabel.textColor = Color.ratingViewTitleTextColor
        $0.stackView.spacing = Metric.ratingViewStackViewSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(avatarView)
        addSubview(descriptionStackView)
        addSubview(ratingView)
    }

    private func setupLayout() {
        avatarView.snp.makeConstraints { (make) in
            make.left.top.bottom.equalToSuperview()
            make.width.height.equalTo(Metric.avatarViewPartSize)
        }

        ratingView.snp.makeConstraints { (make) in
            make.top.right.equalToSuperview()
        }

        descriptionStackView.snp.makeConstraints { (make) in
            make.left.equalTo(avatarView.snp.right).offset(Metric.descriptionStackViewLeftOffset)
            make.right.lessThanOrEqualTo(ratingView.snp.left).offset(Metric.descriptionStackViewRightOffset)
            make.centerY.equalTo(avatarView)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewHeaderView {
    fileprivate enum Color {
        static let descriptionStackViewTitleTextColor = Style.Color.black
        static let descriptionStackViewSubtitleTextColor = Style.Color.secondaryText

        static let ratingViewTitleTextColor = Style.Color.black
    }

    fileprivate enum CornerRadius {
        static let avatarViewCornerRadius: CGFloat = 20
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
        static let descriptionStackViewSubtitleFont = Style.Font.sanFrancisco(.medium, size: 14)

        static let ratingViewTitleFont = Style.Font.sanFrancisco(.semibold, size: 16)
    }

    fileprivate enum Image {
        static let ratingViewImage = Media.image(.starFatFilled)
    }

    enum Metric {
        static let avatarViewPartSize: CGFloat = 40

        static let descriptionStackViewLeftOffset: CGFloat = 12
        static let descriptionStackViewRightOffset: CGFloat = -12
        static let descriptionStackViewSpacing: CGFloat = 2

        static let ratingViewImageViewPartSize: CGFloat = 14
        static let ratingViewStackViewSpacing: CGFloat = 4
    }
}
