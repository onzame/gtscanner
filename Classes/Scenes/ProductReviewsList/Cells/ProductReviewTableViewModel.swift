//
//  ProductReviewTableViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class ProductReviewTableViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(review: Review) {
        initialState = State(review: review)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductReviewTableViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ProductReviewTableViewModel {
    struct State {
        let review: Review
    }
}

// MARK: - Equatable

extension ProductReviewTableViewModel.State: Equatable {
    static func == (lhs: ProductReviewTableViewModel.State, rhs: ProductReviewTableViewModel.State) -> Bool {
        return lhs.review == rhs.review
    }
}
