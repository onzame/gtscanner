//
//  ProductReviewTableViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ProductReviewTableViewCell: BaseTableViewCell, View {

    static func cellHeight(insideOf tableView: UITableView, review: Review) -> CGFloat {
        let headerMetricType = ProductReviewHeaderView.Metric.self

        let horizontalOffsets = abs(Metric.contentLeftOffset) + abs(Metric.contentRightOffset)
        let additionalOffset = abs(headerMetricType.avatarViewPartSize) + abs(headerMetricType.descriptionStackViewLeftOffset)

        let nonCommentContentWidth = tableView.frame.width - horizontalOffsets + additionalOffset
        let commentContentWidth = tableView.frame.width - horizontalOffsets

        var cellHeight: CGFloat = 0
        cellHeight += abs(Metric.contentTopOffset)
        cellHeight += abs(Metric.contentBottomOffset)
        cellHeight += abs(headerMetricType.avatarViewPartSize)

        if let text = review.positiveText, text.isNotEmpty {
            let positiveHeight = text.boundingHeight(width: nonCommentContentWidth, font: Font.positiveLabelFont)
            cellHeight += positiveHeight
        }

        if let text = review.negativeText, text.isNotEmpty {
            let negativeHeight = text.boundingHeight(width: nonCommentContentWidth, font: Font.negativeLabelFont)
            cellHeight += negativeHeight
        }

        if let text = review.text, text.isNotEmpty {
            let commentHeight = text.boundingHeight(width: commentContentWidth, font: Font.commentLabelFont)
            cellHeight += commentHeight
        }

        if review.text.orEmpty.isNotEmpty {
            cellHeight += Metric.bodyToFooterOffset
        }

        if review.positiveText.orEmpty.isNotEmpty || review.negativeText.orEmpty.isNotEmpty {
            cellHeight += Metric.headerToBodyOffset
        }

        if review.positiveText.orEmpty.isNotEmpty && review.negativeText.orEmpty.isNotEmpty {
            cellHeight += Metric.bodyMargin
        }

        return cellHeight
    }

    // MARK: - UI components

    let headerView = ProductReviewHeaderView()

    let positiveImageView = UIImageView().then {
        $0.image = Image.positiveImageViewImage
    }

    let positiveLabel = UILabel().then {
        $0.font = Font.positiveLabelFont
        $0.textColor = Color.positiveLabelTextColor
        $0.numberOfLines = 0
    }

    let negativeImageView = UIImageView().then {
        $0.image = Image.negativeImageViewImage
    }

    let negativeLabel = UILabel().then {
        $0.font = Font.negativeLabelFont
        $0.textColor = Color.negativeLabelTextColor
        $0.numberOfLines = 0
    }

    let commentLabel = UILabel().then {
        $0.font = Font.commentLabelFont
        $0.textColor = Color.commentLabelTextColor
        $0.numberOfLines = 0
    }

    // MARK: - Lifecycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(headerView)
    }

    private func setupLayout() {
        headerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalToSuperview().offset(Metric.contentTopOffset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }
    }

    // MARK: - Bindings

    func bind(reactor: ProductReviewTableViewModel) {

        reactor.state
            .map({ $0.review })
            .bind(to: rx.reloadViews)
            .disposed(by: disposeBag)

        let placeholderSize = CGSize(squareSide: ProductReviewHeaderView.Metric.avatarViewPartSize)
        let placeholder = ImagePlaceholderView(image: Image.avatarPlaceholderImage, imageSize: placeholderSize)

        reactor.state
            .map({ $0.review.author.avatarUrl })
            .bind(to: headerView.avatarView.imageView.rx.image(placeholder: placeholder))
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.author.name })
            .bind(to: headerView.descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.updatedAt.date.stringBy(dateFormat: "d MMMM HH:mm") })
            .map({ $0.lowercased() })
            .bind(to: headerView.descriptionStackView.rx.subtitle)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.rating.clean })
            .bind(to: headerView.ratingView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.positiveText })
            .bind(to: positiveLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.negativeText })
            .bind(to: negativeLabel.rx.text)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.review.text })
            .bind(to: commentLabel.rx.text)
            .disposed(by: disposeBag)

    }

    // MARK: - Utils

    fileprivate func reloadViews(review: Review) {
        removeElements()

        if let text = review.positiveText, text.isNotEmpty {
            addBodyElement(imageView: positiveImageView, label: positiveLabel, under: headerView, offset: Metric.headerToBodyOffset)
        }

        if let text = review.negativeText, text.isNotEmpty {
            if positiveLabel.superview != nil {
                addBodyElement(imageView: negativeImageView, label: negativeLabel, under: positiveLabel, offset: Metric.bodyMargin)
            } else {
                addBodyElement(imageView: negativeImageView, label: negativeLabel, under: headerView, offset: Metric.headerToBodyOffset)
            }
        }

        if let text = review.text, text.isNotEmpty {
            if negativeLabel.superview != nil {
                addCommentSectionUnder(view: negativeLabel, offset: Metric.bodyToFooterOffset)
            } else if positiveLabel.superview != nil {
                addCommentSectionUnder(view: positiveLabel, offset: Metric.bodyToFooterOffset)
            } else {
                addCommentSectionUnder(view: headerView, offset: Metric.headerToBodyOffset)
            }
        }
    }

    private func removeElements() {
        positiveImageView.removeFromSuperview()
        positiveLabel.removeFromSuperview()
        negativeImageView.removeFromSuperview()
        negativeLabel.removeFromSuperview()
        commentLabel.removeFromSuperview()
    }

    private func addBodyElement(imageView: UIImageView, label: UILabel, under view: UIView, offset: CGFloat) {
        addSubview(imageView)
        addSubview(label)

        imageView.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.bottom).offset(offset)
            make.centerX.equalTo(headerView.avatarView)
        }

        label.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.bottom).offset(offset)
            make.left.equalTo(headerView.descriptionStackView)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }
    }

    private func addCommentSectionUnder(view: UIView, offset: CGFloat) {
        addSubview(commentLabel)

        commentLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.contentLeftOffset)
            make.top.equalTo(view.snp.bottom).offset(offset)
            make.right.equalToSuperview().offset(Metric.contentRightOffset)
        }
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductReviewTableViewCell {
    fileprivate enum Color {
        static let positiveLabelTextColor = Style.Color.black
        static let negativeLabelTextColor = Style.Color.black
        static let commentLabelTextColor = Style.Color.secondaryText
    }

    fileprivate enum Font {
        static let positiveLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let negativeLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
        static let commentLabelFont = Style.Font.sanFrancisco(.medium, size: 12)
    }

    fileprivate enum Image {
        static let positiveImageViewImage = Media.icon(.plus, size: .x18)?.tint(with: UIColor(hex: "#26CB5A"))
        static let negativeImageViewImage = Media.icon(.minus, size: .x18)?.tint(with: UIColor(hex: "#ED4C34"))
        static let avatarPlaceholderImage = Media.image(.avatarPlaceholder)
    }

    fileprivate enum Metric {
        static let contentLeftOffset: CGFloat = 20
        static let contentTopOffset: CGFloat = 20
        static let contentRightOffset: CGFloat = -20
        static let contentBottomOffset: CGFloat = -20

        static let headerToBodyOffset: CGFloat = 20
        static let bodyMargin: CGFloat = 4
        static let bodyToFooterOffset: CGFloat = 20
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductReviewTableViewCell {
    var reloadViews: Binder<Review> {
        return Binder(base, binding: { view, review in
            view.reloadViews(review: review)
        })
    }
}
