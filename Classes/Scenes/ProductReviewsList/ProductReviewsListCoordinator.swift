//
//  ProductReviewsListCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 16.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum ProductReviewsListCoordinationResult {
    case popped(Product)
}

final class ProductReviewsListCoordinator: Coordinator<ProductReviewsListCoordinationResult>, ProductModificationCoordinatable {

    private let rootNavigationController: UINavigationController
    private let container: DependenciesContainer
    private let input: ProductReviewsListInput

    private let actionPublisher = PublishSubject<ProductReviewsListViewModel.Action>()

    init(rootNavigationController: UINavigationController, container: DependenciesContainer, input: ProductReviewsListInput) {
        self.rootNavigationController = rootNavigationController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<ProductReviewsListCoordinationResult> {
        let viewModel = container.productReviewsListViewModel(input: input)
        let viewController = ProductReviewsListViewController(reactor: viewModel)
        rootNavigationController.pushViewController(viewController, animated: true)

        let popAction = viewController.rx.popAction
            .withLatestFrom(viewModel.state)
            .map({ ProductReviewsListCoordinationResult.popped($0.product) })

        actionPublisher
            .bind(to: viewModel.action)
            .disposed(by: disposeBag)

        viewController.rx.addReviewAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductReviewFlowMetadata(product: product, selectedRating: nil, selectedShop: nil, reviewText: .stub)
                self.startProductReviewRecursiveFlow(
                    viewController: viewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: disposeBag)

        return popAction
            .do(onSubscribe: { [weak self] in
                self?.container.analyticsService.sendEventSync(event: .reviewMore, inCategory: .review, tag: .none)
            })
            .take(1)

    }

    // MARK: - ProductModificationCoordinatable

    func updateProduct(type: ProductUpdateResultType) {
        if case ProductUpdateResultType.review(let metadata) = type {
            actionPublisher.onNext(.insertAddedReview(review: metadata.review))
        }
    }

}
