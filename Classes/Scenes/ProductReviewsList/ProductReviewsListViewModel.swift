//
//  ProductReviewsListViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 05.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift

final class ProductReviewsListViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let reviewsService: ReviewsServiceType

    // MARK: - Lifecycle

    init(reviewsService: ReviewsServiceType, input: ProductReviewsListInput) {
        self.reviewsService = reviewsService
        self.initialState = State(product: input.product)
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .bindUpdates:
            return reviewsService.observeReviewsWithReset()
                .map(Mutation.update)

        case .fetch(product: let product):
            return reviewsService.refreshReviews(product: product)
                .map(Mutation.updateChanges)
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: false)) })
                .startWith(.setLoaded(false))

        case .refresh(product: let product):
            return reviewsService.refreshReviews(product: product)
                .map(Mutation.updateChanges)
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: true)) })
                .startWith(.setRefreshing(true))

        case .loadNext(product: let product, page: let page):
            return reviewsService.loadReviews(product: product, page: page)
                .map(Mutation.updateChanges)
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: true)) })
                .startWith(.beginLoadingNext)

        case.delete(product: let product, review: let review):
            return reviewsService.deleteReview(product: product, review: review)
                .map({ Mutation.commitDeleting($0.productUpdates)})
                .catchError({ .just(Mutation.showError(error: $0, isLoaded: true)) })
                .startWith(.beginDeleting)

        case .insertAddedReview(let review):
            return reviewsService.saveReview(review: review)
                .andThen(.just(.stub))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        switch mutation {
        case .update(let reviews):
            state.sections = reviews.mapToSections()
            state.error = nil

        case .updateChanges(let pagination):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.pagination = pagination
            state.isLoaded = true
            state.error = nil

        case .setRefreshing(let isRefreshing):
            state.isRefreshing = isRefreshing
            state.error = nil

        case .setLoaded(let isLoaded):
            state.isRefreshing = false
            state.isLoaded = isLoaded
            state.error = nil

        case .beginDeleting:
            state.isDeleting = true

        case .commitDeleting(let productUpdates):
            state.isDeleting = false
            state.product = state.product.update(updates: productUpdates)

        case .showError(error: let error, isLoaded: let isLoaded):
            state.isLoadingNext = false
            state.isRefreshing = false
            state.isDeleting = false
            state.isLoaded = isLoaded
            state.error = error

        case .beginLoadingNext:
            state.isLoadingNext = true

        case .stub:
            break
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductReviewsListViewModel {
    enum Action {
        case bindUpdates
        case fetch(product: Product)
        case refresh(product: Product)
        case loadNext(product: Product, page: Int)
        case delete(product: Product, review: Review)
        case insertAddedReview(review: Review)
    }
}

// MARK: - Mutations

extension ProductReviewsListViewModel {
    enum Mutation {
        case stub
        case update([Review])
        case updateChanges(Pagination)
        case setRefreshing(Bool)
        case setLoaded(Bool)
        case beginDeleting
        case commitDeleting(ProductUpdates)
        case showError(error: Error, isLoaded: Bool)
        case beginLoadingNext
    }
}

// MARK: - State

extension ProductReviewsListViewModel {
    struct State {
        var product: Product
        var sections: [ProductReviewsSectionModel] = []
        var isLoadingNext: Bool = false
        var isRefreshing: Bool = false
        var isDeleting: Bool = false
        var isLoaded: Bool = false
        var pagination = Pagination.initial
        var error: Error?
    }
}
