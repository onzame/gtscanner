//
//  ProductReviewsSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 05.02.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

// MARK: - Sections

enum ProductReviewsSectionModel {
    case section(items: [ProductReviewsItemModel])
}

extension ProductReviewsSectionModel: AnimatableSectionModelType {
    var identity: String {
        return "section"
    }

    var items: [ProductReviewsItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: ProductReviewsSectionModel, items: [ProductReviewsItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }
}

// MARK: - Items

enum ProductReviewsItemModel {
    case review(ProductReviewTableViewModel)
}

extension ProductReviewsItemModel: IdentifiableType {
    var identity: Int {
        switch self {
        case .review(let viewModel):
            return viewModel.currentState.review.id
        }
    }
}

extension ProductReviewsItemModel: Equatable {
    static func == (lhs: ProductReviewsItemModel, rhs: ProductReviewsItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.review(let left), .review(let right)):
            return left.currentState == right.currentState
        }
    }
}

extension Array where Element == Review {
    func mapToSections() -> [ProductReviewsSectionModel] {
        let items = self
            .sorted(by: { (lhs, rhs) -> Bool in
                if lhs.updatedAt > rhs.updatedAt {
                    return lhs.id > rhs.id
                } else {
                    return false
                }
            })
            .map({ review -> ProductReviewsItemModel in
                let viewModel = ProductReviewTableViewModel(review: review)
                let itemModel = ProductReviewsItemModel.review(viewModel)
                return itemModel
            })

        return items.isEmpty ? [] : [ProductReviewsSectionModel.section(items: items)]
    }
}

extension Observable where Element == ProductReviewsItemModel {
    func mapToProductReview() -> Observable<Review?> {
        return map({ model in
            if case ProductReviewsItemModel.review(let viewModel) = model {
                return viewModel.currentState.review
            } else {
                return nil
            }
        })
    }
}
