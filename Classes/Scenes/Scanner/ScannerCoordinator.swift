//
//  ScannerCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 25.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class ScannerCoordinator: Coordinator<Void>, ProductModificationCoordinatable {

    let rootViewModel: ScannerViewModel
    let rootViewController: ScannerViewController

    private let container: DependenciesContainer

    init(container: DependenciesContainer) {
        self.container = container
        self.rootViewModel = container.scannerViewModel
        self.rootViewController = ScannerViewController(reactor: rootViewModel)
    }

    override func start() -> Observable<Void> {

        rootViewController.rx.showHistoryAction
            .flatMap({ [weak self] _ -> Observable<Void> in
                guard let `self` = self else { return .empty() }
                return self.showScanHistoryScene()
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        rootViewController.rx.showListAction
            .flatMap({ [weak self] _ -> Observable<ModalScanListCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                return self.showScanListScene()
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        rootViewController.rx.showProductInfoAction
            .flatMap({ [weak self] product -> Observable<ModalProductInfoCoordinator.CoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductInfoInput(product: product)
                return self.showProductInfoScene(input: input)
            })
            .subscribe()
            .disposed(by: disposeBag)

        rootViewController.rx.showAddProductAction
            .do(onNext: { [weak self] unknownProduct in
                guard let `self` = self else { return }

                let metadata = ProductAddFlowMetadata(gtin: unknownProduct.gtin)

                self.startProductAddRecursiveFlow(
                    viewController: self.rootViewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.disposeBag
                )
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        rootViewController.rx.addPriceAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductPriceFlowMetadata(product: product, selectedPrice: nil, selectedShop: nil)

                self.startProductPriceRecursiveFlow(
                    viewController: self.rootViewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.rootViewController.disposeBag
                )
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        rootViewController.rx.addReviewAction
            .do(onNext: { [weak self] product in
                guard let `self` = self else { return }

                let metadata = ProductReviewFlowMetadata(product: product, selectedRating: nil, selectedShop: nil, reviewText: .stub)

                self.startProductReviewRecursiveFlow(
                    viewController: self.rootViewController,
                    container: self.container,
                    metadata: metadata,
                    disposeBag: self.rootViewController.disposeBag
                )
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        rootViewController.rx.saveProductAction
            .flatMap({ [weak self] product -> Observable<ModalProductAmountCoordinationResult> in
                guard let `self` = self else { return .empty() }
                let input = ProductAmountInput(product: product, amount: nil, mode: .add)
                return self.showProductAmountScene(input: input)
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        rootViewController.rx.showCompareAction
            .flatMap({ [weak self] _ -> Observable<Void> in
                guard let `self` = self else { return .empty() }
                return self.showCompareScene()
            })
            .subscribe()
            .disposed(by: rootViewController.disposeBag)

        return Observable.never()
    }

    private func showScanHistoryScene() -> Observable<ModalScanHistoryCoordinator.CoordinationResult> {
        let coordinator = ModalScanHistoryCoordinator(rootViewController: rootViewController, container: container)
        return coordinate(to: coordinator)
    }

    private func showScanListScene() -> Observable<ModalScanListCoordinator.CoordinationResult> {
        let coordinator = ModalScanListCoordinator(rootViewController: rootViewController, container: container)
        return coordinate(to: coordinator)
    }

    private func showProductInfoScene(input: ProductInfoInput) -> Observable<ModalProductInfoCoordinator.CoordinationResult> {
        let coordinator = ModalProductInfoCoordinator(rootViewController: rootViewController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    private func showProductAmountScene(input: ProductAmountInput) -> Observable<ModalProductAmountCoordinationResult> {
        let coordinator = ModalProductAmountCoordinator(rootViewController: rootViewController, container: container, input: input)
        return coordinate(to: coordinator)
    }

    private func showCompareScene() -> Observable<ModalCompareCategoriesCoordinator.CoordinationResult> {
        let coordinator = ModalCompareCategoriesCoordinator(rootViewController: rootViewController, container: container)
        return coordinate(to: coordinator)
    }

}
