//
//  ScannerViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import AVFoundation
import ReactorKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class ScannerViewController: BaseViewController<ScannerViewModel> {

    // MARK: - UI сomponents

    let noAccessCameraPlaceholderView = ErrorStateView().then {
        $0.image = Image.noDataPlaceholderImage
        $0.title = Localization.Scanner.noAccessTitle
        $0.subtitle = Localization.Scanner.noAccessSubtitle
        $0.actionTitle = Localization.Scanner.noAccessActionTitle
    }

    let noAccessLocationPlaceholderView = ErrorStateView().then {
        $0.image = Image.noDataPlaceholderImage
        $0.title = Localization.Location.noPermissionTitle
        $0.subtitle = Localization.Location.noPermissionSubtitle
        $0.actionTitle = Localization.Location.noPermissionActionTitle
    }

    let captureCameraVideoView = CaptureCameraVideoView()

    let scannerActionsView = ScannerActionsComponentView()

    let scannerTipView = ScannerTipComponentView()

    let scannedProductsView = ScannedProductsComponentView()

    let featureSnackbarView = FeatureSnackbackView()

    let compareSnackbarView = ScannerCompareComponentView().then {
        $0.alpha = 0
    }

    // MARK: - Gestures

    fileprivate let swipeScannedProductsViewGesture = UISwipeGestureRecognizer().then {
        $0.direction = .down
    }

    // MARK: - Properties

    private let videoCaptureSessionController = VideoCaptureSessionController()

    private let locationSessionController = LocationSessionController.shared

    // MARK: - Lifecycle

    override init(reactor: Reactor) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupView()
    }

    private func setupView() {
        title = Localization.Scanner.title
        view.backgroundColor = Color.backgroundColor
        scannedProductsView.addGestureRecognizer(swipeScannedProductsViewGesture)
        captureCameraVideoView.bindSession(videoCaptureSessionController.captureSession)
    }

    // Showing camera interface

    private func setupCameraEnabledStateHierarchy() {
        noAccessCameraPlaceholderView.removeFromSuperview()
        noAccessLocationPlaceholderView.removeFromSuperview()

        view.addSubview(captureCameraVideoView)

        captureCameraVideoView.addSubview(scannerActionsView)
        captureCameraVideoView.addSubview(scannerTipView)
        captureCameraVideoView.addSubview(compareSnackbarView)
    }

    private func setupCameraEnabledStateLayout() {
        captureCameraVideoView.addEdgesConstraints()

        scannerActionsView.snp.makeConstraints { (make) in
            make.top.equalTo(view.safeTopConstraintItem).offset(Metric.scannerActionsViewTopOffset)
            make.right.equalTo(view.safeRightConstraintItem).offset(Metric.contentRightOffset)
            make.left.equalTo(view.safeLeftConstraintItem).offset(Metric.contentLeftOffset)
        }

        scannerTipView.snp.makeConstraints { (make) in
            make.top.equalTo(scannerActionsView.snp.bottom).offset(Metric.scannerTipViewTopOffset)
            make.right.equalTo(view.safeRightConstraintItem).offset(Metric.contentRightOffset)
            make.left.equalTo(view.safeLeftConstraintItem).offset(Metric.contentLeftOffset)
            make.height.equalTo(Metric.scannerTipViewHeight)
        }

        compareSnackbarView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(safeBottomConstraintItem)
            make.height.equalTo(Metric.compareSnackbarViewHeight)
        }
    }

    // Showing no access placeholder interaface

    private func setupCameraDisabledStateHierarchy() {
        captureCameraVideoView.removeFromSuperview()
        noAccessLocationPlaceholderView.removeFromSuperview()
        view.addSubview(noAccessCameraPlaceholderView)
    }

    private func setupCameraDisabledStateLayout() {
        noAccessCameraPlaceholderView.addSafeEdgesConstraints()
    }

    private func setupLocationDisabledStateHierarchy() {
        captureCameraVideoView.removeFromSuperview()
        noAccessCameraPlaceholderView.removeFromSuperview()
        view.addSubview(noAccessLocationPlaceholderView)
    }

    private func setupLocationDisabledStateLayout() {
        noAccessLocationPlaceholderView.addSafeEdgesConstraints()
    }

    // Scanned products collection elements settings

    private func setupShowScannedProductsViewHierarchy() {
        if let loadingView = view.loadingSubview {
            view.insertSubview(scannedProductsView, belowSubview: loadingView)
        } else {
            view.addSubview(scannedProductsView)
        }
    }

    private func setupHiddenScannedProductsViewLayout() {
        scannedProductsView.snp.remakeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(Metric.scannedProductsViewHeight)
            make.bottom.equalToSuperview().offset(Metric.scannedProductsViewHeight)
        }

        view.layoutIfNeeded()
    }

    private func setupShownScannedProductsViewLayout() {
        scannedProductsView.snp.remakeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(Metric.scannedProductsViewHeight)

            if self.compareSnackbarView.alpha == 0 {
                make.bottom.equalTo(view.safeBottomConstraintItem).offset(Metric.scannedProductsViewBottomOffset)
            } else {
                make.bottom.equalTo(compareSnackbarView.snp.top).offset(Metric.scannedProductsViewBottomOffset)
            }
        }

        view.layoutIfNeeded()
    }

    private func animateShowingScannedProductsViewLayout() {
        AnimationFactory.animate(
            duration: Duration.scannedProductsViewShowAnimationDuration,
            dampingRatio: Duration.scannedProductsViewShowDamping,
            options: .curveEaseIn,
            animations: { [weak self] in self?.setupShownScannedProductsViewLayout() }
        )
    }

    private func animateHidingScannedProductsViewLayout() {
        AnimationFactory.animate(
            duration: Duration.scannedProductsViewHideAnimationDuration,
            options: .curveEaseOut,
            animations: { [weak self] in self?.setupHiddenScannedProductsViewLayout() },
            completion: { [weak self] in self?.scannedProductsView.removeFromSuperview() }
        )
    }

    private func setupShowScannedProductsViewLayout() {
        setupHiddenScannedProductsViewLayout()
        animateShowingScannedProductsViewLayout()
    }

    // MARK: - Public methods

    func setupCameraEnabledState(isEnabled: Bool) {
        if isEnabled {
            setupCameraEnabledStateHierarchy()
            setupCameraEnabledStateLayout()
        } else {
            setupCameraDisabledStateHierarchy()
            setupCameraDisabledStateLayout()
        }
    }

    func setupLocationEnabledState(isEnabled: Bool) {
        if isEnabled {
            videoCaptureSessionController.captureSession.startRunning()
            setupCameraEnabledStateHierarchy()
            setupCameraEnabledStateLayout()
        } else {
            videoCaptureSessionController.captureSession.stopRunning()
            setupLocationDisabledStateHierarchy()
            setupLocationDisabledStateLayout()
        }
    }

    func showScannedProductsCollection() {
        if scannedProductsView.superview == nil {
            setupShowScannedProductsViewHierarchy()
            setupShowScannedProductsViewLayout()
        }
    }

    func hideScannedProductsCollection() {
        animateHidingScannedProductsViewLayout()
    }

    // MARK: - FeatureBanner methods

    fileprivate func shouldShowFeatureBanner(shouldShow: Bool) {
        if shouldShow {
            if featureSnackbarView.superview != nil { return }
            view.addSubview(featureSnackbarView)

            featureSnackbarView.snp.makeConstraints { (make) in
                make.left.equalToSuperview().offset(Metric.featureSnackbarViewLeftOffset)
                make.right.equalToSuperview().offset(Metric.featureSnackbarViewRightOffset)
                make.bottom.equalTo(safeBottomConstraintItem).offset(Metric.featureSnackbarViewBottomOffset)
            }

            view.layoutIfNeeded()

        } else {
            if featureSnackbarView.superview == nil { return }

            AnimationFactory.animate(
                duration: Duration.scannedProductsViewShowAnimationDuration,
                options: .curveEaseOut,
                animations: { [weak self] in self?.featureSnackbarView.alpha = 0 },
                completion: { [weak self] in self?.featureSnackbarView.removeFromSuperview() }
            )
        }
    }

    fileprivate func shouldShowCompareDetailsBanner(shouldShow: Bool) {
        if shouldShow {
            if compareSnackbarView.alpha != 0 { return }

            if scannedProductsView.superview != nil {
                scannedProductsView.snp.remakeConstraints { (make) in
                    make.left.right.equalToSuperview()
                    make.height.equalTo(Metric.scannedProductsViewHeight)
                    make.bottom.equalTo(compareSnackbarView.snp.top).offset(Metric.scannedProductsViewBottomOffset)
                }
            }

            AnimationFactory.animate(
                duration: Duration.scannedProductsViewShowAnimationDuration,
                dampingRatio: Duration.scannedProductsViewShowDamping,
                options: .curveEaseOut,
                animations: { [weak self] in
                    self?.compareSnackbarView.alpha = 1
                    self?.view.layoutIfNeeded()
                }
            )
        }
    }

    // MARK: - Bindings

    override func bind(reactor: ScannerViewModel) {

        // Settings

        swipeScannedProductsViewGesture.rx.event
            .bind(to: rx.swipeScannedProductsViewBinding)
            .disposed(by: disposeBag)

        noAccessCameraPlaceholderView.actionButton.rx.tap
            .bind(to: rx.openAppSettings)
            .disposed(by: disposeBag)

        noAccessLocationPlaceholderView.actionButton.rx.tap
            .flatMap({ _ in
                Observable.merge(
                    UIApplication.shared.rx.openSettingsAction(),
                    UIApplication.shared.rx.didBecomeActiveObservable().take(1)
                )
            })
            .bind(to: rx.locationAccessBinding)
            .disposed(by: disposeBag)

        rx.viewWillAppear
            .withLatestFrom(reactor.state)
            .map({ $0.isCameraEnabled })
            .filterNil()
            .bind(to: videoCaptureSessionController.rx.runSession)
            .disposed(by: disposeBag)

        rx.viewWillDisappear
            .withLatestFrom(reactor.state)
            .map({ $0.isCameraEnabled })
            .filterNil()
            .filter({ $0 })
            .not()
            .bind(to: videoCaptureSessionController.rx.runSession)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .asObservable()
            .flatMap({ [weak self] _ -> Observable<Void> in
                guard let `self` = self else { return .empty() }
                return self.locationSessionController.prepareSession()
                    .asObservable()
                    .mapToVoid()
            })
            .bind(to: locationSessionController.rx.enableUpdating)
            .disposed(by: disposeBag)

        // Input

        rx.viewDidLoad
            .take(1)
            .mapTo(Reactor.Action.bindProducuts)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .take(1)
            .mapTo(Reactor.Action.bindCompareProducts)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        rx.viewDidLoad
            .asObservable()
            .flatMap({ [weak self] _ -> Observable<Reactor.Action> in
                guard let `self` = self else { return .empty() }
                return self.videoCaptureSessionController.prepareSession()
                    .andThen(Observable.just(Reactor.Action.requestCameraSuccessed))
                    .catchErrorJustReturn(Reactor.Action.requestCameraFailed)
            })
            .take(1)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        let locationSequence = locationSessionController.rx.location
            .mapToDomain()

        videoCaptureSessionController.rx.scannedObject
            .withLatestFrom(locationSequence, resultSelector: { (object, location) -> ScannedObjectMetadata in
                ScannedObjectMetadata(gtin: object.value, location: location)
            })
            .withLatestFrom(reactor.state, resultSelector: { (metadata, state) -> ScannedObjectMetadata? in
                state.sections.containsProduct(gtin: metadata.gtin) || state.isLoading ? nil : metadata
            })
            .filterNil()
            .map({ Reactor.Action.searchObject($0) })
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        scannedProductsView.rx.removeFromSuperview
            .mapTo(Reactor.Action.clearSearchedProducts)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        featureSnackbarView.cancelButton.rx.tap
            .mapTo(Reactor.Action.clearStartBanner)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        videoCaptureSessionController.rx.scannedObject
            .take(1)
            .withLatestFrom(reactor.state)
            .filter({ $0.shouldShowStartBanner })
            .mapTo(Reactor.Action.clearStartBanner)
            .bind(to: reactor.action)
            .disposed(by: disposeBag)

        // Output

        reactor.state
            .map({ $0.isCameraEnabled })
            .filterNil()
            .distinctUntilChanged()
            .bind(to: rx.cameraEnabled)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error })
            .filterNil()
            .bind(to: rx.errorSnackbar)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.error as? LocationSessionError })
            .unwrap()
            .filter({ $0 == .accessDenied })
            .mapToVoid()
            .bind(to: rx.noLocationAccessBinding)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.sections })
            .bind(to: scannedProductsView.collectionView.rx.items(dataSource: scannedProductsView.dataSource))
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.shouldScrollToLastItem })
            .mapToVoid()
            .bind(to: scannedProductsView.rx.scrollToLastItem)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.sections.isNotEmpty })
            .distinctUntilChanged()
            .filter({ $0 })
            .mapToVoid()
            .bind(to: rx.showScannedProductsCollection)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.sections.isNotEmpty })
            .distinctUntilChanged()
            .bind(to: scannerTipView.rx.filledState)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isLoading })
            .distinctUntilChanged()
            .bind(to: rx.loading)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isCameraEnabled })
            .filterNil()
            .distinctUntilChanged()
            .bind(to: videoCaptureSessionController.rx.runSession)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.isCameraEnabled })
            .filterNil()
            .filter({ $0 })
            .withLatestFrom(reactor.state)
            .map({ $0.shouldShowStartBanner })
            .distinctUntilChanged()
            .bind(to: rx.shouldShowFeatureBanner)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.compareDetails })
            .bind(to: compareSnackbarView.rx.update)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.compareDetails.products.count > 1 })
            .distinctUntilChanged()
            .bind(to: rx.shouldShowCompareDetailsBanner)
            .disposed(by: disposeBag)
    }
}

// MARK: - Extensions
// MARK: - Constants

extension ScannerViewController {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.lightBlue
    }

    fileprivate enum Duration {
        static let compareSnackbarViewShowAnimationDuration: TimeInterval = 0.01
        static let scannedProductsViewShowAnimationDuration: TimeInterval = 0.5
        static let scannedProductsViewShowDamping: CGFloat = 0.7
        static let scannedProductsViewHideAnimationDuration: TimeInterval = 0.5
    }

    fileprivate enum Image {
        static let noDataPlaceholderImage = Media.image(.circleError)
    }

    fileprivate enum Metric {
        static let scannerActionsViewTopOffset: CGFloat = 16

        static let contentLeftOffset: CGFloat = 20
        static let contentRightOffset: CGFloat = -20

        static let scannerTipViewTopOffset: CGFloat = 19
        static let scannerTipViewHeight: CGFloat = 64
        static let scannedProductsViewHeight: CGFloat = 250
        static let scannedProductsViewBottomOffset: CGFloat = -12

        static let featureSnackbarViewLeftOffset: CGFloat = 8
        static let featureSnackbarViewRightOffset: CGFloat = -8
        static let featureSnackbarViewBottomOffset: CGFloat = -8

        static let compareSnackbarViewHeight: CGFloat = 64
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScannerViewController {
    var showHistoryAction: ControlEvent<Void> {
        return ControlEvent(events: base.scannerActionsView.openScanHistoryButton.rx.tap)
    }

    var showListAction: ControlEvent<Void> {
        return ControlEvent(events: base.scannerActionsView.openScanListButton.rx.tap)
    }

    var showProductInfoAction: ControlEvent<Product> {
        return ControlEvent(events: base.scannedProductsView.rx.showProductInfoAction)
    }

    var showAddProductAction: ControlEvent<UnknownProduct> {
        return ControlEvent(events: base.scannedProductsView.rx.showAddProductAction)
    }

    var addPriceAction: ControlEvent<Product> {
        return ControlEvent(events: base.scannedProductsView.rx.addPriceAction)
    }

    var addReviewAction: ControlEvent<Product> {
        return ControlEvent(events: base.scannedProductsView.rx.addReviewAction)
    }

    var saveProductAction: ControlEvent<Product> {
        return ControlEvent(events: base.scannedProductsView.rx.saveProductAction)
    }

    var showCompareAction: ControlEvent<Void> {
        return ControlEvent(events: base.compareSnackbarView.actionButton.rx.tap)
    }

    var cameraEnabled: Binder<Bool> {
        return Binder(base, binding: { viewController, isEnabled in
            viewController.setupCameraEnabledState(isEnabled: isEnabled)
        })
    }

    var showScannedProductsCollection: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.showScannedProductsCollection()
        })
    }

    var swipeScannedProductsViewBinding: Binder<UISwipeGestureRecognizer> {
        return Binder(base, binding: { viewController, _ in
            viewController.hideScannedProductsCollection()
        })
    }

    var shouldShowFeatureBanner: Binder<Bool> {
        return Binder(base, binding: { viewController, shouldShow in
            viewController.shouldShowFeatureBanner(shouldShow: shouldShow)
        })
    }

    var shouldShowCompareDetailsBanner: Binder<Bool> {
        return Binder(base, binding: { viewController, shouldShow in
            viewController.shouldShowCompareDetailsBanner(shouldShow: shouldShow)
        })
    }

    var noLocationAccessBinding: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.setupLocationEnabledState(isEnabled: false)
        })
    }

    var locationAccessBinding: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            viewController.setupLocationEnabledState(isEnabled: true)
        })
    }
}
