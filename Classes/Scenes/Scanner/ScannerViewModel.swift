//
//  ScannerViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 21.11.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import AVFoundation
import ReactorKit
import RxSwift

final class ScannerViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Dependencies

    private let productsService: ProductsServiceType
    private let featuresService: FeaturesServiceType
    private let comparesService: ComparesServiceType

    // MARK: - Lifecycle

    init(productsService: ProductsServiceType, featuresService: FeaturesServiceType, comparesService: ComparesServiceType) {
        self.productsService = productsService
        self.featuresService = featuresService
        self.comparesService = comparesService
        self.initialState = State(featuresState: featuresService.fetchState())
    }

    // MARK: - Transformations

    func mutate(action: Action) -> Observable<Mutation> {
        switch action {
        case .bindProducuts:
            return productsService.observeScannedProductsWithReset()
                .map(Mutation.updateScannedProducts)

        case .bindCompareProducts:
            return comparesService.observeCompareProducts()
                .map(Mutation.updateCompareProducts)

        case .requestCameraSuccessed:
            return .just(.allowCameraUsage(true))

        case .requestCameraFailed:
            return .just(.allowCameraUsage(false))

        case .searchObject(let metadata):
            return productsService.searchProduct(gtin: metadata.gtin, location: metadata.location)
                .mapTo(.showLoading(false))
                .catchError({ .just(Mutation.showError($0)) })
                .startWith(.showLoading(true))

        case .clearSearchedProducts:
            return productsService.deleteAllScannedProducts()
                .andThen(.just(.clearProducts))

        case .clearStartBanner:
            return featuresService.markStartBannerAsShown()
                .andThen(.just(.clearBanner))
        }
    }

    func reduce(state: State, mutation: Mutation) -> State {
        var state = state

        state.shouldScrollToLastItem = false

        switch mutation {
        case .allowCameraUsage(let isAllowed):
            state.isCameraEnabled = isAllowed

        case .clearProducts:
            state.isLoading = false
            state.error = nil

        case .clearBanner:
            state.shouldShowStartBanner = false

        case .showError(let error):
            state.isLoading = false
            state.error = error

        case .showLoading(let isLoading):
            state.isLoading = isLoading
            state.error = nil

        case .updateScannedProducts(let savedProducts):
            let newSections = savedProducts.mapToSections()

            if let oldItemsCount = state.sections.first?.items.count,
                let newItemsCount = newSections.first?.items.count {
                state.shouldScrollToLastItem = oldItemsCount != newItemsCount
            }

            state.sections = newSections

        case .updateCompareProducts(let compareProducts):
            state.compareDetails = CompareDetails(products: compareProducts)
        }

        return state
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScannerViewModel {
    enum Action {
        case bindProducuts
        case bindCompareProducts
        case requestCameraSuccessed
        case requestCameraFailed
        case searchObject(ScannedObjectMetadata)
        case clearSearchedProducts
        case clearStartBanner
    }
}

// MARK: - Mutations

extension ScannerViewModel {
    enum Mutation {
        case allowCameraUsage(Bool)
        case showLoading(Bool)
        case showError(Error)
        case updateScannedProducts([ScannedProduct])
        case updateCompareProducts([CompareProductImage])
        case clearProducts
        case clearBanner
    }
}

// MARK: - State

extension ScannerViewModel {
    struct State {
        var isCameraEnabled: Bool?
        var isLoading = false
        var sections: [ScanSectionModel] = []
        var compareDetails: CompareDetails = .stub
        var shouldScrollToLastItem = false
        var shouldShowStartBanner = false
        var error: Error?

        init(featuresState: FeaturesState) {
            self.shouldShowStartBanner = !featuresState.isStartBannerShown
        }
    }
}
