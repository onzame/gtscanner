//
//  CompareDetails.swift
//  Gorod
//
//  Created by Sergei Fabian on 03.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

struct CompareDetails {
    static let stub = CompareDetails(products: [])

    let products: [CompareProductImage]
}
