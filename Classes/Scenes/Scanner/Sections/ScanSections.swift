//
//  ScanSections.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit
import RxSwift
import RxCocoa
import RxDataSources

// MARK: - Sections

enum ScanSectionModel {
    case section(items: [ScanItemModel])
}

// MARK: - AnimatableSectionModelType

extension ScanSectionModel: AnimatableSectionModelType {
    var items: [ScanItemModel] {
        switch self {
        case .section(items: let items):
            return items
        }
    }

    init(original: ScanSectionModel, items: [ScanItemModel]) {
        switch original {
        case .section:
            self = .section(items: items)
        }
    }

    var identity: String {
        switch self {
        case .section:
            return "ScanSectionModel.section"
        }
    }
}

// MARK: - Items

enum ScanItemModel {
    case scannedProduct(ScannedProductCollectionViewModel)
    case scannedEmptyProduct(ScannedEmptyProductCollectionViewModel)
}

// MARK: - Extensions
// MARK: - IdentifiableType

extension ScanItemModel: IdentifiableType {
    typealias Identity = String

    var identity: String {
        switch self {
        case .scannedEmptyProduct(let viewModel):
            return viewModel.currentState.product.gtin
        case .scannedProduct(let viewModel):
            return viewModel.currentState.product.gtin
        }
    }
}

// MARK: - Equatable

extension ScanItemModel: Equatable {
    static func == (lhs: ScanItemModel, rhs: ScanItemModel) -> Bool {
        switch (lhs, rhs) {
        case (.scannedProduct(let left), .scannedProduct(let right)):
            return left == right
        case (.scannedEmptyProduct(let left), .scannedEmptyProduct(gtin: let right)):
            return left == right
        default:
            return false
        }
    }
}

// MARK: - Array + ScanSectionModel

extension Array where Element == ScannedProduct {
    func mapToSections() -> [ScanSectionModel] {
        let items = map({ scannedProduct -> ScanItemModel in
            switch scannedProduct {
            case .founded(let product):
                let viewModel = ScannedProductCollectionViewModel(product: product)
                return .scannedProduct(viewModel)
            case .unknown(let product):
                let viewModel = ScannedEmptyProductCollectionViewModel(product: product)
                return .scannedEmptyProduct(viewModel)
            }
        })

        return items.isNotEmpty ? [ScanSectionModel.section(items: items)] : []
    }
}

extension Array where Element == ScanSectionModel {
    func containsProduct(gtin: String) -> Bool {
        return contains(where: { sectionModel -> Bool in
            return sectionModel.items.contains(where: { itemModel -> Bool in
                return itemModel.identity == gtin
            })
        })
    }
}

// MARK: - Observable + ScanItemModel

extension Observable where Element == ScanItemModel {
    func mapToProduct() -> Observable<Product?> {
        return map({ model in
            if case ScanItemModel.scannedProduct(let viewModel) = model {
                return viewModel.currentState.product
            } else {
                return nil
            }
        })
    }
}
