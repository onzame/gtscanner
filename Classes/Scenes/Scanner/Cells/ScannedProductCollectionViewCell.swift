//
//  ScannedProductCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 04.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

class ScannedProductCollectionViewCell: BaseCollectionViewCell, View {

    // MARK: - UI components

    let headerView = ModalHeaderView()

    let bodyView = UIView().then {
        $0.backgroundColor = Color.bodyViewBackgroundColor
    }

    let descriptionStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        $0.titleLabel.numberOfLines = 3
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
    }

    let priceStackView = DescriptionStackView().then {
        $0.titleLabel.font = Font.descriptionStackViewTitleLabelFont
        $0.titleLabel.textColor = Color.descriptionStackViewTitleLabelTextColor
        $0.titleLabel.textAlignment = .right
        $0.subtitleLabel.font = Font.descriptionStackViewSubtitleLabelFont
        $0.subtitleLabel.textColor = Color.descriptionStackViewSubtitleLabelTextColor
        $0.subtitleLabel.textAlignment = .right
        $0.isUserInteractionEnabled = true
    }

    let addPriceButton = UIButton(type: .system).then {
        $0.setTitle(Localization.Scanner.addPriceActionTitle, for: .normal)
        $0.setTitleFont(Font.addPriceButtonTitleFont)
        $0.setTitleColor(Color.addPriceButtonTitleTextColor, for: .normal)
        $0.setBackgroundColor(Color.addPriceButtonBackgroundColor)
        $0.setHorizontalInsets(Metric.addPriceButtonHorizontalInset)
        $0.setCornerRadius(CornerRadius.actionsCornerRadius)
    }

    let ratingContainerView = UIView().then {
        $0.setBackgroundColor(Color.ratingContainerViewBackgroundColor)
        $0.setCornerRadius(CornerRadius.ratingContainerViewCornerRadius)
    }

    let ratingView = RatingView().then {
        $0.settings.starSize = Metric.rateViewHeight
        $0.settings.starMargin = Metric.rateViewMargin
        $0.settings.updateOnTouch = false
        $0.rating = 0
    }

    let ratingAmountContainerView = UIView().then {
        $0.backgroundColor = Color.ratingAmountContainerViewNeutralBackgroundColor
    }

    let ratingAmountView = TitledImageView().then {
        $0.imageView.image = Image.ratingAmountViewImage
        $0.titleLabel.font = Font.ratingAmountViewTitleLabelFont
        $0.titleLabel.textColor = Color.ratingAmountViewTitleLabelNeutralTextColor
        $0.stackView.spacing = Metric.ratingAmountViewStackViewSpacing
    }

    let ratesCounterContainerView = UIView().then {
        $0.backgroundColor = Color.ratesCounterContainerViewBackgroundColor
    }

    let ratesCounterView = TitledImageView().then {
        $0.imageView.image = Image.ratesCounterViewImage
        $0.titleLabel.font = Font.ratesCounterViewTitleLabelFont
        $0.titleLabel.textColor = Color.ratesCounterViewTitleLabelTextColor
        $0.stackView.spacing = Metric.ratesCounterViewStackViewSpacing
    }

    let countersStackView = UIStackView().then {
        $0.spacing = Metric.countersStackViewSpacing
        $0.distribution = .fillEqually
    }

    let saveButton = UIButton(type: .system).then {
        $0.setImage(Image.saveButtonImage, for: .normal)
        $0.backgroundColor = Color.actionsBackgroundColor
        $0.layer.cornerRadius = CornerRadius.actionsCornerRadius
    }

    let infoButton = UIButton(type: .system).then {
        $0.setImage(Image.infoButtonImage, for: .normal)
        $0.backgroundColor = Color.actionsBackgroundColor
        $0.layer.cornerRadius = CornerRadius.actionsCornerRadius
    }

    let infoAreaView = UIView().then {
        $0.isUserInteractionEnabled = true
    }

    let actionsStackView = UIStackView().then {
        $0.spacing = Metric.actionsStackViewSpacing
    }

    // MARK: - Gestures

    let infoAreaTapGesture = UITapGestureRecognizer()
    let priceTapGesture = UITapGestureRecognizer()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Overrides

    override func layoutSubviews() {
        super.layoutSubviews()

        ratingAmountContainerView.setCornerRadius(
            corners: [.layerMinXMinYCorner, .layerMinXMaxYCorner],
            radius: CornerRadius.actionsCornerRadius
        )

        ratesCounterContainerView.setCornerRadius(
            corners: [.layerMaxXMinYCorner, .layerMaxXMaxYCorner],
            radius: CornerRadius.actionsCornerRadius
        )
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(headerView)
        addSubview(bodyView)

        bodyView.addSubview(descriptionStackView)
        bodyView.addSubview(ratingContainerView)
        bodyView.addSubview(actionsStackView)
        bodyView.addSubview(infoAreaView)

        ratingContainerView.addSubview(ratingView)

        actionsStackView.addArrangedSubview(countersStackView)
        actionsStackView.addArrangedSubview(saveButton)
        actionsStackView.addArrangedSubview(infoButton)

        countersStackView.addArrangedSubview(ratingAmountContainerView)
        countersStackView.addArrangedSubview(ratesCounterContainerView)

        ratingAmountContainerView.addSubview(ratingAmountView)

        ratesCounterContainerView.addSubview(ratesCounterView)
    }

    private func setupLayout() {
        headerView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }

        bodyView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
        }

        actionsStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.actionsStackViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.actionsStackViewRightOffset)
            make.bottom.equalToSuperview().offset(Metric.actionsStackViewBottomOffset)
            make.height.equalTo(Metric.actionsStackViewHeight)
        }

        infoButton.snp.makeConstraints { (make) in
            make.width.equalTo(Metric.infoButtonWidth)
        }

        saveButton.snp.makeConstraints { (make) in
            make.width.equalTo(Metric.copyButtonWidth)
        }

        ratingContainerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.ratingContainerViewLeftOffset)
            make.right.equalToSuperview().offset(Metric.ratingContainerViewRightOffset)
            make.bottom.equalTo(actionsStackView.snp.top).offset(Metric.ratingContainerViewBottomOffset)
            make.height.equalTo(Metric.ratingContainerViewHeight)
        }

        ratingView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }

        [ratingAmountView, ratesCounterView].forEach {
            $0.snp.makeConstraints { (make) in
                make.center.equalToSuperview()
                make.left.greaterThanOrEqualToSuperview()
                make.right.lessThanOrEqualToSuperview()
            }
        }

        descriptionStackView.snp.remakeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.descriptionStackViewLeftOffset)
            make.top.equalToSuperview().offset(Metric.descriptionStackViewTopOffset)
        }

        infoAreaView.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.right.equalTo(descriptionStackView)
            make.bottom.equalTo(ratingContainerView.snp.top)
        }
    }

    private func setupView() {
        layer.cornerRadius = CornerRadius.cornerRadius
        layer.masksToBounds = true

        infoAreaView.addGestureRecognizer(infoAreaTapGesture)
        priceStackView.addGestureRecognizer(priceTapGesture)
    }

    // MARK: - Bindings

    func bind(reactor: ScannedProductCollectionViewModel) {
        reactor.state
            .map({ $0.product.name })
            .bind(to: descriptionStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.rating })
            .mapToString()
            .bind(to: ratingAmountView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.rating })
            .bind(to: ratingView.rx.rating)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.amounts.rates })
            .mapToString()
            .bind(to: ratesCounterView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ !$0.product.permissions.hasAddedPrice })
            .mapToVoid()
            .bind(to: rx.enableAddPrice)
            .disposed(by: disposeBag)

        reactor.state
            .filter({ $0.product.permissions.hasAddedPrice })
            .map({ $0.product.avgPrice })
            .mapToPrice()
            .bind(to: rx.showPrice)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.avgPrice })
            .mapToPrice()
            .bind(to: priceStackView.rx.title)
            .disposed(by: disposeBag)

        reactor.state
            .map({ $0.product.amounts.prices })
            .map({ Localization.Common.pricesAmount($0) })
            .bind(to: priceStackView.rx.subtitle)
            .disposed(by: disposeBag)
    }

    // MARK: - Private methods

    fileprivate func enableAddPrice() {
        if addPriceButton.superview == nil {

            priceStackView.removeFromSuperview()

            bodyView.addSubview(addPriceButton)

            addPriceButton.snp.remakeConstraints { (make) in
                make.centerY.equalTo(descriptionStackView)
                make.right.equalToSuperview().offset(Metric.addPriceButtonRightOffset)
                make.height.equalTo(Metric.addPriceButtonHeight)
                make.width.equalTo(Metric.addPriceButtonWidth)
                make.left.greaterThanOrEqualTo(descriptionStackView.snp.right).offset(Metric.addPriceButtonLeftOffset)
            }

            layoutIfNeeded()
        }
    }

    fileprivate func showPrice(price: String) {
        if priceStackView.superview == nil {

            addPriceButton.removeFromSuperview()

            bodyView.addSubview(priceStackView)

            let width = price.boundingWidth(height: CGFloat.greatestFiniteMagnitude, font: Font.descriptionStackViewTitleLabelFont)

            priceStackView.snp.remakeConstraints { (make) in
                make.top.equalTo(descriptionStackView)
                make.right.equalToSuperview().offset(Metric.priceStackViewRightOffset)
                make.left.equalTo(descriptionStackView.snp.right).offset(Metric.priceStackViewLeftOffset)
                make.width.equalTo(width)
            }

            layoutIfNeeded()
        }
    }
}

// MARK: - Extensions
// MARK: - Constants

extension ScannedProductCollectionViewCell {
    fileprivate enum Color {
        static let bodyViewBackgroundColor = Style.Color.white
        static let descriptionStackViewTitleLabelTextColor = Style.Color.black
        static let descriptionStackViewSubtitleLabelTextColor = Style.Color.secondaryText
        static let ratingContainerViewBackgroundColor = Style.Color.brightYellow?.withAlphaComponent(0.1)
        static let actionsBackgroundColor = Style.Color.lightBlue
        static let ratingAmountContainerViewNeutralBackgroundColor = Style.Color.lightBlue
        static let ratingAmountViewTitleLabelNeutralTextColor = UIColor(hex: "#262A3C")
        static let ratesCounterContainerViewBackgroundColor = Style.Color.lightBlue
        static let ratesCounterViewTitleLabelTextColor = UIColor(hex: "#262A3C")
        static let addPriceButtonBackgroundColor = Style.Color.lightBlue
        static let addPriceButtonTitleTextColor = Style.Color.brightBlue
    }

    fileprivate enum CornerRadius {
        static let cornerRadius: CGFloat = 16
        static let ratingContainerViewCornerRadius: CGFloat = 28
        static let actionsCornerRadius: CGFloat = 22
    }

    fileprivate enum Font {
        static let descriptionStackViewTitleLabelFont = Style.Font.sanFrancisco(.bold, size: .headline)
        static let descriptionStackViewSubtitleLabelFont = Style.Font.sanFrancisco(.medium, size: .subheadline)
        static let ratingAmountViewTitleLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
        static let ratesCounterViewTitleLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
        static let addPriceButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Image {
        static let ratingAmountViewImage = Media.icon(.starFilled, size: .x18)
        static let ratesCounterViewImage = Media.icon(.profile, size: .x18)
        static let saveButtonImage = Media.icon(.add, size: .x24)
        static let infoButtonImage = Media.icon(.info, size: .x24)
    }

    fileprivate enum Metric {
        static let descriptionStackViewLeftOffset: CGFloat = 20
        static let descriptionStackViewTopOffset: CGFloat = 12

        static let ratingContainerViewLeftOffset: CGFloat = 20
        static let ratingContainerViewBottomOffset: CGFloat = -8
        static let ratingContainerViewRightOffset: CGFloat = -20
        static let ratingContainerViewHeight: CGFloat = 70

        static let rateViewHeight: Double = 32
        static let rateViewMargin: Double = Screen.isLowWidthScreen ? 8 : 16

        static let actionsStackViewSpacing: CGFloat = 8
        static let actionsStackViewLeftOffset: CGFloat = 20
        static let actionsStackViewRightOffset: CGFloat = -20
        static let actionsStackViewBottomOffset: CGFloat = -24
        static let actionsStackViewHeight: CGFloat = 52

        static let countersStackViewSpacing: CGFloat = 2
        static let infoButtonWidth: CGFloat = 52
        static let copyButtonWidth: CGFloat = 52
        static let ratingAmountViewStackViewSpacing: CGFloat = 4
        static let ratesCounterViewStackViewSpacing: CGFloat = 4

        static let addPriceButtonWidth: CGFloat = 132
        static let addPriceButtonHeight: CGFloat = 52
        static let addPriceButtonRightOffset: CGFloat = -20
        static let addPriceButtonHorizontalInset: CGFloat = 12
        static let addPriceButtonLeftOffset: CGFloat = 8

        static let priceStackViewRightOffset: CGFloat = -20
        static let priceStackViewLeftOffset: CGFloat = 8
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScannedProductCollectionViewCell {
    var infoSelection: ControlEvent<Void> {
        let infoAreaTapAction = base.infoAreaTapGesture.rx.event.mapToVoid()
        let priceTapAction = base.priceTapGesture.rx.event.mapToVoid()
        let infoTapAction = base.infoButton.rx.tap.asObservable()
        return ControlEvent(events: Observable.merge(infoAreaTapAction, priceTapAction, infoTapAction))
    }

    var enableAddPrice: Binder<Void> {
        return Binder(base, binding: { view, _ in
            view.enableAddPrice()
        })
    }

    var showPrice: Binder<String> {
        return Binder(base, binding: { view, priceString in
            view.showPrice(price: priceString)
        })
    }
}
