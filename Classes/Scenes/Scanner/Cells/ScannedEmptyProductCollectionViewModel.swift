//
//  ScannedEmptyProductCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

final class ScannedEmptyProductCollectionViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(product: UnknownProduct) {
        initialState = State(product: product)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScannedEmptyProductCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ScannedEmptyProductCollectionViewModel {
    struct State {
        let product: UnknownProduct
    }
}

// MARK: - Equatable

extension ScannedEmptyProductCollectionViewModel: Equatable {
    static func == (lhs: ScannedEmptyProductCollectionViewModel, rhs: ScannedEmptyProductCollectionViewModel) -> Bool {
        return lhs.currentState == rhs.currentState
    }
}

// MARK: - State Equatable

extension ScannedEmptyProductCollectionViewModel.State: Equatable {
    static func == (lhs: ScannedEmptyProductCollectionViewModel.State, rhs: ScannedEmptyProductCollectionViewModel.State) -> Bool {
        return lhs.product == rhs.product
    }
}
