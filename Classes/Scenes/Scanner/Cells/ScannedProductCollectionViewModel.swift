//
//  ScannedProductCollectionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

final class ScannedProductCollectionViewModel: Reactor {

    // MARK: - Properties

    let initialState: State

    // MARK: - Lifecycle

    init(product: Product) {
        initialState = State(product: product)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ScannedProductCollectionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ScannedProductCollectionViewModel {
    struct State {
        let product: Product
    }
}

// MARK: - Equatable

extension ScannedProductCollectionViewModel: Equatable {
    static func == (lhs: ScannedProductCollectionViewModel, rhs: ScannedProductCollectionViewModel) -> Bool {
        return lhs.currentState == rhs.currentState
    }
}

// MARK: - State Equatable

extension ScannedProductCollectionViewModel.State: Equatable {
    static func == (lhs: ScannedProductCollectionViewModel.State, rhs: ScannedProductCollectionViewModel.State) -> Bool {
        return lhs.product == rhs.product
    }
}
