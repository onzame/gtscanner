//
//  ScannedEmptyProductCollectionViewCell.swift
//  Gorod
//
//  Created by Sergei Fabian on 06.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit

class ScannedEmptyProductCollectionViewCell: BaseCollectionViewCell, View {

    let headerView = ModalHeaderView()

    let bodyView = UIView().then {
        $0.backgroundColor = UIColor.white
    }

    let titleLabel = UILabel().then {
        $0.text = Localization.Scanner.notFoundTitle
        $0.font = Font.titleLabelFont
        $0.textColor = Color.titleLabelTextColor
        $0.numberOfLines = 0
    }

    let subtitleLabel = UILabel().then {
        $0.text = Localization.Scanner.notFoundSubtitle
        $0.font = Font.subtitleLabelFont
        $0.textColor = Color.subtitleLabelTextColor
        $0.numberOfLines = 0
    }

    let textStackView = UIStackView().then {
        $0.axis = .vertical
        $0.distribution = .equalSpacing
    }

    let imageView = UIImageView().then {
        $0.image = Image.imageViewImage
    }

    let actionContainerView = UIView().then {
        $0.backgroundColor = Color.actionContainerViewBackgroundColor
        $0.setCornerRadius(corners: .layerMaxXMinYCorner, radius: CornerRadius.actionContainerViewCornerRadius)
    }

    let addProductButton = UIButton(type: .system).then {
        $0.setTitle(Localization.Scanner.notFoundActionTitle, for: .normal)
        $0.setTitleColor(Color.addProductButtonTitleColor, for: .normal)
        $0.setTitleFont(Font.addProductButtonTitleFont)
    }

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {

        addSubview(headerView)
        addSubview(bodyView)

        bodyView.addSubview(textStackView)
        bodyView.addSubview(imageView)
        bodyView.addSubview(actionContainerView)

        actionContainerView.addSubview(addProductButton)

        textStackView.addArrangedSubview(titleLabel)
        textStackView.addArrangedSubview(subtitleLabel)

    }

    private func setupLayout() {
        headerView.snp.makeConstraints { (make) in
            make.left.top.right.equalToSuperview()
        }

        bodyView.snp.makeConstraints { (make) in
            make.left.right.bottom.equalToSuperview()
            make.top.equalTo(headerView.snp.bottom)
        }

        actionContainerView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview().offset(Metric.actionContainerViewRightOffset)
            make.bottom.equalToSuperview().offset(Metric.actionContainerViewBottomOffset)
        }

        addProductButton.snp.makeConstraints { (make) in
            make.left.top.equalToSuperview()
            make.right.bottom.equalTo(bodyView)
            make.height.equalTo(Metric.addProductButtonHeight)
        }

        imageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(Metric.imageViewRightOffset)
            make.bottom.lessThanOrEqualToSuperview().offset(Metric.imageViewBottomOffset)
            make.width.height.equalTo(Metric.imageViewPartSize)
        }

        textStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.textStackViewLeftOffset)
            make.top.equalToSuperview().offset(Metric.textStackViewTopOffset)
            make.right.equalTo(imageView.snp.left).offset(Metric.textStackViewRightOffset)
            make.bottom.equalTo(actionContainerView.snp.top).offset(Metric.textStackViewBottomOffset)
        }
    }

    private func setupView() {
        layer.cornerRadius = CornerRadius.cornerRadius
        layer.masksToBounds = true
    }

    // MARK: - Bindings

    func bind(reactor: ScannedEmptyProductCollectionViewModel) { }

}

// MARK: - Extensions
// MARK: - Constants

extension ScannedEmptyProductCollectionViewCell {

    fileprivate enum Color {
        static let backgroundColor = Style.Color.white
        static let titleLabelTextColor = Style.Color.black
        static let subtitleLabelTextColor = Style.Color.black
        static let actionContainerViewBackgroundColor = UIColor(hex: "#F2F8FF")
        static let addProductButtonTitleColor = Style.Color.brightBlue
    }

    fileprivate enum CornerRadius {
        static let cornerRadius: CGFloat = 16
        static let actionContainerViewCornerRadius: CGFloat = 70

    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {
        static let titleLabelFont = Style.Font.sanFrancisco(.bold, size: .headline)
        static let subtitleLabelFont = Style.Font.sanFrancisco(.regular, size: .subheadline)
        static let addProductButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 18)
    }

    fileprivate enum Image {
        static let imageViewImage = Media.image(.handsError)
    }

    fileprivate enum Metric {
        static let textStackViewTopOffset: CGFloat = 24
        static let textStackViewLeftOffset: CGFloat = 24
        static let textStackViewRightOffset: CGFloat = -8
        static let textStackViewBottomOffset: CGFloat = -24
        static let imageViewPartSize: CGFloat = 80
        static let imageViewRightOffset: CGFloat = -20
        static let imageViewBottomOffset: CGFloat = -20
        static let actionContainerViewRightOffset: CGFloat = -16
        static let actionContainerViewBottomOffset: CGFloat = 60
        static let addProductButtonHeight: CGFloat = 80
    }

    fileprivate enum Reusable {

    }

}
