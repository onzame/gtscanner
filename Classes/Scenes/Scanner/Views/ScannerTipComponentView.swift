//
//  ScannerTipComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ScannerTipComponentView: UIView {

    // MARK: - UI components

    let textContainerView = TextContainerView().then {
        $0.textLabel.text = Localization.Scanner.tipEmptyState
        $0.textLabel.font = Font.tipViewLabelFont
        $0.textLabel.textColor = Color.tipViewLabelTextColor
        $0.textLabel.textAlignment = .center
        $0.backgroundColor = Color.tipViewBackgroundColor
        $0.layer.cornerRadius = CornerRadius.tipViewCornerRadius
    }

    // MARK: Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
    }

    private func setupHierarchy() {
        addSubview(textContainerView)
    }

    private func setupLayout() {
        textContainerView.addEdgesConstraints()
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScannerTipComponentView {
    fileprivate enum Color {
        static let tipViewBackgroundColor = Style.Color.black.withAlphaComponent(0.4)
        static let tipViewLabelTextColor = Style.Color.white
    }

    fileprivate enum CornerRadius {
        static let tipViewCornerRadius: CGFloat = 26
    }

    fileprivate enum Font {
        static let tipViewLabelFont = Style.Font.sanFrancisco(.semibold, size: 16)
    }
}

extension Reactive where Base: ScannerTipComponentView {
    var filledState: Binder<Bool> {
        return Binder(base, binding: { viewController, isFilled in
            let emptyText = Localization.Scanner.tipEmptyState
            let filledText = Localization.Scanner.tipFilledState
            viewController.textContainerView.textLabel.text = isFilled ? filledText : emptyText
        })
    }
}
