//
//  ScannedProductsComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import ReusableKit

class ScannedProductsComponentView: UIView {

    // MARK: - UI components

    let collectionViewFlowLayout = UICollectionViewFlowLayout().then {
        let horizontalInset = Metric.collectionViewSectionSpacing
        $0.minimumLineSpacing = Metric.collectionViewItemSpacing
        $0.scrollDirection = .horizontal
        $0.sectionInset = UIEdgeInsets(top: 0, left: horizontalInset, bottom: 0, right: horizontalInset)
        $0.itemSize = CGSize(width: Metric.collectionViewItemWidth, height: Metric.collectionViewItemHeight)
    }

    lazy var collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionViewFlowLayout).then {
        $0.showsHorizontalScrollIndicator = false
        $0.isUserInteractionEnabled = true
        $0.decelerationRate = .fast
        $0.setBackgroundColor(Color.collectionViewBackgroundColor)
        $0.register(Reusable.scannedProductCell)
        $0.register(Reusable.scannedEmptyProductCell)
    }

    // MARK: - DataSource

    lazy var dataSource = RxCollectionViewSectionedAnimatedDataSource<ScanSectionModel>(
        configureCell: { [unowned self] (_, collectionView, indexPath, item) -> UICollectionViewCell in
            switch item {
            case .scannedEmptyProduct(let viewModel):
                let cell = collectionView.dequeue(Reusable.scannedEmptyProductCell, for: indexPath)
                cell.reactor = viewModel

                cell.addProductButton.rx.tap
                    .withLatestFrom(viewModel.state)
                    .map({ $0.product })
                    .bind(to: self.showAddProductActionPublisher)
                    .disposed(by: cell.disposeBag)

                return cell

            case .scannedProduct(let viewModel):
                let cell = collectionView.dequeue(Reusable.scannedProductCell, for: indexPath)
                cell.reactor = viewModel

                cell.addPriceButton.rx.tap
                    .withLatestFrom(viewModel.state)
                    .map({ $0.product })
                    .bind(to: self.addPriceActionPublisher)
                    .disposed(by: cell.disposeBag)

                cell.saveButton.rx.tap
                    .withLatestFrom(viewModel.state)
                    .map({ $0.product })
                    .bind(to: self.saveProductActionPublisher)
                    .disposed(by: cell.disposeBag)

                cell.ratingView.rx.selectRatingAction
                    .withLatestFrom(viewModel.state)
                    .map({ $0.product })
                    .bind(to: self.addReviewActionPublisher)
                    .disposed(by: cell.disposeBag)

                cell.rx.infoSelection
                    .withLatestFrom(viewModel.state)
                    .map({ $0.product })
                    .bind(to: self.showProductInfoActionPublisher)
                    .disposed(by: cell.disposeBag)

                return cell
            }
        }
    )

    // MARK: - Publishers

    fileprivate let addPriceActionPublisher = PublishSubject<Product>()
    fileprivate let addReviewActionPublisher = PublishSubject<Product>()
    fileprivate let showProductInfoActionPublisher = PublishSubject<Product>()
    fileprivate let showAddProductActionPublisher = PublishSubject<UnknownProduct>()
    fileprivate let saveProductActionPublisher = PublishSubject<Product>()

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(collectionView)
    }

    private func setupLayout() {
        collectionView.addEdgesConstraints()
    }

    private func setupView() {
        collectionView.delegate = self
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScannedProductsComponentView {
    fileprivate enum Color {
        static let collectionViewBackgroundColor = UIColor.clear
    }

    fileprivate enum Metric {
        static let collectionViewSectionSpacing: CGFloat = 20
        static let collectionViewItemSpacing: CGFloat = 8
        static let collectionViewItemHeight: CGFloat = 250
        static let collectionViewItemWidth: CGFloat = min(Screen.width - collectionViewSectionSpacing * 2, 374)
    }

    fileprivate enum Reusable {
        static let scannedProductCell = ReusableCell<ScannedProductCollectionViewCell>()
        static let scannedEmptyProductCell = ReusableCell<ScannedEmptyProductCollectionViewCell>()
    }
}

// MARK: - CollectionViewCenterScrollable

extension ScannedProductsComponentView: CollectionViewCenterScrollable { }

// MARK: - UICollectionViewDelegateFlowLayout

extension ScannedProductsComponentView: UICollectionViewDelegateFlowLayout {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        collectionViewWillScrollToVisibleCellCenter()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate { collectionViewWillScrollToVisibleCellCenter() }
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ScannedProductsComponentView {
    var scrollToLastItem: Binder<Void> {
        return Binder(base, binding: { viewController, _ in
            guard let items = viewController.dataSource.sectionModels.last?.items.count else { return }
            let indexPathToScroll = IndexPath(item: items - 1, section: 0)
            viewController.collectionView.scrollToItem(at: indexPathToScroll, at: .centeredHorizontally, animated: true)
        })
    }

    var addPriceAction: ControlEvent<Product> {
        return ControlEvent(events: base.addPriceActionPublisher)
    }

    var addReviewAction: ControlEvent<Product> {
        return ControlEvent(events: base.addReviewActionPublisher)
    }

    var showProductInfoAction: ControlEvent<Product> {
        return ControlEvent(events: base.showProductInfoActionPublisher)
    }

    var showAddProductAction: ControlEvent<UnknownProduct> {
        return ControlEvent(events: base.showAddProductActionPublisher)
    }

    var saveProductAction: ControlEvent<Product> {
        return ControlEvent(events: base.saveProductActionPublisher)
    }
}
