//
//  ScannerCompareComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 03.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ScannerCompareComponentView: UIView {

    // MARK: - UI components

    let imagesStackView = ImagesCollectionStackView().then {
        $0.spacing = Metric.imagesStackViewSpacing
    }

    let amountLabel = UILabel().then {
        $0.font = Font.amountLabelFont
        $0.textColor = Color.amountLabelTextColor
    }

    let actionButton = UIButton(type: .system).then {
        $0.setTitleFont(Font.actionButtonTitleFont)
        $0.setTitleColor(Color.actionButtonTitleColor, for: .normal)
        $0.setBackgroundColor(Color.actionButtonBackgroundColor)
        $0.setTitle(Localization.Scanner.compareBannerActionTitle, for: .normal)
        $0.setCornerRadius(CornerRadius.actionButtonCornerRadius)
        $0.contentEdgeInsets = UIEdgeInsets(
            top: Metric.actionButtonVerticalInset,
            left: Metric.actionButtonHorizontalInset,
            bottom: Metric.actionButtonVerticalInset,
            right: Metric.actionButtonHorizontalInset
        )
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(imagesStackView)
        addSubview(amountLabel)
        addSubview(actionButton)
    }

    private func setupLayout() {
        imagesStackView.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(Metric.imagesStackViewLeftOffset)
            make.centerY.equalToSuperview()
        }

        amountLabel.snp.makeConstraints { (make) in
            make.left.equalTo(imagesStackView.snp.right).offset(Metric.amountLabelLeftOffset)
            make.centerY.equalToSuperview()
        }

        actionButton.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(Metric.actionButtonRightOffset)
            make.centerY.equalToSuperview()
        }
    }

    // MARK: - Public methods

    func update(details: CompareDetails) {
        let urls = details.products.prefix(6).map({ $0.image })
        let placeholderSize = CGSize(squareSide: Metric.imagesStackViewImageViewPartSize / 2)
        let placeholder = ImagePlaceholderView.product(imageSize: placeholderSize)
        let settings = ImagesCollectionStackView.Settings(
            cornerRadius: CornerRadius.imagesStackImageViewCornerRadius,
            partSize: Metric.imagesStackViewImageViewPartSize,
            borderColor: Color.brightProductsImageViewBorderColor,
            borderWidth: Metric.imagesStackViewImageViewBorderWidth,
            backgroundColor: Color.imagesStackViewImageViewBackgroundColor
        )

        imagesStackView.updateItems(urls: urls, placeholder: placeholder, settings: settings)

        let resultAmount = details.products.count - urls.count

        if resultAmount > 0 {
           amountLabel.text = "+\(resultAmount)"
        } else {
           amountLabel.text = ""
        }
    }

    private func setupView() {
        setBackgroundColor(Color.backgroundColor)
    }
}

// MARK: - Extensions
// MARK: - Constants

extension ScannerCompareComponentView {
    fileprivate enum Color {
        static let backgroundColor = Style.Color.brightYellow
        static let imagesStackViewImageViewBackgroundColor = Style.Color.white
        static let amountLabelTextColor = UIColor(hex: "#262A3C")
        static let actionButtonTitleColor = UIColor(hex: "#262A3C")
        static let actionButtonBackgroundColor = UIColor(hex: "#262A3C")?.withAlphaComponent(0.05)
        static let brightProductsImageViewBorderColor = UIColor(hex: "#FCD846")
    }

    fileprivate enum CornerRadius {
        static let imagesStackImageViewCornerRadius: CGFloat = 14
        static let actionButtonCornerRadius: CGFloat = 14
    }

    fileprivate enum Font {
        static let amountLabelFont = Style.Font.sanFrancisco(.semibold, size: 14)
        static let actionButtonTitleFont = Style.Font.sanFrancisco(.medium, size: 14)
    }

    fileprivate enum Metric {
        static let imagesStackViewSpacing: CGFloat = -12
        static let imagesStackViewLeftOffset: CGFloat = 20
        static let imagesStackViewImageViewPartSize: CGFloat = 28
        static let imagesStackViewImageViewBorderWidth: CGFloat = 2

        static let amountLabelLeftOffset: CGFloat = 8

        static let actionButtonRightOffset: CGFloat = -20
        static let actionButtonHorizontalInset: CGFloat = 14
        static let actionButtonVerticalInset: CGFloat = 12
    }
}

extension Reactive where Base: ScannerCompareComponentView {
    var update: Binder<CompareDetails> {
        return Binder(base, binding: { view, details in
            view.update(details: details)
        })
    }
}
