//
//  ScannerActionsComponentView.swift
//  Gorod
//
//  Created by Sergei Fabian on 12.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit

class ScannerActionsComponentView: UIView {

    // MARK: - UI components

    let openScanHistoryButton = UIButton(type: .system).then {
        $0.setImage(Image.openScanHistoryButtonImage, for: .normal)
        $0.backgroundColor = Color.openScanHistoryButtonBackgroundColor
        $0.tintColor = Color.openScanHistoryButtonTintColor
        $0.layer.cornerRadius = CornerRadius.headerButtonCornerRadius
    }

    let openScanListButton = UIButton(type: .system).then {
        $0.setImage(Image.openScanListButtonImage, for: .normal)
        $0.backgroundColor = Color.openScanListButtonBackgroundColor
        $0.tintColor = Color.openScanListButtonTintColor
        $0.layer.cornerRadius = CornerRadius.headerButtonCornerRadius
    }

    let actionsStackView = UIStackView().then {
        $0.distribution = .equalSpacing
    }

    // MARK: - Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        addSubview(actionsStackView)

        actionsStackView.addArrangedSubview(openScanHistoryButton)
        actionsStackView.addArrangedSubview(openScanListButton)
    }

    private func setupLayout() {
        actionsStackView.addEdgesConstraints()

        [openScanHistoryButton, openScanListButton].forEach({
            $0.snp.makeConstraints { (make) in
                make.width.height.equalTo(Metric.headerButtonPartSize)
            }
        })
    }

    private func setupView() {

    }

}

// MARK: - Extensions
// MARK: - Constants

extension ScannerActionsComponentView {

    fileprivate enum Color {
        static let openScanHistoryButtonBackgroundColor = Style.Color.white
        static let openScanHistoryButtonTintColor = Style.Color.black
        static let openScanListButtonBackgroundColor = Style.Color.brightYellow
        static let openScanListButtonTintColor = Style.Color.black
    }

    fileprivate enum CornerRadius {
        static let headerButtonCornerRadius: CGFloat = 14
    }

    fileprivate enum Duration {

    }

    fileprivate enum Font {

    }

    fileprivate enum Image {
        static let openScanHistoryButtonImage = Media.icon(.history, size: .x18)
        static let openScanListButtonImage = Media.icon(.fileText, size: .x18)
    }

    fileprivate enum Metric {
        static let headerButtonPartSize: CGFloat = 40
    }

    fileprivate enum Reusable {

    }

}
