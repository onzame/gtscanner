//
//  ProductPriceCompletionCoordinator.swift
//  Gorod
//
//  Created by Sergei Fabian on 31.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import UIKit
import RxSwift

enum ProductPriceCompletionCoordinationResult {
    case canceled
}

final class ProductPriceCompletionCoordinator: Coordinator<ProductPriceCompletionCoordinationResult> {

    private let rootViewController: UIViewController
    private let container: DependenciesContainer
    private let input: ProductPriceCompletionInput

    init(rootViewController: UIViewController, container: DependenciesContainer, input: ProductPriceCompletionInput) {
        self.rootViewController = rootViewController
        self.container = container
        self.input = input
    }

    override func start() -> Observable<ProductPriceCompletionCoordinationResult> {
        let viewModel = container.productPriceCompletionViewModel(input: input)
        let viewController = ProductPriceCompletionViewController(reactor: viewModel)
        let bottomSheetController = BottomSheetController(contentViewController: viewController)

        rootViewController.present(bottomSheetController, animated: true)

        // ProductPriceCompletionCoordinationResult.canceled

        let cancelAction = viewController.rx.cancelAction
            .asObservable()
            .mapTo(ProductPriceCompletionCoordinationResult.canceled)
            .do(onNext: { _ in bottomSheetController.dismiss(animated: true) })

        let dismissAction = viewController.rx.dismissAction
            .asObservable()
            .mapTo(ProductPriceCompletionCoordinationResult.canceled)

        return Observable.merge(cancelAction, dismissAction)
            .take(1)
    }

}
