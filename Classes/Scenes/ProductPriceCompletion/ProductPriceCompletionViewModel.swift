//
//  ProductPriceCompletionViewModel.swift
//  Gorod
//
//  Created by Sergei Fabian on 31.01.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation
import ReactorKit

class ProductPriceCompletionViewModel: Reactor {

    // MARK: - Properties

    var initialState: State

    // MARK: - Lifecycle

    init(input: ProductPriceCompletionInput) {
        initialState = State(product: input.product, shop: input.shop, price: input.price)
    }

}

// MARK: - Extensions
// MARK: - Actions

extension ProductPriceCompletionViewModel {
    typealias Action = NoAction
}

// MARK: - State

extension ProductPriceCompletionViewModel {
    struct State {
        let product: Product
        let shop: Shop
        let price: Price
    }
}
