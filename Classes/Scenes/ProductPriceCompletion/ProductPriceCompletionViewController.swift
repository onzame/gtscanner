//
//  ProductPriceCompletionViewController.swift
//  Gorod
//
//  Created by Sergei Fabian on 01.12.2019.
//  Copyright © 2019 Onza.Me LLC. All rights reserved.
//

import UIKit
import ReactorKit
import RxSwift
import RxCocoa

final class ProductPriceCompletionViewController: BaseViewController<ProductPriceCompletionViewModel> {

    // MARK: - UI components

    let contentView = ProductPriceCompletionView().then {
        $0.descriptionView.titleLabel.text = Localization.ProductPriceCompletion.title
    }

    // MARK: - Lifecycle

    override init(reactor: ProductPriceCompletionViewModel) {
        super.init(reactor: reactor)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Settings

    private func commonInit() {
        setupHierarchy()
        setupLayout()
        setupView()
    }

    private func setupHierarchy() {
        view.addSubview(contentView)
    }

    private func setupLayout() {
        contentView.addEdgesConstraints()
    }

    private func setupView() {
        preferredContentSize = CGSize(width: Metric.width, height: Metric.height)
    }

    // MARK: - Bindings

    override func bind(reactor: ProductPriceCompletionViewModel) {
        reactor.state
            .map({ Localization.ProductPriceCompletion.subtitle($0.price.price) })
            .bind(to: contentView.descriptionView.subtitleLabel.rx.text)
            .disposed(by: disposeBag)
    }

}

// MARK: - Extensions
// MARK: - Constants

extension ProductPriceCompletionViewController {
    fileprivate enum Metric {
        static var width: CGFloat = Screen.width
        static let height: CGFloat = 280
    }
}

// MARK: - Reactive interface

extension Reactive where Base: ProductPriceCompletionViewController {
    var cancelAction: ControlEvent<Void> {
        return ControlEvent(events: base.contentView.actionView.primaryButton.rx.tap)
    }
}
