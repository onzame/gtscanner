//
//  GorodGoodsDelegate.swift
//  Gorod
//
//  Created by Sergei Fabian on 14.03.2020.
//  Copyright © 2020 Onza.Me LLC. All rights reserved.
//

import Foundation

public protocol GorodGoodsDelegate: class {
    func sendEvent(_ eventName: String, inCategory category: String, withTag tag: String)
}
