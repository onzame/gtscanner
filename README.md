# GTScanner

![Swift](https://img.shields.io/badge/swift-5.2-orange)
![GTScanner](https://img.shields.io/badge/version-1.0.13-brightgreen)

## Usage

### Environment configuration

First you should configure `ScannerModuleFactory` with `EnvironmentConfiguration` to setup module

```swift
let apiUrl: URL = ...
let isDebugMode: Bool = ...
let configuration = EnvironmentConfiguration(apiURL: apiURL, isDebugMode: isDebugMode) 

// Use default to connect to the dev server
let defaultConfiguration =  EnvironmentConfiguration.default 
```

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    // ...

    ScanModuleFactory.configure(with: configuration)
    
    // ...
    
}
```

### Access Token

Implement `AccessTokenType` to provide access key which allow retrieve data from server

```swift
public protocol AccessTokenType {
    var token: String { get }
}
```

### Provider

Entity to provide mechanism to retrieve and refresh invalid AccessToken

```swift
public protocol AccessTokenProviderType {
    func resolveAccessToken(completion: @escaping (AccessTokenType) -> Void)
    func resolveInvalidAccessToken(completion: @escaping (Result<AccessTokenType, Error>) -> Void)
}
```

### Analytics

Pass analytics delegate to retrieve events from module

```swift
public protocol GorodGoodsDelegate: class {
    func sendEvent(_ eventName: String, inCategory category: String, withTag tag: String)
}
```

### Building root view controller

Building `UIViewController` instance with multipurpose scanner logic by providing `AccessTokenProviderType`

```swift
let analyticsDelegate: GorodGoodsDelegate = ...
let accessTokenProvider: AccessTokenProviderType = ...
let scannerViewController = ScanModuleFactory.buildRootViewController(provider: accessTokenProvider, analyticsDelegate: analyticsDelegate)
```

### Invalidation 

Removing module elements from memory

```swift
ScanModuleFactory.freeRootViewController()
```

## Installation

**Podfile**
```ruby
pod 'GTScanner'
```

## Changelog

* 2020-04-08
    * Add environment configuration
* 2020-03-14
    * Add analytics interface
* 2020-03-11
    * Add interface to communicate with main application
* 2020-03-06
    * Add all compare components, downgrade Moya version
* 2020-02-28
    * Release via CocoaPods
