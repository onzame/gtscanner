//
//  AccessTokenProvider.swift
//  ScannerSample
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me. All rights reserved.
//

import Foundation
import GTScanner

final class AccessTokenProvider: AccessTokenProviderType {

    private let authorizationService: AuthorizationServiceType

    private var currentAccessToken = AccessToken.invalidStub

    init(authorizationService: AuthorizationServiceType) {
        self.authorizationService = authorizationService
    }

    func resolveAccessToken(completion: @escaping (AccessTokenType) -> Void) {
        completion(currentAccessToken)
    }

    func resolveInvalidAccessToken(completion: @escaping (Result<AccessTokenType, Error>) -> Void) {
        authorizationService.refreshAccessToken { (result) in
            switch result {
            case .success(let accessToken):
                self.currentAccessToken = accessToken
                completion(.success(accessToken))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }

}
