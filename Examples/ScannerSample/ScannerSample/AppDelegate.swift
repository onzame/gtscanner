//
//  AppDelegate.swift
//  ScannerSample
//
//  Created by Sergei Fabian on 28.02.2020.
//  Copyright © 2020 Onza.Me. All rights reserved.
//

import UIKit
import GTScanner

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let configuration = EnvironmentConfiguration.default
        ScanModuleFactory.configure(with: configuration)

        let analyticsDelegate: GorodGoodsDelegate = AnalyticsDelegate()
        let accessTokenProvider: AccessTokenProviderType = AccessTokenProvider(authorizationService: AuthorizationService())
        let rootViewController = ScanModuleFactory.buildRootViewController(provider: accessTokenProvider, analyticsDelegate: analyticsDelegate)
        let rootTabBarController = UITabBarController()
        rootTabBarController.setViewControllers([rootViewController, UIViewController()], animated: false)

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootTabBarController
        window?.makeKeyAndVisible()

        return true
    }

}
