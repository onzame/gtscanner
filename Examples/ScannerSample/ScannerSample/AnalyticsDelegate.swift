//
//  AnalyticsDelegate.swift
//  ScannerSample
//
//  Created by Sergei Fabian on 14.03.2020.
//  Copyright © 2020 Onza.Me. All rights reserved.
//

import Foundation
import GTScanner

final class AnalyticsDelegate: GorodGoodsDelegate {
    func sendEvent(_ eventName: String, inCategory category: String, withTag tag: String) {
        print("Analytics category: \(category) action: \(eventName) value: \(tag)")
    }
}
