//
//  AuthorizationService.swift
//  ScannerSample
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me. All rights reserved.
//

import Foundation

protocol AuthorizationServiceType {
    func refreshAccessToken(completion: @escaping (Result<AccessToken, Error>) -> Void)
}

final class AuthorizationService: AuthorizationServiceType {
    func refreshAccessToken(completion: @escaping (Result<AccessToken, Error>) -> Void) {
        guard let url = URL(string: "https://google.com") else {
            completion(.failure(NSError()))
            return
        }

        let task = URLSession.shared.dataTask(with: url) { (_, _, error) in
            if let error = error {
                completion(Result.failure(error))
            } else {
                completion(Result.success(AccessToken.validStub))
            }
        }

        task.resume()
    }
}

