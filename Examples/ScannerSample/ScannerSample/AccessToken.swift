//
//  AccessToken.swift
//  ScannerSample
//
//  Created by Sergei Fabian on 11.03.2020.
//  Copyright © 2020 Onza.Me. All rights reserved.
//

import Foundation
import GTScanner

struct AccessToken: AccessTokenType {
    let token: String
}

extension AccessToken {
    static var validStub: AccessToken {
        AccessToken(token: "insert_your_valid_token")
    }

    static var invalidStub: AccessToken {
        AccessToken(token: "wrong_token")
    }
}
